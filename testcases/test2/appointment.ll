; ModuleID = 'appointment.cpp'
source_filename = "appointment.cpp"
target datalayout = "e-m:e-p270:32:32-p271:32:32-p272:64:64-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

%"class.std::ios_base::Init" = type { i8 }
%"class.std::basic_ostream" = type { i32 (...)**, %"class.std::basic_ios" }
%"class.std::basic_ios" = type { %"class.std::ios_base", %"class.std::basic_ostream"*, i8, i8, %"class.std::basic_streambuf"*, %"class.std::ctype"*, %"class.std::num_put"*, %"class.std::num_get"* }
%"class.std::ios_base" = type { i32 (...)**, i64, i64, i32, i32, i32, %"struct.std::ios_base::_Callback_list"*, %"struct.std::ios_base::_Words", [8 x %"struct.std::ios_base::_Words"], i32, %"struct.std::ios_base::_Words"*, %"class.std::locale" }
%"struct.std::ios_base::_Callback_list" = type { %"struct.std::ios_base::_Callback_list"*, void (i32, %"class.std::ios_base"*, i32)*, i32, i32 }
%"struct.std::ios_base::_Words" = type { i8*, i64 }
%"class.std::locale" = type { %"class.std::locale::_Impl"* }
%"class.std::locale::_Impl" = type { i32, %"class.std::locale::facet"**, i64, %"class.std::locale::facet"**, i8** }
%"class.std::locale::facet" = type <{ i32 (...)**, i32, [4 x i8] }>
%"class.std::basic_streambuf" = type { i32 (...)**, i8*, i8*, i8*, i8*, i8*, i8*, %"class.std::locale" }
%"class.std::ctype" = type <{ %"class.std::locale::facet.base", [4 x i8], %struct.__locale_struct*, i8, [7 x i8], i32*, i32*, i16*, i8, [256 x i8], [256 x i8], i8, [6 x i8] }>
%"class.std::locale::facet.base" = type <{ i32 (...)**, i32 }>
%struct.__locale_struct = type { [13 x %struct.__locale_data*], i16*, i32*, i32*, [13 x i8*] }
%struct.__locale_data = type opaque
%"class.std::num_put" = type { %"class.std::locale::facet.base", [4 x i8] }
%"class.std::num_get" = type { %"class.std::locale::facet.base", [4 x i8] }
%"class.std::basic_istream" = type { i32 (...)**, i64, %"class.std::basic_ios" }
%class.Student = type { %class.People, %"class.std::__cxx11::basic_string" }
%class.People = type { %"class.std::__cxx11::basic_string", %"class.std::__cxx11::basic_string", %"class.std::__cxx11::basic_string", %"class.std::__cxx11::basic_string", %"class.std::__cxx11::basic_string", %"class.std::__cxx11::basic_string" }
%"class.std::__cxx11::basic_string" = type { %"struct.std::__cxx11::basic_string<char>::_Alloc_hider", i64, %union.anon }
%"struct.std::__cxx11::basic_string<char>::_Alloc_hider" = type { i8* }
%union.anon = type { i64, [8 x i8] }
%class.Lecturer = type { %class.People, %"class.std::__cxx11::basic_string" }
%class.Appointment = type { %"class.std::__cxx11::basic_string", %"class.std::__cxx11::basic_string", %"class.std::__cxx11::basic_string", %class.Lecturer, %class.Student }
%"class.std::basic_ifstream" = type { %"class.std::basic_istream.base", %"class.std::basic_filebuf", %"class.std::basic_ios" }
%"class.std::basic_istream.base" = type { i32 (...)**, i64 }
%"class.std::basic_filebuf" = type { %"class.std::basic_streambuf", %union.pthread_mutex_t, %"class.std::__basic_file", i32, %struct.__mbstate_t, %struct.__mbstate_t, %struct.__mbstate_t, i8*, i64, i8, i8, i8, i8, i8*, i8*, i8, %"class.std::codecvt"*, i8*, i64, i8*, i8* }
%union.pthread_mutex_t = type { %struct.__pthread_mutex_s }
%struct.__pthread_mutex_s = type { i32, i32, i32, i32, i32, i16, i16, %struct.__pthread_internal_list }
%struct.__pthread_internal_list = type { %struct.__pthread_internal_list*, %struct.__pthread_internal_list* }
%"class.std::__basic_file" = type <{ %struct._IO_FILE*, i8, [7 x i8] }>
%struct._IO_FILE = type { i32, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, %struct._IO_marker*, %struct._IO_FILE*, i32, i32, i64, i16, i8, [1 x i8], i8*, i64, %struct._IO_codecvt*, %struct._IO_wide_data*, %struct._IO_FILE*, i8*, i64, i32, [20 x i8] }
%struct._IO_marker = type opaque
%struct._IO_codecvt = type opaque
%struct._IO_wide_data = type opaque
%struct.__mbstate_t = type { i32, %union.anon.0 }
%union.anon.0 = type { i32 }
%"class.std::codecvt" = type { %"class.std::__codecvt_abstract_base.base", %struct.__locale_struct* }
%"class.std::__codecvt_abstract_base.base" = type { %"class.std::locale::facet.base" }
%"class.std::basic_ofstream" = type { %"class.std::basic_ostream.base", %"class.std::basic_filebuf", %"class.std::basic_ios" }
%"class.std::basic_ostream.base" = type { i32 (...)** }

$_ZN6PeopleC2Ev = comdat any

$_ZN6People9setNumberENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE = comdat any

$_ZN6People7setNameENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE = comdat any

$_ZN6People10setSurnameENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE = comdat any

$_ZN6People13setDepartmentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE = comdat any

$_ZN7Student7setYearENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE = comdat any

$_ZN6People8setEmailENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE = comdat any

$_ZN6People8setPhoneENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE = comdat any

$_ZN6PeopleD2Ev = comdat any

$_ZN8Lecturer8setChairENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE = comdat any

$_ZN8LecturerC2Ev = comdat any

$_ZN7StudentC2Ev = comdat any

$_ZN11Appointment7setDateENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE = comdat any

$_ZN11Appointment8setStartENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE = comdat any

$_ZN11Appointment6setEndENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE = comdat any

$_ZN7StudentD2Ev = comdat any

$_ZN8LecturerD2Ev = comdat any

$_ZN7StudentaSERKS_ = comdat any

$_ZN8LectureraSERKS_ = comdat any

$_ZN11AppointmentaSERKS_ = comdat any

$_ZN11AppointmentD2Ev = comdat any

$_ZSteqIcEN9__gnu_cxx11__enable_ifIXsr9__is_charIT_EE7__valueEbE6__typeERKNSt7__cxx1112basic_stringIS2_St11char_traitsIS2_ESaIS2_EEESC_ = comdat any

$_ZN11Appointment7getDateB5cxx11Ev = comdat any

$_ZN11Appointment8getStartB5cxx11Ev = comdat any

$_ZN11Appointment6getEndB5cxx11Ev = comdat any

$_ZN11AppointmentC2Ev = comdat any

$_ZN7Student7displayEv = comdat any

$_ZN6People9getNumberB5cxx11Ev = comdat any

$_ZN8Lecturer7displayEv = comdat any

$_ZN11Appointment7displayEv = comdat any

$_ZN6People7getNameB5cxx11Ev = comdat any

$_ZN6People10getSurnameB5cxx11Ev = comdat any

$_ZN6People13getDepartmentB5cxx11Ev = comdat any

$_ZN7Student7getYearB5cxx11Ev = comdat any

$_ZN6People8getEmailB5cxx11Ev = comdat any

$_ZN6People8getPhoneB5cxx11Ev = comdat any

$_ZN8Lecturer8getChairB5cxx11Ev = comdat any

$_ZN6PeopleaSERKS_ = comdat any

$_ZNSt11char_traitsIcE7compareEPKcS2_m = comdat any

$__clang_call_terminate = comdat any

@_ZStL8__ioinit = internal global %"class.std::ios_base::Init" zeroinitializer, align 1
@__dso_handle = external hidden global i8
@count = dso_local global [3 x i32] zeroinitializer, align 4
@_ZSt4cout = external dso_local global %"class.std::basic_ostream", align 8
@.str = private unnamed_addr constant [8 x i8] c"Error. \00", align 1
@.str.1 = private unnamed_addr constant [10 x i8] c" between \00", align 1
@.str.2 = private unnamed_addr constant [34 x i8] c" appointment cannot be scheduled.\00", align 1
@.str.3 = private unnamed_addr constant [27 x i8] c"This time period is full. \00", align 1
@.str.4 = private unnamed_addr constant [38 x i8] c"Give 3 file names to run the program.\00", align 1
@.str.5 = private unnamed_addr constant [38 x i8] c"Student - Lecturer Appointment System\00", align 1
@.str.6 = private unnamed_addr constant [6 x i8] c"Menu:\00", align 1
@.str.7 = private unnamed_addr constant [17 x i8] c"1 - Student Menu\00", align 1
@.str.8 = private unnamed_addr constant [18 x i8] c"2 - Lecturer Menu\00", align 1
@.str.9 = private unnamed_addr constant [21 x i8] c"3 - Appointment Menu\00", align 1
@.str.10 = private unnamed_addr constant [20 x i8] c"4 - Get Final Lists\00", align 1
@.str.11 = private unnamed_addr constant [9 x i8] c"5 - Exit\00", align 1
@_ZSt3cin = external dso_local global %"class.std::basic_istream", align 8
@.str.12 = private unnamed_addr constant [16 x i8] c"1 - Add Student\00", align 1
@.str.13 = private unnamed_addr constant [18 x i8] c"2 - List Students\00", align 1
@.str.14 = private unnamed_addr constant [19 x i8] c"3 - Remove Student\00", align 1
@.str.15 = private unnamed_addr constant [19 x i8] c"4 - Update Student\00", align 1
@.str.16 = private unnamed_addr constant [12 x i8] c"Student No:\00", align 1
@.str.17 = private unnamed_addr constant [14 x i8] c"Student Name:\00", align 1
@.str.18 = private unnamed_addr constant [19 x i8] c"Student Last Name:\00", align 1
@.str.19 = private unnamed_addr constant [20 x i8] c"Student Department:\00", align 1
@.str.20 = private unnamed_addr constant [23 x i8] c"Student Starting Year:\00", align 1
@.str.21 = private unnamed_addr constant [16 x i8] c"Student E-Mail:\00", align 1
@.str.22 = private unnamed_addr constant [22 x i8] c"Student Phone Number:\00", align 1
@.str.23 = private unnamed_addr constant [32 x i8] c"Enter the Student No to remove:\00", align 1
@.str.24 = private unnamed_addr constant [32 x i8] c"Enter the Student No to update:\00", align 1
@.str.25 = private unnamed_addr constant [22 x i8] c"Updated Student Name:\00", align 1
@.str.26 = private unnamed_addr constant [27 x i8] c"Updated Student Last Name:\00", align 1
@.str.27 = private unnamed_addr constant [28 x i8] c"Updated Student Department:\00", align 1
@.str.28 = private unnamed_addr constant [31 x i8] c"Updated Student Starting Year:\00", align 1
@.str.29 = private unnamed_addr constant [24 x i8] c"Updated Student E-Mail:\00", align 1
@.str.30 = private unnamed_addr constant [30 x i8] c"Updated Student Phone Number:\00", align 1
@.str.31 = private unnamed_addr constant [7 x i8] c"Error.\00", align 1
@.str.32 = private unnamed_addr constant [17 x i8] c"1 - Add Lecturer\00", align 1
@.str.33 = private unnamed_addr constant [19 x i8] c"2 - List Lecturers\00", align 1
@.str.34 = private unnamed_addr constant [20 x i8] c"3 - Remove Lecturer\00", align 1
@.str.35 = private unnamed_addr constant [20 x i8] c"4 - Update Lecturer\00", align 1
@.str.36 = private unnamed_addr constant [13 x i8] c"Lecturer No:\00", align 1
@.str.37 = private unnamed_addr constant [15 x i8] c"Lecturer Name:\00", align 1
@.str.38 = private unnamed_addr constant [20 x i8] c"Lecturer Last Name:\00", align 1
@.str.39 = private unnamed_addr constant [21 x i8] c"Lecturer Department:\00", align 1
@.str.40 = private unnamed_addr constant [17 x i8] c"Lecturer E-Mail:\00", align 1
@.str.41 = private unnamed_addr constant [23 x i8] c"Lecturer Phone Number:\00", align 1
@.str.42 = private unnamed_addr constant [16 x i8] c"Lecturer Title:\00", align 1
@.str.43 = private unnamed_addr constant [33 x i8] c"Enter the Lecturer No to remove:\00", align 1
@.str.44 = private unnamed_addr constant [33 x i8] c"Enter the Lecturer No to update:\00", align 1
@.str.45 = private unnamed_addr constant [23 x i8] c"Updated Lecturer Name:\00", align 1
@.str.46 = private unnamed_addr constant [28 x i8] c"Updated Lecturer Last Name:\00", align 1
@.str.47 = private unnamed_addr constant [29 x i8] c"Updated Lecturer Department:\00", align 1
@.str.48 = private unnamed_addr constant [25 x i8] c"Updated Lecturer E-Mail:\00", align 1
@.str.49 = private unnamed_addr constant [31 x i8] c"Updated Lecturer Phone Number:\00", align 1
@.str.50 = private unnamed_addr constant [24 x i8] c"Updated Lecturer title:\00", align 1
@.str.51 = private unnamed_addr constant [20 x i8] c"1 - Add Appointment\00", align 1
@.str.52 = private unnamed_addr constant [22 x i8] c"2 - List Appointments\00", align 1
@.str.53 = private unnamed_addr constant [23 x i8] c"3 - Remove Appointment\00", align 1
@.str.54 = private unnamed_addr constant [23 x i8] c"4 - Update Appointment\00", align 1
@.str.55 = private unnamed_addr constant [18 x i8] c"Appointment Date:\00", align 1
@.str.56 = private unnamed_addr constant [15 x i8] c"Starting Hour:\00", align 1
@.str.57 = private unnamed_addr constant [13 x i8] c"Ending Hour:\00", align 1
@.str.58 = private unnamed_addr constant [26 x i8] c"Updated Appointment Date:\00", align 1
@.str.59 = private unnamed_addr constant [23 x i8] c"Updated Starting Hour:\00", align 1
@.str.60 = private unnamed_addr constant [21 x i8] c"Updated Ending Hour:\00", align 1
@.str.61 = private unnamed_addr constant [18 x i8] c"Guncelogrenci.txt\00", align 1
@.str.62 = private unnamed_addr constant [2 x i8] c" \00", align 1
@.str.63 = private unnamed_addr constant [22 x i8] c"Guncelakademisyen.txt\00", align 1
@.str.64 = private unnamed_addr constant [18 x i8] c"Guncelrandevu.txt\00", align 1
@.str.65 = private unnamed_addr constant [20 x i8] c"Program terminated.\00", align 1
@.str.66 = private unnamed_addr constant [4 x i8] c" - \00", align 1
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_appointment.cpp, i8* null }]

@_ZN7StudentC1EPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE = dso_local unnamed_addr alias void (%class.Student*, %"class.std::__cxx11::basic_string"*), void (%class.Student*, %"class.std::__cxx11::basic_string"*)* @_ZN7StudentC2EPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
@_ZN8LecturerC1EPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE = dso_local unnamed_addr alias void (%class.Lecturer*, %"class.std::__cxx11::basic_string"*), void (%class.Lecturer*, %"class.std::__cxx11::basic_string"*)* @_ZN8LecturerC2EPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
@_ZN11AppointmentC1EPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE = dso_local unnamed_addr alias void (%class.Appointment*, %"class.std::__cxx11::basic_string"*), void (%class.Appointment*, %"class.std::__cxx11::basic_string"*)* @_ZN11AppointmentC2EPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE

; Function Attrs: noinline uwtable
define internal void @__cxx_global_var_init() #0 section ".text.startup" {
entry:
  call void @_ZNSt8ios_base4InitC1Ev(%"class.std::ios_base::Init"* nonnull dereferenceable(1) @_ZStL8__ioinit)
  %0 = call i32 @__cxa_atexit(void (i8*)* bitcast (void (%"class.std::ios_base::Init"*)* @_ZNSt8ios_base4InitD1Ev to void (i8*)*), i8* getelementptr inbounds (%"class.std::ios_base::Init", %"class.std::ios_base::Init"* @_ZStL8__ioinit, i32 0, i32 0), i8* @__dso_handle) #3
  ret void
}

declare dso_local void @_ZNSt8ios_base4InitC1Ev(%"class.std::ios_base::Init"* nonnull dereferenceable(1)) unnamed_addr #1

; Function Attrs: nounwind
declare dso_local void @_ZNSt8ios_base4InitD1Ev(%"class.std::ios_base::Init"* nonnull dereferenceable(1)) unnamed_addr #2

; Function Attrs: nounwind
declare dso_local i32 @__cxa_atexit(void (i8*)*, i8*, i8*) #3

; Function Attrs: noinline optnone uwtable
define dso_local void @_ZN7StudentC2EPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE(%class.Student* nonnull dereferenceable(224) %this, %"class.std::__cxx11::basic_string"* %_temp) unnamed_addr #4 align 2 personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
entry:
  %this.addr = alloca %class.Student*, align 8
  %_temp.addr = alloca %"class.std::__cxx11::basic_string"*, align 8
  %agg.tmp = alloca %"class.std::__cxx11::basic_string", align 8
  %exn.slot = alloca i8*, align 8
  %ehselector.slot = alloca i32, align 4
  %agg.tmp4 = alloca %"class.std::__cxx11::basic_string", align 8
  %agg.tmp9 = alloca %"class.std::__cxx11::basic_string", align 8
  %agg.tmp14 = alloca %"class.std::__cxx11::basic_string", align 8
  %agg.tmp19 = alloca %"class.std::__cxx11::basic_string", align 8
  %agg.tmp24 = alloca %"class.std::__cxx11::basic_string", align 8
  %agg.tmp29 = alloca %"class.std::__cxx11::basic_string", align 8
  store %class.Student* %this, %class.Student** %this.addr, align 8
  store %"class.std::__cxx11::basic_string"* %_temp, %"class.std::__cxx11::basic_string"** %_temp.addr, align 8
  %this1 = load %class.Student*, %class.Student** %this.addr, align 8
  %0 = bitcast %class.Student* %this1 to %class.People*
  call void @_ZN6PeopleC2Ev(%class.People* nonnull dereferenceable(192) %0) #3
  %year = getelementptr inbounds %class.Student, %class.Student* %this1, i32 0, i32 1
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %year) #3
  %1 = bitcast %class.Student* %this1 to %class.People*
  %2 = load %"class.std::__cxx11::basic_string"*, %"class.std::__cxx11::basic_string"** %_temp.addr, align 8
  %arrayidx = getelementptr inbounds %"class.std::__cxx11::basic_string", %"class.std::__cxx11::basic_string"* %2, i64 0
  invoke void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1ERKS4_(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %arrayidx)
          to label %invoke.cont unwind label %lpad

invoke.cont:                                      ; preds = %entry
  invoke void @_ZN6People9setNumberENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE(%class.People* nonnull dereferenceable(192) %1, %"class.std::__cxx11::basic_string"* %agg.tmp)
          to label %invoke.cont3 unwind label %lpad2

invoke.cont3:                                     ; preds = %invoke.cont
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp) #3
  %3 = bitcast %class.Student* %this1 to %class.People*
  %4 = load %"class.std::__cxx11::basic_string"*, %"class.std::__cxx11::basic_string"** %_temp.addr, align 8
  %arrayidx5 = getelementptr inbounds %"class.std::__cxx11::basic_string", %"class.std::__cxx11::basic_string"* %4, i64 1
  invoke void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1ERKS4_(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp4, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %arrayidx5)
          to label %invoke.cont6 unwind label %lpad

invoke.cont6:                                     ; preds = %invoke.cont3
  invoke void @_ZN6People7setNameENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE(%class.People* nonnull dereferenceable(192) %3, %"class.std::__cxx11::basic_string"* %agg.tmp4)
          to label %invoke.cont8 unwind label %lpad7

invoke.cont8:                                     ; preds = %invoke.cont6
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp4) #3
  %5 = bitcast %class.Student* %this1 to %class.People*
  %6 = load %"class.std::__cxx11::basic_string"*, %"class.std::__cxx11::basic_string"** %_temp.addr, align 8
  %arrayidx10 = getelementptr inbounds %"class.std::__cxx11::basic_string", %"class.std::__cxx11::basic_string"* %6, i64 2
  invoke void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1ERKS4_(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp9, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %arrayidx10)
          to label %invoke.cont11 unwind label %lpad

invoke.cont11:                                    ; preds = %invoke.cont8
  invoke void @_ZN6People10setSurnameENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE(%class.People* nonnull dereferenceable(192) %5, %"class.std::__cxx11::basic_string"* %agg.tmp9)
          to label %invoke.cont13 unwind label %lpad12

invoke.cont13:                                    ; preds = %invoke.cont11
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp9) #3
  %7 = bitcast %class.Student* %this1 to %class.People*
  %8 = load %"class.std::__cxx11::basic_string"*, %"class.std::__cxx11::basic_string"** %_temp.addr, align 8
  %arrayidx15 = getelementptr inbounds %"class.std::__cxx11::basic_string", %"class.std::__cxx11::basic_string"* %8, i64 3
  invoke void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1ERKS4_(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp14, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %arrayidx15)
          to label %invoke.cont16 unwind label %lpad

invoke.cont16:                                    ; preds = %invoke.cont13
  invoke void @_ZN6People13setDepartmentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE(%class.People* nonnull dereferenceable(192) %7, %"class.std::__cxx11::basic_string"* %agg.tmp14)
          to label %invoke.cont18 unwind label %lpad17

invoke.cont18:                                    ; preds = %invoke.cont16
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp14) #3
  %9 = load %"class.std::__cxx11::basic_string"*, %"class.std::__cxx11::basic_string"** %_temp.addr, align 8
  %arrayidx20 = getelementptr inbounds %"class.std::__cxx11::basic_string", %"class.std::__cxx11::basic_string"* %9, i64 4
  invoke void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1ERKS4_(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp19, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %arrayidx20)
          to label %invoke.cont21 unwind label %lpad

invoke.cont21:                                    ; preds = %invoke.cont18
  invoke void @_ZN7Student7setYearENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE(%class.Student* nonnull dereferenceable(224) %this1, %"class.std::__cxx11::basic_string"* %agg.tmp19)
          to label %invoke.cont23 unwind label %lpad22

invoke.cont23:                                    ; preds = %invoke.cont21
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp19) #3
  %10 = bitcast %class.Student* %this1 to %class.People*
  %11 = load %"class.std::__cxx11::basic_string"*, %"class.std::__cxx11::basic_string"** %_temp.addr, align 8
  %arrayidx25 = getelementptr inbounds %"class.std::__cxx11::basic_string", %"class.std::__cxx11::basic_string"* %11, i64 5
  invoke void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1ERKS4_(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp24, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %arrayidx25)
          to label %invoke.cont26 unwind label %lpad

invoke.cont26:                                    ; preds = %invoke.cont23
  invoke void @_ZN6People8setEmailENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE(%class.People* nonnull dereferenceable(192) %10, %"class.std::__cxx11::basic_string"* %agg.tmp24)
          to label %invoke.cont28 unwind label %lpad27

invoke.cont28:                                    ; preds = %invoke.cont26
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp24) #3
  %12 = bitcast %class.Student* %this1 to %class.People*
  %13 = load %"class.std::__cxx11::basic_string"*, %"class.std::__cxx11::basic_string"** %_temp.addr, align 8
  %arrayidx30 = getelementptr inbounds %"class.std::__cxx11::basic_string", %"class.std::__cxx11::basic_string"* %13, i64 6
  invoke void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1ERKS4_(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp29, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %arrayidx30)
          to label %invoke.cont31 unwind label %lpad

invoke.cont31:                                    ; preds = %invoke.cont28
  invoke void @_ZN6People8setPhoneENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE(%class.People* nonnull dereferenceable(192) %12, %"class.std::__cxx11::basic_string"* %agg.tmp29)
          to label %invoke.cont33 unwind label %lpad32

invoke.cont33:                                    ; preds = %invoke.cont31
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp29) #3
  ret void

lpad:                                             ; preds = %invoke.cont28, %invoke.cont23, %invoke.cont18, %invoke.cont13, %invoke.cont8, %invoke.cont3, %entry
  %14 = landingpad { i8*, i32 }
          cleanup
  %15 = extractvalue { i8*, i32 } %14, 0
  store i8* %15, i8** %exn.slot, align 8
  %16 = extractvalue { i8*, i32 } %14, 1
  store i32 %16, i32* %ehselector.slot, align 4
  br label %ehcleanup

lpad2:                                            ; preds = %invoke.cont
  %17 = landingpad { i8*, i32 }
          cleanup
  %18 = extractvalue { i8*, i32 } %17, 0
  store i8* %18, i8** %exn.slot, align 8
  %19 = extractvalue { i8*, i32 } %17, 1
  store i32 %19, i32* %ehselector.slot, align 4
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp) #3
  br label %ehcleanup

lpad7:                                            ; preds = %invoke.cont6
  %20 = landingpad { i8*, i32 }
          cleanup
  %21 = extractvalue { i8*, i32 } %20, 0
  store i8* %21, i8** %exn.slot, align 8
  %22 = extractvalue { i8*, i32 } %20, 1
  store i32 %22, i32* %ehselector.slot, align 4
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp4) #3
  br label %ehcleanup

lpad12:                                           ; preds = %invoke.cont11
  %23 = landingpad { i8*, i32 }
          cleanup
  %24 = extractvalue { i8*, i32 } %23, 0
  store i8* %24, i8** %exn.slot, align 8
  %25 = extractvalue { i8*, i32 } %23, 1
  store i32 %25, i32* %ehselector.slot, align 4
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp9) #3
  br label %ehcleanup

lpad17:                                           ; preds = %invoke.cont16
  %26 = landingpad { i8*, i32 }
          cleanup
  %27 = extractvalue { i8*, i32 } %26, 0
  store i8* %27, i8** %exn.slot, align 8
  %28 = extractvalue { i8*, i32 } %26, 1
  store i32 %28, i32* %ehselector.slot, align 4
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp14) #3
  br label %ehcleanup

lpad22:                                           ; preds = %invoke.cont21
  %29 = landingpad { i8*, i32 }
          cleanup
  %30 = extractvalue { i8*, i32 } %29, 0
  store i8* %30, i8** %exn.slot, align 8
  %31 = extractvalue { i8*, i32 } %29, 1
  store i32 %31, i32* %ehselector.slot, align 4
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp19) #3
  br label %ehcleanup

lpad27:                                           ; preds = %invoke.cont26
  %32 = landingpad { i8*, i32 }
          cleanup
  %33 = extractvalue { i8*, i32 } %32, 0
  store i8* %33, i8** %exn.slot, align 8
  %34 = extractvalue { i8*, i32 } %32, 1
  store i32 %34, i32* %ehselector.slot, align 4
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp24) #3
  br label %ehcleanup

lpad32:                                           ; preds = %invoke.cont31
  %35 = landingpad { i8*, i32 }
          cleanup
  %36 = extractvalue { i8*, i32 } %35, 0
  store i8* %36, i8** %exn.slot, align 8
  %37 = extractvalue { i8*, i32 } %35, 1
  store i32 %37, i32* %ehselector.slot, align 4
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp29) #3
  br label %ehcleanup

ehcleanup:                                        ; preds = %lpad32, %lpad27, %lpad22, %lpad17, %lpad12, %lpad7, %lpad2, %lpad
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %year) #3
  %38 = bitcast %class.Student* %this1 to %class.People*
  call void @_ZN6PeopleD2Ev(%class.People* nonnull dereferenceable(192) %38) #3
  br label %eh.resume

eh.resume:                                        ; preds = %ehcleanup
  %exn = load i8*, i8** %exn.slot, align 8
  %sel = load i32, i32* %ehselector.slot, align 4
  %lpad.val = insertvalue { i8*, i32 } undef, i8* %exn, 0
  %lpad.val35 = insertvalue { i8*, i32 } %lpad.val, i32 %sel, 1
  resume { i8*, i32 } %lpad.val35
}

; Function Attrs: noinline nounwind optnone uwtable
define linkonce_odr dso_local void @_ZN6PeopleC2Ev(%class.People* nonnull dereferenceable(192) %this) unnamed_addr #5 comdat align 2 {
entry:
  %this.addr = alloca %class.People*, align 8
  store %class.People* %this, %class.People** %this.addr, align 8
  %this1 = load %class.People*, %class.People** %this.addr, align 8
  %number = getelementptr inbounds %class.People, %class.People* %this1, i32 0, i32 0
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %number) #3
  %name = getelementptr inbounds %class.People, %class.People* %this1, i32 0, i32 1
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %name) #3
  %surname = getelementptr inbounds %class.People, %class.People* %this1, i32 0, i32 2
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %surname) #3
  %department = getelementptr inbounds %class.People, %class.People* %this1, i32 0, i32 3
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %department) #3
  %email = getelementptr inbounds %class.People, %class.People* %this1, i32 0, i32 4
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %email) #3
  %phone = getelementptr inbounds %class.People, %class.People* %this1, i32 0, i32 5
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %phone) #3
  ret void
}

; Function Attrs: nounwind
declare dso_local void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32)) unnamed_addr #2

; Function Attrs: noinline optnone uwtable mustprogress
define linkonce_odr dso_local void @_ZN6People9setNumberENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE(%class.People* nonnull dereferenceable(192) %this, %"class.std::__cxx11::basic_string"* %_number) #6 comdat align 2 {
entry:
  %this.addr = alloca %class.People*, align 8
  store %class.People* %this, %class.People** %this.addr, align 8
  %this1 = load %class.People*, %class.People** %this.addr, align 8
  %number = getelementptr inbounds %class.People, %class.People* %this1, i32 0, i32 0
  %call = call nonnull align 8 dereferenceable(32) %"class.std::__cxx11::basic_string"* @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEaSERKS4_(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %number, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %_number)
  ret void
}

declare dso_local void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1ERKS4_(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32), %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32)) unnamed_addr #1

declare dso_local i32 @__gxx_personality_v0(...)

; Function Attrs: nounwind
declare dso_local void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32)) unnamed_addr #2

; Function Attrs: noinline optnone uwtable mustprogress
define linkonce_odr dso_local void @_ZN6People7setNameENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE(%class.People* nonnull dereferenceable(192) %this, %"class.std::__cxx11::basic_string"* %_name) #6 comdat align 2 {
entry:
  %this.addr = alloca %class.People*, align 8
  store %class.People* %this, %class.People** %this.addr, align 8
  %this1 = load %class.People*, %class.People** %this.addr, align 8
  %name = getelementptr inbounds %class.People, %class.People* %this1, i32 0, i32 1
  %call = call nonnull align 8 dereferenceable(32) %"class.std::__cxx11::basic_string"* @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEaSERKS4_(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %name, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %_name)
  ret void
}

; Function Attrs: noinline optnone uwtable mustprogress
define linkonce_odr dso_local void @_ZN6People10setSurnameENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE(%class.People* nonnull dereferenceable(192) %this, %"class.std::__cxx11::basic_string"* %_surname) #6 comdat align 2 {
entry:
  %this.addr = alloca %class.People*, align 8
  store %class.People* %this, %class.People** %this.addr, align 8
  %this1 = load %class.People*, %class.People** %this.addr, align 8
  %surname = getelementptr inbounds %class.People, %class.People* %this1, i32 0, i32 2
  %call = call nonnull align 8 dereferenceable(32) %"class.std::__cxx11::basic_string"* @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEaSERKS4_(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %surname, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %_surname)
  ret void
}

; Function Attrs: noinline optnone uwtable mustprogress
define linkonce_odr dso_local void @_ZN6People13setDepartmentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE(%class.People* nonnull dereferenceable(192) %this, %"class.std::__cxx11::basic_string"* %_department) #6 comdat align 2 {
entry:
  %this.addr = alloca %class.People*, align 8
  store %class.People* %this, %class.People** %this.addr, align 8
  %this1 = load %class.People*, %class.People** %this.addr, align 8
  %department = getelementptr inbounds %class.People, %class.People* %this1, i32 0, i32 3
  %call = call nonnull align 8 dereferenceable(32) %"class.std::__cxx11::basic_string"* @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEaSERKS4_(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %department, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %_department)
  ret void
}

; Function Attrs: noinline optnone uwtable mustprogress
define linkonce_odr dso_local void @_ZN7Student7setYearENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE(%class.Student* nonnull dereferenceable(224) %this, %"class.std::__cxx11::basic_string"* %_year) #6 comdat align 2 {
entry:
  %this.addr = alloca %class.Student*, align 8
  store %class.Student* %this, %class.Student** %this.addr, align 8
  %this1 = load %class.Student*, %class.Student** %this.addr, align 8
  %year = getelementptr inbounds %class.Student, %class.Student* %this1, i32 0, i32 1
  %call = call nonnull align 8 dereferenceable(32) %"class.std::__cxx11::basic_string"* @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEaSERKS4_(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %year, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %_year)
  ret void
}

; Function Attrs: noinline optnone uwtable mustprogress
define linkonce_odr dso_local void @_ZN6People8setEmailENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE(%class.People* nonnull dereferenceable(192) %this, %"class.std::__cxx11::basic_string"* %_email) #6 comdat align 2 {
entry:
  %this.addr = alloca %class.People*, align 8
  store %class.People* %this, %class.People** %this.addr, align 8
  %this1 = load %class.People*, %class.People** %this.addr, align 8
  %email = getelementptr inbounds %class.People, %class.People* %this1, i32 0, i32 4
  %call = call nonnull align 8 dereferenceable(32) %"class.std::__cxx11::basic_string"* @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEaSERKS4_(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %email, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %_email)
  ret void
}

; Function Attrs: noinline optnone uwtable mustprogress
define linkonce_odr dso_local void @_ZN6People8setPhoneENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE(%class.People* nonnull dereferenceable(192) %this, %"class.std::__cxx11::basic_string"* %_phone) #6 comdat align 2 {
entry:
  %this.addr = alloca %class.People*, align 8
  store %class.People* %this, %class.People** %this.addr, align 8
  %this1 = load %class.People*, %class.People** %this.addr, align 8
  %phone = getelementptr inbounds %class.People, %class.People* %this1, i32 0, i32 5
  %call = call nonnull align 8 dereferenceable(32) %"class.std::__cxx11::basic_string"* @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEaSERKS4_(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %phone, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %_phone)
  ret void
}

; Function Attrs: noinline nounwind optnone uwtable
define linkonce_odr dso_local void @_ZN6PeopleD2Ev(%class.People* nonnull dereferenceable(192) %this) unnamed_addr #5 comdat align 2 {
entry:
  %this.addr = alloca %class.People*, align 8
  store %class.People* %this, %class.People** %this.addr, align 8
  %this1 = load %class.People*, %class.People** %this.addr, align 8
  %phone = getelementptr inbounds %class.People, %class.People* %this1, i32 0, i32 5
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %phone) #3
  %email = getelementptr inbounds %class.People, %class.People* %this1, i32 0, i32 4
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %email) #3
  %department = getelementptr inbounds %class.People, %class.People* %this1, i32 0, i32 3
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %department) #3
  %surname = getelementptr inbounds %class.People, %class.People* %this1, i32 0, i32 2
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %surname) #3
  %name = getelementptr inbounds %class.People, %class.People* %this1, i32 0, i32 1
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %name) #3
  %number = getelementptr inbounds %class.People, %class.People* %this1, i32 0, i32 0
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %number) #3
  ret void
}

; Function Attrs: noinline optnone uwtable
define dso_local void @_ZN8LecturerC2EPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE(%class.Lecturer* nonnull dereferenceable(224) %this, %"class.std::__cxx11::basic_string"* %_temp) unnamed_addr #4 align 2 personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
entry:
  %this.addr = alloca %class.Lecturer*, align 8
  %_temp.addr = alloca %"class.std::__cxx11::basic_string"*, align 8
  %agg.tmp = alloca %"class.std::__cxx11::basic_string", align 8
  %exn.slot = alloca i8*, align 8
  %ehselector.slot = alloca i32, align 4
  %agg.tmp4 = alloca %"class.std::__cxx11::basic_string", align 8
  %agg.tmp9 = alloca %"class.std::__cxx11::basic_string", align 8
  %agg.tmp14 = alloca %"class.std::__cxx11::basic_string", align 8
  %agg.tmp19 = alloca %"class.std::__cxx11::basic_string", align 8
  %agg.tmp24 = alloca %"class.std::__cxx11::basic_string", align 8
  %agg.tmp29 = alloca %"class.std::__cxx11::basic_string", align 8
  store %class.Lecturer* %this, %class.Lecturer** %this.addr, align 8
  store %"class.std::__cxx11::basic_string"* %_temp, %"class.std::__cxx11::basic_string"** %_temp.addr, align 8
  %this1 = load %class.Lecturer*, %class.Lecturer** %this.addr, align 8
  %0 = bitcast %class.Lecturer* %this1 to %class.People*
  call void @_ZN6PeopleC2Ev(%class.People* nonnull dereferenceable(192) %0) #3
  %chair = getelementptr inbounds %class.Lecturer, %class.Lecturer* %this1, i32 0, i32 1
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %chair) #3
  %1 = bitcast %class.Lecturer* %this1 to %class.People*
  %2 = load %"class.std::__cxx11::basic_string"*, %"class.std::__cxx11::basic_string"** %_temp.addr, align 8
  %arrayidx = getelementptr inbounds %"class.std::__cxx11::basic_string", %"class.std::__cxx11::basic_string"* %2, i64 0
  invoke void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1ERKS4_(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %arrayidx)
          to label %invoke.cont unwind label %lpad

invoke.cont:                                      ; preds = %entry
  invoke void @_ZN6People9setNumberENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE(%class.People* nonnull dereferenceable(192) %1, %"class.std::__cxx11::basic_string"* %agg.tmp)
          to label %invoke.cont3 unwind label %lpad2

invoke.cont3:                                     ; preds = %invoke.cont
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp) #3
  %3 = bitcast %class.Lecturer* %this1 to %class.People*
  %4 = load %"class.std::__cxx11::basic_string"*, %"class.std::__cxx11::basic_string"** %_temp.addr, align 8
  %arrayidx5 = getelementptr inbounds %"class.std::__cxx11::basic_string", %"class.std::__cxx11::basic_string"* %4, i64 1
  invoke void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1ERKS4_(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp4, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %arrayidx5)
          to label %invoke.cont6 unwind label %lpad

invoke.cont6:                                     ; preds = %invoke.cont3
  invoke void @_ZN6People7setNameENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE(%class.People* nonnull dereferenceable(192) %3, %"class.std::__cxx11::basic_string"* %agg.tmp4)
          to label %invoke.cont8 unwind label %lpad7

invoke.cont8:                                     ; preds = %invoke.cont6
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp4) #3
  %5 = bitcast %class.Lecturer* %this1 to %class.People*
  %6 = load %"class.std::__cxx11::basic_string"*, %"class.std::__cxx11::basic_string"** %_temp.addr, align 8
  %arrayidx10 = getelementptr inbounds %"class.std::__cxx11::basic_string", %"class.std::__cxx11::basic_string"* %6, i64 2
  invoke void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1ERKS4_(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp9, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %arrayidx10)
          to label %invoke.cont11 unwind label %lpad

invoke.cont11:                                    ; preds = %invoke.cont8
  invoke void @_ZN6People10setSurnameENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE(%class.People* nonnull dereferenceable(192) %5, %"class.std::__cxx11::basic_string"* %agg.tmp9)
          to label %invoke.cont13 unwind label %lpad12

invoke.cont13:                                    ; preds = %invoke.cont11
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp9) #3
  %7 = bitcast %class.Lecturer* %this1 to %class.People*
  %8 = load %"class.std::__cxx11::basic_string"*, %"class.std::__cxx11::basic_string"** %_temp.addr, align 8
  %arrayidx15 = getelementptr inbounds %"class.std::__cxx11::basic_string", %"class.std::__cxx11::basic_string"* %8, i64 3
  invoke void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1ERKS4_(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp14, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %arrayidx15)
          to label %invoke.cont16 unwind label %lpad

invoke.cont16:                                    ; preds = %invoke.cont13
  invoke void @_ZN6People13setDepartmentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE(%class.People* nonnull dereferenceable(192) %7, %"class.std::__cxx11::basic_string"* %agg.tmp14)
          to label %invoke.cont18 unwind label %lpad17

invoke.cont18:                                    ; preds = %invoke.cont16
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp14) #3
  %9 = bitcast %class.Lecturer* %this1 to %class.People*
  %10 = load %"class.std::__cxx11::basic_string"*, %"class.std::__cxx11::basic_string"** %_temp.addr, align 8
  %arrayidx20 = getelementptr inbounds %"class.std::__cxx11::basic_string", %"class.std::__cxx11::basic_string"* %10, i64 4
  invoke void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1ERKS4_(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp19, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %arrayidx20)
          to label %invoke.cont21 unwind label %lpad

invoke.cont21:                                    ; preds = %invoke.cont18
  invoke void @_ZN6People8setEmailENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE(%class.People* nonnull dereferenceable(192) %9, %"class.std::__cxx11::basic_string"* %agg.tmp19)
          to label %invoke.cont23 unwind label %lpad22

invoke.cont23:                                    ; preds = %invoke.cont21
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp19) #3
  %11 = bitcast %class.Lecturer* %this1 to %class.People*
  %12 = load %"class.std::__cxx11::basic_string"*, %"class.std::__cxx11::basic_string"** %_temp.addr, align 8
  %arrayidx25 = getelementptr inbounds %"class.std::__cxx11::basic_string", %"class.std::__cxx11::basic_string"* %12, i64 5
  invoke void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1ERKS4_(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp24, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %arrayidx25)
          to label %invoke.cont26 unwind label %lpad

invoke.cont26:                                    ; preds = %invoke.cont23
  invoke void @_ZN6People8setPhoneENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE(%class.People* nonnull dereferenceable(192) %11, %"class.std::__cxx11::basic_string"* %agg.tmp24)
          to label %invoke.cont28 unwind label %lpad27

invoke.cont28:                                    ; preds = %invoke.cont26
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp24) #3
  %13 = load %"class.std::__cxx11::basic_string"*, %"class.std::__cxx11::basic_string"** %_temp.addr, align 8
  %arrayidx30 = getelementptr inbounds %"class.std::__cxx11::basic_string", %"class.std::__cxx11::basic_string"* %13, i64 6
  invoke void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1ERKS4_(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp29, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %arrayidx30)
          to label %invoke.cont31 unwind label %lpad

invoke.cont31:                                    ; preds = %invoke.cont28
  invoke void @_ZN8Lecturer8setChairENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE(%class.Lecturer* nonnull dereferenceable(224) %this1, %"class.std::__cxx11::basic_string"* %agg.tmp29)
          to label %invoke.cont33 unwind label %lpad32

invoke.cont33:                                    ; preds = %invoke.cont31
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp29) #3
  ret void

lpad:                                             ; preds = %invoke.cont28, %invoke.cont23, %invoke.cont18, %invoke.cont13, %invoke.cont8, %invoke.cont3, %entry
  %14 = landingpad { i8*, i32 }
          cleanup
  %15 = extractvalue { i8*, i32 } %14, 0
  store i8* %15, i8** %exn.slot, align 8
  %16 = extractvalue { i8*, i32 } %14, 1
  store i32 %16, i32* %ehselector.slot, align 4
  br label %ehcleanup

lpad2:                                            ; preds = %invoke.cont
  %17 = landingpad { i8*, i32 }
          cleanup
  %18 = extractvalue { i8*, i32 } %17, 0
  store i8* %18, i8** %exn.slot, align 8
  %19 = extractvalue { i8*, i32 } %17, 1
  store i32 %19, i32* %ehselector.slot, align 4
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp) #3
  br label %ehcleanup

lpad7:                                            ; preds = %invoke.cont6
  %20 = landingpad { i8*, i32 }
          cleanup
  %21 = extractvalue { i8*, i32 } %20, 0
  store i8* %21, i8** %exn.slot, align 8
  %22 = extractvalue { i8*, i32 } %20, 1
  store i32 %22, i32* %ehselector.slot, align 4
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp4) #3
  br label %ehcleanup

lpad12:                                           ; preds = %invoke.cont11
  %23 = landingpad { i8*, i32 }
          cleanup
  %24 = extractvalue { i8*, i32 } %23, 0
  store i8* %24, i8** %exn.slot, align 8
  %25 = extractvalue { i8*, i32 } %23, 1
  store i32 %25, i32* %ehselector.slot, align 4
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp9) #3
  br label %ehcleanup

lpad17:                                           ; preds = %invoke.cont16
  %26 = landingpad { i8*, i32 }
          cleanup
  %27 = extractvalue { i8*, i32 } %26, 0
  store i8* %27, i8** %exn.slot, align 8
  %28 = extractvalue { i8*, i32 } %26, 1
  store i32 %28, i32* %ehselector.slot, align 4
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp14) #3
  br label %ehcleanup

lpad22:                                           ; preds = %invoke.cont21
  %29 = landingpad { i8*, i32 }
          cleanup
  %30 = extractvalue { i8*, i32 } %29, 0
  store i8* %30, i8** %exn.slot, align 8
  %31 = extractvalue { i8*, i32 } %29, 1
  store i32 %31, i32* %ehselector.slot, align 4
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp19) #3
  br label %ehcleanup

lpad27:                                           ; preds = %invoke.cont26
  %32 = landingpad { i8*, i32 }
          cleanup
  %33 = extractvalue { i8*, i32 } %32, 0
  store i8* %33, i8** %exn.slot, align 8
  %34 = extractvalue { i8*, i32 } %32, 1
  store i32 %34, i32* %ehselector.slot, align 4
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp24) #3
  br label %ehcleanup

lpad32:                                           ; preds = %invoke.cont31
  %35 = landingpad { i8*, i32 }
          cleanup
  %36 = extractvalue { i8*, i32 } %35, 0
  store i8* %36, i8** %exn.slot, align 8
  %37 = extractvalue { i8*, i32 } %35, 1
  store i32 %37, i32* %ehselector.slot, align 4
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp29) #3
  br label %ehcleanup

ehcleanup:                                        ; preds = %lpad32, %lpad27, %lpad22, %lpad17, %lpad12, %lpad7, %lpad2, %lpad
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %chair) #3
  %38 = bitcast %class.Lecturer* %this1 to %class.People*
  call void @_ZN6PeopleD2Ev(%class.People* nonnull dereferenceable(192) %38) #3
  br label %eh.resume

eh.resume:                                        ; preds = %ehcleanup
  %exn = load i8*, i8** %exn.slot, align 8
  %sel = load i32, i32* %ehselector.slot, align 4
  %lpad.val = insertvalue { i8*, i32 } undef, i8* %exn, 0
  %lpad.val35 = insertvalue { i8*, i32 } %lpad.val, i32 %sel, 1
  resume { i8*, i32 } %lpad.val35
}

; Function Attrs: noinline optnone uwtable mustprogress
define linkonce_odr dso_local void @_ZN8Lecturer8setChairENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE(%class.Lecturer* nonnull dereferenceable(224) %this, %"class.std::__cxx11::basic_string"* %_chair) #6 comdat align 2 {
entry:
  %this.addr = alloca %class.Lecturer*, align 8
  store %class.Lecturer* %this, %class.Lecturer** %this.addr, align 8
  %this1 = load %class.Lecturer*, %class.Lecturer** %this.addr, align 8
  %chair = getelementptr inbounds %class.Lecturer, %class.Lecturer* %this1, i32 0, i32 1
  %call = call nonnull align 8 dereferenceable(32) %"class.std::__cxx11::basic_string"* @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEaSERKS4_(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %chair, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %_chair)
  ret void
}

; Function Attrs: noinline optnone uwtable
define dso_local void @_ZN11AppointmentC2EPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE(%class.Appointment* nonnull dereferenceable(544) %this, %"class.std::__cxx11::basic_string"* %_temp) unnamed_addr #4 align 2 personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
entry:
  %this.addr = alloca %class.Appointment*, align 8
  %_temp.addr = alloca %"class.std::__cxx11::basic_string"*, align 8
  %exn.slot = alloca i8*, align 8
  %ehselector.slot = alloca i32, align 4
  %agg.tmp = alloca %"class.std::__cxx11::basic_string", align 8
  %agg.tmp10 = alloca %"class.std::__cxx11::basic_string", align 8
  %agg.tmp15 = alloca %"class.std::__cxx11::basic_string", align 8
  %agg.tmp20 = alloca %"class.std::__cxx11::basic_string", align 8
  %agg.tmp25 = alloca %"class.std::__cxx11::basic_string", align 8
  store %class.Appointment* %this, %class.Appointment** %this.addr, align 8
  store %"class.std::__cxx11::basic_string"* %_temp, %"class.std::__cxx11::basic_string"** %_temp.addr, align 8
  %this1 = load %class.Appointment*, %class.Appointment** %this.addr, align 8
  %date = getelementptr inbounds %class.Appointment, %class.Appointment* %this1, i32 0, i32 0
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %date) #3
  %start = getelementptr inbounds %class.Appointment, %class.Appointment* %this1, i32 0, i32 1
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %start) #3
  %end = getelementptr inbounds %class.Appointment, %class.Appointment* %this1, i32 0, i32 2
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %end) #3
  %L = getelementptr inbounds %class.Appointment, %class.Appointment* %this1, i32 0, i32 3
  invoke void @_ZN8LecturerC2Ev(%class.Lecturer* nonnull dereferenceable(224) %L)
          to label %invoke.cont unwind label %lpad

invoke.cont:                                      ; preds = %entry
  %S = getelementptr inbounds %class.Appointment, %class.Appointment* %this1, i32 0, i32 4
  invoke void @_ZN7StudentC2Ev(%class.Student* nonnull dereferenceable(224) %S)
          to label %invoke.cont3 unwind label %lpad2

invoke.cont3:                                     ; preds = %invoke.cont
  %S4 = getelementptr inbounds %class.Appointment, %class.Appointment* %this1, i32 0, i32 4
  %0 = bitcast %class.Student* %S4 to %class.People*
  %1 = load %"class.std::__cxx11::basic_string"*, %"class.std::__cxx11::basic_string"** %_temp.addr, align 8
  %arrayidx = getelementptr inbounds %"class.std::__cxx11::basic_string", %"class.std::__cxx11::basic_string"* %1, i64 0
  invoke void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1ERKS4_(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %arrayidx)
          to label %invoke.cont6 unwind label %lpad5

invoke.cont6:                                     ; preds = %invoke.cont3
  invoke void @_ZN6People9setNumberENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE(%class.People* nonnull dereferenceable(192) %0, %"class.std::__cxx11::basic_string"* %agg.tmp)
          to label %invoke.cont8 unwind label %lpad7

invoke.cont8:                                     ; preds = %invoke.cont6
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp) #3
  %L9 = getelementptr inbounds %class.Appointment, %class.Appointment* %this1, i32 0, i32 3
  %2 = bitcast %class.Lecturer* %L9 to %class.People*
  %3 = load %"class.std::__cxx11::basic_string"*, %"class.std::__cxx11::basic_string"** %_temp.addr, align 8
  %arrayidx11 = getelementptr inbounds %"class.std::__cxx11::basic_string", %"class.std::__cxx11::basic_string"* %3, i64 1
  invoke void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1ERKS4_(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp10, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %arrayidx11)
          to label %invoke.cont12 unwind label %lpad5

invoke.cont12:                                    ; preds = %invoke.cont8
  invoke void @_ZN6People9setNumberENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE(%class.People* nonnull dereferenceable(192) %2, %"class.std::__cxx11::basic_string"* %agg.tmp10)
          to label %invoke.cont14 unwind label %lpad13

invoke.cont14:                                    ; preds = %invoke.cont12
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp10) #3
  %4 = load %"class.std::__cxx11::basic_string"*, %"class.std::__cxx11::basic_string"** %_temp.addr, align 8
  %arrayidx16 = getelementptr inbounds %"class.std::__cxx11::basic_string", %"class.std::__cxx11::basic_string"* %4, i64 2
  invoke void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1ERKS4_(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp15, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %arrayidx16)
          to label %invoke.cont17 unwind label %lpad5

invoke.cont17:                                    ; preds = %invoke.cont14
  invoke void @_ZN11Appointment7setDateENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE(%class.Appointment* nonnull dereferenceable(544) %this1, %"class.std::__cxx11::basic_string"* %agg.tmp15)
          to label %invoke.cont19 unwind label %lpad18

invoke.cont19:                                    ; preds = %invoke.cont17
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp15) #3
  %5 = load %"class.std::__cxx11::basic_string"*, %"class.std::__cxx11::basic_string"** %_temp.addr, align 8
  %arrayidx21 = getelementptr inbounds %"class.std::__cxx11::basic_string", %"class.std::__cxx11::basic_string"* %5, i64 3
  invoke void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1ERKS4_(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp20, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %arrayidx21)
          to label %invoke.cont22 unwind label %lpad5

invoke.cont22:                                    ; preds = %invoke.cont19
  invoke void @_ZN11Appointment8setStartENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE(%class.Appointment* nonnull dereferenceable(544) %this1, %"class.std::__cxx11::basic_string"* %agg.tmp20)
          to label %invoke.cont24 unwind label %lpad23

invoke.cont24:                                    ; preds = %invoke.cont22
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp20) #3
  %6 = load %"class.std::__cxx11::basic_string"*, %"class.std::__cxx11::basic_string"** %_temp.addr, align 8
  %arrayidx26 = getelementptr inbounds %"class.std::__cxx11::basic_string", %"class.std::__cxx11::basic_string"* %6, i64 4
  invoke void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1ERKS4_(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp25, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %arrayidx26)
          to label %invoke.cont27 unwind label %lpad5

invoke.cont27:                                    ; preds = %invoke.cont24
  invoke void @_ZN11Appointment6setEndENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE(%class.Appointment* nonnull dereferenceable(544) %this1, %"class.std::__cxx11::basic_string"* %agg.tmp25)
          to label %invoke.cont29 unwind label %lpad28

invoke.cont29:                                    ; preds = %invoke.cont27
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp25) #3
  ret void

lpad:                                             ; preds = %entry
  %7 = landingpad { i8*, i32 }
          cleanup
  %8 = extractvalue { i8*, i32 } %7, 0
  store i8* %8, i8** %exn.slot, align 8
  %9 = extractvalue { i8*, i32 } %7, 1
  store i32 %9, i32* %ehselector.slot, align 4
  br label %ehcleanup31

lpad2:                                            ; preds = %invoke.cont
  %10 = landingpad { i8*, i32 }
          cleanup
  %11 = extractvalue { i8*, i32 } %10, 0
  store i8* %11, i8** %exn.slot, align 8
  %12 = extractvalue { i8*, i32 } %10, 1
  store i32 %12, i32* %ehselector.slot, align 4
  br label %ehcleanup30

lpad5:                                            ; preds = %invoke.cont24, %invoke.cont19, %invoke.cont14, %invoke.cont8, %invoke.cont3
  %13 = landingpad { i8*, i32 }
          cleanup
  %14 = extractvalue { i8*, i32 } %13, 0
  store i8* %14, i8** %exn.slot, align 8
  %15 = extractvalue { i8*, i32 } %13, 1
  store i32 %15, i32* %ehselector.slot, align 4
  br label %ehcleanup

lpad7:                                            ; preds = %invoke.cont6
  %16 = landingpad { i8*, i32 }
          cleanup
  %17 = extractvalue { i8*, i32 } %16, 0
  store i8* %17, i8** %exn.slot, align 8
  %18 = extractvalue { i8*, i32 } %16, 1
  store i32 %18, i32* %ehselector.slot, align 4
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp) #3
  br label %ehcleanup

lpad13:                                           ; preds = %invoke.cont12
  %19 = landingpad { i8*, i32 }
          cleanup
  %20 = extractvalue { i8*, i32 } %19, 0
  store i8* %20, i8** %exn.slot, align 8
  %21 = extractvalue { i8*, i32 } %19, 1
  store i32 %21, i32* %ehselector.slot, align 4
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp10) #3
  br label %ehcleanup

lpad18:                                           ; preds = %invoke.cont17
  %22 = landingpad { i8*, i32 }
          cleanup
  %23 = extractvalue { i8*, i32 } %22, 0
  store i8* %23, i8** %exn.slot, align 8
  %24 = extractvalue { i8*, i32 } %22, 1
  store i32 %24, i32* %ehselector.slot, align 4
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp15) #3
  br label %ehcleanup

lpad23:                                           ; preds = %invoke.cont22
  %25 = landingpad { i8*, i32 }
          cleanup
  %26 = extractvalue { i8*, i32 } %25, 0
  store i8* %26, i8** %exn.slot, align 8
  %27 = extractvalue { i8*, i32 } %25, 1
  store i32 %27, i32* %ehselector.slot, align 4
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp20) #3
  br label %ehcleanup

lpad28:                                           ; preds = %invoke.cont27
  %28 = landingpad { i8*, i32 }
          cleanup
  %29 = extractvalue { i8*, i32 } %28, 0
  store i8* %29, i8** %exn.slot, align 8
  %30 = extractvalue { i8*, i32 } %28, 1
  store i32 %30, i32* %ehselector.slot, align 4
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp25) #3
  br label %ehcleanup

ehcleanup:                                        ; preds = %lpad28, %lpad23, %lpad18, %lpad13, %lpad7, %lpad5
  call void @_ZN7StudentD2Ev(%class.Student* nonnull dereferenceable(224) %S) #3
  br label %ehcleanup30

ehcleanup30:                                      ; preds = %ehcleanup, %lpad2
  call void @_ZN8LecturerD2Ev(%class.Lecturer* nonnull dereferenceable(224) %L) #3
  br label %ehcleanup31

ehcleanup31:                                      ; preds = %ehcleanup30, %lpad
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %end) #3
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %start) #3
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %date) #3
  br label %eh.resume

eh.resume:                                        ; preds = %ehcleanup31
  %exn = load i8*, i8** %exn.slot, align 8
  %sel = load i32, i32* %ehselector.slot, align 4
  %lpad.val = insertvalue { i8*, i32 } undef, i8* %exn, 0
  %lpad.val34 = insertvalue { i8*, i32 } %lpad.val, i32 %sel, 1
  resume { i8*, i32 } %lpad.val34
}

; Function Attrs: noinline nounwind optnone uwtable
define linkonce_odr dso_local void @_ZN8LecturerC2Ev(%class.Lecturer* nonnull dereferenceable(224) %this) unnamed_addr #5 comdat align 2 {
entry:
  %this.addr = alloca %class.Lecturer*, align 8
  store %class.Lecturer* %this, %class.Lecturer** %this.addr, align 8
  %this1 = load %class.Lecturer*, %class.Lecturer** %this.addr, align 8
  %0 = bitcast %class.Lecturer* %this1 to %class.People*
  call void @_ZN6PeopleC2Ev(%class.People* nonnull dereferenceable(192) %0) #3
  %chair = getelementptr inbounds %class.Lecturer, %class.Lecturer* %this1, i32 0, i32 1
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %chair) #3
  ret void
}

; Function Attrs: noinline nounwind optnone uwtable
define linkonce_odr dso_local void @_ZN7StudentC2Ev(%class.Student* nonnull dereferenceable(224) %this) unnamed_addr #5 comdat align 2 {
entry:
  %this.addr = alloca %class.Student*, align 8
  store %class.Student* %this, %class.Student** %this.addr, align 8
  %this1 = load %class.Student*, %class.Student** %this.addr, align 8
  %0 = bitcast %class.Student* %this1 to %class.People*
  call void @_ZN6PeopleC2Ev(%class.People* nonnull dereferenceable(192) %0) #3
  %year = getelementptr inbounds %class.Student, %class.Student* %this1, i32 0, i32 1
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %year) #3
  ret void
}

; Function Attrs: noinline optnone uwtable mustprogress
define linkonce_odr dso_local void @_ZN11Appointment7setDateENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE(%class.Appointment* nonnull dereferenceable(544) %this, %"class.std::__cxx11::basic_string"* %_date) #6 comdat align 2 {
entry:
  %this.addr = alloca %class.Appointment*, align 8
  store %class.Appointment* %this, %class.Appointment** %this.addr, align 8
  %this1 = load %class.Appointment*, %class.Appointment** %this.addr, align 8
  %date = getelementptr inbounds %class.Appointment, %class.Appointment* %this1, i32 0, i32 0
  %call = call nonnull align 8 dereferenceable(32) %"class.std::__cxx11::basic_string"* @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEaSERKS4_(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %date, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %_date)
  ret void
}

; Function Attrs: noinline optnone uwtable mustprogress
define linkonce_odr dso_local void @_ZN11Appointment8setStartENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE(%class.Appointment* nonnull dereferenceable(544) %this, %"class.std::__cxx11::basic_string"* %_start) #6 comdat align 2 {
entry:
  %this.addr = alloca %class.Appointment*, align 8
  store %class.Appointment* %this, %class.Appointment** %this.addr, align 8
  %this1 = load %class.Appointment*, %class.Appointment** %this.addr, align 8
  %start = getelementptr inbounds %class.Appointment, %class.Appointment* %this1, i32 0, i32 1
  %call = call nonnull align 8 dereferenceable(32) %"class.std::__cxx11::basic_string"* @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEaSERKS4_(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %start, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %_start)
  ret void
}

; Function Attrs: noinline optnone uwtable mustprogress
define linkonce_odr dso_local void @_ZN11Appointment6setEndENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE(%class.Appointment* nonnull dereferenceable(544) %this, %"class.std::__cxx11::basic_string"* %_end) #6 comdat align 2 {
entry:
  %this.addr = alloca %class.Appointment*, align 8
  store %class.Appointment* %this, %class.Appointment** %this.addr, align 8
  %this1 = load %class.Appointment*, %class.Appointment** %this.addr, align 8
  %end = getelementptr inbounds %class.Appointment, %class.Appointment* %this1, i32 0, i32 2
  %call = call nonnull align 8 dereferenceable(32) %"class.std::__cxx11::basic_string"* @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEaSERKS4_(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %end, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %_end)
  ret void
}

; Function Attrs: noinline nounwind optnone uwtable
define linkonce_odr dso_local void @_ZN7StudentD2Ev(%class.Student* nonnull dereferenceable(224) %this) unnamed_addr #5 comdat align 2 {
entry:
  %this.addr = alloca %class.Student*, align 8
  store %class.Student* %this, %class.Student** %this.addr, align 8
  %this1 = load %class.Student*, %class.Student** %this.addr, align 8
  %year = getelementptr inbounds %class.Student, %class.Student* %this1, i32 0, i32 1
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %year) #3
  %0 = bitcast %class.Student* %this1 to %class.People*
  call void @_ZN6PeopleD2Ev(%class.People* nonnull dereferenceable(192) %0) #3
  ret void
}

; Function Attrs: noinline nounwind optnone uwtable
define linkonce_odr dso_local void @_ZN8LecturerD2Ev(%class.Lecturer* nonnull dereferenceable(224) %this) unnamed_addr #5 comdat align 2 {
entry:
  %this.addr = alloca %class.Lecturer*, align 8
  store %class.Lecturer* %this, %class.Lecturer** %this.addr, align 8
  %this1 = load %class.Lecturer*, %class.Lecturer** %this.addr, align 8
  %chair = getelementptr inbounds %class.Lecturer, %class.Lecturer* %this1, i32 0, i32 1
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %chair) #3
  %0 = bitcast %class.Lecturer* %this1 to %class.People*
  call void @_ZN6PeopleD2Ev(%class.People* nonnull dereferenceable(192) %0) #3
  ret void
}

; Function Attrs: noinline optnone uwtable mustprogress
define dso_local void @_Z9countFilePci(i8* %filename, i32 %filenum) #6 personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
entry:
  %filename.addr = alloca i8*, align 8
  %filenum.addr = alloca i32, align 4
  %temp = alloca %"class.std::__cxx11::basic_string", align 8
  %readFile = alloca %"class.std::basic_ifstream", align 8
  %exn.slot = alloca i8*, align 8
  %ehselector.slot = alloca i32, align 4
  store i8* %filename, i8** %filename.addr, align 8
  store i32 %filenum, i32* %filenum.addr, align 4
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %temp) #3
  invoke void @_ZNSt14basic_ifstreamIcSt11char_traitsIcEEC1Ev(%"class.std::basic_ifstream"* nonnull dereferenceable(256) %readFile)
          to label %invoke.cont unwind label %lpad

invoke.cont:                                      ; preds = %entry
  %0 = load i8*, i8** %filename.addr, align 8
  invoke void @_ZNSt14basic_ifstreamIcSt11char_traitsIcEE4openEPKcSt13_Ios_Openmode(%"class.std::basic_ifstream"* nonnull dereferenceable(256) %readFile, i8* %0, i32 8)
          to label %invoke.cont2 unwind label %lpad1

invoke.cont2:                                     ; preds = %invoke.cont
  br label %while.cond

while.cond:                                       ; preds = %while.body, %invoke.cont2
  %1 = bitcast %"class.std::basic_ifstream"* %readFile to %"class.std::basic_istream"*
  %call = invoke nonnull align 8 dereferenceable(16) %"class.std::basic_istream"* @_ZSt7getlineIcSt11char_traitsIcESaIcEERSt13basic_istreamIT_T0_ES7_RNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_istream"* nonnull align 8 dereferenceable(16) %1, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %temp)
          to label %invoke.cont3 unwind label %lpad1

invoke.cont3:                                     ; preds = %while.cond
  %2 = bitcast %"class.std::basic_istream"* %call to i8**
  %vtable = load i8*, i8** %2, align 8
  %vbase.offset.ptr = getelementptr i8, i8* %vtable, i64 -24
  %3 = bitcast i8* %vbase.offset.ptr to i64*
  %vbase.offset = load i64, i64* %3, align 8
  %4 = bitcast %"class.std::basic_istream"* %call to i8*
  %add.ptr = getelementptr inbounds i8, i8* %4, i64 %vbase.offset
  %5 = bitcast i8* %add.ptr to %"class.std::basic_ios"*
  %call5 = invoke zeroext i1 @_ZNKSt9basic_iosIcSt11char_traitsIcEEcvbEv(%"class.std::basic_ios"* nonnull dereferenceable(264) %5)
          to label %invoke.cont4 unwind label %lpad1

invoke.cont4:                                     ; preds = %invoke.cont3
  br i1 %call5, label %while.body, label %while.end

while.body:                                       ; preds = %invoke.cont4
  %6 = load i32, i32* %filenum.addr, align 4
  %idxprom = sext i32 %6 to i64
  %arrayidx = getelementptr inbounds [3 x i32], [3 x i32]* @count, i64 0, i64 %idxprom
  %7 = load i32, i32* %arrayidx, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %arrayidx, align 4
  br label %while.cond, !llvm.loop !4

lpad:                                             ; preds = %entry
  %8 = landingpad { i8*, i32 }
          cleanup
  %9 = extractvalue { i8*, i32 } %8, 0
  store i8* %9, i8** %exn.slot, align 8
  %10 = extractvalue { i8*, i32 } %8, 1
  store i32 %10, i32* %ehselector.slot, align 4
  br label %ehcleanup

lpad1:                                            ; preds = %while.end, %invoke.cont3, %while.cond, %invoke.cont
  %11 = landingpad { i8*, i32 }
          cleanup
  %12 = extractvalue { i8*, i32 } %11, 0
  store i8* %12, i8** %exn.slot, align 8
  %13 = extractvalue { i8*, i32 } %11, 1
  store i32 %13, i32* %ehselector.slot, align 4
  call void @_ZNSt14basic_ifstreamIcSt11char_traitsIcEED1Ev(%"class.std::basic_ifstream"* nonnull dereferenceable(256) %readFile) #3
  br label %ehcleanup

while.end:                                        ; preds = %invoke.cont4
  invoke void @_ZNSt14basic_ifstreamIcSt11char_traitsIcEE5closeEv(%"class.std::basic_ifstream"* nonnull dereferenceable(256) %readFile)
          to label %invoke.cont6 unwind label %lpad1

invoke.cont6:                                     ; preds = %while.end
  call void @_ZNSt14basic_ifstreamIcSt11char_traitsIcEED1Ev(%"class.std::basic_ifstream"* nonnull dereferenceable(256) %readFile) #3
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %temp) #3
  ret void

ehcleanup:                                        ; preds = %lpad1, %lpad
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %temp) #3
  br label %eh.resume

eh.resume:                                        ; preds = %ehcleanup
  %exn = load i8*, i8** %exn.slot, align 8
  %sel = load i32, i32* %ehselector.slot, align 4
  %lpad.val = insertvalue { i8*, i32 } undef, i8* %exn, 0
  %lpad.val7 = insertvalue { i8*, i32 } %lpad.val, i32 %sel, 1
  resume { i8*, i32 } %lpad.val7
}

declare dso_local void @_ZNSt14basic_ifstreamIcSt11char_traitsIcEEC1Ev(%"class.std::basic_ifstream"* nonnull dereferenceable(256)) unnamed_addr #1

declare dso_local void @_ZNSt14basic_ifstreamIcSt11char_traitsIcEE4openEPKcSt13_Ios_Openmode(%"class.std::basic_ifstream"* nonnull dereferenceable(256), i8*, i32) #1

declare dso_local nonnull align 8 dereferenceable(16) %"class.std::basic_istream"* @_ZSt7getlineIcSt11char_traitsIcESaIcEERSt13basic_istreamIT_T0_ES7_RNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_istream"* nonnull align 8 dereferenceable(16), %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32)) #1

declare dso_local zeroext i1 @_ZNKSt9basic_iosIcSt11char_traitsIcEEcvbEv(%"class.std::basic_ios"* nonnull dereferenceable(264)) #1

declare dso_local void @_ZNSt14basic_ifstreamIcSt11char_traitsIcEE5closeEv(%"class.std::basic_ifstream"* nonnull dereferenceable(256)) #1

; Function Attrs: nounwind
declare dso_local void @_ZNSt14basic_ifstreamIcSt11char_traitsIcEED1Ev(%"class.std::basic_ifstream"* nonnull dereferenceable(256)) unnamed_addr #2

; Function Attrs: noinline optnone uwtable mustprogress
define dso_local void @_Z12readFromFileP7StudentP8LecturerP11AppointmentPcS5_S5_(%class.Student* %_s, %class.Lecturer* %_l, %class.Appointment* %_a, i8* %f1, i8* %f2, i8* %f3) #6 personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
entry:
  %_s.addr = alloca %class.Student*, align 8
  %_l.addr = alloca %class.Lecturer*, align 8
  %_a.addr = alloca %class.Appointment*, align 8
  %f1.addr = alloca i8*, align 8
  %f2.addr = alloca i8*, align 8
  %f3.addr = alloca i8*, align 8
  %temp = alloca [10 x %"class.std::__cxx11::basic_string"], align 16
  %i = alloca i32, align 4
  %num = alloca i32, align 4
  %j = alloca i32, align 4
  %readFile = alloca %"class.std::basic_ifstream", align 8
  %exn.slot = alloca i8*, align 8
  %ehselector.slot = alloca i32, align 4
  %s = alloca %class.Student, align 8
  %l = alloca %class.Lecturer, align 8
  %control = alloca i8, align 1
  %a = alloca %class.Appointment, align 8
  %ref.tmp = alloca %"class.std::__cxx11::basic_string", align 8
  %ref.tmp83 = alloca %"class.std::__cxx11::basic_string", align 8
  %cleanup.cond = alloca i1, align 1
  %ref.tmp90 = alloca %"class.std::__cxx11::basic_string", align 8
  %cleanup.cond95 = alloca i1, align 1
  %a129 = alloca %class.Appointment, align 8
  store %class.Student* %_s, %class.Student** %_s.addr, align 8
  store %class.Lecturer* %_l, %class.Lecturer** %_l.addr, align 8
  store %class.Appointment* %_a, %class.Appointment** %_a.addr, align 8
  store i8* %f1, i8** %f1.addr, align 8
  store i8* %f2, i8** %f2.addr, align 8
  store i8* %f3, i8** %f3.addr, align 8
  %array.begin = getelementptr inbounds [10 x %"class.std::__cxx11::basic_string"], [10 x %"class.std::__cxx11::basic_string"]* %temp, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %"class.std::__cxx11::basic_string", %"class.std::__cxx11::basic_string"* %array.begin, i64 10
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %"class.std::__cxx11::basic_string"* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %arrayctor.cur) #3
  %arrayctor.next = getelementptr inbounds %"class.std::__cxx11::basic_string", %"class.std::__cxx11::basic_string"* %arrayctor.cur, i64 1
  %arrayctor.done = icmp eq %"class.std::__cxx11::basic_string"* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  store i32 0, i32* %i, align 4
  store i32 0, i32* %num, align 4
  store i32 0, i32* %j, align 4
  %0 = load i8*, i8** %f1.addr, align 8
  invoke void @_ZNSt14basic_ifstreamIcSt11char_traitsIcEEC1EPKcSt13_Ios_Openmode(%"class.std::basic_ifstream"* nonnull dereferenceable(256) %readFile, i8* %0, i32 8)
          to label %invoke.cont unwind label %lpad

invoke.cont:                                      ; preds = %arrayctor.cont
  %call = invoke zeroext i1 @_ZNSt14basic_ifstreamIcSt11char_traitsIcEE7is_openEv(%"class.std::basic_ifstream"* nonnull dereferenceable(256) %readFile)
          to label %invoke.cont2 unwind label %lpad1

invoke.cont2:                                     ; preds = %invoke.cont
  br i1 %call, label %if.then, label %if.end15

if.then:                                          ; preds = %invoke.cont2
  br label %while.cond

while.cond:                                       ; preds = %if.end, %if.then
  %1 = bitcast %"class.std::basic_ifstream"* %readFile to %"class.std::basic_istream"*
  %2 = load i32, i32* %i, align 4
  %idxprom = sext i32 %2 to i64
  %arrayidx = getelementptr inbounds [10 x %"class.std::__cxx11::basic_string"], [10 x %"class.std::__cxx11::basic_string"]* %temp, i64 0, i64 %idxprom
  %call4 = invoke nonnull align 8 dereferenceable(16) %"class.std::basic_istream"* @_ZStrsIcSt11char_traitsIcESaIcEERSt13basic_istreamIT_T0_ES7_RNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_istream"* nonnull align 8 dereferenceable(16) %1, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %arrayidx)
          to label %invoke.cont3 unwind label %lpad1

invoke.cont3:                                     ; preds = %while.cond
  %3 = bitcast %"class.std::basic_istream"* %call4 to i8**
  %vtable = load i8*, i8** %3, align 8
  %vbase.offset.ptr = getelementptr i8, i8* %vtable, i64 -24
  %4 = bitcast i8* %vbase.offset.ptr to i64*
  %vbase.offset = load i64, i64* %4, align 8
  %5 = bitcast %"class.std::basic_istream"* %call4 to i8*
  %add.ptr = getelementptr inbounds i8, i8* %5, i64 %vbase.offset
  %6 = bitcast i8* %add.ptr to %"class.std::basic_ios"*
  %call6 = invoke zeroext i1 @_ZNKSt9basic_iosIcSt11char_traitsIcEEcvbEv(%"class.std::basic_ios"* nonnull dereferenceable(264) %6)
          to label %invoke.cont5 unwind label %lpad1

invoke.cont5:                                     ; preds = %invoke.cont3
  br i1 %call6, label %while.body, label %while.end

while.body:                                       ; preds = %invoke.cont5
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  %8 = load i32, i32* %i, align 4
  %cmp = icmp eq i32 %8, 7
  br i1 %cmp, label %if.then7, label %if.end

if.then7:                                         ; preds = %while.body
  %arraydecay = getelementptr inbounds [10 x %"class.std::__cxx11::basic_string"], [10 x %"class.std::__cxx11::basic_string"]* %temp, i64 0, i64 0
  invoke void @_ZN7StudentC1EPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE(%class.Student* nonnull dereferenceable(224) %s, %"class.std::__cxx11::basic_string"* %arraydecay)
          to label %invoke.cont8 unwind label %lpad1

invoke.cont8:                                     ; preds = %if.then7
  %9 = load %class.Student*, %class.Student** %_s.addr, align 8
  %10 = load i32, i32* %num, align 4
  %idxprom9 = sext i32 %10 to i64
  %arrayidx10 = getelementptr inbounds %class.Student, %class.Student* %9, i64 %idxprom9
  %call13 = invoke nonnull align 8 dereferenceable(224) %class.Student* @_ZN7StudentaSERKS_(%class.Student* nonnull dereferenceable(224) %arrayidx10, %class.Student* nonnull align 8 dereferenceable(224) %s)
          to label %invoke.cont12 unwind label %lpad11

invoke.cont12:                                    ; preds = %invoke.cont8
  %11 = load i32, i32* %num, align 4
  %inc14 = add nsw i32 %11, 1
  store i32 %inc14, i32* %num, align 4
  store i32 0, i32* %i, align 4
  call void @_ZN7StudentD2Ev(%class.Student* nonnull dereferenceable(224) %s) #3
  br label %if.end

lpad:                                             ; preds = %arrayctor.cont
  %12 = landingpad { i8*, i32 }
          cleanup
  %13 = extractvalue { i8*, i32 } %12, 0
  store i8* %13, i8** %exn.slot, align 8
  %14 = extractvalue { i8*, i32 } %12, 1
  store i32 %14, i32* %ehselector.slot, align 4
  br label %ehcleanup148

lpad1:                                            ; preds = %if.end143, %if.then128, %invoke.cont121, %invoke.cont119, %invoke.cont117, %invoke.cont115, %invoke.cont113, %invoke.cont110, %invoke.cont108, %invoke.cont105, %if.then104, %for.body, %if.then68, %invoke.cont55, %while.cond52, %invoke.cont48, %invoke.cont47, %if.end46, %if.then35, %invoke.cont24, %while.cond21, %invoke.cont17, %invoke.cont16, %if.end15, %if.then7, %invoke.cont3, %while.cond, %invoke.cont
  %15 = landingpad { i8*, i32 }
          cleanup
  %16 = extractvalue { i8*, i32 } %15, 0
  store i8* %16, i8** %exn.slot, align 8
  %17 = extractvalue { i8*, i32 } %15, 1
  store i32 %17, i32* %ehselector.slot, align 4
  br label %ehcleanup145

lpad11:                                           ; preds = %invoke.cont8
  %18 = landingpad { i8*, i32 }
          cleanup
  %19 = extractvalue { i8*, i32 } %18, 0
  store i8* %19, i8** %exn.slot, align 8
  %20 = extractvalue { i8*, i32 } %18, 1
  store i32 %20, i32* %ehselector.slot, align 4
  call void @_ZN7StudentD2Ev(%class.Student* nonnull dereferenceable(224) %s) #3
  br label %ehcleanup145

if.end:                                           ; preds = %invoke.cont12, %while.body
  br label %while.cond, !llvm.loop !6

while.end:                                        ; preds = %invoke.cont5
  br label %if.end15

if.end15:                                         ; preds = %while.end, %invoke.cont2
  invoke void @_ZNSt14basic_ifstreamIcSt11char_traitsIcEE5closeEv(%"class.std::basic_ifstream"* nonnull dereferenceable(256) %readFile)
          to label %invoke.cont16 unwind label %lpad1

invoke.cont16:                                    ; preds = %if.end15
  store i32 0, i32* %i, align 4
  store i32 0, i32* %num, align 4
  %21 = load i8*, i8** %f2.addr, align 8
  invoke void @_ZNSt14basic_ifstreamIcSt11char_traitsIcEE4openEPKcSt13_Ios_Openmode(%"class.std::basic_ifstream"* nonnull dereferenceable(256) %readFile, i8* %21, i32 8)
          to label %invoke.cont17 unwind label %lpad1

invoke.cont17:                                    ; preds = %invoke.cont16
  %call19 = invoke zeroext i1 @_ZNSt14basic_ifstreamIcSt11char_traitsIcEE7is_openEv(%"class.std::basic_ifstream"* nonnull dereferenceable(256) %readFile)
          to label %invoke.cont18 unwind label %lpad1

invoke.cont18:                                    ; preds = %invoke.cont17
  br i1 %call19, label %if.then20, label %if.end46

if.then20:                                        ; preds = %invoke.cont18
  br label %while.cond21

while.cond21:                                     ; preds = %if.end44, %if.then20
  %22 = bitcast %"class.std::basic_ifstream"* %readFile to %"class.std::basic_istream"*
  %23 = load i32, i32* %i, align 4
  %idxprom22 = sext i32 %23 to i64
  %arrayidx23 = getelementptr inbounds [10 x %"class.std::__cxx11::basic_string"], [10 x %"class.std::__cxx11::basic_string"]* %temp, i64 0, i64 %idxprom22
  %call25 = invoke nonnull align 8 dereferenceable(16) %"class.std::basic_istream"* @_ZStrsIcSt11char_traitsIcESaIcEERSt13basic_istreamIT_T0_ES7_RNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_istream"* nonnull align 8 dereferenceable(16) %22, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %arrayidx23)
          to label %invoke.cont24 unwind label %lpad1

invoke.cont24:                                    ; preds = %while.cond21
  %24 = bitcast %"class.std::basic_istream"* %call25 to i8**
  %vtable26 = load i8*, i8** %24, align 8
  %vbase.offset.ptr27 = getelementptr i8, i8* %vtable26, i64 -24
  %25 = bitcast i8* %vbase.offset.ptr27 to i64*
  %vbase.offset28 = load i64, i64* %25, align 8
  %26 = bitcast %"class.std::basic_istream"* %call25 to i8*
  %add.ptr29 = getelementptr inbounds i8, i8* %26, i64 %vbase.offset28
  %27 = bitcast i8* %add.ptr29 to %"class.std::basic_ios"*
  %call31 = invoke zeroext i1 @_ZNKSt9basic_iosIcSt11char_traitsIcEEcvbEv(%"class.std::basic_ios"* nonnull dereferenceable(264) %27)
          to label %invoke.cont30 unwind label %lpad1

invoke.cont30:                                    ; preds = %invoke.cont24
  br i1 %call31, label %while.body32, label %while.end45

while.body32:                                     ; preds = %invoke.cont30
  %28 = load i32, i32* %i, align 4
  %inc33 = add nsw i32 %28, 1
  store i32 %inc33, i32* %i, align 4
  %29 = load i32, i32* %i, align 4
  %cmp34 = icmp eq i32 %29, 7
  br i1 %cmp34, label %if.then35, label %if.end44

if.then35:                                        ; preds = %while.body32
  %arraydecay36 = getelementptr inbounds [10 x %"class.std::__cxx11::basic_string"], [10 x %"class.std::__cxx11::basic_string"]* %temp, i64 0, i64 0
  invoke void @_ZN8LecturerC1EPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE(%class.Lecturer* nonnull dereferenceable(224) %l, %"class.std::__cxx11::basic_string"* %arraydecay36)
          to label %invoke.cont37 unwind label %lpad1

invoke.cont37:                                    ; preds = %if.then35
  %30 = load %class.Lecturer*, %class.Lecturer** %_l.addr, align 8
  %31 = load i32, i32* %num, align 4
  %idxprom38 = sext i32 %31 to i64
  %arrayidx39 = getelementptr inbounds %class.Lecturer, %class.Lecturer* %30, i64 %idxprom38
  %call42 = invoke nonnull align 8 dereferenceable(224) %class.Lecturer* @_ZN8LectureraSERKS_(%class.Lecturer* nonnull dereferenceable(224) %arrayidx39, %class.Lecturer* nonnull align 8 dereferenceable(224) %l)
          to label %invoke.cont41 unwind label %lpad40

invoke.cont41:                                    ; preds = %invoke.cont37
  %32 = load i32, i32* %num, align 4
  %inc43 = add nsw i32 %32, 1
  store i32 %inc43, i32* %num, align 4
  store i32 0, i32* %i, align 4
  call void @_ZN8LecturerD2Ev(%class.Lecturer* nonnull dereferenceable(224) %l) #3
  br label %if.end44

lpad40:                                           ; preds = %invoke.cont37
  %33 = landingpad { i8*, i32 }
          cleanup
  %34 = extractvalue { i8*, i32 } %33, 0
  store i8* %34, i8** %exn.slot, align 8
  %35 = extractvalue { i8*, i32 } %33, 1
  store i32 %35, i32* %ehselector.slot, align 4
  call void @_ZN8LecturerD2Ev(%class.Lecturer* nonnull dereferenceable(224) %l) #3
  br label %ehcleanup145

if.end44:                                         ; preds = %invoke.cont41, %while.body32
  br label %while.cond21, !llvm.loop !7

while.end45:                                      ; preds = %invoke.cont30
  br label %if.end46

if.end46:                                         ; preds = %while.end45, %invoke.cont18
  invoke void @_ZNSt14basic_ifstreamIcSt11char_traitsIcEE5closeEv(%"class.std::basic_ifstream"* nonnull dereferenceable(256) %readFile)
          to label %invoke.cont47 unwind label %lpad1

invoke.cont47:                                    ; preds = %if.end46
  store i32 0, i32* %i, align 4
  store i32 0, i32* %num, align 4
  %36 = load i8*, i8** %f3.addr, align 8
  invoke void @_ZNSt14basic_ifstreamIcSt11char_traitsIcEE4openEPKcSt13_Ios_Openmode(%"class.std::basic_ifstream"* nonnull dereferenceable(256) %readFile, i8* %36, i32 8)
          to label %invoke.cont48 unwind label %lpad1

invoke.cont48:                                    ; preds = %invoke.cont47
  %call50 = invoke zeroext i1 @_ZNSt14basic_ifstreamIcSt11char_traitsIcEE7is_openEv(%"class.std::basic_ifstream"* nonnull dereferenceable(256) %readFile)
          to label %invoke.cont49 unwind label %lpad1

invoke.cont49:                                    ; preds = %invoke.cont48
  br i1 %call50, label %if.then51, label %if.end143

if.then51:                                        ; preds = %invoke.cont49
  br label %while.cond52

while.cond52:                                     ; preds = %if.end141, %if.then51
  %37 = bitcast %"class.std::basic_ifstream"* %readFile to %"class.std::basic_istream"*
  %38 = load i32, i32* %i, align 4
  %idxprom53 = sext i32 %38 to i64
  %arrayidx54 = getelementptr inbounds [10 x %"class.std::__cxx11::basic_string"], [10 x %"class.std::__cxx11::basic_string"]* %temp, i64 0, i64 %idxprom53
  %call56 = invoke nonnull align 8 dereferenceable(16) %"class.std::basic_istream"* @_ZStrsIcSt11char_traitsIcESaIcEERSt13basic_istreamIT_T0_ES7_RNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_istream"* nonnull align 8 dereferenceable(16) %37, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %arrayidx54)
          to label %invoke.cont55 unwind label %lpad1

invoke.cont55:                                    ; preds = %while.cond52
  %39 = bitcast %"class.std::basic_istream"* %call56 to i8**
  %vtable57 = load i8*, i8** %39, align 8
  %vbase.offset.ptr58 = getelementptr i8, i8* %vtable57, i64 -24
  %40 = bitcast i8* %vbase.offset.ptr58 to i64*
  %vbase.offset59 = load i64, i64* %40, align 8
  %41 = bitcast %"class.std::basic_istream"* %call56 to i8*
  %add.ptr60 = getelementptr inbounds i8, i8* %41, i64 %vbase.offset59
  %42 = bitcast i8* %add.ptr60 to %"class.std::basic_ios"*
  %call62 = invoke zeroext i1 @_ZNKSt9basic_iosIcSt11char_traitsIcEEcvbEv(%"class.std::basic_ios"* nonnull dereferenceable(264) %42)
          to label %invoke.cont61 unwind label %lpad1

invoke.cont61:                                    ; preds = %invoke.cont55
  br i1 %call62, label %while.body63, label %while.end142

while.body63:                                     ; preds = %invoke.cont61
  %43 = load i32, i32* %i, align 4
  %inc64 = add nsw i32 %43, 1
  store i32 %inc64, i32* %i, align 4
  %44 = load i32, i32* %i, align 4
  %cmp65 = icmp eq i32 %44, 5
  br i1 %cmp65, label %if.then66, label %if.end141

if.then66:                                        ; preds = %while.body63
  %45 = load i32, i32* %num, align 4
  %cmp67 = icmp eq i32 %45, 0
  br i1 %cmp67, label %if.then68, label %if.else

if.then68:                                        ; preds = %if.then66
  %arraydecay69 = getelementptr inbounds [10 x %"class.std::__cxx11::basic_string"], [10 x %"class.std::__cxx11::basic_string"]* %temp, i64 0, i64 0
  invoke void @_ZN11AppointmentC1EPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE(%class.Appointment* nonnull dereferenceable(544) %a, %"class.std::__cxx11::basic_string"* %arraydecay69)
          to label %invoke.cont70 unwind label %lpad1

invoke.cont70:                                    ; preds = %if.then68
  %46 = load %class.Appointment*, %class.Appointment** %_a.addr, align 8
  %47 = load i32, i32* %num, align 4
  %idxprom71 = sext i32 %47 to i64
  %arrayidx72 = getelementptr inbounds %class.Appointment, %class.Appointment* %46, i64 %idxprom71
  %call75 = invoke nonnull align 8 dereferenceable(544) %class.Appointment* @_ZN11AppointmentaSERKS_(%class.Appointment* nonnull dereferenceable(544) %arrayidx72, %class.Appointment* nonnull align 8 dereferenceable(544) %a)
          to label %invoke.cont74 unwind label %lpad73

invoke.cont74:                                    ; preds = %invoke.cont70
  %48 = load i32, i32* %num, align 4
  %inc76 = add nsw i32 %48, 1
  store i32 %inc76, i32* %num, align 4
  store i32 0, i32* %i, align 4
  call void @_ZN11AppointmentD2Ev(%class.Appointment* nonnull dereferenceable(544) %a) #3
  br label %if.end140

lpad73:                                           ; preds = %invoke.cont70
  %49 = landingpad { i8*, i32 }
          cleanup
  %50 = extractvalue { i8*, i32 } %49, 0
  store i8* %50, i8** %exn.slot, align 8
  %51 = extractvalue { i8*, i32 } %49, 1
  store i32 %51, i32* %ehselector.slot, align 4
  call void @_ZN11AppointmentD2Ev(%class.Appointment* nonnull dereferenceable(544) %a) #3
  br label %ehcleanup145

if.else:                                          ; preds = %if.then66
  store i32 0, i32* %j, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.else
  %52 = load i32, i32* %j, align 4
  %53 = load i32, i32* %num, align 4
  %cmp77 = icmp slt i32 %52, %53
  br i1 %cmp77, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %54 = load %class.Appointment*, %class.Appointment** %_a.addr, align 8
  %55 = load i32, i32* %j, align 4
  %idxprom78 = sext i32 %55 to i64
  %arrayidx79 = getelementptr inbounds %class.Appointment, %class.Appointment* %54, i64 %idxprom78
  store i1 false, i1* %cleanup.cond, align 1
  store i1 false, i1* %cleanup.cond95, align 1
  invoke void @_ZN11Appointment7getDateB5cxx11Ev(%"class.std::__cxx11::basic_string"* sret(%"class.std::__cxx11::basic_string") align 8 %ref.tmp, %class.Appointment* nonnull dereferenceable(544) %arrayidx79)
          to label %invoke.cont80 unwind label %lpad1

invoke.cont80:                                    ; preds = %for.body
  %arrayidx81 = getelementptr inbounds [10 x %"class.std::__cxx11::basic_string"], [10 x %"class.std::__cxx11::basic_string"]* %temp, i64 0, i64 2
  %call82 = call zeroext i1 @_ZSteqIcEN9__gnu_cxx11__enable_ifIXsr9__is_charIT_EE7__valueEbE6__typeERKNSt7__cxx1112basic_stringIS2_St11char_traitsIS2_ESaIS2_EEESC_(%"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %ref.tmp, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %arrayidx81) #3
  br i1 %call82, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %invoke.cont80
  %56 = load %class.Appointment*, %class.Appointment** %_a.addr, align 8
  %57 = load i32, i32* %j, align 4
  %idxprom84 = sext i32 %57 to i64
  %arrayidx85 = getelementptr inbounds %class.Appointment, %class.Appointment* %56, i64 %idxprom84
  invoke void @_ZN11Appointment8getStartB5cxx11Ev(%"class.std::__cxx11::basic_string"* sret(%"class.std::__cxx11::basic_string") align 8 %ref.tmp83, %class.Appointment* nonnull dereferenceable(544) %arrayidx85)
          to label %invoke.cont87 unwind label %lpad86

invoke.cont87:                                    ; preds = %land.rhs
  store i1 true, i1* %cleanup.cond, align 1
  %arrayidx88 = getelementptr inbounds [10 x %"class.std::__cxx11::basic_string"], [10 x %"class.std::__cxx11::basic_string"]* %temp, i64 0, i64 3
  %call89 = call zeroext i1 @_ZSteqIcEN9__gnu_cxx11__enable_ifIXsr9__is_charIT_EE7__valueEbE6__typeERKNSt7__cxx1112basic_stringIS2_St11char_traitsIS2_ESaIS2_EEESC_(%"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %ref.tmp83, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %arrayidx88) #3
  br i1 %call89, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %invoke.cont87
  %58 = load %class.Appointment*, %class.Appointment** %_a.addr, align 8
  %59 = load i32, i32* %j, align 4
  %idxprom91 = sext i32 %59 to i64
  %arrayidx92 = getelementptr inbounds %class.Appointment, %class.Appointment* %58, i64 %idxprom91
  invoke void @_ZN11Appointment6getEndB5cxx11Ev(%"class.std::__cxx11::basic_string"* sret(%"class.std::__cxx11::basic_string") align 8 %ref.tmp90, %class.Appointment* nonnull dereferenceable(544) %arrayidx92)
          to label %invoke.cont94 unwind label %lpad93

invoke.cont94:                                    ; preds = %lor.rhs
  store i1 true, i1* %cleanup.cond95, align 1
  %arrayidx96 = getelementptr inbounds [10 x %"class.std::__cxx11::basic_string"], [10 x %"class.std::__cxx11::basic_string"]* %temp, i64 0, i64 4
  %call97 = call zeroext i1 @_ZSteqIcEN9__gnu_cxx11__enable_ifIXsr9__is_charIT_EE7__valueEbE6__typeERKNSt7__cxx1112basic_stringIS2_St11char_traitsIS2_ESaIS2_EEESC_(%"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %ref.tmp90, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %arrayidx96) #3
  br label %lor.end

lor.end:                                          ; preds = %invoke.cont94, %invoke.cont87
  %60 = phi i1 [ true, %invoke.cont87 ], [ %call97, %invoke.cont94 ]
  br label %land.end

land.end:                                         ; preds = %lor.end, %invoke.cont80
  %61 = phi i1 [ false, %invoke.cont80 ], [ %60, %lor.end ]
  %cleanup.is_active = load i1, i1* %cleanup.cond95, align 1
  br i1 %cleanup.is_active, label %cleanup.action, label %cleanup.done

cleanup.action:                                   ; preds = %land.end
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %ref.tmp90) #3
  br label %cleanup.done

cleanup.done:                                     ; preds = %cleanup.action, %land.end
  %cleanup.is_active98 = load i1, i1* %cleanup.cond, align 1
  br i1 %cleanup.is_active98, label %cleanup.action99, label %cleanup.done100

cleanup.action99:                                 ; preds = %cleanup.done
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %ref.tmp83) #3
  br label %cleanup.done100

cleanup.done100:                                  ; preds = %cleanup.action99, %cleanup.done
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %ref.tmp) #3
  br i1 %61, label %if.then104, label %if.else125

if.then104:                                       ; preds = %cleanup.done100
  %call106 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) @_ZSt4cout, i8* getelementptr inbounds ([8 x i8], [8 x i8]* @.str, i64 0, i64 0))
          to label %invoke.cont105 unwind label %lpad1

invoke.cont105:                                   ; preds = %if.then104
  %arrayidx107 = getelementptr inbounds [10 x %"class.std::__cxx11::basic_string"], [10 x %"class.std::__cxx11::basic_string"]* %temp, i64 0, i64 0
  %call109 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsIcSt11char_traitsIcESaIcEERSt13basic_ostreamIT_T0_ES7_RKNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) %call106, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %arrayidx107)
          to label %invoke.cont108 unwind label %lpad1

invoke.cont108:                                   ; preds = %invoke.cont105
  %call111 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) %call109, i8* getelementptr inbounds ([10 x i8], [10 x i8]* @.str.1, i64 0, i64 0))
          to label %invoke.cont110 unwind label %lpad1

invoke.cont110:                                   ; preds = %invoke.cont108
  %arrayidx112 = getelementptr inbounds [10 x %"class.std::__cxx11::basic_string"], [10 x %"class.std::__cxx11::basic_string"]* %temp, i64 0, i64 1
  %call114 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsIcSt11char_traitsIcESaIcEERSt13basic_ostreamIT_T0_ES7_RKNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) %call111, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %arrayidx112)
          to label %invoke.cont113 unwind label %lpad1

invoke.cont113:                                   ; preds = %invoke.cont110
  %call116 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) %call114, i8* getelementptr inbounds ([34 x i8], [34 x i8]* @.str.2, i64 0, i64 0))
          to label %invoke.cont115 unwind label %lpad1

invoke.cont115:                                   ; preds = %invoke.cont113
  %call118 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* nonnull dereferenceable(8) %call116, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
          to label %invoke.cont117 unwind label %lpad1

invoke.cont117:                                   ; preds = %invoke.cont115
  %call120 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) @_ZSt4cout, i8* getelementptr inbounds ([27 x i8], [27 x i8]* @.str.3, i64 0, i64 0))
          to label %invoke.cont119 unwind label %lpad1

invoke.cont119:                                   ; preds = %invoke.cont117
  %call122 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* nonnull dereferenceable(8) %call120, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
          to label %invoke.cont121 unwind label %lpad1

invoke.cont121:                                   ; preds = %invoke.cont119
  %call124 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* nonnull dereferenceable(8) %call122, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
          to label %invoke.cont123 unwind label %lpad1

invoke.cont123:                                   ; preds = %invoke.cont121
  store i8 0, i8* %control, align 1
  br label %for.end

lpad86:                                           ; preds = %land.rhs
  %62 = landingpad { i8*, i32 }
          cleanup
  %63 = extractvalue { i8*, i32 } %62, 0
  store i8* %63, i8** %exn.slot, align 8
  %64 = extractvalue { i8*, i32 } %62, 1
  store i32 %64, i32* %ehselector.slot, align 4
  br label %ehcleanup

lpad93:                                           ; preds = %lor.rhs
  %65 = landingpad { i8*, i32 }
          cleanup
  %66 = extractvalue { i8*, i32 } %65, 0
  store i8* %66, i8** %exn.slot, align 8
  %67 = extractvalue { i8*, i32 } %65, 1
  store i32 %67, i32* %ehselector.slot, align 4
  %cleanup.is_active101 = load i1, i1* %cleanup.cond, align 1
  br i1 %cleanup.is_active101, label %cleanup.action102, label %cleanup.done103

cleanup.action102:                                ; preds = %lpad93
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %ref.tmp83) #3
  br label %cleanup.done103

cleanup.done103:                                  ; preds = %cleanup.action102, %lpad93
  br label %ehcleanup

ehcleanup:                                        ; preds = %cleanup.done103, %lpad86
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %ref.tmp) #3
  br label %ehcleanup145

if.else125:                                       ; preds = %cleanup.done100
  store i8 1, i8* %control, align 1
  br label %if.end126

if.end126:                                        ; preds = %if.else125
  br label %for.inc

for.inc:                                          ; preds = %if.end126
  %68 = load i32, i32* %j, align 4
  %inc127 = add nsw i32 %68, 1
  store i32 %inc127, i32* %j, align 4
  br label %for.cond, !llvm.loop !8

for.end:                                          ; preds = %invoke.cont123, %for.cond
  %69 = load i8, i8* %control, align 1
  %tobool = trunc i8 %69 to i1
  br i1 %tobool, label %if.then128, label %if.end139

if.then128:                                       ; preds = %for.end
  %arraydecay130 = getelementptr inbounds [10 x %"class.std::__cxx11::basic_string"], [10 x %"class.std::__cxx11::basic_string"]* %temp, i64 0, i64 0
  invoke void @_ZN11AppointmentC1EPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE(%class.Appointment* nonnull dereferenceable(544) %a129, %"class.std::__cxx11::basic_string"* %arraydecay130)
          to label %invoke.cont131 unwind label %lpad1

invoke.cont131:                                   ; preds = %if.then128
  %70 = load %class.Appointment*, %class.Appointment** %_a.addr, align 8
  %71 = load i32, i32* %num, align 4
  %idxprom132 = sext i32 %71 to i64
  %arrayidx133 = getelementptr inbounds %class.Appointment, %class.Appointment* %70, i64 %idxprom132
  %call136 = invoke nonnull align 8 dereferenceable(544) %class.Appointment* @_ZN11AppointmentaSERKS_(%class.Appointment* nonnull dereferenceable(544) %arrayidx133, %class.Appointment* nonnull align 8 dereferenceable(544) %a129)
          to label %invoke.cont135 unwind label %lpad134

invoke.cont135:                                   ; preds = %invoke.cont131
  %72 = load i32, i32* %num, align 4
  %inc137 = add nsw i32 %72, 1
  store i32 %inc137, i32* %num, align 4
  call void @_ZN11AppointmentD2Ev(%class.Appointment* nonnull dereferenceable(544) %a129) #3
  br label %if.end139

lpad134:                                          ; preds = %invoke.cont131
  %73 = landingpad { i8*, i32 }
          cleanup
  %74 = extractvalue { i8*, i32 } %73, 0
  store i8* %74, i8** %exn.slot, align 8
  %75 = extractvalue { i8*, i32 } %73, 1
  store i32 %75, i32* %ehselector.slot, align 4
  call void @_ZN11AppointmentD2Ev(%class.Appointment* nonnull dereferenceable(544) %a129) #3
  br label %ehcleanup145

if.end139:                                        ; preds = %invoke.cont135, %for.end
  store i32 0, i32* %i, align 4
  br label %if.end140

if.end140:                                        ; preds = %if.end139, %invoke.cont74
  br label %if.end141

if.end141:                                        ; preds = %if.end140, %while.body63
  br label %while.cond52, !llvm.loop !9

while.end142:                                     ; preds = %invoke.cont61
  br label %if.end143

if.end143:                                        ; preds = %while.end142, %invoke.cont49
  invoke void @_ZNSt14basic_ifstreamIcSt11char_traitsIcEE5closeEv(%"class.std::basic_ifstream"* nonnull dereferenceable(256) %readFile)
          to label %invoke.cont144 unwind label %lpad1

invoke.cont144:                                   ; preds = %if.end143
  call void @_ZNSt14basic_ifstreamIcSt11char_traitsIcEED1Ev(%"class.std::basic_ifstream"* nonnull dereferenceable(256) %readFile) #3
  %array.begin146 = getelementptr inbounds [10 x %"class.std::__cxx11::basic_string"], [10 x %"class.std::__cxx11::basic_string"]* %temp, i32 0, i32 0
  %76 = getelementptr inbounds %"class.std::__cxx11::basic_string", %"class.std::__cxx11::basic_string"* %array.begin146, i64 10
  br label %arraydestroy.body

arraydestroy.body:                                ; preds = %arraydestroy.body, %invoke.cont144
  %arraydestroy.elementPast = phi %"class.std::__cxx11::basic_string"* [ %76, %invoke.cont144 ], [ %arraydestroy.element, %arraydestroy.body ]
  %arraydestroy.element = getelementptr inbounds %"class.std::__cxx11::basic_string", %"class.std::__cxx11::basic_string"* %arraydestroy.elementPast, i64 -1
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %arraydestroy.element) #3
  %arraydestroy.done = icmp eq %"class.std::__cxx11::basic_string"* %arraydestroy.element, %array.begin146
  br i1 %arraydestroy.done, label %arraydestroy.done147, label %arraydestroy.body

arraydestroy.done147:                             ; preds = %arraydestroy.body
  ret void

ehcleanup145:                                     ; preds = %lpad134, %ehcleanup, %lpad73, %lpad40, %lpad11, %lpad1
  call void @_ZNSt14basic_ifstreamIcSt11char_traitsIcEED1Ev(%"class.std::basic_ifstream"* nonnull dereferenceable(256) %readFile) #3
  br label %ehcleanup148

ehcleanup148:                                     ; preds = %ehcleanup145, %lpad
  %array.begin149 = getelementptr inbounds [10 x %"class.std::__cxx11::basic_string"], [10 x %"class.std::__cxx11::basic_string"]* %temp, i32 0, i32 0
  %77 = getelementptr inbounds %"class.std::__cxx11::basic_string", %"class.std::__cxx11::basic_string"* %array.begin149, i64 10
  br label %arraydestroy.body150

arraydestroy.body150:                             ; preds = %arraydestroy.body150, %ehcleanup148
  %arraydestroy.elementPast151 = phi %"class.std::__cxx11::basic_string"* [ %77, %ehcleanup148 ], [ %arraydestroy.element152, %arraydestroy.body150 ]
  %arraydestroy.element152 = getelementptr inbounds %"class.std::__cxx11::basic_string", %"class.std::__cxx11::basic_string"* %arraydestroy.elementPast151, i64 -1
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %arraydestroy.element152) #3
  %arraydestroy.done153 = icmp eq %"class.std::__cxx11::basic_string"* %arraydestroy.element152, %array.begin149
  br i1 %arraydestroy.done153, label %arraydestroy.done154, label %arraydestroy.body150

arraydestroy.done154:                             ; preds = %arraydestroy.body150
  br label %eh.resume

eh.resume:                                        ; preds = %arraydestroy.done154
  %exn = load i8*, i8** %exn.slot, align 8
  %sel = load i32, i32* %ehselector.slot, align 4
  %lpad.val = insertvalue { i8*, i32 } undef, i8* %exn, 0
  %lpad.val155 = insertvalue { i8*, i32 } %lpad.val, i32 %sel, 1
  resume { i8*, i32 } %lpad.val155
}

declare dso_local void @_ZNSt14basic_ifstreamIcSt11char_traitsIcEEC1EPKcSt13_Ios_Openmode(%"class.std::basic_ifstream"* nonnull dereferenceable(256), i8*, i32) unnamed_addr #1

declare dso_local zeroext i1 @_ZNSt14basic_ifstreamIcSt11char_traitsIcEE7is_openEv(%"class.std::basic_ifstream"* nonnull dereferenceable(256)) #1

declare dso_local nonnull align 8 dereferenceable(16) %"class.std::basic_istream"* @_ZStrsIcSt11char_traitsIcESaIcEERSt13basic_istreamIT_T0_ES7_RNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_istream"* nonnull align 8 dereferenceable(16), %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32)) #1

; Function Attrs: noinline optnone uwtable
define linkonce_odr dso_local nonnull align 8 dereferenceable(224) %class.Student* @_ZN7StudentaSERKS_(%class.Student* nonnull dereferenceable(224) %this, %class.Student* nonnull align 8 dereferenceable(224) %0) #4 comdat align 2 {
entry:
  %this.addr = alloca %class.Student*, align 8
  %.addr = alloca %class.Student*, align 8
  store %class.Student* %this, %class.Student** %this.addr, align 8
  store %class.Student* %0, %class.Student** %.addr, align 8
  %this1 = load %class.Student*, %class.Student** %this.addr, align 8
  %1 = bitcast %class.Student* %this1 to %class.People*
  %2 = load %class.Student*, %class.Student** %.addr, align 8
  %3 = bitcast %class.Student* %2 to %class.People*
  %call = call nonnull align 8 dereferenceable(192) %class.People* @_ZN6PeopleaSERKS_(%class.People* nonnull dereferenceable(192) %1, %class.People* nonnull align 8 dereferenceable(192) %3)
  %year = getelementptr inbounds %class.Student, %class.Student* %this1, i32 0, i32 1
  %4 = load %class.Student*, %class.Student** %.addr, align 8
  %year2 = getelementptr inbounds %class.Student, %class.Student* %4, i32 0, i32 1
  %call3 = call nonnull align 8 dereferenceable(32) %"class.std::__cxx11::basic_string"* @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEaSERKS4_(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %year, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %year2)
  ret %class.Student* %this1
}

; Function Attrs: noinline optnone uwtable
define linkonce_odr dso_local nonnull align 8 dereferenceable(224) %class.Lecturer* @_ZN8LectureraSERKS_(%class.Lecturer* nonnull dereferenceable(224) %this, %class.Lecturer* nonnull align 8 dereferenceable(224) %0) #4 comdat align 2 {
entry:
  %this.addr = alloca %class.Lecturer*, align 8
  %.addr = alloca %class.Lecturer*, align 8
  store %class.Lecturer* %this, %class.Lecturer** %this.addr, align 8
  store %class.Lecturer* %0, %class.Lecturer** %.addr, align 8
  %this1 = load %class.Lecturer*, %class.Lecturer** %this.addr, align 8
  %1 = bitcast %class.Lecturer* %this1 to %class.People*
  %2 = load %class.Lecturer*, %class.Lecturer** %.addr, align 8
  %3 = bitcast %class.Lecturer* %2 to %class.People*
  %call = call nonnull align 8 dereferenceable(192) %class.People* @_ZN6PeopleaSERKS_(%class.People* nonnull dereferenceable(192) %1, %class.People* nonnull align 8 dereferenceable(192) %3)
  %chair = getelementptr inbounds %class.Lecturer, %class.Lecturer* %this1, i32 0, i32 1
  %4 = load %class.Lecturer*, %class.Lecturer** %.addr, align 8
  %chair2 = getelementptr inbounds %class.Lecturer, %class.Lecturer* %4, i32 0, i32 1
  %call3 = call nonnull align 8 dereferenceable(32) %"class.std::__cxx11::basic_string"* @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEaSERKS4_(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %chair, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %chair2)
  ret %class.Lecturer* %this1
}

; Function Attrs: noinline optnone uwtable
define linkonce_odr dso_local nonnull align 8 dereferenceable(544) %class.Appointment* @_ZN11AppointmentaSERKS_(%class.Appointment* nonnull dereferenceable(544) %this, %class.Appointment* nonnull align 8 dereferenceable(544) %0) #4 comdat align 2 {
entry:
  %this.addr = alloca %class.Appointment*, align 8
  %.addr = alloca %class.Appointment*, align 8
  store %class.Appointment* %this, %class.Appointment** %this.addr, align 8
  store %class.Appointment* %0, %class.Appointment** %.addr, align 8
  %this1 = load %class.Appointment*, %class.Appointment** %this.addr, align 8
  %date = getelementptr inbounds %class.Appointment, %class.Appointment* %this1, i32 0, i32 0
  %1 = load %class.Appointment*, %class.Appointment** %.addr, align 8
  %date2 = getelementptr inbounds %class.Appointment, %class.Appointment* %1, i32 0, i32 0
  %call = call nonnull align 8 dereferenceable(32) %"class.std::__cxx11::basic_string"* @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEaSERKS4_(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %date, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %date2)
  %start = getelementptr inbounds %class.Appointment, %class.Appointment* %this1, i32 0, i32 1
  %2 = load %class.Appointment*, %class.Appointment** %.addr, align 8
  %start3 = getelementptr inbounds %class.Appointment, %class.Appointment* %2, i32 0, i32 1
  %call4 = call nonnull align 8 dereferenceable(32) %"class.std::__cxx11::basic_string"* @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEaSERKS4_(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %start, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %start3)
  %end = getelementptr inbounds %class.Appointment, %class.Appointment* %this1, i32 0, i32 2
  %3 = load %class.Appointment*, %class.Appointment** %.addr, align 8
  %end5 = getelementptr inbounds %class.Appointment, %class.Appointment* %3, i32 0, i32 2
  %call6 = call nonnull align 8 dereferenceable(32) %"class.std::__cxx11::basic_string"* @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEaSERKS4_(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %end, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %end5)
  %L = getelementptr inbounds %class.Appointment, %class.Appointment* %this1, i32 0, i32 3
  %4 = load %class.Appointment*, %class.Appointment** %.addr, align 8
  %L7 = getelementptr inbounds %class.Appointment, %class.Appointment* %4, i32 0, i32 3
  %call8 = call nonnull align 8 dereferenceable(224) %class.Lecturer* @_ZN8LectureraSERKS_(%class.Lecturer* nonnull dereferenceable(224) %L, %class.Lecturer* nonnull align 8 dereferenceable(224) %L7)
  %S = getelementptr inbounds %class.Appointment, %class.Appointment* %this1, i32 0, i32 4
  %5 = load %class.Appointment*, %class.Appointment** %.addr, align 8
  %S9 = getelementptr inbounds %class.Appointment, %class.Appointment* %5, i32 0, i32 4
  %call10 = call nonnull align 8 dereferenceable(224) %class.Student* @_ZN7StudentaSERKS_(%class.Student* nonnull dereferenceable(224) %S, %class.Student* nonnull align 8 dereferenceable(224) %S9)
  ret %class.Appointment* %this1
}

; Function Attrs: noinline nounwind optnone uwtable
define linkonce_odr dso_local void @_ZN11AppointmentD2Ev(%class.Appointment* nonnull dereferenceable(544) %this) unnamed_addr #5 comdat align 2 {
entry:
  %this.addr = alloca %class.Appointment*, align 8
  store %class.Appointment* %this, %class.Appointment** %this.addr, align 8
  %this1 = load %class.Appointment*, %class.Appointment** %this.addr, align 8
  %S = getelementptr inbounds %class.Appointment, %class.Appointment* %this1, i32 0, i32 4
  call void @_ZN7StudentD2Ev(%class.Student* nonnull dereferenceable(224) %S) #3
  %L = getelementptr inbounds %class.Appointment, %class.Appointment* %this1, i32 0, i32 3
  call void @_ZN8LecturerD2Ev(%class.Lecturer* nonnull dereferenceable(224) %L) #3
  %end = getelementptr inbounds %class.Appointment, %class.Appointment* %this1, i32 0, i32 2
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %end) #3
  %start = getelementptr inbounds %class.Appointment, %class.Appointment* %this1, i32 0, i32 1
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %start) #3
  %date = getelementptr inbounds %class.Appointment, %class.Appointment* %this1, i32 0, i32 0
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %date) #3
  ret void
}

; Function Attrs: noinline nounwind optnone uwtable mustprogress
define linkonce_odr dso_local zeroext i1 @_ZSteqIcEN9__gnu_cxx11__enable_ifIXsr9__is_charIT_EE7__valueEbE6__typeERKNSt7__cxx1112basic_stringIS2_St11char_traitsIS2_ESaIS2_EEESC_(%"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %__lhs, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %__rhs) #7 comdat personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
entry:
  %__lhs.addr = alloca %"class.std::__cxx11::basic_string"*, align 8
  %__rhs.addr = alloca %"class.std::__cxx11::basic_string"*, align 8
  store %"class.std::__cxx11::basic_string"* %__lhs, %"class.std::__cxx11::basic_string"** %__lhs.addr, align 8
  store %"class.std::__cxx11::basic_string"* %__rhs, %"class.std::__cxx11::basic_string"** %__rhs.addr, align 8
  %0 = load %"class.std::__cxx11::basic_string"*, %"class.std::__cxx11::basic_string"** %__lhs.addr, align 8
  %call = call i64 @_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE4sizeEv(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %0) #3
  %1 = load %"class.std::__cxx11::basic_string"*, %"class.std::__cxx11::basic_string"** %__rhs.addr, align 8
  %call1 = call i64 @_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE4sizeEv(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %1) #3
  %cmp = icmp eq i64 %call, %call1
  br i1 %cmp, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %entry
  %2 = load %"class.std::__cxx11::basic_string"*, %"class.std::__cxx11::basic_string"** %__lhs.addr, align 8
  %call2 = call i8* @_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE4dataEv(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %2) #3
  %3 = load %"class.std::__cxx11::basic_string"*, %"class.std::__cxx11::basic_string"** %__rhs.addr, align 8
  %call3 = call i8* @_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE4dataEv(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %3) #3
  %4 = load %"class.std::__cxx11::basic_string"*, %"class.std::__cxx11::basic_string"** %__lhs.addr, align 8
  %call4 = call i64 @_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE4sizeEv(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %4) #3
  %call5 = invoke i32 @_ZNSt11char_traitsIcE7compareEPKcS2_m(i8* %call2, i8* %call3, i64 %call4)
          to label %invoke.cont unwind label %terminate.lpad

invoke.cont:                                      ; preds = %land.rhs
  %tobool = icmp ne i32 %call5, 0
  %lnot = xor i1 %tobool, true
  br label %land.end

land.end:                                         ; preds = %invoke.cont, %entry
  %5 = phi i1 [ false, %entry ], [ %lnot, %invoke.cont ]
  ret i1 %5

terminate.lpad:                                   ; preds = %land.rhs
  %6 = landingpad { i8*, i32 }
          catch i8* null
  %7 = extractvalue { i8*, i32 } %6, 0
  call void @__clang_call_terminate(i8* %7) #10
  unreachable
}

; Function Attrs: noinline optnone uwtable mustprogress
define linkonce_odr dso_local void @_ZN11Appointment7getDateB5cxx11Ev(%"class.std::__cxx11::basic_string"* noalias sret(%"class.std::__cxx11::basic_string") align 8 %agg.result, %class.Appointment* nonnull dereferenceable(544) %this) #6 comdat align 2 {
entry:
  %result.ptr = alloca i8*, align 8
  %this.addr = alloca %class.Appointment*, align 8
  %0 = bitcast %"class.std::__cxx11::basic_string"* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 8
  store %class.Appointment* %this, %class.Appointment** %this.addr, align 8
  %this1 = load %class.Appointment*, %class.Appointment** %this.addr, align 8
  %date = getelementptr inbounds %class.Appointment, %class.Appointment* %this1, i32 0, i32 0
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1ERKS4_(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.result, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %date)
  ret void
}

; Function Attrs: noinline optnone uwtable mustprogress
define linkonce_odr dso_local void @_ZN11Appointment8getStartB5cxx11Ev(%"class.std::__cxx11::basic_string"* noalias sret(%"class.std::__cxx11::basic_string") align 8 %agg.result, %class.Appointment* nonnull dereferenceable(544) %this) #6 comdat align 2 {
entry:
  %result.ptr = alloca i8*, align 8
  %this.addr = alloca %class.Appointment*, align 8
  %0 = bitcast %"class.std::__cxx11::basic_string"* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 8
  store %class.Appointment* %this, %class.Appointment** %this.addr, align 8
  %this1 = load %class.Appointment*, %class.Appointment** %this.addr, align 8
  %start = getelementptr inbounds %class.Appointment, %class.Appointment* %this1, i32 0, i32 1
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1ERKS4_(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.result, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %start)
  ret void
}

; Function Attrs: noinline optnone uwtable mustprogress
define linkonce_odr dso_local void @_ZN11Appointment6getEndB5cxx11Ev(%"class.std::__cxx11::basic_string"* noalias sret(%"class.std::__cxx11::basic_string") align 8 %agg.result, %class.Appointment* nonnull dereferenceable(544) %this) #6 comdat align 2 {
entry:
  %result.ptr = alloca i8*, align 8
  %this.addr = alloca %class.Appointment*, align 8
  %0 = bitcast %"class.std::__cxx11::basic_string"* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 8
  store %class.Appointment* %this, %class.Appointment** %this.addr, align 8
  %this1 = load %class.Appointment*, %class.Appointment** %this.addr, align 8
  %end = getelementptr inbounds %class.Appointment, %class.Appointment* %this1, i32 0, i32 2
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1ERKS4_(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.result, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %end)
  ret void
}

declare dso_local nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8), i8*) #1

declare dso_local nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsIcSt11char_traitsIcESaIcEERSt13basic_ostreamIT_T0_ES7_RKNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8), %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32)) #1

declare dso_local nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* nonnull dereferenceable(8), %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)*) #1

declare dso_local nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8)) #1

; Function Attrs: noinline norecurse optnone uwtable mustprogress
define dso_local i32 @main(i32 %argc, i8** %argv) #8 personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
entry:
  %retval = alloca i32, align 4
  %argc.addr = alloca i32, align 4
  %argv.addr = alloca i8**, align 8
  %file1 = alloca i8*, align 8
  %file2 = alloca i8*, align 8
  %file3 = alloca i8*, align 8
  %x = alloca i32, align 4
  %y = alloca i32, align 4
  %z = alloca i32, align 4
  %_S = alloca [100 x %class.Student], align 16
  %exn.slot = alloca i8*, align 8
  %ehselector.slot = alloca i32, align 4
  %_L = alloca [100 x %class.Lecturer], align 16
  %_A = alloca [100 x %class.Appointment], align 16
  %menuLoop = alloca i32, align 4
  %menu = alloca i32, align 4
  %i = alloca i32, align 4
  %temp_ = alloca %"class.std::__cxx11::basic_string", align 8
  %newS = alloca %class.Student, align 8
  %agg.tmp = alloca %"class.std::__cxx11::basic_string", align 8
  %agg.tmp114 = alloca %"class.std::__cxx11::basic_string", align 8
  %agg.tmp124 = alloca %"class.std::__cxx11::basic_string", align 8
  %agg.tmp134 = alloca %"class.std::__cxx11::basic_string", align 8
  %agg.tmp144 = alloca %"class.std::__cxx11::basic_string", align 8
  %agg.tmp154 = alloca %"class.std::__cxx11::basic_string", align 8
  %agg.tmp164 = alloca %"class.std::__cxx11::basic_string", align 8
  %temp_184 = alloca %"class.std::__cxx11::basic_string", align 8
  %deleted = alloca i32, align 4
  %ref.tmp = alloca %"class.std::__cxx11::basic_string", align 8
  %temp_218 = alloca %"class.std::__cxx11::basic_string", align 8
  %updated = alloca i32, align 4
  %ref.tmp229 = alloca %"class.std::__cxx11::basic_string", align 8
  %agg.tmp246 = alloca %"class.std::__cxx11::basic_string", align 8
  %agg.tmp259 = alloca %"class.std::__cxx11::basic_string", align 8
  %agg.tmp272 = alloca %"class.std::__cxx11::basic_string", align 8
  %agg.tmp285 = alloca %"class.std::__cxx11::basic_string", align 8
  %agg.tmp298 = alloca %"class.std::__cxx11::basic_string", align 8
  %agg.tmp311 = alloca %"class.std::__cxx11::basic_string", align 8
  %temp_349 = alloca %"class.std::__cxx11::basic_string", align 8
  %newL = alloca %class.Lecturer, align 8
  %agg.tmp359 = alloca %"class.std::__cxx11::basic_string", align 8
  %agg.tmp370 = alloca %"class.std::__cxx11::basic_string", align 8
  %agg.tmp381 = alloca %"class.std::__cxx11::basic_string", align 8
  %agg.tmp392 = alloca %"class.std::__cxx11::basic_string", align 8
  %agg.tmp403 = alloca %"class.std::__cxx11::basic_string", align 8
  %agg.tmp414 = alloca %"class.std::__cxx11::basic_string", align 8
  %agg.tmp425 = alloca %"class.std::__cxx11::basic_string", align 8
  %temp_454 = alloca %"class.std::__cxx11::basic_string", align 8
  %deleted455 = alloca i32, align 4
  %ref.tmp466 = alloca %"class.std::__cxx11::basic_string", align 8
  %temp_492 = alloca %"class.std::__cxx11::basic_string", align 8
  %updated493 = alloca i32, align 4
  %ref.tmp504 = alloca %"class.std::__cxx11::basic_string", align 8
  %agg.tmp521 = alloca %"class.std::__cxx11::basic_string", align 8
  %agg.tmp534 = alloca %"class.std::__cxx11::basic_string", align 8
  %agg.tmp547 = alloca %"class.std::__cxx11::basic_string", align 8
  %agg.tmp560 = alloca %"class.std::__cxx11::basic_string", align 8
  %agg.tmp573 = alloca %"class.std::__cxx11::basic_string", align 8
  %agg.tmp586 = alloca %"class.std::__cxx11::basic_string", align 8
  %temp_624 = alloca %"class.std::__cxx11::basic_string", align 8
  %newA = alloca %class.Appointment, align 8
  %agg.tmp634 = alloca %"class.std::__cxx11::basic_string", align 8
  %agg.tmp645 = alloca %"class.std::__cxx11::basic_string", align 8
  %agg.tmp656 = alloca %"class.std::__cxx11::basic_string", align 8
  %agg.tmp667 = alloca %"class.std::__cxx11::basic_string", align 8
  %agg.tmp678 = alloca %"class.std::__cxx11::basic_string", align 8
  %temp_707 = alloca %"class.std::__cxx11::basic_string", align 8
  %temp_2 = alloca %"class.std::__cxx11::basic_string", align 8
  %deleted708 = alloca i32, align 4
  %ref.tmp725 = alloca %"class.std::__cxx11::basic_string", align 8
  %ref.tmp731 = alloca %"class.std::__cxx11::basic_string", align 8
  %cleanup.cond = alloca i1, align 1
  %temp_761 = alloca %"class.std::__cxx11::basic_string", align 8
  %temp_2762 = alloca %"class.std::__cxx11::basic_string", align 8
  %updated763 = alloca i32, align 4
  %ref.tmp780 = alloca %"class.std::__cxx11::basic_string", align 8
  %ref.tmp787 = alloca %"class.std::__cxx11::basic_string", align 8
  %cleanup.cond793 = alloca i1, align 1
  %agg.tmp812 = alloca %"class.std::__cxx11::basic_string", align 8
  %agg.tmp825 = alloca %"class.std::__cxx11::basic_string", align 8
  %agg.tmp838 = alloca %"class.std::__cxx11::basic_string", align 8
  %writeToFile = alloca %"class.std::basic_ofstream", align 8
  %ref.tmp863 = alloca %"class.std::__cxx11::basic_string", align 8
  %ref.tmp872 = alloca %"class.std::__cxx11::basic_string", align 8
  %ref.tmp881 = alloca %"class.std::__cxx11::basic_string", align 8
  %ref.tmp890 = alloca %"class.std::__cxx11::basic_string", align 8
  %ref.tmp899 = alloca %"class.std::__cxx11::basic_string", align 8
  %ref.tmp908 = alloca %"class.std::__cxx11::basic_string", align 8
  %ref.tmp917 = alloca %"class.std::__cxx11::basic_string", align 8
  %ref.tmp941 = alloca %"class.std::__cxx11::basic_string", align 8
  %ref.tmp950 = alloca %"class.std::__cxx11::basic_string", align 8
  %ref.tmp959 = alloca %"class.std::__cxx11::basic_string", align 8
  %ref.tmp968 = alloca %"class.std::__cxx11::basic_string", align 8
  %ref.tmp977 = alloca %"class.std::__cxx11::basic_string", align 8
  %ref.tmp986 = alloca %"class.std::__cxx11::basic_string", align 8
  %ref.tmp995 = alloca %"class.std::__cxx11::basic_string", align 8
  %ref.tmp1019 = alloca %"class.std::__cxx11::basic_string", align 8
  %ref.tmp1029 = alloca %"class.std::__cxx11::basic_string", align 8
  %ref.tmp1039 = alloca %"class.std::__cxx11::basic_string", align 8
  %ref.tmp1048 = alloca %"class.std::__cxx11::basic_string", align 8
  %ref.tmp1057 = alloca %"class.std::__cxx11::basic_string", align 8
  %cleanup.dest.slot = alloca i32, align 4
  store i32 0, i32* %retval, align 4
  store i32 %argc, i32* %argc.addr, align 4
  store i8** %argv, i8*** %argv.addr, align 8
  %0 = load i32, i32* %argc.addr, align 4
  %cmp = icmp slt i32 %0, 4
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call = call nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) @_ZSt4cout, i8* getelementptr inbounds ([38 x i8], [38 x i8]* @.str.4, i64 0, i64 0))
  %call1 = call nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* nonnull dereferenceable(8) %call, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 0, i32* getelementptr inbounds ([3 x i32], [3 x i32]* @count, i64 0, i64 0), align 4
  store i32 0, i32* getelementptr inbounds ([3 x i32], [3 x i32]* @count, i64 0, i64 1), align 4
  store i32 0, i32* getelementptr inbounds ([3 x i32], [3 x i32]* @count, i64 0, i64 2), align 4
  %1 = load i8**, i8*** %argv.addr, align 8
  %arrayidx = getelementptr inbounds i8*, i8** %1, i64 1
  %2 = load i8*, i8** %arrayidx, align 8
  store i8* %2, i8** %file1, align 8
  %3 = load i8**, i8*** %argv.addr, align 8
  %arrayidx2 = getelementptr inbounds i8*, i8** %3, i64 2
  %4 = load i8*, i8** %arrayidx2, align 8
  store i8* %4, i8** %file2, align 8
  %5 = load i8**, i8*** %argv.addr, align 8
  %arrayidx3 = getelementptr inbounds i8*, i8** %5, i64 3
  %6 = load i8*, i8** %arrayidx3, align 8
  store i8* %6, i8** %file3, align 8
  %7 = load i8*, i8** %file1, align 8
  call void @_Z9countFilePci(i8* %7, i32 0)
  %8 = load i8*, i8** %file2, align 8
  call void @_Z9countFilePci(i8* %8, i32 1)
  %9 = load i8*, i8** %file3, align 8
  call void @_Z9countFilePci(i8* %9, i32 2)
  %10 = load i32, i32* getelementptr inbounds ([3 x i32], [3 x i32]* @count, i64 0, i64 0), align 4
  store i32 %10, i32* %x, align 4
  %11 = load i32, i32* getelementptr inbounds ([3 x i32], [3 x i32]* @count, i64 0, i64 1), align 4
  store i32 %11, i32* %y, align 4
  %12 = load i32, i32* getelementptr inbounds ([3 x i32], [3 x i32]* @count, i64 0, i64 2), align 4
  store i32 %12, i32* %z, align 4
  %array.begin = getelementptr inbounds [100 x %class.Student], [100 x %class.Student]* %_S, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.Student, %class.Student* %array.begin, i64 100
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %invoke.cont, %if.end
  %arrayctor.cur = phi %class.Student* [ %array.begin, %if.end ], [ %arrayctor.next, %invoke.cont ]
  invoke void @_ZN7StudentC2Ev(%class.Student* nonnull dereferenceable(224) %arrayctor.cur)
          to label %invoke.cont unwind label %lpad

invoke.cont:                                      ; preds = %arrayctor.loop
  %arrayctor.next = getelementptr inbounds %class.Student, %class.Student* %arrayctor.cur, i64 1
  %arrayctor.done = icmp eq %class.Student* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %invoke.cont
  %array.begin5 = getelementptr inbounds [100 x %class.Lecturer], [100 x %class.Lecturer]* %_L, i32 0, i32 0
  %arrayctor.end6 = getelementptr inbounds %class.Lecturer, %class.Lecturer* %array.begin5, i64 100
  br label %arrayctor.loop7

arrayctor.loop7:                                  ; preds = %invoke.cont10, %arrayctor.cont
  %arrayctor.cur8 = phi %class.Lecturer* [ %array.begin5, %arrayctor.cont ], [ %arrayctor.next17, %invoke.cont10 ]
  invoke void @_ZN8LecturerC2Ev(%class.Lecturer* nonnull dereferenceable(224) %arrayctor.cur8)
          to label %invoke.cont10 unwind label %lpad9

invoke.cont10:                                    ; preds = %arrayctor.loop7
  %arrayctor.next17 = getelementptr inbounds %class.Lecturer, %class.Lecturer* %arrayctor.cur8, i64 1
  %arrayctor.done18 = icmp eq %class.Lecturer* %arrayctor.next17, %arrayctor.end6
  br i1 %arrayctor.done18, label %arrayctor.cont19, label %arrayctor.loop7

arrayctor.cont19:                                 ; preds = %invoke.cont10
  %array.begin20 = getelementptr inbounds [100 x %class.Appointment], [100 x %class.Appointment]* %_A, i32 0, i32 0
  %arrayctor.end21 = getelementptr inbounds %class.Appointment, %class.Appointment* %array.begin20, i64 100
  br label %arrayctor.loop22

arrayctor.loop22:                                 ; preds = %invoke.cont25, %arrayctor.cont19
  %arrayctor.cur23 = phi %class.Appointment* [ %array.begin20, %arrayctor.cont19 ], [ %arrayctor.next32, %invoke.cont25 ]
  invoke void @_ZN11AppointmentC2Ev(%class.Appointment* nonnull dereferenceable(544) %arrayctor.cur23)
          to label %invoke.cont25 unwind label %lpad24

invoke.cont25:                                    ; preds = %arrayctor.loop22
  %arrayctor.next32 = getelementptr inbounds %class.Appointment, %class.Appointment* %arrayctor.cur23, i64 1
  %arrayctor.done33 = icmp eq %class.Appointment* %arrayctor.next32, %arrayctor.end21
  br i1 %arrayctor.done33, label %arrayctor.cont34, label %arrayctor.loop22

arrayctor.cont34:                                 ; preds = %invoke.cont25
  %arraydecay = getelementptr inbounds [100 x %class.Student], [100 x %class.Student]* %_S, i64 0, i64 0
  %arraydecay35 = getelementptr inbounds [100 x %class.Lecturer], [100 x %class.Lecturer]* %_L, i64 0, i64 0
  %arraydecay36 = getelementptr inbounds [100 x %class.Appointment], [100 x %class.Appointment]* %_A, i64 0, i64 0
  %13 = load i8*, i8** %file1, align 8
  %14 = load i8*, i8** %file2, align 8
  %15 = load i8*, i8** %file3, align 8
  invoke void @_Z12readFromFileP7StudentP8LecturerP11AppointmentPcS5_S5_(%class.Student* %arraydecay, %class.Lecturer* %arraydecay35, %class.Appointment* %arraydecay36, i8* %13, i8* %14, i8* %15)
          to label %invoke.cont38 unwind label %lpad37

invoke.cont38:                                    ; preds = %arrayctor.cont34
  store i32 0, i32* %menuLoop, align 4
  store i32 0, i32* %menu, align 4
  br label %while.cond

while.cond:                                       ; preds = %if.end1091, %invoke.cont38
  %16 = load i32, i32* %menuLoop, align 4
  %cmp39 = icmp ne i32 %16, 5
  br i1 %cmp39, label %while.body, label %while.end1092

while.body:                                       ; preds = %while.cond
  %call41 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* nonnull dereferenceable(8) @_ZSt4cout, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
          to label %invoke.cont40 unwind label %lpad37

invoke.cont40:                                    ; preds = %while.body
  %call43 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) %call41, i8* getelementptr inbounds ([38 x i8], [38 x i8]* @.str.5, i64 0, i64 0))
          to label %invoke.cont42 unwind label %lpad37

invoke.cont42:                                    ; preds = %invoke.cont40
  %call45 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* nonnull dereferenceable(8) %call43, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
          to label %invoke.cont44 unwind label %lpad37

invoke.cont44:                                    ; preds = %invoke.cont42
  %call47 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* nonnull dereferenceable(8) %call45, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
          to label %invoke.cont46 unwind label %lpad37

invoke.cont46:                                    ; preds = %invoke.cont44
  %call49 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) @_ZSt4cout, i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.6, i64 0, i64 0))
          to label %invoke.cont48 unwind label %lpad37

invoke.cont48:                                    ; preds = %invoke.cont46
  %call51 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* nonnull dereferenceable(8) %call49, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
          to label %invoke.cont50 unwind label %lpad37

invoke.cont50:                                    ; preds = %invoke.cont48
  %call53 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) @_ZSt4cout, i8* getelementptr inbounds ([17 x i8], [17 x i8]* @.str.7, i64 0, i64 0))
          to label %invoke.cont52 unwind label %lpad37

invoke.cont52:                                    ; preds = %invoke.cont50
  %call55 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* nonnull dereferenceable(8) %call53, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
          to label %invoke.cont54 unwind label %lpad37

invoke.cont54:                                    ; preds = %invoke.cont52
  %call57 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) @_ZSt4cout, i8* getelementptr inbounds ([18 x i8], [18 x i8]* @.str.8, i64 0, i64 0))
          to label %invoke.cont56 unwind label %lpad37

invoke.cont56:                                    ; preds = %invoke.cont54
  %call59 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* nonnull dereferenceable(8) %call57, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
          to label %invoke.cont58 unwind label %lpad37

invoke.cont58:                                    ; preds = %invoke.cont56
  %call61 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) @_ZSt4cout, i8* getelementptr inbounds ([21 x i8], [21 x i8]* @.str.9, i64 0, i64 0))
          to label %invoke.cont60 unwind label %lpad37

invoke.cont60:                                    ; preds = %invoke.cont58
  %call63 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* nonnull dereferenceable(8) %call61, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
          to label %invoke.cont62 unwind label %lpad37

invoke.cont62:                                    ; preds = %invoke.cont60
  %call65 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) @_ZSt4cout, i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str.10, i64 0, i64 0))
          to label %invoke.cont64 unwind label %lpad37

invoke.cont64:                                    ; preds = %invoke.cont62
  %call67 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* nonnull dereferenceable(8) %call65, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
          to label %invoke.cont66 unwind label %lpad37

invoke.cont66:                                    ; preds = %invoke.cont64
  %call69 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) @_ZSt4cout, i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str.11, i64 0, i64 0))
          to label %invoke.cont68 unwind label %lpad37

invoke.cont68:                                    ; preds = %invoke.cont66
  %call71 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* nonnull dereferenceable(8) %call69, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
          to label %invoke.cont70 unwind label %lpad37

invoke.cont70:                                    ; preds = %invoke.cont68
  %call73 = invoke nonnull align 8 dereferenceable(16) %"class.std::basic_istream"* @_ZNSirsERi(%"class.std::basic_istream"* nonnull dereferenceable(16) @_ZSt3cin, i32* nonnull align 4 dereferenceable(4) %menuLoop)
          to label %invoke.cont72 unwind label %lpad37

invoke.cont72:                                    ; preds = %invoke.cont70
  %17 = load i32, i32* %menuLoop, align 4
  %cmp74 = icmp eq i32 %17, 1
  br i1 %cmp74, label %if.then75, label %if.else326

if.then75:                                        ; preds = %invoke.cont72
  %call77 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) @_ZSt4cout, i8* getelementptr inbounds ([16 x i8], [16 x i8]* @.str.12, i64 0, i64 0))
          to label %invoke.cont76 unwind label %lpad37

invoke.cont76:                                    ; preds = %if.then75
  %call79 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* nonnull dereferenceable(8) %call77, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
          to label %invoke.cont78 unwind label %lpad37

invoke.cont78:                                    ; preds = %invoke.cont76
  %call81 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) @_ZSt4cout, i8* getelementptr inbounds ([18 x i8], [18 x i8]* @.str.13, i64 0, i64 0))
          to label %invoke.cont80 unwind label %lpad37

invoke.cont80:                                    ; preds = %invoke.cont78
  %call83 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* nonnull dereferenceable(8) %call81, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
          to label %invoke.cont82 unwind label %lpad37

invoke.cont82:                                    ; preds = %invoke.cont80
  %call85 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) @_ZSt4cout, i8* getelementptr inbounds ([19 x i8], [19 x i8]* @.str.14, i64 0, i64 0))
          to label %invoke.cont84 unwind label %lpad37

invoke.cont84:                                    ; preds = %invoke.cont82
  %call87 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* nonnull dereferenceable(8) %call85, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
          to label %invoke.cont86 unwind label %lpad37

invoke.cont86:                                    ; preds = %invoke.cont84
  %call89 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) @_ZSt4cout, i8* getelementptr inbounds ([19 x i8], [19 x i8]* @.str.15, i64 0, i64 0))
          to label %invoke.cont88 unwind label %lpad37

invoke.cont88:                                    ; preds = %invoke.cont86
  %call91 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* nonnull dereferenceable(8) %call89, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
          to label %invoke.cont90 unwind label %lpad37

invoke.cont90:                                    ; preds = %invoke.cont88
  %call93 = invoke nonnull align 8 dereferenceable(16) %"class.std::basic_istream"* @_ZNSirsERi(%"class.std::basic_istream"* nonnull dereferenceable(16) @_ZSt3cin, i32* nonnull align 4 dereferenceable(4) %menu)
          to label %invoke.cont92 unwind label %lpad37

invoke.cont92:                                    ; preds = %invoke.cont90
  %18 = load i32, i32* %menu, align 4
  %cmp94 = icmp eq i32 %18, 1
  br i1 %cmp94, label %if.then95, label %if.else

if.then95:                                        ; preds = %invoke.cont92
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %temp_) #3
  invoke void @_ZN7StudentC2Ev(%class.Student* nonnull dereferenceable(224) %newS)
          to label %invoke.cont97 unwind label %lpad96

invoke.cont97:                                    ; preds = %if.then95
  %call100 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) @_ZSt4cout, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.16, i64 0, i64 0))
          to label %invoke.cont99 unwind label %lpad98

invoke.cont99:                                    ; preds = %invoke.cont97
  %call102 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* nonnull dereferenceable(8) %call100, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
          to label %invoke.cont101 unwind label %lpad98

invoke.cont101:                                   ; preds = %invoke.cont99
  %call104 = invoke nonnull align 8 dereferenceable(16) %"class.std::basic_istream"* @_ZStrsIcSt11char_traitsIcESaIcEERSt13basic_istreamIT_T0_ES7_RNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_istream"* nonnull align 8 dereferenceable(16) @_ZSt3cin, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %temp_)
          to label %invoke.cont103 unwind label %lpad98

invoke.cont103:                                   ; preds = %invoke.cont101
  %19 = bitcast %class.Student* %newS to %class.People*
  invoke void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1ERKS4_(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %temp_)
          to label %invoke.cont105 unwind label %lpad98

invoke.cont105:                                   ; preds = %invoke.cont103
  invoke void @_ZN6People9setNumberENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE(%class.People* nonnull dereferenceable(192) %19, %"class.std::__cxx11::basic_string"* %agg.tmp)
          to label %invoke.cont107 unwind label %lpad106

invoke.cont107:                                   ; preds = %invoke.cont105
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp) #3
  %call109 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) @_ZSt4cout, i8* getelementptr inbounds ([14 x i8], [14 x i8]* @.str.17, i64 0, i64 0))
          to label %invoke.cont108 unwind label %lpad98

invoke.cont108:                                   ; preds = %invoke.cont107
  %call111 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* nonnull dereferenceable(8) %call109, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
          to label %invoke.cont110 unwind label %lpad98

invoke.cont110:                                   ; preds = %invoke.cont108
  %call113 = invoke nonnull align 8 dereferenceable(16) %"class.std::basic_istream"* @_ZStrsIcSt11char_traitsIcESaIcEERSt13basic_istreamIT_T0_ES7_RNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_istream"* nonnull align 8 dereferenceable(16) @_ZSt3cin, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %temp_)
          to label %invoke.cont112 unwind label %lpad98

invoke.cont112:                                   ; preds = %invoke.cont110
  %20 = bitcast %class.Student* %newS to %class.People*
  invoke void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1ERKS4_(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp114, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %temp_)
          to label %invoke.cont115 unwind label %lpad98

invoke.cont115:                                   ; preds = %invoke.cont112
  invoke void @_ZN6People7setNameENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE(%class.People* nonnull dereferenceable(192) %20, %"class.std::__cxx11::basic_string"* %agg.tmp114)
          to label %invoke.cont117 unwind label %lpad116

invoke.cont117:                                   ; preds = %invoke.cont115
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp114) #3
  %call119 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) @_ZSt4cout, i8* getelementptr inbounds ([19 x i8], [19 x i8]* @.str.18, i64 0, i64 0))
          to label %invoke.cont118 unwind label %lpad98

invoke.cont118:                                   ; preds = %invoke.cont117
  %call121 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* nonnull dereferenceable(8) %call119, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
          to label %invoke.cont120 unwind label %lpad98

invoke.cont120:                                   ; preds = %invoke.cont118
  %call123 = invoke nonnull align 8 dereferenceable(16) %"class.std::basic_istream"* @_ZStrsIcSt11char_traitsIcESaIcEERSt13basic_istreamIT_T0_ES7_RNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_istream"* nonnull align 8 dereferenceable(16) @_ZSt3cin, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %temp_)
          to label %invoke.cont122 unwind label %lpad98

invoke.cont122:                                   ; preds = %invoke.cont120
  %21 = bitcast %class.Student* %newS to %class.People*
  invoke void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1ERKS4_(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp124, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %temp_)
          to label %invoke.cont125 unwind label %lpad98

invoke.cont125:                                   ; preds = %invoke.cont122
  invoke void @_ZN6People10setSurnameENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE(%class.People* nonnull dereferenceable(192) %21, %"class.std::__cxx11::basic_string"* %agg.tmp124)
          to label %invoke.cont127 unwind label %lpad126

invoke.cont127:                                   ; preds = %invoke.cont125
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp124) #3
  %call129 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) @_ZSt4cout, i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str.19, i64 0, i64 0))
          to label %invoke.cont128 unwind label %lpad98

invoke.cont128:                                   ; preds = %invoke.cont127
  %call131 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* nonnull dereferenceable(8) %call129, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
          to label %invoke.cont130 unwind label %lpad98

invoke.cont130:                                   ; preds = %invoke.cont128
  %call133 = invoke nonnull align 8 dereferenceable(16) %"class.std::basic_istream"* @_ZStrsIcSt11char_traitsIcESaIcEERSt13basic_istreamIT_T0_ES7_RNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_istream"* nonnull align 8 dereferenceable(16) @_ZSt3cin, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %temp_)
          to label %invoke.cont132 unwind label %lpad98

invoke.cont132:                                   ; preds = %invoke.cont130
  %22 = bitcast %class.Student* %newS to %class.People*
  invoke void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1ERKS4_(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp134, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %temp_)
          to label %invoke.cont135 unwind label %lpad98

invoke.cont135:                                   ; preds = %invoke.cont132
  invoke void @_ZN6People13setDepartmentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE(%class.People* nonnull dereferenceable(192) %22, %"class.std::__cxx11::basic_string"* %agg.tmp134)
          to label %invoke.cont137 unwind label %lpad136

invoke.cont137:                                   ; preds = %invoke.cont135
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp134) #3
  %call139 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) @_ZSt4cout, i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.20, i64 0, i64 0))
          to label %invoke.cont138 unwind label %lpad98

invoke.cont138:                                   ; preds = %invoke.cont137
  %call141 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* nonnull dereferenceable(8) %call139, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
          to label %invoke.cont140 unwind label %lpad98

invoke.cont140:                                   ; preds = %invoke.cont138
  %call143 = invoke nonnull align 8 dereferenceable(16) %"class.std::basic_istream"* @_ZStrsIcSt11char_traitsIcESaIcEERSt13basic_istreamIT_T0_ES7_RNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_istream"* nonnull align 8 dereferenceable(16) @_ZSt3cin, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %temp_)
          to label %invoke.cont142 unwind label %lpad98

invoke.cont142:                                   ; preds = %invoke.cont140
  invoke void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1ERKS4_(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp144, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %temp_)
          to label %invoke.cont145 unwind label %lpad98

invoke.cont145:                                   ; preds = %invoke.cont142
  invoke void @_ZN7Student7setYearENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE(%class.Student* nonnull dereferenceable(224) %newS, %"class.std::__cxx11::basic_string"* %agg.tmp144)
          to label %invoke.cont147 unwind label %lpad146

invoke.cont147:                                   ; preds = %invoke.cont145
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp144) #3
  %call149 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) @_ZSt4cout, i8* getelementptr inbounds ([16 x i8], [16 x i8]* @.str.21, i64 0, i64 0))
          to label %invoke.cont148 unwind label %lpad98

invoke.cont148:                                   ; preds = %invoke.cont147
  %call151 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* nonnull dereferenceable(8) %call149, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
          to label %invoke.cont150 unwind label %lpad98

invoke.cont150:                                   ; preds = %invoke.cont148
  %call153 = invoke nonnull align 8 dereferenceable(16) %"class.std::basic_istream"* @_ZStrsIcSt11char_traitsIcESaIcEERSt13basic_istreamIT_T0_ES7_RNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_istream"* nonnull align 8 dereferenceable(16) @_ZSt3cin, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %temp_)
          to label %invoke.cont152 unwind label %lpad98

invoke.cont152:                                   ; preds = %invoke.cont150
  %23 = bitcast %class.Student* %newS to %class.People*
  invoke void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1ERKS4_(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp154, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %temp_)
          to label %invoke.cont155 unwind label %lpad98

invoke.cont155:                                   ; preds = %invoke.cont152
  invoke void @_ZN6People8setEmailENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE(%class.People* nonnull dereferenceable(192) %23, %"class.std::__cxx11::basic_string"* %agg.tmp154)
          to label %invoke.cont157 unwind label %lpad156

invoke.cont157:                                   ; preds = %invoke.cont155
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp154) #3
  %call159 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) @_ZSt4cout, i8* getelementptr inbounds ([22 x i8], [22 x i8]* @.str.22, i64 0, i64 0))
          to label %invoke.cont158 unwind label %lpad98

invoke.cont158:                                   ; preds = %invoke.cont157
  %call161 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* nonnull dereferenceable(8) %call159, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
          to label %invoke.cont160 unwind label %lpad98

invoke.cont160:                                   ; preds = %invoke.cont158
  %call163 = invoke nonnull align 8 dereferenceable(16) %"class.std::basic_istream"* @_ZStrsIcSt11char_traitsIcESaIcEERSt13basic_istreamIT_T0_ES7_RNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_istream"* nonnull align 8 dereferenceable(16) @_ZSt3cin, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %temp_)
          to label %invoke.cont162 unwind label %lpad98

invoke.cont162:                                   ; preds = %invoke.cont160
  %24 = bitcast %class.Student* %newS to %class.People*
  invoke void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1ERKS4_(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp164, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %temp_)
          to label %invoke.cont165 unwind label %lpad98

invoke.cont165:                                   ; preds = %invoke.cont162
  invoke void @_ZN6People8setPhoneENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE(%class.People* nonnull dereferenceable(192) %24, %"class.std::__cxx11::basic_string"* %agg.tmp164)
          to label %invoke.cont167 unwind label %lpad166

invoke.cont167:                                   ; preds = %invoke.cont165
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp164) #3
  %25 = load i32, i32* %x, align 4
  %idxprom = sext i32 %25 to i64
  %arrayidx168 = getelementptr inbounds [100 x %class.Student], [100 x %class.Student]* %_S, i64 0, i64 %idxprom
  %call170 = invoke nonnull align 8 dereferenceable(224) %class.Student* @_ZN7StudentaSERKS_(%class.Student* nonnull dereferenceable(224) %arrayidx168, %class.Student* nonnull align 8 dereferenceable(224) %newS)
          to label %invoke.cont169 unwind label %lpad98

invoke.cont169:                                   ; preds = %invoke.cont167
  %26 = load i32, i32* %x, align 4
  %inc = add nsw i32 %26, 1
  store i32 %inc, i32* %x, align 4
  call void @_ZN7StudentD2Ev(%class.Student* nonnull dereferenceable(224) %newS) #3
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %temp_) #3
  br label %if.end325

lpad:                                             ; preds = %arrayctor.loop
  %27 = landingpad { i8*, i32 }
          cleanup
  %28 = extractvalue { i8*, i32 } %27, 0
  store i8* %28, i8** %exn.slot, align 8
  %29 = extractvalue { i8*, i32 } %27, 1
  store i32 %29, i32* %ehselector.slot, align 4
  %arraydestroy.isempty = icmp eq %class.Student* %array.begin, %arrayctor.cur
  br i1 %arraydestroy.isempty, label %arraydestroy.done4, label %arraydestroy.body

arraydestroy.body:                                ; preds = %arraydestroy.body, %lpad
  %arraydestroy.elementPast = phi %class.Student* [ %arrayctor.cur, %lpad ], [ %arraydestroy.element, %arraydestroy.body ]
  %arraydestroy.element = getelementptr inbounds %class.Student, %class.Student* %arraydestroy.elementPast, i64 -1
  call void @_ZN7StudentD2Ev(%class.Student* nonnull dereferenceable(224) %arraydestroy.element) #3
  %arraydestroy.done = icmp eq %class.Student* %arraydestroy.element, %array.begin
  br i1 %arraydestroy.done, label %arraydestroy.done4, label %arraydestroy.body

arraydestroy.done4:                               ; preds = %arraydestroy.body, %lpad
  br label %eh.resume

lpad9:                                            ; preds = %arrayctor.loop7
  %30 = landingpad { i8*, i32 }
          cleanup
  %31 = extractvalue { i8*, i32 } %30, 0
  store i8* %31, i8** %exn.slot, align 8
  %32 = extractvalue { i8*, i32 } %30, 1
  store i32 %32, i32* %ehselector.slot, align 4
  %arraydestroy.isempty11 = icmp eq %class.Lecturer* %array.begin5, %arrayctor.cur8
  br i1 %arraydestroy.isempty11, label %arraydestroy.done16, label %arraydestroy.body12

arraydestroy.body12:                              ; preds = %arraydestroy.body12, %lpad9
  %arraydestroy.elementPast13 = phi %class.Lecturer* [ %arrayctor.cur8, %lpad9 ], [ %arraydestroy.element14, %arraydestroy.body12 ]
  %arraydestroy.element14 = getelementptr inbounds %class.Lecturer, %class.Lecturer* %arraydestroy.elementPast13, i64 -1
  call void @_ZN8LecturerD2Ev(%class.Lecturer* nonnull dereferenceable(224) %arraydestroy.element14) #3
  %arraydestroy.done15 = icmp eq %class.Lecturer* %arraydestroy.element14, %array.begin5
  br i1 %arraydestroy.done15, label %arraydestroy.done16, label %arraydestroy.body12

arraydestroy.done16:                              ; preds = %arraydestroy.body12, %lpad9
  br label %ehcleanup1127

lpad24:                                           ; preds = %arrayctor.loop22
  %33 = landingpad { i8*, i32 }
          cleanup
  %34 = extractvalue { i8*, i32 } %33, 0
  store i8* %34, i8** %exn.slot, align 8
  %35 = extractvalue { i8*, i32 } %33, 1
  store i32 %35, i32* %ehselector.slot, align 4
  %arraydestroy.isempty26 = icmp eq %class.Appointment* %array.begin20, %arrayctor.cur23
  br i1 %arraydestroy.isempty26, label %arraydestroy.done31, label %arraydestroy.body27

arraydestroy.body27:                              ; preds = %arraydestroy.body27, %lpad24
  %arraydestroy.elementPast28 = phi %class.Appointment* [ %arrayctor.cur23, %lpad24 ], [ %arraydestroy.element29, %arraydestroy.body27 ]
  %arraydestroy.element29 = getelementptr inbounds %class.Appointment, %class.Appointment* %arraydestroy.elementPast28, i64 -1
  call void @_ZN11AppointmentD2Ev(%class.Appointment* nonnull dereferenceable(544) %arraydestroy.element29) #3
  %arraydestroy.done30 = icmp eq %class.Appointment* %arraydestroy.element29, %array.begin20
  br i1 %arraydestroy.done30, label %arraydestroy.done31, label %arraydestroy.body27

arraydestroy.done31:                              ; preds = %arraydestroy.body27, %lpad24
  br label %ehcleanup1113

lpad37:                                           ; preds = %invoke.cont1083, %if.else1082, %invoke.cont1078, %if.then1077, %if.then856, %invoke.cont846, %if.else845, %for.body697, %if.then692, %invoke.cont618, %invoke.cont616, %invoke.cont614, %invoke.cont612, %invoke.cont610, %invoke.cont608, %invoke.cont606, %invoke.cont604, %if.then603, %invoke.cont593, %if.else592, %for.body444, %if.then439, %invoke.cont343, %invoke.cont341, %invoke.cont339, %invoke.cont337, %invoke.cont335, %invoke.cont333, %invoke.cont331, %invoke.cont329, %if.then328, %invoke.cont318, %if.else317, %for.body, %if.then173, %invoke.cont90, %invoke.cont88, %invoke.cont86, %invoke.cont84, %invoke.cont82, %invoke.cont80, %invoke.cont78, %invoke.cont76, %if.then75, %invoke.cont70, %invoke.cont68, %invoke.cont66, %invoke.cont64, %invoke.cont62, %invoke.cont60, %invoke.cont58, %invoke.cont56, %invoke.cont54, %invoke.cont52, %invoke.cont50, %invoke.cont48, %invoke.cont46, %invoke.cont44, %invoke.cont42, %invoke.cont40, %while.body, %arrayctor.cont34
  %36 = landingpad { i8*, i32 }
          cleanup
  %37 = extractvalue { i8*, i32 } %36, 0
  store i8* %37, i8** %exn.slot, align 8
  %38 = extractvalue { i8*, i32 } %36, 1
  store i32 %38, i32* %ehselector.slot, align 4
  br label %ehcleanup1099

lpad96:                                           ; preds = %if.then95
  %39 = landingpad { i8*, i32 }
          cleanup
  %40 = extractvalue { i8*, i32 } %39, 0
  store i8* %40, i8** %exn.slot, align 8
  %41 = extractvalue { i8*, i32 } %39, 1
  store i32 %41, i32* %ehselector.slot, align 4
  br label %ehcleanup171

lpad98:                                           ; preds = %invoke.cont167, %invoke.cont162, %invoke.cont160, %invoke.cont158, %invoke.cont157, %invoke.cont152, %invoke.cont150, %invoke.cont148, %invoke.cont147, %invoke.cont142, %invoke.cont140, %invoke.cont138, %invoke.cont137, %invoke.cont132, %invoke.cont130, %invoke.cont128, %invoke.cont127, %invoke.cont122, %invoke.cont120, %invoke.cont118, %invoke.cont117, %invoke.cont112, %invoke.cont110, %invoke.cont108, %invoke.cont107, %invoke.cont103, %invoke.cont101, %invoke.cont99, %invoke.cont97
  %42 = landingpad { i8*, i32 }
          cleanup
  %43 = extractvalue { i8*, i32 } %42, 0
  store i8* %43, i8** %exn.slot, align 8
  %44 = extractvalue { i8*, i32 } %42, 1
  store i32 %44, i32* %ehselector.slot, align 4
  br label %ehcleanup

lpad106:                                          ; preds = %invoke.cont105
  %45 = landingpad { i8*, i32 }
          cleanup
  %46 = extractvalue { i8*, i32 } %45, 0
  store i8* %46, i8** %exn.slot, align 8
  %47 = extractvalue { i8*, i32 } %45, 1
  store i32 %47, i32* %ehselector.slot, align 4
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp) #3
  br label %ehcleanup

lpad116:                                          ; preds = %invoke.cont115
  %48 = landingpad { i8*, i32 }
          cleanup
  %49 = extractvalue { i8*, i32 } %48, 0
  store i8* %49, i8** %exn.slot, align 8
  %50 = extractvalue { i8*, i32 } %48, 1
  store i32 %50, i32* %ehselector.slot, align 4
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp114) #3
  br label %ehcleanup

lpad126:                                          ; preds = %invoke.cont125
  %51 = landingpad { i8*, i32 }
          cleanup
  %52 = extractvalue { i8*, i32 } %51, 0
  store i8* %52, i8** %exn.slot, align 8
  %53 = extractvalue { i8*, i32 } %51, 1
  store i32 %53, i32* %ehselector.slot, align 4
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp124) #3
  br label %ehcleanup

lpad136:                                          ; preds = %invoke.cont135
  %54 = landingpad { i8*, i32 }
          cleanup
  %55 = extractvalue { i8*, i32 } %54, 0
  store i8* %55, i8** %exn.slot, align 8
  %56 = extractvalue { i8*, i32 } %54, 1
  store i32 %56, i32* %ehselector.slot, align 4
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp134) #3
  br label %ehcleanup

lpad146:                                          ; preds = %invoke.cont145
  %57 = landingpad { i8*, i32 }
          cleanup
  %58 = extractvalue { i8*, i32 } %57, 0
  store i8* %58, i8** %exn.slot, align 8
  %59 = extractvalue { i8*, i32 } %57, 1
  store i32 %59, i32* %ehselector.slot, align 4
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp144) #3
  br label %ehcleanup

lpad156:                                          ; preds = %invoke.cont155
  %60 = landingpad { i8*, i32 }
          cleanup
  %61 = extractvalue { i8*, i32 } %60, 0
  store i8* %61, i8** %exn.slot, align 8
  %62 = extractvalue { i8*, i32 } %60, 1
  store i32 %62, i32* %ehselector.slot, align 4
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp154) #3
  br label %ehcleanup

lpad166:                                          ; preds = %invoke.cont165
  %63 = landingpad { i8*, i32 }
          cleanup
  %64 = extractvalue { i8*, i32 } %63, 0
  store i8* %64, i8** %exn.slot, align 8
  %65 = extractvalue { i8*, i32 } %63, 1
  store i32 %65, i32* %ehselector.slot, align 4
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp164) #3
  br label %ehcleanup

ehcleanup:                                        ; preds = %lpad166, %lpad156, %lpad146, %lpad136, %lpad126, %lpad116, %lpad106, %lpad98
  call void @_ZN7StudentD2Ev(%class.Student* nonnull dereferenceable(224) %newS) #3
  br label %ehcleanup171

ehcleanup171:                                     ; preds = %ehcleanup, %lpad96
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %temp_) #3
  br label %ehcleanup1099

if.else:                                          ; preds = %invoke.cont92
  %66 = load i32, i32* %menu, align 4
  %cmp172 = icmp eq i32 %66, 2
  br i1 %cmp172, label %if.then173, label %if.else181

if.then173:                                       ; preds = %if.else
  %call175 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* nonnull dereferenceable(8) @_ZSt4cout, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
          to label %invoke.cont174 unwind label %lpad37

invoke.cont174:                                   ; preds = %if.then173
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %invoke.cont174
  %67 = load i32, i32* %i, align 4
  %68 = load i32, i32* %x, align 4
  %cmp176 = icmp slt i32 %67, %68
  br i1 %cmp176, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %69 = load i32, i32* %i, align 4
  %idxprom177 = sext i32 %69 to i64
  %arrayidx178 = getelementptr inbounds [100 x %class.Student], [100 x %class.Student]* %_S, i64 0, i64 %idxprom177
  invoke void @_ZN7Student7displayEv(%class.Student* nonnull dereferenceable(224) %arrayidx178)
          to label %invoke.cont179 unwind label %lpad37

invoke.cont179:                                   ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %invoke.cont179
  %70 = load i32, i32* %i, align 4
  %inc180 = add nsw i32 %70, 1
  store i32 %inc180, i32* %i, align 4
  br label %for.cond, !llvm.loop !10

for.end:                                          ; preds = %for.cond
  br label %if.end324

if.else181:                                       ; preds = %if.else
  %71 = load i32, i32* %menu, align 4
  %cmp182 = icmp eq i32 %71, 3
  br i1 %cmp182, label %if.then183, label %if.else215

if.then183:                                       ; preds = %if.else181
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %temp_184) #3
  %call187 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) @_ZSt4cout, i8* getelementptr inbounds ([32 x i8], [32 x i8]* @.str.23, i64 0, i64 0))
          to label %invoke.cont186 unwind label %lpad185

invoke.cont186:                                   ; preds = %if.then183
  %call189 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* nonnull dereferenceable(8) %call187, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
          to label %invoke.cont188 unwind label %lpad185

invoke.cont188:                                   ; preds = %invoke.cont186
  %call191 = invoke nonnull align 8 dereferenceable(16) %"class.std::basic_istream"* @_ZStrsIcSt11char_traitsIcESaIcEERSt13basic_istreamIT_T0_ES7_RNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_istream"* nonnull align 8 dereferenceable(16) @_ZSt3cin, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %temp_184)
          to label %invoke.cont190 unwind label %lpad185

invoke.cont190:                                   ; preds = %invoke.cont188
  store i32 0, i32* %i, align 4
  br label %while.cond192

while.cond192:                                    ; preds = %if.end200, %invoke.cont190
  %72 = load i32, i32* %i, align 4
  %73 = load i32, i32* %x, align 4
  %cmp193 = icmp slt i32 %72, %73
  br i1 %cmp193, label %while.body194, label %while.end

while.body194:                                    ; preds = %while.cond192
  %74 = load i32, i32* %i, align 4
  %idxprom195 = sext i32 %74 to i64
  %arrayidx196 = getelementptr inbounds [100 x %class.Student], [100 x %class.Student]* %_S, i64 0, i64 %idxprom195
  %75 = bitcast %class.Student* %arrayidx196 to %class.People*
  invoke void @_ZN6People9getNumberB5cxx11Ev(%"class.std::__cxx11::basic_string"* sret(%"class.std::__cxx11::basic_string") align 8 %ref.tmp, %class.People* nonnull dereferenceable(192) %75)
          to label %invoke.cont197 unwind label %lpad185

invoke.cont197:                                   ; preds = %while.body194
  %call198 = call zeroext i1 @_ZSteqIcEN9__gnu_cxx11__enable_ifIXsr9__is_charIT_EE7__valueEbE6__typeERKNSt7__cxx1112basic_stringIS2_St11char_traitsIS2_ESaIS2_EEESC_(%"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %ref.tmp, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %temp_184) #3
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %ref.tmp) #3
  br i1 %call198, label %if.then199, label %if.end200

if.then199:                                       ; preds = %invoke.cont197
  %76 = load i32, i32* %i, align 4
  store i32 %76, i32* %deleted, align 4
  br label %if.end200

lpad185:                                          ; preds = %for.body204, %while.body194, %invoke.cont188, %invoke.cont186, %if.then183
  %77 = landingpad { i8*, i32 }
          cleanup
  %78 = extractvalue { i8*, i32 } %77, 0
  store i8* %78, i8** %exn.slot, align 8
  %79 = extractvalue { i8*, i32 } %77, 1
  store i32 %79, i32* %ehselector.slot, align 4
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %temp_184) #3
  br label %ehcleanup1099

if.end200:                                        ; preds = %if.then199, %invoke.cont197
  %80 = load i32, i32* %i, align 4
  %inc201 = add nsw i32 %80, 1
  store i32 %inc201, i32* %i, align 4
  br label %while.cond192, !llvm.loop !11

while.end:                                        ; preds = %while.cond192
  %81 = load i32, i32* %deleted, align 4
  store i32 %81, i32* %i, align 4
  br label %for.cond202

for.cond202:                                      ; preds = %for.inc211, %while.end
  %82 = load i32, i32* %i, align 4
  %83 = load i32, i32* %x, align 4
  %cmp203 = icmp slt i32 %82, %83
  br i1 %cmp203, label %for.body204, label %for.end213

for.body204:                                      ; preds = %for.cond202
  %84 = load i32, i32* %i, align 4
  %add = add nsw i32 %84, 1
  %idxprom205 = sext i32 %add to i64
  %arrayidx206 = getelementptr inbounds [100 x %class.Student], [100 x %class.Student]* %_S, i64 0, i64 %idxprom205
  %85 = load i32, i32* %i, align 4
  %idxprom207 = sext i32 %85 to i64
  %arrayidx208 = getelementptr inbounds [100 x %class.Student], [100 x %class.Student]* %_S, i64 0, i64 %idxprom207
  %call210 = invoke nonnull align 8 dereferenceable(224) %class.Student* @_ZN7StudentaSERKS_(%class.Student* nonnull dereferenceable(224) %arrayidx208, %class.Student* nonnull align 8 dereferenceable(224) %arrayidx206)
          to label %invoke.cont209 unwind label %lpad185

invoke.cont209:                                   ; preds = %for.body204
  br label %for.inc211

for.inc211:                                       ; preds = %invoke.cont209
  %86 = load i32, i32* %i, align 4
  %inc212 = add nsw i32 %86, 1
  store i32 %inc212, i32* %i, align 4
  br label %for.cond202, !llvm.loop !12

for.end213:                                       ; preds = %for.cond202
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %temp_184) #3
  br label %if.end323

if.else215:                                       ; preds = %if.else181
  %87 = load i32, i32* %menu, align 4
  %cmp216 = icmp eq i32 %87, 4
  br i1 %cmp216, label %if.then217, label %if.else317

if.then217:                                       ; preds = %if.else215
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %temp_218) #3
  %call221 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) @_ZSt4cout, i8* getelementptr inbounds ([32 x i8], [32 x i8]* @.str.24, i64 0, i64 0))
          to label %invoke.cont220 unwind label %lpad219

invoke.cont220:                                   ; preds = %if.then217
  %call223 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* nonnull dereferenceable(8) %call221, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
          to label %invoke.cont222 unwind label %lpad219

invoke.cont222:                                   ; preds = %invoke.cont220
  %call225 = invoke nonnull align 8 dereferenceable(16) %"class.std::basic_istream"* @_ZStrsIcSt11char_traitsIcESaIcEERSt13basic_istreamIT_T0_ES7_RNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_istream"* nonnull align 8 dereferenceable(16) @_ZSt3cin, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %temp_218)
          to label %invoke.cont224 unwind label %lpad219

invoke.cont224:                                   ; preds = %invoke.cont222
  store i32 0, i32* %i, align 4
  br label %while.cond226

while.cond226:                                    ; preds = %if.end235, %invoke.cont224
  %88 = load i32, i32* %i, align 4
  %89 = load i32, i32* %x, align 4
  %cmp227 = icmp slt i32 %88, %89
  br i1 %cmp227, label %while.body228, label %while.end237

while.body228:                                    ; preds = %while.cond226
  %90 = load i32, i32* %i, align 4
  %idxprom230 = sext i32 %90 to i64
  %arrayidx231 = getelementptr inbounds [100 x %class.Student], [100 x %class.Student]* %_S, i64 0, i64 %idxprom230
  %91 = bitcast %class.Student* %arrayidx231 to %class.People*
  invoke void @_ZN6People9getNumberB5cxx11Ev(%"class.std::__cxx11::basic_string"* sret(%"class.std::__cxx11::basic_string") align 8 %ref.tmp229, %class.People* nonnull dereferenceable(192) %91)
          to label %invoke.cont232 unwind label %lpad219

invoke.cont232:                                   ; preds = %while.body228
  %call233 = call zeroext i1 @_ZSteqIcEN9__gnu_cxx11__enable_ifIXsr9__is_charIT_EE7__valueEbE6__typeERKNSt7__cxx1112basic_stringIS2_St11char_traitsIS2_ESaIS2_EEESC_(%"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %ref.tmp229, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %temp_218) #3
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %ref.tmp229) #3
  br i1 %call233, label %if.then234, label %if.end235

if.then234:                                       ; preds = %invoke.cont232
  %92 = load i32, i32* %i, align 4
  store i32 %92, i32* %updated, align 4
  br label %if.end235

lpad219:                                          ; preds = %invoke.cont307, %invoke.cont305, %invoke.cont303, %invoke.cont301, %invoke.cont294, %invoke.cont292, %invoke.cont290, %invoke.cont288, %invoke.cont281, %invoke.cont279, %invoke.cont277, %invoke.cont275, %invoke.cont268, %invoke.cont266, %invoke.cont264, %invoke.cont262, %invoke.cont255, %invoke.cont253, %invoke.cont251, %invoke.cont249, %invoke.cont242, %invoke.cont240, %invoke.cont238, %while.end237, %while.body228, %invoke.cont222, %invoke.cont220, %if.then217
  %93 = landingpad { i8*, i32 }
          cleanup
  %94 = extractvalue { i8*, i32 } %93, 0
  store i8* %94, i8** %exn.slot, align 8
  %95 = extractvalue { i8*, i32 } %93, 1
  store i32 %95, i32* %ehselector.slot, align 4
  br label %ehcleanup316

if.end235:                                        ; preds = %if.then234, %invoke.cont232
  %96 = load i32, i32* %i, align 4
  %inc236 = add nsw i32 %96, 1
  store i32 %inc236, i32* %i, align 4
  br label %while.cond226, !llvm.loop !13

while.end237:                                     ; preds = %while.cond226
  %call239 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) @_ZSt4cout, i8* getelementptr inbounds ([22 x i8], [22 x i8]* @.str.25, i64 0, i64 0))
          to label %invoke.cont238 unwind label %lpad219

invoke.cont238:                                   ; preds = %while.end237
  %call241 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* nonnull dereferenceable(8) %call239, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
          to label %invoke.cont240 unwind label %lpad219

invoke.cont240:                                   ; preds = %invoke.cont238
  %call243 = invoke nonnull align 8 dereferenceable(16) %"class.std::basic_istream"* @_ZStrsIcSt11char_traitsIcESaIcEERSt13basic_istreamIT_T0_ES7_RNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_istream"* nonnull align 8 dereferenceable(16) @_ZSt3cin, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %temp_218)
          to label %invoke.cont242 unwind label %lpad219

invoke.cont242:                                   ; preds = %invoke.cont240
  %97 = load i32, i32* %updated, align 4
  %idxprom244 = sext i32 %97 to i64
  %arrayidx245 = getelementptr inbounds [100 x %class.Student], [100 x %class.Student]* %_S, i64 0, i64 %idxprom244
  %98 = bitcast %class.Student* %arrayidx245 to %class.People*
  invoke void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1ERKS4_(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp246, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %temp_218)
          to label %invoke.cont247 unwind label %lpad219

invoke.cont247:                                   ; preds = %invoke.cont242
  invoke void @_ZN6People7setNameENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE(%class.People* nonnull dereferenceable(192) %98, %"class.std::__cxx11::basic_string"* %agg.tmp246)
          to label %invoke.cont249 unwind label %lpad248

invoke.cont249:                                   ; preds = %invoke.cont247
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp246) #3
  %call252 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) @_ZSt4cout, i8* getelementptr inbounds ([27 x i8], [27 x i8]* @.str.26, i64 0, i64 0))
          to label %invoke.cont251 unwind label %lpad219

invoke.cont251:                                   ; preds = %invoke.cont249
  %call254 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* nonnull dereferenceable(8) %call252, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
          to label %invoke.cont253 unwind label %lpad219

invoke.cont253:                                   ; preds = %invoke.cont251
  %call256 = invoke nonnull align 8 dereferenceable(16) %"class.std::basic_istream"* @_ZStrsIcSt11char_traitsIcESaIcEERSt13basic_istreamIT_T0_ES7_RNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_istream"* nonnull align 8 dereferenceable(16) @_ZSt3cin, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %temp_218)
          to label %invoke.cont255 unwind label %lpad219

invoke.cont255:                                   ; preds = %invoke.cont253
  %99 = load i32, i32* %updated, align 4
  %idxprom257 = sext i32 %99 to i64
  %arrayidx258 = getelementptr inbounds [100 x %class.Student], [100 x %class.Student]* %_S, i64 0, i64 %idxprom257
  %100 = bitcast %class.Student* %arrayidx258 to %class.People*
  invoke void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1ERKS4_(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp259, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %temp_218)
          to label %invoke.cont260 unwind label %lpad219

invoke.cont260:                                   ; preds = %invoke.cont255
  invoke void @_ZN6People10setSurnameENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE(%class.People* nonnull dereferenceable(192) %100, %"class.std::__cxx11::basic_string"* %agg.tmp259)
          to label %invoke.cont262 unwind label %lpad261

invoke.cont262:                                   ; preds = %invoke.cont260
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp259) #3
  %call265 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) @_ZSt4cout, i8* getelementptr inbounds ([28 x i8], [28 x i8]* @.str.27, i64 0, i64 0))
          to label %invoke.cont264 unwind label %lpad219

invoke.cont264:                                   ; preds = %invoke.cont262
  %call267 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* nonnull dereferenceable(8) %call265, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
          to label %invoke.cont266 unwind label %lpad219

invoke.cont266:                                   ; preds = %invoke.cont264
  %call269 = invoke nonnull align 8 dereferenceable(16) %"class.std::basic_istream"* @_ZStrsIcSt11char_traitsIcESaIcEERSt13basic_istreamIT_T0_ES7_RNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_istream"* nonnull align 8 dereferenceable(16) @_ZSt3cin, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %temp_218)
          to label %invoke.cont268 unwind label %lpad219

invoke.cont268:                                   ; preds = %invoke.cont266
  %101 = load i32, i32* %updated, align 4
  %idxprom270 = sext i32 %101 to i64
  %arrayidx271 = getelementptr inbounds [100 x %class.Student], [100 x %class.Student]* %_S, i64 0, i64 %idxprom270
  %102 = bitcast %class.Student* %arrayidx271 to %class.People*
  invoke void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1ERKS4_(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp272, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %temp_218)
          to label %invoke.cont273 unwind label %lpad219

invoke.cont273:                                   ; preds = %invoke.cont268
  invoke void @_ZN6People13setDepartmentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE(%class.People* nonnull dereferenceable(192) %102, %"class.std::__cxx11::basic_string"* %agg.tmp272)
          to label %invoke.cont275 unwind label %lpad274

invoke.cont275:                                   ; preds = %invoke.cont273
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp272) #3
  %call278 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) @_ZSt4cout, i8* getelementptr inbounds ([31 x i8], [31 x i8]* @.str.28, i64 0, i64 0))
          to label %invoke.cont277 unwind label %lpad219

invoke.cont277:                                   ; preds = %invoke.cont275
  %call280 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* nonnull dereferenceable(8) %call278, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
          to label %invoke.cont279 unwind label %lpad219

invoke.cont279:                                   ; preds = %invoke.cont277
  %call282 = invoke nonnull align 8 dereferenceable(16) %"class.std::basic_istream"* @_ZStrsIcSt11char_traitsIcESaIcEERSt13basic_istreamIT_T0_ES7_RNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_istream"* nonnull align 8 dereferenceable(16) @_ZSt3cin, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %temp_218)
          to label %invoke.cont281 unwind label %lpad219

invoke.cont281:                                   ; preds = %invoke.cont279
  %103 = load i32, i32* %updated, align 4
  %idxprom283 = sext i32 %103 to i64
  %arrayidx284 = getelementptr inbounds [100 x %class.Student], [100 x %class.Student]* %_S, i64 0, i64 %idxprom283
  invoke void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1ERKS4_(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp285, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %temp_218)
          to label %invoke.cont286 unwind label %lpad219

invoke.cont286:                                   ; preds = %invoke.cont281
  invoke void @_ZN7Student7setYearENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE(%class.Student* nonnull dereferenceable(224) %arrayidx284, %"class.std::__cxx11::basic_string"* %agg.tmp285)
          to label %invoke.cont288 unwind label %lpad287

invoke.cont288:                                   ; preds = %invoke.cont286
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp285) #3
  %call291 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) @_ZSt4cout, i8* getelementptr inbounds ([24 x i8], [24 x i8]* @.str.29, i64 0, i64 0))
          to label %invoke.cont290 unwind label %lpad219

invoke.cont290:                                   ; preds = %invoke.cont288
  %call293 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* nonnull dereferenceable(8) %call291, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
          to label %invoke.cont292 unwind label %lpad219

invoke.cont292:                                   ; preds = %invoke.cont290
  %call295 = invoke nonnull align 8 dereferenceable(16) %"class.std::basic_istream"* @_ZStrsIcSt11char_traitsIcESaIcEERSt13basic_istreamIT_T0_ES7_RNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_istream"* nonnull align 8 dereferenceable(16) @_ZSt3cin, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %temp_218)
          to label %invoke.cont294 unwind label %lpad219

invoke.cont294:                                   ; preds = %invoke.cont292
  %104 = load i32, i32* %updated, align 4
  %idxprom296 = sext i32 %104 to i64
  %arrayidx297 = getelementptr inbounds [100 x %class.Student], [100 x %class.Student]* %_S, i64 0, i64 %idxprom296
  %105 = bitcast %class.Student* %arrayidx297 to %class.People*
  invoke void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1ERKS4_(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp298, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %temp_218)
          to label %invoke.cont299 unwind label %lpad219

invoke.cont299:                                   ; preds = %invoke.cont294
  invoke void @_ZN6People8setEmailENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE(%class.People* nonnull dereferenceable(192) %105, %"class.std::__cxx11::basic_string"* %agg.tmp298)
          to label %invoke.cont301 unwind label %lpad300

invoke.cont301:                                   ; preds = %invoke.cont299
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp298) #3
  %call304 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) @_ZSt4cout, i8* getelementptr inbounds ([30 x i8], [30 x i8]* @.str.30, i64 0, i64 0))
          to label %invoke.cont303 unwind label %lpad219

invoke.cont303:                                   ; preds = %invoke.cont301
  %call306 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* nonnull dereferenceable(8) %call304, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
          to label %invoke.cont305 unwind label %lpad219

invoke.cont305:                                   ; preds = %invoke.cont303
  %call308 = invoke nonnull align 8 dereferenceable(16) %"class.std::basic_istream"* @_ZStrsIcSt11char_traitsIcESaIcEERSt13basic_istreamIT_T0_ES7_RNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_istream"* nonnull align 8 dereferenceable(16) @_ZSt3cin, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %temp_218)
          to label %invoke.cont307 unwind label %lpad219

invoke.cont307:                                   ; preds = %invoke.cont305
  %106 = load i32, i32* %updated, align 4
  %idxprom309 = sext i32 %106 to i64
  %arrayidx310 = getelementptr inbounds [100 x %class.Student], [100 x %class.Student]* %_S, i64 0, i64 %idxprom309
  %107 = bitcast %class.Student* %arrayidx310 to %class.People*
  invoke void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1ERKS4_(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp311, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %temp_218)
          to label %invoke.cont312 unwind label %lpad219

invoke.cont312:                                   ; preds = %invoke.cont307
  invoke void @_ZN6People8setPhoneENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE(%class.People* nonnull dereferenceable(192) %107, %"class.std::__cxx11::basic_string"* %agg.tmp311)
          to label %invoke.cont314 unwind label %lpad313

invoke.cont314:                                   ; preds = %invoke.cont312
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp311) #3
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %temp_218) #3
  br label %if.end322

lpad248:                                          ; preds = %invoke.cont247
  %108 = landingpad { i8*, i32 }
          cleanup
  %109 = extractvalue { i8*, i32 } %108, 0
  store i8* %109, i8** %exn.slot, align 8
  %110 = extractvalue { i8*, i32 } %108, 1
  store i32 %110, i32* %ehselector.slot, align 4
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp246) #3
  br label %ehcleanup316

lpad261:                                          ; preds = %invoke.cont260
  %111 = landingpad { i8*, i32 }
          cleanup
  %112 = extractvalue { i8*, i32 } %111, 0
  store i8* %112, i8** %exn.slot, align 8
  %113 = extractvalue { i8*, i32 } %111, 1
  store i32 %113, i32* %ehselector.slot, align 4
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp259) #3
  br label %ehcleanup316

lpad274:                                          ; preds = %invoke.cont273
  %114 = landingpad { i8*, i32 }
          cleanup
  %115 = extractvalue { i8*, i32 } %114, 0
  store i8* %115, i8** %exn.slot, align 8
  %116 = extractvalue { i8*, i32 } %114, 1
  store i32 %116, i32* %ehselector.slot, align 4
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp272) #3
  br label %ehcleanup316

lpad287:                                          ; preds = %invoke.cont286
  %117 = landingpad { i8*, i32 }
          cleanup
  %118 = extractvalue { i8*, i32 } %117, 0
  store i8* %118, i8** %exn.slot, align 8
  %119 = extractvalue { i8*, i32 } %117, 1
  store i32 %119, i32* %ehselector.slot, align 4
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp285) #3
  br label %ehcleanup316

lpad300:                                          ; preds = %invoke.cont299
  %120 = landingpad { i8*, i32 }
          cleanup
  %121 = extractvalue { i8*, i32 } %120, 0
  store i8* %121, i8** %exn.slot, align 8
  %122 = extractvalue { i8*, i32 } %120, 1
  store i32 %122, i32* %ehselector.slot, align 4
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp298) #3
  br label %ehcleanup316

lpad313:                                          ; preds = %invoke.cont312
  %123 = landingpad { i8*, i32 }
          cleanup
  %124 = extractvalue { i8*, i32 } %123, 0
  store i8* %124, i8** %exn.slot, align 8
  %125 = extractvalue { i8*, i32 } %123, 1
  store i32 %125, i32* %ehselector.slot, align 4
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp311) #3
  br label %ehcleanup316

ehcleanup316:                                     ; preds = %lpad313, %lpad300, %lpad287, %lpad274, %lpad261, %lpad248, %lpad219
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %temp_218) #3
  br label %ehcleanup1099

if.else317:                                       ; preds = %if.else215
  %call319 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) @_ZSt4cout, i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.31, i64 0, i64 0))
          to label %invoke.cont318 unwind label %lpad37

invoke.cont318:                                   ; preds = %if.else317
  %call321 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* nonnull dereferenceable(8) %call319, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
          to label %invoke.cont320 unwind label %lpad37

invoke.cont320:                                   ; preds = %invoke.cont318
  br label %if.end322

if.end322:                                        ; preds = %invoke.cont320, %invoke.cont314
  br label %if.end323

if.end323:                                        ; preds = %if.end322, %for.end213
  br label %if.end324

if.end324:                                        ; preds = %if.end323, %for.end
  br label %if.end325

if.end325:                                        ; preds = %if.end324, %invoke.cont169
  br label %if.end1091

if.else326:                                       ; preds = %invoke.cont72
  %126 = load i32, i32* %menuLoop, align 4
  %cmp327 = icmp eq i32 %126, 2
  br i1 %cmp327, label %if.then328, label %if.else601

if.then328:                                       ; preds = %if.else326
  %call330 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) @_ZSt4cout, i8* getelementptr inbounds ([17 x i8], [17 x i8]* @.str.32, i64 0, i64 0))
          to label %invoke.cont329 unwind label %lpad37

invoke.cont329:                                   ; preds = %if.then328
  %call332 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* nonnull dereferenceable(8) %call330, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
          to label %invoke.cont331 unwind label %lpad37

invoke.cont331:                                   ; preds = %invoke.cont329
  %call334 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) @_ZSt4cout, i8* getelementptr inbounds ([19 x i8], [19 x i8]* @.str.33, i64 0, i64 0))
          to label %invoke.cont333 unwind label %lpad37

invoke.cont333:                                   ; preds = %invoke.cont331
  %call336 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* nonnull dereferenceable(8) %call334, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
          to label %invoke.cont335 unwind label %lpad37

invoke.cont335:                                   ; preds = %invoke.cont333
  %call338 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) @_ZSt4cout, i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str.34, i64 0, i64 0))
          to label %invoke.cont337 unwind label %lpad37

invoke.cont337:                                   ; preds = %invoke.cont335
  %call340 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* nonnull dereferenceable(8) %call338, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
          to label %invoke.cont339 unwind label %lpad37

invoke.cont339:                                   ; preds = %invoke.cont337
  %call342 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) @_ZSt4cout, i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str.35, i64 0, i64 0))
          to label %invoke.cont341 unwind label %lpad37

invoke.cont341:                                   ; preds = %invoke.cont339
  %call344 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* nonnull dereferenceable(8) %call342, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
          to label %invoke.cont343 unwind label %lpad37

invoke.cont343:                                   ; preds = %invoke.cont341
  %call346 = invoke nonnull align 8 dereferenceable(16) %"class.std::basic_istream"* @_ZNSirsERi(%"class.std::basic_istream"* nonnull dereferenceable(16) @_ZSt3cin, i32* nonnull align 4 dereferenceable(4) %menu)
          to label %invoke.cont345 unwind label %lpad37

invoke.cont345:                                   ; preds = %invoke.cont343
  %127 = load i32, i32* %menu, align 4
  %cmp347 = icmp eq i32 %127, 1
  br i1 %cmp347, label %if.then348, label %if.else437

if.then348:                                       ; preds = %invoke.cont345
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %temp_349) #3
  invoke void @_ZN8LecturerC2Ev(%class.Lecturer* nonnull dereferenceable(224) %newL)
          to label %invoke.cont351 unwind label %lpad350

invoke.cont351:                                   ; preds = %if.then348
  %call354 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) @_ZSt4cout, i8* getelementptr inbounds ([13 x i8], [13 x i8]* @.str.36, i64 0, i64 0))
          to label %invoke.cont353 unwind label %lpad352

invoke.cont353:                                   ; preds = %invoke.cont351
  %call356 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* nonnull dereferenceable(8) %call354, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
          to label %invoke.cont355 unwind label %lpad352

invoke.cont355:                                   ; preds = %invoke.cont353
  %call358 = invoke nonnull align 8 dereferenceable(16) %"class.std::basic_istream"* @_ZStrsIcSt11char_traitsIcESaIcEERSt13basic_istreamIT_T0_ES7_RNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_istream"* nonnull align 8 dereferenceable(16) @_ZSt3cin, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %temp_349)
          to label %invoke.cont357 unwind label %lpad352

invoke.cont357:                                   ; preds = %invoke.cont355
  %128 = bitcast %class.Lecturer* %newL to %class.People*
  invoke void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1ERKS4_(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp359, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %temp_349)
          to label %invoke.cont360 unwind label %lpad352

invoke.cont360:                                   ; preds = %invoke.cont357
  invoke void @_ZN6People9setNumberENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE(%class.People* nonnull dereferenceable(192) %128, %"class.std::__cxx11::basic_string"* %agg.tmp359)
          to label %invoke.cont362 unwind label %lpad361

invoke.cont362:                                   ; preds = %invoke.cont360
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp359) #3
  %call365 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) @_ZSt4cout, i8* getelementptr inbounds ([15 x i8], [15 x i8]* @.str.37, i64 0, i64 0))
          to label %invoke.cont364 unwind label %lpad352

invoke.cont364:                                   ; preds = %invoke.cont362
  %call367 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* nonnull dereferenceable(8) %call365, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
          to label %invoke.cont366 unwind label %lpad352

invoke.cont366:                                   ; preds = %invoke.cont364
  %call369 = invoke nonnull align 8 dereferenceable(16) %"class.std::basic_istream"* @_ZStrsIcSt11char_traitsIcESaIcEERSt13basic_istreamIT_T0_ES7_RNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_istream"* nonnull align 8 dereferenceable(16) @_ZSt3cin, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %temp_349)
          to label %invoke.cont368 unwind label %lpad352

invoke.cont368:                                   ; preds = %invoke.cont366
  %129 = bitcast %class.Lecturer* %newL to %class.People*
  invoke void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1ERKS4_(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp370, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %temp_349)
          to label %invoke.cont371 unwind label %lpad352

invoke.cont371:                                   ; preds = %invoke.cont368
  invoke void @_ZN6People7setNameENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE(%class.People* nonnull dereferenceable(192) %129, %"class.std::__cxx11::basic_string"* %agg.tmp370)
          to label %invoke.cont373 unwind label %lpad372

invoke.cont373:                                   ; preds = %invoke.cont371
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp370) #3
  %call376 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) @_ZSt4cout, i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str.38, i64 0, i64 0))
          to label %invoke.cont375 unwind label %lpad352

invoke.cont375:                                   ; preds = %invoke.cont373
  %call378 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* nonnull dereferenceable(8) %call376, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
          to label %invoke.cont377 unwind label %lpad352

invoke.cont377:                                   ; preds = %invoke.cont375
  %call380 = invoke nonnull align 8 dereferenceable(16) %"class.std::basic_istream"* @_ZStrsIcSt11char_traitsIcESaIcEERSt13basic_istreamIT_T0_ES7_RNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_istream"* nonnull align 8 dereferenceable(16) @_ZSt3cin, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %temp_349)
          to label %invoke.cont379 unwind label %lpad352

invoke.cont379:                                   ; preds = %invoke.cont377
  %130 = bitcast %class.Lecturer* %newL to %class.People*
  invoke void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1ERKS4_(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp381, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %temp_349)
          to label %invoke.cont382 unwind label %lpad352

invoke.cont382:                                   ; preds = %invoke.cont379
  invoke void @_ZN6People10setSurnameENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE(%class.People* nonnull dereferenceable(192) %130, %"class.std::__cxx11::basic_string"* %agg.tmp381)
          to label %invoke.cont384 unwind label %lpad383

invoke.cont384:                                   ; preds = %invoke.cont382
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp381) #3
  %call387 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) @_ZSt4cout, i8* getelementptr inbounds ([21 x i8], [21 x i8]* @.str.39, i64 0, i64 0))
          to label %invoke.cont386 unwind label %lpad352

invoke.cont386:                                   ; preds = %invoke.cont384
  %call389 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* nonnull dereferenceable(8) %call387, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
          to label %invoke.cont388 unwind label %lpad352

invoke.cont388:                                   ; preds = %invoke.cont386
  %call391 = invoke nonnull align 8 dereferenceable(16) %"class.std::basic_istream"* @_ZStrsIcSt11char_traitsIcESaIcEERSt13basic_istreamIT_T0_ES7_RNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_istream"* nonnull align 8 dereferenceable(16) @_ZSt3cin, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %temp_349)
          to label %invoke.cont390 unwind label %lpad352

invoke.cont390:                                   ; preds = %invoke.cont388
  %131 = bitcast %class.Lecturer* %newL to %class.People*
  invoke void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1ERKS4_(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp392, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %temp_349)
          to label %invoke.cont393 unwind label %lpad352

invoke.cont393:                                   ; preds = %invoke.cont390
  invoke void @_ZN6People13setDepartmentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE(%class.People* nonnull dereferenceable(192) %131, %"class.std::__cxx11::basic_string"* %agg.tmp392)
          to label %invoke.cont395 unwind label %lpad394

invoke.cont395:                                   ; preds = %invoke.cont393
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp392) #3
  %call398 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) @_ZSt4cout, i8* getelementptr inbounds ([17 x i8], [17 x i8]* @.str.40, i64 0, i64 0))
          to label %invoke.cont397 unwind label %lpad352

invoke.cont397:                                   ; preds = %invoke.cont395
  %call400 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* nonnull dereferenceable(8) %call398, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
          to label %invoke.cont399 unwind label %lpad352

invoke.cont399:                                   ; preds = %invoke.cont397
  %call402 = invoke nonnull align 8 dereferenceable(16) %"class.std::basic_istream"* @_ZStrsIcSt11char_traitsIcESaIcEERSt13basic_istreamIT_T0_ES7_RNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_istream"* nonnull align 8 dereferenceable(16) @_ZSt3cin, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %temp_349)
          to label %invoke.cont401 unwind label %lpad352

invoke.cont401:                                   ; preds = %invoke.cont399
  %132 = bitcast %class.Lecturer* %newL to %class.People*
  invoke void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1ERKS4_(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp403, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %temp_349)
          to label %invoke.cont404 unwind label %lpad352

invoke.cont404:                                   ; preds = %invoke.cont401
  invoke void @_ZN6People8setEmailENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE(%class.People* nonnull dereferenceable(192) %132, %"class.std::__cxx11::basic_string"* %agg.tmp403)
          to label %invoke.cont406 unwind label %lpad405

invoke.cont406:                                   ; preds = %invoke.cont404
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp403) #3
  %call409 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) @_ZSt4cout, i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.41, i64 0, i64 0))
          to label %invoke.cont408 unwind label %lpad352

invoke.cont408:                                   ; preds = %invoke.cont406
  %call411 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* nonnull dereferenceable(8) %call409, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
          to label %invoke.cont410 unwind label %lpad352

invoke.cont410:                                   ; preds = %invoke.cont408
  %call413 = invoke nonnull align 8 dereferenceable(16) %"class.std::basic_istream"* @_ZStrsIcSt11char_traitsIcESaIcEERSt13basic_istreamIT_T0_ES7_RNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_istream"* nonnull align 8 dereferenceable(16) @_ZSt3cin, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %temp_349)
          to label %invoke.cont412 unwind label %lpad352

invoke.cont412:                                   ; preds = %invoke.cont410
  %133 = bitcast %class.Lecturer* %newL to %class.People*
  invoke void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1ERKS4_(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp414, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %temp_349)
          to label %invoke.cont415 unwind label %lpad352

invoke.cont415:                                   ; preds = %invoke.cont412
  invoke void @_ZN6People8setPhoneENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE(%class.People* nonnull dereferenceable(192) %133, %"class.std::__cxx11::basic_string"* %agg.tmp414)
          to label %invoke.cont417 unwind label %lpad416

invoke.cont417:                                   ; preds = %invoke.cont415
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp414) #3
  %call420 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) @_ZSt4cout, i8* getelementptr inbounds ([16 x i8], [16 x i8]* @.str.42, i64 0, i64 0))
          to label %invoke.cont419 unwind label %lpad352

invoke.cont419:                                   ; preds = %invoke.cont417
  %call422 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* nonnull dereferenceable(8) %call420, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
          to label %invoke.cont421 unwind label %lpad352

invoke.cont421:                                   ; preds = %invoke.cont419
  %call424 = invoke nonnull align 8 dereferenceable(16) %"class.std::basic_istream"* @_ZStrsIcSt11char_traitsIcESaIcEERSt13basic_istreamIT_T0_ES7_RNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_istream"* nonnull align 8 dereferenceable(16) @_ZSt3cin, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %temp_349)
          to label %invoke.cont423 unwind label %lpad352

invoke.cont423:                                   ; preds = %invoke.cont421
  invoke void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1ERKS4_(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp425, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %temp_349)
          to label %invoke.cont426 unwind label %lpad352

invoke.cont426:                                   ; preds = %invoke.cont423
  invoke void @_ZN8Lecturer8setChairENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE(%class.Lecturer* nonnull dereferenceable(224) %newL, %"class.std::__cxx11::basic_string"* %agg.tmp425)
          to label %invoke.cont428 unwind label %lpad427

invoke.cont428:                                   ; preds = %invoke.cont426
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp425) #3
  %134 = load i32, i32* %y, align 4
  %idxprom430 = sext i32 %134 to i64
  %arrayidx431 = getelementptr inbounds [100 x %class.Lecturer], [100 x %class.Lecturer]* %_L, i64 0, i64 %idxprom430
  %call433 = invoke nonnull align 8 dereferenceable(224) %class.Lecturer* @_ZN8LectureraSERKS_(%class.Lecturer* nonnull dereferenceable(224) %arrayidx431, %class.Lecturer* nonnull align 8 dereferenceable(224) %newL)
          to label %invoke.cont432 unwind label %lpad352

invoke.cont432:                                   ; preds = %invoke.cont428
  %135 = load i32, i32* %y, align 4
  %inc434 = add nsw i32 %135, 1
  store i32 %inc434, i32* %y, align 4
  call void @_ZN8LecturerD2Ev(%class.Lecturer* nonnull dereferenceable(224) %newL) #3
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %temp_349) #3
  br label %if.end600

lpad350:                                          ; preds = %if.then348
  %136 = landingpad { i8*, i32 }
          cleanup
  %137 = extractvalue { i8*, i32 } %136, 0
  store i8* %137, i8** %exn.slot, align 8
  %138 = extractvalue { i8*, i32 } %136, 1
  store i32 %138, i32* %ehselector.slot, align 4
  br label %ehcleanup436

lpad352:                                          ; preds = %invoke.cont428, %invoke.cont423, %invoke.cont421, %invoke.cont419, %invoke.cont417, %invoke.cont412, %invoke.cont410, %invoke.cont408, %invoke.cont406, %invoke.cont401, %invoke.cont399, %invoke.cont397, %invoke.cont395, %invoke.cont390, %invoke.cont388, %invoke.cont386, %invoke.cont384, %invoke.cont379, %invoke.cont377, %invoke.cont375, %invoke.cont373, %invoke.cont368, %invoke.cont366, %invoke.cont364, %invoke.cont362, %invoke.cont357, %invoke.cont355, %invoke.cont353, %invoke.cont351
  %139 = landingpad { i8*, i32 }
          cleanup
  %140 = extractvalue { i8*, i32 } %139, 0
  store i8* %140, i8** %exn.slot, align 8
  %141 = extractvalue { i8*, i32 } %139, 1
  store i32 %141, i32* %ehselector.slot, align 4
  br label %ehcleanup435

lpad361:                                          ; preds = %invoke.cont360
  %142 = landingpad { i8*, i32 }
          cleanup
  %143 = extractvalue { i8*, i32 } %142, 0
  store i8* %143, i8** %exn.slot, align 8
  %144 = extractvalue { i8*, i32 } %142, 1
  store i32 %144, i32* %ehselector.slot, align 4
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp359) #3
  br label %ehcleanup435

lpad372:                                          ; preds = %invoke.cont371
  %145 = landingpad { i8*, i32 }
          cleanup
  %146 = extractvalue { i8*, i32 } %145, 0
  store i8* %146, i8** %exn.slot, align 8
  %147 = extractvalue { i8*, i32 } %145, 1
  store i32 %147, i32* %ehselector.slot, align 4
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp370) #3
  br label %ehcleanup435

lpad383:                                          ; preds = %invoke.cont382
  %148 = landingpad { i8*, i32 }
          cleanup
  %149 = extractvalue { i8*, i32 } %148, 0
  store i8* %149, i8** %exn.slot, align 8
  %150 = extractvalue { i8*, i32 } %148, 1
  store i32 %150, i32* %ehselector.slot, align 4
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp381) #3
  br label %ehcleanup435

lpad394:                                          ; preds = %invoke.cont393
  %151 = landingpad { i8*, i32 }
          cleanup
  %152 = extractvalue { i8*, i32 } %151, 0
  store i8* %152, i8** %exn.slot, align 8
  %153 = extractvalue { i8*, i32 } %151, 1
  store i32 %153, i32* %ehselector.slot, align 4
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp392) #3
  br label %ehcleanup435

lpad405:                                          ; preds = %invoke.cont404
  %154 = landingpad { i8*, i32 }
          cleanup
  %155 = extractvalue { i8*, i32 } %154, 0
  store i8* %155, i8** %exn.slot, align 8
  %156 = extractvalue { i8*, i32 } %154, 1
  store i32 %156, i32* %ehselector.slot, align 4
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp403) #3
  br label %ehcleanup435

lpad416:                                          ; preds = %invoke.cont415
  %157 = landingpad { i8*, i32 }
          cleanup
  %158 = extractvalue { i8*, i32 } %157, 0
  store i8* %158, i8** %exn.slot, align 8
  %159 = extractvalue { i8*, i32 } %157, 1
  store i32 %159, i32* %ehselector.slot, align 4
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp414) #3
  br label %ehcleanup435

lpad427:                                          ; preds = %invoke.cont426
  %160 = landingpad { i8*, i32 }
          cleanup
  %161 = extractvalue { i8*, i32 } %160, 0
  store i8* %161, i8** %exn.slot, align 8
  %162 = extractvalue { i8*, i32 } %160, 1
  store i32 %162, i32* %ehselector.slot, align 4
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp425) #3
  br label %ehcleanup435

ehcleanup435:                                     ; preds = %lpad427, %lpad416, %lpad405, %lpad394, %lpad383, %lpad372, %lpad361, %lpad352
  call void @_ZN8LecturerD2Ev(%class.Lecturer* nonnull dereferenceable(224) %newL) #3
  br label %ehcleanup436

ehcleanup436:                                     ; preds = %ehcleanup435, %lpad350
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %temp_349) #3
  br label %ehcleanup1099

if.else437:                                       ; preds = %invoke.cont345
  %163 = load i32, i32* %menu, align 4
  %cmp438 = icmp eq i32 %163, 2
  br i1 %cmp438, label %if.then439, label %if.else451

if.then439:                                       ; preds = %if.else437
  %call441 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* nonnull dereferenceable(8) @_ZSt4cout, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
          to label %invoke.cont440 unwind label %lpad37

invoke.cont440:                                   ; preds = %if.then439
  store i32 0, i32* %i, align 4
  br label %for.cond442

for.cond442:                                      ; preds = %for.inc448, %invoke.cont440
  %164 = load i32, i32* %i, align 4
  %165 = load i32, i32* %y, align 4
  %cmp443 = icmp slt i32 %164, %165
  br i1 %cmp443, label %for.body444, label %for.end450

for.body444:                                      ; preds = %for.cond442
  %166 = load i32, i32* %i, align 4
  %idxprom445 = sext i32 %166 to i64
  %arrayidx446 = getelementptr inbounds [100 x %class.Lecturer], [100 x %class.Lecturer]* %_L, i64 0, i64 %idxprom445
  invoke void @_ZN8Lecturer7displayEv(%class.Lecturer* nonnull dereferenceable(224) %arrayidx446)
          to label %invoke.cont447 unwind label %lpad37

invoke.cont447:                                   ; preds = %for.body444
  br label %for.inc448

for.inc448:                                       ; preds = %invoke.cont447
  %167 = load i32, i32* %i, align 4
  %inc449 = add nsw i32 %167, 1
  store i32 %inc449, i32* %i, align 4
  br label %for.cond442, !llvm.loop !14

for.end450:                                       ; preds = %for.cond442
  br label %if.end599

if.else451:                                       ; preds = %if.else437
  %168 = load i32, i32* %menu, align 4
  %cmp452 = icmp eq i32 %168, 3
  br i1 %cmp452, label %if.then453, label %if.else489

if.then453:                                       ; preds = %if.else451
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %temp_454) #3
  %call458 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) @_ZSt4cout, i8* getelementptr inbounds ([33 x i8], [33 x i8]* @.str.43, i64 0, i64 0))
          to label %invoke.cont457 unwind label %lpad456

invoke.cont457:                                   ; preds = %if.then453
  %call460 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* nonnull dereferenceable(8) %call458, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
          to label %invoke.cont459 unwind label %lpad456

invoke.cont459:                                   ; preds = %invoke.cont457
  %call462 = invoke nonnull align 8 dereferenceable(16) %"class.std::basic_istream"* @_ZStrsIcSt11char_traitsIcESaIcEERSt13basic_istreamIT_T0_ES7_RNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_istream"* nonnull align 8 dereferenceable(16) @_ZSt3cin, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %temp_454)
          to label %invoke.cont461 unwind label %lpad456

invoke.cont461:                                   ; preds = %invoke.cont459
  store i32 0, i32* %i, align 4
  br label %while.cond463

while.cond463:                                    ; preds = %if.end472, %invoke.cont461
  %169 = load i32, i32* %i, align 4
  %170 = load i32, i32* %y, align 4
  %cmp464 = icmp slt i32 %169, %170
  br i1 %cmp464, label %while.body465, label %while.end474

while.body465:                                    ; preds = %while.cond463
  %171 = load i32, i32* %i, align 4
  %idxprom467 = sext i32 %171 to i64
  %arrayidx468 = getelementptr inbounds [100 x %class.Lecturer], [100 x %class.Lecturer]* %_L, i64 0, i64 %idxprom467
  %172 = bitcast %class.Lecturer* %arrayidx468 to %class.People*
  invoke void @_ZN6People9getNumberB5cxx11Ev(%"class.std::__cxx11::basic_string"* sret(%"class.std::__cxx11::basic_string") align 8 %ref.tmp466, %class.People* nonnull dereferenceable(192) %172)
          to label %invoke.cont469 unwind label %lpad456

invoke.cont469:                                   ; preds = %while.body465
  %call470 = call zeroext i1 @_ZSteqIcEN9__gnu_cxx11__enable_ifIXsr9__is_charIT_EE7__valueEbE6__typeERKNSt7__cxx1112basic_stringIS2_St11char_traitsIS2_ESaIS2_EEESC_(%"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %ref.tmp466, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %temp_454) #3
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %ref.tmp466) #3
  br i1 %call470, label %if.then471, label %if.end472

if.then471:                                       ; preds = %invoke.cont469
  %173 = load i32, i32* %i, align 4
  store i32 %173, i32* %deleted455, align 4
  br label %if.end472

lpad456:                                          ; preds = %for.body477, %while.body465, %invoke.cont459, %invoke.cont457, %if.then453
  %174 = landingpad { i8*, i32 }
          cleanup
  %175 = extractvalue { i8*, i32 } %174, 0
  store i8* %175, i8** %exn.slot, align 8
  %176 = extractvalue { i8*, i32 } %174, 1
  store i32 %176, i32* %ehselector.slot, align 4
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %temp_454) #3
  br label %ehcleanup1099

if.end472:                                        ; preds = %if.then471, %invoke.cont469
  %177 = load i32, i32* %i, align 4
  %inc473 = add nsw i32 %177, 1
  store i32 %inc473, i32* %i, align 4
  br label %while.cond463, !llvm.loop !15

while.end474:                                     ; preds = %while.cond463
  %178 = load i32, i32* %deleted455, align 4
  store i32 %178, i32* %i, align 4
  br label %for.cond475

for.cond475:                                      ; preds = %for.inc485, %while.end474
  %179 = load i32, i32* %i, align 4
  %180 = load i32, i32* %y, align 4
  %cmp476 = icmp slt i32 %179, %180
  br i1 %cmp476, label %for.body477, label %for.end487

for.body477:                                      ; preds = %for.cond475
  %181 = load i32, i32* %i, align 4
  %add478 = add nsw i32 %181, 1
  %idxprom479 = sext i32 %add478 to i64
  %arrayidx480 = getelementptr inbounds [100 x %class.Lecturer], [100 x %class.Lecturer]* %_L, i64 0, i64 %idxprom479
  %182 = load i32, i32* %i, align 4
  %idxprom481 = sext i32 %182 to i64
  %arrayidx482 = getelementptr inbounds [100 x %class.Lecturer], [100 x %class.Lecturer]* %_L, i64 0, i64 %idxprom481
  %call484 = invoke nonnull align 8 dereferenceable(224) %class.Lecturer* @_ZN8LectureraSERKS_(%class.Lecturer* nonnull dereferenceable(224) %arrayidx482, %class.Lecturer* nonnull align 8 dereferenceable(224) %arrayidx480)
          to label %invoke.cont483 unwind label %lpad456

invoke.cont483:                                   ; preds = %for.body477
  br label %for.inc485

for.inc485:                                       ; preds = %invoke.cont483
  %183 = load i32, i32* %i, align 4
  %inc486 = add nsw i32 %183, 1
  store i32 %inc486, i32* %i, align 4
  br label %for.cond475, !llvm.loop !16

for.end487:                                       ; preds = %for.cond475
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %temp_454) #3
  br label %if.end598

if.else489:                                       ; preds = %if.else451
  %184 = load i32, i32* %menu, align 4
  %cmp490 = icmp eq i32 %184, 4
  br i1 %cmp490, label %if.then491, label %if.else592

if.then491:                                       ; preds = %if.else489
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %temp_492) #3
  %call496 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) @_ZSt4cout, i8* getelementptr inbounds ([33 x i8], [33 x i8]* @.str.44, i64 0, i64 0))
          to label %invoke.cont495 unwind label %lpad494

invoke.cont495:                                   ; preds = %if.then491
  %call498 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* nonnull dereferenceable(8) %call496, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
          to label %invoke.cont497 unwind label %lpad494

invoke.cont497:                                   ; preds = %invoke.cont495
  %call500 = invoke nonnull align 8 dereferenceable(16) %"class.std::basic_istream"* @_ZStrsIcSt11char_traitsIcESaIcEERSt13basic_istreamIT_T0_ES7_RNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_istream"* nonnull align 8 dereferenceable(16) @_ZSt3cin, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %temp_492)
          to label %invoke.cont499 unwind label %lpad494

invoke.cont499:                                   ; preds = %invoke.cont497
  store i32 0, i32* %i, align 4
  br label %while.cond501

while.cond501:                                    ; preds = %if.end510, %invoke.cont499
  %185 = load i32, i32* %i, align 4
  %186 = load i32, i32* %y, align 4
  %cmp502 = icmp slt i32 %185, %186
  br i1 %cmp502, label %while.body503, label %while.end512

while.body503:                                    ; preds = %while.cond501
  %187 = load i32, i32* %i, align 4
  %idxprom505 = sext i32 %187 to i64
  %arrayidx506 = getelementptr inbounds [100 x %class.Lecturer], [100 x %class.Lecturer]* %_L, i64 0, i64 %idxprom505
  %188 = bitcast %class.Lecturer* %arrayidx506 to %class.People*
  invoke void @_ZN6People9getNumberB5cxx11Ev(%"class.std::__cxx11::basic_string"* sret(%"class.std::__cxx11::basic_string") align 8 %ref.tmp504, %class.People* nonnull dereferenceable(192) %188)
          to label %invoke.cont507 unwind label %lpad494

invoke.cont507:                                   ; preds = %while.body503
  %call508 = call zeroext i1 @_ZSteqIcEN9__gnu_cxx11__enable_ifIXsr9__is_charIT_EE7__valueEbE6__typeERKNSt7__cxx1112basic_stringIS2_St11char_traitsIS2_ESaIS2_EEESC_(%"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %ref.tmp504, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %temp_492) #3
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %ref.tmp504) #3
  br i1 %call508, label %if.then509, label %if.end510

if.then509:                                       ; preds = %invoke.cont507
  %189 = load i32, i32* %i, align 4
  store i32 %189, i32* %updated493, align 4
  br label %if.end510

lpad494:                                          ; preds = %invoke.cont582, %invoke.cont580, %invoke.cont578, %invoke.cont576, %invoke.cont569, %invoke.cont567, %invoke.cont565, %invoke.cont563, %invoke.cont556, %invoke.cont554, %invoke.cont552, %invoke.cont550, %invoke.cont543, %invoke.cont541, %invoke.cont539, %invoke.cont537, %invoke.cont530, %invoke.cont528, %invoke.cont526, %invoke.cont524, %invoke.cont517, %invoke.cont515, %invoke.cont513, %while.end512, %while.body503, %invoke.cont497, %invoke.cont495, %if.then491
  %190 = landingpad { i8*, i32 }
          cleanup
  %191 = extractvalue { i8*, i32 } %190, 0
  store i8* %191, i8** %exn.slot, align 8
  %192 = extractvalue { i8*, i32 } %190, 1
  store i32 %192, i32* %ehselector.slot, align 4
  br label %ehcleanup591

if.end510:                                        ; preds = %if.then509, %invoke.cont507
  %193 = load i32, i32* %i, align 4
  %inc511 = add nsw i32 %193, 1
  store i32 %inc511, i32* %i, align 4
  br label %while.cond501, !llvm.loop !17

while.end512:                                     ; preds = %while.cond501
  %call514 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) @_ZSt4cout, i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.45, i64 0, i64 0))
          to label %invoke.cont513 unwind label %lpad494

invoke.cont513:                                   ; preds = %while.end512
  %call516 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* nonnull dereferenceable(8) %call514, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
          to label %invoke.cont515 unwind label %lpad494

invoke.cont515:                                   ; preds = %invoke.cont513
  %call518 = invoke nonnull align 8 dereferenceable(16) %"class.std::basic_istream"* @_ZStrsIcSt11char_traitsIcESaIcEERSt13basic_istreamIT_T0_ES7_RNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_istream"* nonnull align 8 dereferenceable(16) @_ZSt3cin, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %temp_492)
          to label %invoke.cont517 unwind label %lpad494

invoke.cont517:                                   ; preds = %invoke.cont515
  %194 = load i32, i32* %updated493, align 4
  %idxprom519 = sext i32 %194 to i64
  %arrayidx520 = getelementptr inbounds [100 x %class.Lecturer], [100 x %class.Lecturer]* %_L, i64 0, i64 %idxprom519
  %195 = bitcast %class.Lecturer* %arrayidx520 to %class.People*
  invoke void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1ERKS4_(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp521, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %temp_492)
          to label %invoke.cont522 unwind label %lpad494

invoke.cont522:                                   ; preds = %invoke.cont517
  invoke void @_ZN6People7setNameENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE(%class.People* nonnull dereferenceable(192) %195, %"class.std::__cxx11::basic_string"* %agg.tmp521)
          to label %invoke.cont524 unwind label %lpad523

invoke.cont524:                                   ; preds = %invoke.cont522
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp521) #3
  %call527 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) @_ZSt4cout, i8* getelementptr inbounds ([28 x i8], [28 x i8]* @.str.46, i64 0, i64 0))
          to label %invoke.cont526 unwind label %lpad494

invoke.cont526:                                   ; preds = %invoke.cont524
  %call529 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* nonnull dereferenceable(8) %call527, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
          to label %invoke.cont528 unwind label %lpad494

invoke.cont528:                                   ; preds = %invoke.cont526
  %call531 = invoke nonnull align 8 dereferenceable(16) %"class.std::basic_istream"* @_ZStrsIcSt11char_traitsIcESaIcEERSt13basic_istreamIT_T0_ES7_RNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_istream"* nonnull align 8 dereferenceable(16) @_ZSt3cin, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %temp_492)
          to label %invoke.cont530 unwind label %lpad494

invoke.cont530:                                   ; preds = %invoke.cont528
  %196 = load i32, i32* %updated493, align 4
  %idxprom532 = sext i32 %196 to i64
  %arrayidx533 = getelementptr inbounds [100 x %class.Lecturer], [100 x %class.Lecturer]* %_L, i64 0, i64 %idxprom532
  %197 = bitcast %class.Lecturer* %arrayidx533 to %class.People*
  invoke void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1ERKS4_(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp534, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %temp_492)
          to label %invoke.cont535 unwind label %lpad494

invoke.cont535:                                   ; preds = %invoke.cont530
  invoke void @_ZN6People10setSurnameENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE(%class.People* nonnull dereferenceable(192) %197, %"class.std::__cxx11::basic_string"* %agg.tmp534)
          to label %invoke.cont537 unwind label %lpad536

invoke.cont537:                                   ; preds = %invoke.cont535
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp534) #3
  %call540 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) @_ZSt4cout, i8* getelementptr inbounds ([29 x i8], [29 x i8]* @.str.47, i64 0, i64 0))
          to label %invoke.cont539 unwind label %lpad494

invoke.cont539:                                   ; preds = %invoke.cont537
  %call542 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* nonnull dereferenceable(8) %call540, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
          to label %invoke.cont541 unwind label %lpad494

invoke.cont541:                                   ; preds = %invoke.cont539
  %call544 = invoke nonnull align 8 dereferenceable(16) %"class.std::basic_istream"* @_ZStrsIcSt11char_traitsIcESaIcEERSt13basic_istreamIT_T0_ES7_RNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_istream"* nonnull align 8 dereferenceable(16) @_ZSt3cin, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %temp_492)
          to label %invoke.cont543 unwind label %lpad494

invoke.cont543:                                   ; preds = %invoke.cont541
  %198 = load i32, i32* %updated493, align 4
  %idxprom545 = sext i32 %198 to i64
  %arrayidx546 = getelementptr inbounds [100 x %class.Lecturer], [100 x %class.Lecturer]* %_L, i64 0, i64 %idxprom545
  %199 = bitcast %class.Lecturer* %arrayidx546 to %class.People*
  invoke void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1ERKS4_(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp547, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %temp_492)
          to label %invoke.cont548 unwind label %lpad494

invoke.cont548:                                   ; preds = %invoke.cont543
  invoke void @_ZN6People13setDepartmentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE(%class.People* nonnull dereferenceable(192) %199, %"class.std::__cxx11::basic_string"* %agg.tmp547)
          to label %invoke.cont550 unwind label %lpad549

invoke.cont550:                                   ; preds = %invoke.cont548
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp547) #3
  %call553 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) @_ZSt4cout, i8* getelementptr inbounds ([25 x i8], [25 x i8]* @.str.48, i64 0, i64 0))
          to label %invoke.cont552 unwind label %lpad494

invoke.cont552:                                   ; preds = %invoke.cont550
  %call555 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* nonnull dereferenceable(8) %call553, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
          to label %invoke.cont554 unwind label %lpad494

invoke.cont554:                                   ; preds = %invoke.cont552
  %call557 = invoke nonnull align 8 dereferenceable(16) %"class.std::basic_istream"* @_ZStrsIcSt11char_traitsIcESaIcEERSt13basic_istreamIT_T0_ES7_RNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_istream"* nonnull align 8 dereferenceable(16) @_ZSt3cin, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %temp_492)
          to label %invoke.cont556 unwind label %lpad494

invoke.cont556:                                   ; preds = %invoke.cont554
  %200 = load i32, i32* %updated493, align 4
  %idxprom558 = sext i32 %200 to i64
  %arrayidx559 = getelementptr inbounds [100 x %class.Lecturer], [100 x %class.Lecturer]* %_L, i64 0, i64 %idxprom558
  %201 = bitcast %class.Lecturer* %arrayidx559 to %class.People*
  invoke void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1ERKS4_(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp560, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %temp_492)
          to label %invoke.cont561 unwind label %lpad494

invoke.cont561:                                   ; preds = %invoke.cont556
  invoke void @_ZN6People8setEmailENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE(%class.People* nonnull dereferenceable(192) %201, %"class.std::__cxx11::basic_string"* %agg.tmp560)
          to label %invoke.cont563 unwind label %lpad562

invoke.cont563:                                   ; preds = %invoke.cont561
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp560) #3
  %call566 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) @_ZSt4cout, i8* getelementptr inbounds ([31 x i8], [31 x i8]* @.str.49, i64 0, i64 0))
          to label %invoke.cont565 unwind label %lpad494

invoke.cont565:                                   ; preds = %invoke.cont563
  %call568 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* nonnull dereferenceable(8) %call566, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
          to label %invoke.cont567 unwind label %lpad494

invoke.cont567:                                   ; preds = %invoke.cont565
  %call570 = invoke nonnull align 8 dereferenceable(16) %"class.std::basic_istream"* @_ZStrsIcSt11char_traitsIcESaIcEERSt13basic_istreamIT_T0_ES7_RNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_istream"* nonnull align 8 dereferenceable(16) @_ZSt3cin, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %temp_492)
          to label %invoke.cont569 unwind label %lpad494

invoke.cont569:                                   ; preds = %invoke.cont567
  %202 = load i32, i32* %updated493, align 4
  %idxprom571 = sext i32 %202 to i64
  %arrayidx572 = getelementptr inbounds [100 x %class.Lecturer], [100 x %class.Lecturer]* %_L, i64 0, i64 %idxprom571
  %203 = bitcast %class.Lecturer* %arrayidx572 to %class.People*
  invoke void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1ERKS4_(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp573, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %temp_492)
          to label %invoke.cont574 unwind label %lpad494

invoke.cont574:                                   ; preds = %invoke.cont569
  invoke void @_ZN6People8setPhoneENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE(%class.People* nonnull dereferenceable(192) %203, %"class.std::__cxx11::basic_string"* %agg.tmp573)
          to label %invoke.cont576 unwind label %lpad575

invoke.cont576:                                   ; preds = %invoke.cont574
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp573) #3
  %call579 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) @_ZSt4cout, i8* getelementptr inbounds ([24 x i8], [24 x i8]* @.str.50, i64 0, i64 0))
          to label %invoke.cont578 unwind label %lpad494

invoke.cont578:                                   ; preds = %invoke.cont576
  %call581 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* nonnull dereferenceable(8) %call579, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
          to label %invoke.cont580 unwind label %lpad494

invoke.cont580:                                   ; preds = %invoke.cont578
  %call583 = invoke nonnull align 8 dereferenceable(16) %"class.std::basic_istream"* @_ZStrsIcSt11char_traitsIcESaIcEERSt13basic_istreamIT_T0_ES7_RNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_istream"* nonnull align 8 dereferenceable(16) @_ZSt3cin, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %temp_492)
          to label %invoke.cont582 unwind label %lpad494

invoke.cont582:                                   ; preds = %invoke.cont580
  %204 = load i32, i32* %updated493, align 4
  %idxprom584 = sext i32 %204 to i64
  %arrayidx585 = getelementptr inbounds [100 x %class.Lecturer], [100 x %class.Lecturer]* %_L, i64 0, i64 %idxprom584
  invoke void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1ERKS4_(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp586, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %temp_492)
          to label %invoke.cont587 unwind label %lpad494

invoke.cont587:                                   ; preds = %invoke.cont582
  invoke void @_ZN8Lecturer8setChairENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE(%class.Lecturer* nonnull dereferenceable(224) %arrayidx585, %"class.std::__cxx11::basic_string"* %agg.tmp586)
          to label %invoke.cont589 unwind label %lpad588

invoke.cont589:                                   ; preds = %invoke.cont587
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp586) #3
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %temp_492) #3
  br label %if.end597

lpad523:                                          ; preds = %invoke.cont522
  %205 = landingpad { i8*, i32 }
          cleanup
  %206 = extractvalue { i8*, i32 } %205, 0
  store i8* %206, i8** %exn.slot, align 8
  %207 = extractvalue { i8*, i32 } %205, 1
  store i32 %207, i32* %ehselector.slot, align 4
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp521) #3
  br label %ehcleanup591

lpad536:                                          ; preds = %invoke.cont535
  %208 = landingpad { i8*, i32 }
          cleanup
  %209 = extractvalue { i8*, i32 } %208, 0
  store i8* %209, i8** %exn.slot, align 8
  %210 = extractvalue { i8*, i32 } %208, 1
  store i32 %210, i32* %ehselector.slot, align 4
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp534) #3
  br label %ehcleanup591

lpad549:                                          ; preds = %invoke.cont548
  %211 = landingpad { i8*, i32 }
          cleanup
  %212 = extractvalue { i8*, i32 } %211, 0
  store i8* %212, i8** %exn.slot, align 8
  %213 = extractvalue { i8*, i32 } %211, 1
  store i32 %213, i32* %ehselector.slot, align 4
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp547) #3
  br label %ehcleanup591

lpad562:                                          ; preds = %invoke.cont561
  %214 = landingpad { i8*, i32 }
          cleanup
  %215 = extractvalue { i8*, i32 } %214, 0
  store i8* %215, i8** %exn.slot, align 8
  %216 = extractvalue { i8*, i32 } %214, 1
  store i32 %216, i32* %ehselector.slot, align 4
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp560) #3
  br label %ehcleanup591

lpad575:                                          ; preds = %invoke.cont574
  %217 = landingpad { i8*, i32 }
          cleanup
  %218 = extractvalue { i8*, i32 } %217, 0
  store i8* %218, i8** %exn.slot, align 8
  %219 = extractvalue { i8*, i32 } %217, 1
  store i32 %219, i32* %ehselector.slot, align 4
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp573) #3
  br label %ehcleanup591

lpad588:                                          ; preds = %invoke.cont587
  %220 = landingpad { i8*, i32 }
          cleanup
  %221 = extractvalue { i8*, i32 } %220, 0
  store i8* %221, i8** %exn.slot, align 8
  %222 = extractvalue { i8*, i32 } %220, 1
  store i32 %222, i32* %ehselector.slot, align 4
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp586) #3
  br label %ehcleanup591

ehcleanup591:                                     ; preds = %lpad588, %lpad575, %lpad562, %lpad549, %lpad536, %lpad523, %lpad494
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %temp_492) #3
  br label %ehcleanup1099

if.else592:                                       ; preds = %if.else489
  %call594 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) @_ZSt4cout, i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.31, i64 0, i64 0))
          to label %invoke.cont593 unwind label %lpad37

invoke.cont593:                                   ; preds = %if.else592
  %call596 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* nonnull dereferenceable(8) %call594, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
          to label %invoke.cont595 unwind label %lpad37

invoke.cont595:                                   ; preds = %invoke.cont593
  br label %if.end597

if.end597:                                        ; preds = %invoke.cont595, %invoke.cont589
  br label %if.end598

if.end598:                                        ; preds = %if.end597, %for.end487
  br label %if.end599

if.end599:                                        ; preds = %if.end598, %for.end450
  br label %if.end600

if.end600:                                        ; preds = %if.end599, %invoke.cont432
  br label %if.end1090

if.else601:                                       ; preds = %if.else326
  %223 = load i32, i32* %menuLoop, align 4
  %cmp602 = icmp eq i32 %223, 3
  br i1 %cmp602, label %if.then603, label %if.else854

if.then603:                                       ; preds = %if.else601
  %call605 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) @_ZSt4cout, i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str.51, i64 0, i64 0))
          to label %invoke.cont604 unwind label %lpad37

invoke.cont604:                                   ; preds = %if.then603
  %call607 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* nonnull dereferenceable(8) %call605, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
          to label %invoke.cont606 unwind label %lpad37

invoke.cont606:                                   ; preds = %invoke.cont604
  %call609 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) @_ZSt4cout, i8* getelementptr inbounds ([22 x i8], [22 x i8]* @.str.52, i64 0, i64 0))
          to label %invoke.cont608 unwind label %lpad37

invoke.cont608:                                   ; preds = %invoke.cont606
  %call611 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* nonnull dereferenceable(8) %call609, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
          to label %invoke.cont610 unwind label %lpad37

invoke.cont610:                                   ; preds = %invoke.cont608
  %call613 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) @_ZSt4cout, i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.53, i64 0, i64 0))
          to label %invoke.cont612 unwind label %lpad37

invoke.cont612:                                   ; preds = %invoke.cont610
  %call615 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* nonnull dereferenceable(8) %call613, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
          to label %invoke.cont614 unwind label %lpad37

invoke.cont614:                                   ; preds = %invoke.cont612
  %call617 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) @_ZSt4cout, i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.54, i64 0, i64 0))
          to label %invoke.cont616 unwind label %lpad37

invoke.cont616:                                   ; preds = %invoke.cont614
  %call619 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* nonnull dereferenceable(8) %call617, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
          to label %invoke.cont618 unwind label %lpad37

invoke.cont618:                                   ; preds = %invoke.cont616
  %call621 = invoke nonnull align 8 dereferenceable(16) %"class.std::basic_istream"* @_ZNSirsERi(%"class.std::basic_istream"* nonnull dereferenceable(16) @_ZSt3cin, i32* nonnull align 4 dereferenceable(4) %menu)
          to label %invoke.cont620 unwind label %lpad37

invoke.cont620:                                   ; preds = %invoke.cont618
  %224 = load i32, i32* %menu, align 4
  %cmp622 = icmp eq i32 %224, 1
  br i1 %cmp622, label %if.then623, label %if.else690

if.then623:                                       ; preds = %invoke.cont620
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %temp_624) #3
  invoke void @_ZN11AppointmentC2Ev(%class.Appointment* nonnull dereferenceable(544) %newA)
          to label %invoke.cont626 unwind label %lpad625

invoke.cont626:                                   ; preds = %if.then623
  %call629 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) @_ZSt4cout, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.16, i64 0, i64 0))
          to label %invoke.cont628 unwind label %lpad627

invoke.cont628:                                   ; preds = %invoke.cont626
  %call631 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* nonnull dereferenceable(8) %call629, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
          to label %invoke.cont630 unwind label %lpad627

invoke.cont630:                                   ; preds = %invoke.cont628
  %call633 = invoke nonnull align 8 dereferenceable(16) %"class.std::basic_istream"* @_ZStrsIcSt11char_traitsIcESaIcEERSt13basic_istreamIT_T0_ES7_RNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_istream"* nonnull align 8 dereferenceable(16) @_ZSt3cin, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %temp_624)
          to label %invoke.cont632 unwind label %lpad627

invoke.cont632:                                   ; preds = %invoke.cont630
  %S = getelementptr inbounds %class.Appointment, %class.Appointment* %newA, i32 0, i32 4
  %225 = bitcast %class.Student* %S to %class.People*
  invoke void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1ERKS4_(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp634, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %temp_624)
          to label %invoke.cont635 unwind label %lpad627

invoke.cont635:                                   ; preds = %invoke.cont632
  invoke void @_ZN6People9setNumberENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE(%class.People* nonnull dereferenceable(192) %225, %"class.std::__cxx11::basic_string"* %agg.tmp634)
          to label %invoke.cont637 unwind label %lpad636

invoke.cont637:                                   ; preds = %invoke.cont635
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp634) #3
  %call640 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) @_ZSt4cout, i8* getelementptr inbounds ([13 x i8], [13 x i8]* @.str.36, i64 0, i64 0))
          to label %invoke.cont639 unwind label %lpad627

invoke.cont639:                                   ; preds = %invoke.cont637
  %call642 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* nonnull dereferenceable(8) %call640, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
          to label %invoke.cont641 unwind label %lpad627

invoke.cont641:                                   ; preds = %invoke.cont639
  %call644 = invoke nonnull align 8 dereferenceable(16) %"class.std::basic_istream"* @_ZStrsIcSt11char_traitsIcESaIcEERSt13basic_istreamIT_T0_ES7_RNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_istream"* nonnull align 8 dereferenceable(16) @_ZSt3cin, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %temp_624)
          to label %invoke.cont643 unwind label %lpad627

invoke.cont643:                                   ; preds = %invoke.cont641
  %L = getelementptr inbounds %class.Appointment, %class.Appointment* %newA, i32 0, i32 3
  %226 = bitcast %class.Lecturer* %L to %class.People*
  invoke void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1ERKS4_(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp645, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %temp_624)
          to label %invoke.cont646 unwind label %lpad627

invoke.cont646:                                   ; preds = %invoke.cont643
  invoke void @_ZN6People9setNumberENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE(%class.People* nonnull dereferenceable(192) %226, %"class.std::__cxx11::basic_string"* %agg.tmp645)
          to label %invoke.cont648 unwind label %lpad647

invoke.cont648:                                   ; preds = %invoke.cont646
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp645) #3
  %call651 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) @_ZSt4cout, i8* getelementptr inbounds ([18 x i8], [18 x i8]* @.str.55, i64 0, i64 0))
          to label %invoke.cont650 unwind label %lpad627

invoke.cont650:                                   ; preds = %invoke.cont648
  %call653 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* nonnull dereferenceable(8) %call651, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
          to label %invoke.cont652 unwind label %lpad627

invoke.cont652:                                   ; preds = %invoke.cont650
  %call655 = invoke nonnull align 8 dereferenceable(16) %"class.std::basic_istream"* @_ZStrsIcSt11char_traitsIcESaIcEERSt13basic_istreamIT_T0_ES7_RNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_istream"* nonnull align 8 dereferenceable(16) @_ZSt3cin, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %temp_624)
          to label %invoke.cont654 unwind label %lpad627

invoke.cont654:                                   ; preds = %invoke.cont652
  invoke void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1ERKS4_(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp656, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %temp_624)
          to label %invoke.cont657 unwind label %lpad627

invoke.cont657:                                   ; preds = %invoke.cont654
  invoke void @_ZN11Appointment7setDateENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE(%class.Appointment* nonnull dereferenceable(544) %newA, %"class.std::__cxx11::basic_string"* %agg.tmp656)
          to label %invoke.cont659 unwind label %lpad658

invoke.cont659:                                   ; preds = %invoke.cont657
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp656) #3
  %call662 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) @_ZSt4cout, i8* getelementptr inbounds ([15 x i8], [15 x i8]* @.str.56, i64 0, i64 0))
          to label %invoke.cont661 unwind label %lpad627

invoke.cont661:                                   ; preds = %invoke.cont659
  %call664 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* nonnull dereferenceable(8) %call662, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
          to label %invoke.cont663 unwind label %lpad627

invoke.cont663:                                   ; preds = %invoke.cont661
  %call666 = invoke nonnull align 8 dereferenceable(16) %"class.std::basic_istream"* @_ZStrsIcSt11char_traitsIcESaIcEERSt13basic_istreamIT_T0_ES7_RNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_istream"* nonnull align 8 dereferenceable(16) @_ZSt3cin, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %temp_624)
          to label %invoke.cont665 unwind label %lpad627

invoke.cont665:                                   ; preds = %invoke.cont663
  invoke void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1ERKS4_(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp667, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %temp_624)
          to label %invoke.cont668 unwind label %lpad627

invoke.cont668:                                   ; preds = %invoke.cont665
  invoke void @_ZN11Appointment8setStartENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE(%class.Appointment* nonnull dereferenceable(544) %newA, %"class.std::__cxx11::basic_string"* %agg.tmp667)
          to label %invoke.cont670 unwind label %lpad669

invoke.cont670:                                   ; preds = %invoke.cont668
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp667) #3
  %call673 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) @_ZSt4cout, i8* getelementptr inbounds ([13 x i8], [13 x i8]* @.str.57, i64 0, i64 0))
          to label %invoke.cont672 unwind label %lpad627

invoke.cont672:                                   ; preds = %invoke.cont670
  %call675 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* nonnull dereferenceable(8) %call673, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
          to label %invoke.cont674 unwind label %lpad627

invoke.cont674:                                   ; preds = %invoke.cont672
  %call677 = invoke nonnull align 8 dereferenceable(16) %"class.std::basic_istream"* @_ZStrsIcSt11char_traitsIcESaIcEERSt13basic_istreamIT_T0_ES7_RNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_istream"* nonnull align 8 dereferenceable(16) @_ZSt3cin, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %temp_624)
          to label %invoke.cont676 unwind label %lpad627

invoke.cont676:                                   ; preds = %invoke.cont674
  invoke void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1ERKS4_(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp678, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %temp_624)
          to label %invoke.cont679 unwind label %lpad627

invoke.cont679:                                   ; preds = %invoke.cont676
  invoke void @_ZN11Appointment6setEndENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE(%class.Appointment* nonnull dereferenceable(544) %newA, %"class.std::__cxx11::basic_string"* %agg.tmp678)
          to label %invoke.cont681 unwind label %lpad680

invoke.cont681:                                   ; preds = %invoke.cont679
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp678) #3
  %227 = load i32, i32* %z, align 4
  %idxprom683 = sext i32 %227 to i64
  %arrayidx684 = getelementptr inbounds [100 x %class.Appointment], [100 x %class.Appointment]* %_A, i64 0, i64 %idxprom683
  %call686 = invoke nonnull align 8 dereferenceable(544) %class.Appointment* @_ZN11AppointmentaSERKS_(%class.Appointment* nonnull dereferenceable(544) %arrayidx684, %class.Appointment* nonnull align 8 dereferenceable(544) %newA)
          to label %invoke.cont685 unwind label %lpad627

invoke.cont685:                                   ; preds = %invoke.cont681
  %228 = load i32, i32* %z, align 4
  %inc687 = add nsw i32 %228, 1
  store i32 %inc687, i32* %z, align 4
  call void @_ZN11AppointmentD2Ev(%class.Appointment* nonnull dereferenceable(544) %newA) #3
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %temp_624) #3
  br label %if.end853

lpad625:                                          ; preds = %if.then623
  %229 = landingpad { i8*, i32 }
          cleanup
  %230 = extractvalue { i8*, i32 } %229, 0
  store i8* %230, i8** %exn.slot, align 8
  %231 = extractvalue { i8*, i32 } %229, 1
  store i32 %231, i32* %ehselector.slot, align 4
  br label %ehcleanup689

lpad627:                                          ; preds = %invoke.cont681, %invoke.cont676, %invoke.cont674, %invoke.cont672, %invoke.cont670, %invoke.cont665, %invoke.cont663, %invoke.cont661, %invoke.cont659, %invoke.cont654, %invoke.cont652, %invoke.cont650, %invoke.cont648, %invoke.cont643, %invoke.cont641, %invoke.cont639, %invoke.cont637, %invoke.cont632, %invoke.cont630, %invoke.cont628, %invoke.cont626
  %232 = landingpad { i8*, i32 }
          cleanup
  %233 = extractvalue { i8*, i32 } %232, 0
  store i8* %233, i8** %exn.slot, align 8
  %234 = extractvalue { i8*, i32 } %232, 1
  store i32 %234, i32* %ehselector.slot, align 4
  br label %ehcleanup688

lpad636:                                          ; preds = %invoke.cont635
  %235 = landingpad { i8*, i32 }
          cleanup
  %236 = extractvalue { i8*, i32 } %235, 0
  store i8* %236, i8** %exn.slot, align 8
  %237 = extractvalue { i8*, i32 } %235, 1
  store i32 %237, i32* %ehselector.slot, align 4
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp634) #3
  br label %ehcleanup688

lpad647:                                          ; preds = %invoke.cont646
  %238 = landingpad { i8*, i32 }
          cleanup
  %239 = extractvalue { i8*, i32 } %238, 0
  store i8* %239, i8** %exn.slot, align 8
  %240 = extractvalue { i8*, i32 } %238, 1
  store i32 %240, i32* %ehselector.slot, align 4
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp645) #3
  br label %ehcleanup688

lpad658:                                          ; preds = %invoke.cont657
  %241 = landingpad { i8*, i32 }
          cleanup
  %242 = extractvalue { i8*, i32 } %241, 0
  store i8* %242, i8** %exn.slot, align 8
  %243 = extractvalue { i8*, i32 } %241, 1
  store i32 %243, i32* %ehselector.slot, align 4
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp656) #3
  br label %ehcleanup688

lpad669:                                          ; preds = %invoke.cont668
  %244 = landingpad { i8*, i32 }
          cleanup
  %245 = extractvalue { i8*, i32 } %244, 0
  store i8* %245, i8** %exn.slot, align 8
  %246 = extractvalue { i8*, i32 } %244, 1
  store i32 %246, i32* %ehselector.slot, align 4
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp667) #3
  br label %ehcleanup688

lpad680:                                          ; preds = %invoke.cont679
  %247 = landingpad { i8*, i32 }
          cleanup
  %248 = extractvalue { i8*, i32 } %247, 0
  store i8* %248, i8** %exn.slot, align 8
  %249 = extractvalue { i8*, i32 } %247, 1
  store i32 %249, i32* %ehselector.slot, align 4
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp678) #3
  br label %ehcleanup688

ehcleanup688:                                     ; preds = %lpad680, %lpad669, %lpad658, %lpad647, %lpad636, %lpad627
  call void @_ZN11AppointmentD2Ev(%class.Appointment* nonnull dereferenceable(544) %newA) #3
  br label %ehcleanup689

ehcleanup689:                                     ; preds = %ehcleanup688, %lpad625
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %temp_624) #3
  br label %ehcleanup1099

if.else690:                                       ; preds = %invoke.cont620
  %250 = load i32, i32* %menu, align 4
  %cmp691 = icmp eq i32 %250, 2
  br i1 %cmp691, label %if.then692, label %if.else704

if.then692:                                       ; preds = %if.else690
  %call694 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* nonnull dereferenceable(8) @_ZSt4cout, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
          to label %invoke.cont693 unwind label %lpad37

invoke.cont693:                                   ; preds = %if.then692
  store i32 0, i32* %i, align 4
  br label %for.cond695

for.cond695:                                      ; preds = %for.inc701, %invoke.cont693
  %251 = load i32, i32* %i, align 4
  %252 = load i32, i32* %z, align 4
  %cmp696 = icmp slt i32 %251, %252
  br i1 %cmp696, label %for.body697, label %for.end703

for.body697:                                      ; preds = %for.cond695
  %253 = load i32, i32* %i, align 4
  %idxprom698 = sext i32 %253 to i64
  %arrayidx699 = getelementptr inbounds [100 x %class.Appointment], [100 x %class.Appointment]* %_A, i64 0, i64 %idxprom698
  invoke void @_ZN11Appointment7displayEv(%class.Appointment* nonnull dereferenceable(544) %arrayidx699)
          to label %invoke.cont700 unwind label %lpad37

invoke.cont700:                                   ; preds = %for.body697
  br label %for.inc701

for.inc701:                                       ; preds = %invoke.cont700
  %254 = load i32, i32* %i, align 4
  %inc702 = add nsw i32 %254, 1
  store i32 %inc702, i32* %i, align 4
  br label %for.cond695, !llvm.loop !18

for.end703:                                       ; preds = %for.cond695
  br label %if.end852

if.else704:                                       ; preds = %if.else690
  %255 = load i32, i32* %menu, align 4
  %cmp705 = icmp eq i32 %255, 3
  br i1 %cmp705, label %if.then706, label %if.else758

if.then706:                                       ; preds = %if.else704
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %temp_707) #3
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %temp_2) #3
  %call711 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) @_ZSt4cout, i8* getelementptr inbounds ([32 x i8], [32 x i8]* @.str.23, i64 0, i64 0))
          to label %invoke.cont710 unwind label %lpad709

invoke.cont710:                                   ; preds = %if.then706
  %call713 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* nonnull dereferenceable(8) %call711, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
          to label %invoke.cont712 unwind label %lpad709

invoke.cont712:                                   ; preds = %invoke.cont710
  %call715 = invoke nonnull align 8 dereferenceable(16) %"class.std::basic_istream"* @_ZStrsIcSt11char_traitsIcESaIcEERSt13basic_istreamIT_T0_ES7_RNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_istream"* nonnull align 8 dereferenceable(16) @_ZSt3cin, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %temp_707)
          to label %invoke.cont714 unwind label %lpad709

invoke.cont714:                                   ; preds = %invoke.cont712
  %call717 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) @_ZSt4cout, i8* getelementptr inbounds ([33 x i8], [33 x i8]* @.str.43, i64 0, i64 0))
          to label %invoke.cont716 unwind label %lpad709

invoke.cont716:                                   ; preds = %invoke.cont714
  %call719 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* nonnull dereferenceable(8) %call717, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
          to label %invoke.cont718 unwind label %lpad709

invoke.cont718:                                   ; preds = %invoke.cont716
  %call721 = invoke nonnull align 8 dereferenceable(16) %"class.std::basic_istream"* @_ZStrsIcSt11char_traitsIcESaIcEERSt13basic_istreamIT_T0_ES7_RNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_istream"* nonnull align 8 dereferenceable(16) @_ZSt3cin, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %temp_2)
          to label %invoke.cont720 unwind label %lpad709

invoke.cont720:                                   ; preds = %invoke.cont718
  store i32 0, i32* %i, align 4
  br label %while.cond722

while.cond722:                                    ; preds = %if.end740, %invoke.cont720
  %256 = load i32, i32* %i, align 4
  %257 = load i32, i32* %z, align 4
  %cmp723 = icmp slt i32 %256, %257
  br i1 %cmp723, label %while.body724, label %while.end742

while.body724:                                    ; preds = %while.cond722
  %258 = load i32, i32* %i, align 4
  %idxprom726 = sext i32 %258 to i64
  %arrayidx727 = getelementptr inbounds [100 x %class.Appointment], [100 x %class.Appointment]* %_A, i64 0, i64 %idxprom726
  %S728 = getelementptr inbounds %class.Appointment, %class.Appointment* %arrayidx727, i32 0, i32 4
  %259 = bitcast %class.Student* %S728 to %class.People*
  store i1 false, i1* %cleanup.cond, align 1
  invoke void @_ZN6People9getNumberB5cxx11Ev(%"class.std::__cxx11::basic_string"* sret(%"class.std::__cxx11::basic_string") align 8 %ref.tmp725, %class.People* nonnull dereferenceable(192) %259)
          to label %invoke.cont729 unwind label %lpad709

invoke.cont729:                                   ; preds = %while.body724
  %call730 = call zeroext i1 @_ZSteqIcEN9__gnu_cxx11__enable_ifIXsr9__is_charIT_EE7__valueEbE6__typeERKNSt7__cxx1112basic_stringIS2_St11char_traitsIS2_ESaIS2_EEESC_(%"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %ref.tmp725, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %temp_707) #3
  br i1 %call730, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %invoke.cont729
  %260 = load i32, i32* %i, align 4
  %idxprom732 = sext i32 %260 to i64
  %arrayidx733 = getelementptr inbounds [100 x %class.Appointment], [100 x %class.Appointment]* %_A, i64 0, i64 %idxprom732
  %L734 = getelementptr inbounds %class.Appointment, %class.Appointment* %arrayidx733, i32 0, i32 3
  %261 = bitcast %class.Lecturer* %L734 to %class.People*
  invoke void @_ZN6People9getNumberB5cxx11Ev(%"class.std::__cxx11::basic_string"* sret(%"class.std::__cxx11::basic_string") align 8 %ref.tmp731, %class.People* nonnull dereferenceable(192) %261)
          to label %invoke.cont736 unwind label %lpad735

invoke.cont736:                                   ; preds = %land.rhs
  store i1 true, i1* %cleanup.cond, align 1
  %call737 = call zeroext i1 @_ZSteqIcEN9__gnu_cxx11__enable_ifIXsr9__is_charIT_EE7__valueEbE6__typeERKNSt7__cxx1112basic_stringIS2_St11char_traitsIS2_ESaIS2_EEESC_(%"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %ref.tmp731, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %temp_2) #3
  br label %land.end

land.end:                                         ; preds = %invoke.cont736, %invoke.cont729
  %262 = phi i1 [ false, %invoke.cont729 ], [ %call737, %invoke.cont736 ]
  %cleanup.is_active = load i1, i1* %cleanup.cond, align 1
  br i1 %cleanup.is_active, label %cleanup.action, label %cleanup.done

cleanup.action:                                   ; preds = %land.end
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %ref.tmp731) #3
  br label %cleanup.done

cleanup.done:                                     ; preds = %cleanup.action, %land.end
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %ref.tmp725) #3
  br i1 %262, label %if.then739, label %if.end740

if.then739:                                       ; preds = %cleanup.done
  %263 = load i32, i32* %i, align 4
  store i32 %263, i32* %deleted708, align 4
  br label %if.end740

lpad709:                                          ; preds = %for.body745, %while.body724, %invoke.cont718, %invoke.cont716, %invoke.cont714, %invoke.cont712, %invoke.cont710, %if.then706
  %264 = landingpad { i8*, i32 }
          cleanup
  %265 = extractvalue { i8*, i32 } %264, 0
  store i8* %265, i8** %exn.slot, align 8
  %266 = extractvalue { i8*, i32 } %264, 1
  store i32 %266, i32* %ehselector.slot, align 4
  br label %ehcleanup756

lpad735:                                          ; preds = %land.rhs
  %267 = landingpad { i8*, i32 }
          cleanup
  %268 = extractvalue { i8*, i32 } %267, 0
  store i8* %268, i8** %exn.slot, align 8
  %269 = extractvalue { i8*, i32 } %267, 1
  store i32 %269, i32* %ehselector.slot, align 4
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %ref.tmp725) #3
  br label %ehcleanup756

if.end740:                                        ; preds = %if.then739, %cleanup.done
  %270 = load i32, i32* %i, align 4
  %inc741 = add nsw i32 %270, 1
  store i32 %inc741, i32* %i, align 4
  br label %while.cond722, !llvm.loop !19

while.end742:                                     ; preds = %while.cond722
  %271 = load i32, i32* %deleted708, align 4
  store i32 %271, i32* %i, align 4
  br label %for.cond743

for.cond743:                                      ; preds = %for.inc753, %while.end742
  %272 = load i32, i32* %i, align 4
  %273 = load i32, i32* %z, align 4
  %cmp744 = icmp slt i32 %272, %273
  br i1 %cmp744, label %for.body745, label %for.end755

for.body745:                                      ; preds = %for.cond743
  %274 = load i32, i32* %i, align 4
  %add746 = add nsw i32 %274, 1
  %idxprom747 = sext i32 %add746 to i64
  %arrayidx748 = getelementptr inbounds [100 x %class.Appointment], [100 x %class.Appointment]* %_A, i64 0, i64 %idxprom747
  %275 = load i32, i32* %i, align 4
  %idxprom749 = sext i32 %275 to i64
  %arrayidx750 = getelementptr inbounds [100 x %class.Appointment], [100 x %class.Appointment]* %_A, i64 0, i64 %idxprom749
  %call752 = invoke nonnull align 8 dereferenceable(544) %class.Appointment* @_ZN11AppointmentaSERKS_(%class.Appointment* nonnull dereferenceable(544) %arrayidx750, %class.Appointment* nonnull align 8 dereferenceable(544) %arrayidx748)
          to label %invoke.cont751 unwind label %lpad709

invoke.cont751:                                   ; preds = %for.body745
  br label %for.inc753

for.inc753:                                       ; preds = %invoke.cont751
  %276 = load i32, i32* %i, align 4
  %inc754 = add nsw i32 %276, 1
  store i32 %inc754, i32* %i, align 4
  br label %for.cond743, !llvm.loop !20

for.end755:                                       ; preds = %for.cond743
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %temp_2) #3
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %temp_707) #3
  br label %if.end851

ehcleanup756:                                     ; preds = %lpad735, %lpad709
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %temp_2) #3
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %temp_707) #3
  br label %ehcleanup1099

if.else758:                                       ; preds = %if.else704
  %277 = load i32, i32* %menu, align 4
  %cmp759 = icmp eq i32 %277, 4
  br i1 %cmp759, label %if.then760, label %if.else845

if.then760:                                       ; preds = %if.else758
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %temp_761) #3
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %temp_2762) #3
  %call766 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) @_ZSt4cout, i8* getelementptr inbounds ([32 x i8], [32 x i8]* @.str.24, i64 0, i64 0))
          to label %invoke.cont765 unwind label %lpad764

invoke.cont765:                                   ; preds = %if.then760
  %call768 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* nonnull dereferenceable(8) %call766, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
          to label %invoke.cont767 unwind label %lpad764

invoke.cont767:                                   ; preds = %invoke.cont765
  %call770 = invoke nonnull align 8 dereferenceable(16) %"class.std::basic_istream"* @_ZStrsIcSt11char_traitsIcESaIcEERSt13basic_istreamIT_T0_ES7_RNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_istream"* nonnull align 8 dereferenceable(16) @_ZSt3cin, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %temp_761)
          to label %invoke.cont769 unwind label %lpad764

invoke.cont769:                                   ; preds = %invoke.cont767
  %call772 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) @_ZSt4cout, i8* getelementptr inbounds ([33 x i8], [33 x i8]* @.str.44, i64 0, i64 0))
          to label %invoke.cont771 unwind label %lpad764

invoke.cont771:                                   ; preds = %invoke.cont769
  %call774 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* nonnull dereferenceable(8) %call772, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
          to label %invoke.cont773 unwind label %lpad764

invoke.cont773:                                   ; preds = %invoke.cont771
  %call776 = invoke nonnull align 8 dereferenceable(16) %"class.std::basic_istream"* @_ZStrsIcSt11char_traitsIcESaIcEERSt13basic_istreamIT_T0_ES7_RNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_istream"* nonnull align 8 dereferenceable(16) @_ZSt3cin, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %temp_2762)
          to label %invoke.cont775 unwind label %lpad764

invoke.cont775:                                   ; preds = %invoke.cont773
  store i32 0, i32* %i, align 4
  br label %while.cond777

while.cond777:                                    ; preds = %if.end801, %invoke.cont775
  %278 = load i32, i32* %i, align 4
  %279 = load i32, i32* %z, align 4
  %cmp778 = icmp slt i32 %278, %279
  br i1 %cmp778, label %while.body779, label %while.end803

while.body779:                                    ; preds = %while.cond777
  %280 = load i32, i32* %i, align 4
  %idxprom781 = sext i32 %280 to i64
  %arrayidx782 = getelementptr inbounds [100 x %class.Appointment], [100 x %class.Appointment]* %_A, i64 0, i64 %idxprom781
  %S783 = getelementptr inbounds %class.Appointment, %class.Appointment* %arrayidx782, i32 0, i32 4
  %281 = bitcast %class.Student* %S783 to %class.People*
  store i1 false, i1* %cleanup.cond793, align 1
  invoke void @_ZN6People9getNumberB5cxx11Ev(%"class.std::__cxx11::basic_string"* sret(%"class.std::__cxx11::basic_string") align 8 %ref.tmp780, %class.People* nonnull dereferenceable(192) %281)
          to label %invoke.cont784 unwind label %lpad764

invoke.cont784:                                   ; preds = %while.body779
  %call785 = call zeroext i1 @_ZSteqIcEN9__gnu_cxx11__enable_ifIXsr9__is_charIT_EE7__valueEbE6__typeERKNSt7__cxx1112basic_stringIS2_St11char_traitsIS2_ESaIS2_EEESC_(%"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %ref.tmp780, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %temp_761) #3
  br i1 %call785, label %land.rhs786, label %land.end795

land.rhs786:                                      ; preds = %invoke.cont784
  %282 = load i32, i32* %i, align 4
  %idxprom788 = sext i32 %282 to i64
  %arrayidx789 = getelementptr inbounds [100 x %class.Appointment], [100 x %class.Appointment]* %_A, i64 0, i64 %idxprom788
  %L790 = getelementptr inbounds %class.Appointment, %class.Appointment* %arrayidx789, i32 0, i32 3
  %283 = bitcast %class.Lecturer* %L790 to %class.People*
  invoke void @_ZN6People9getNumberB5cxx11Ev(%"class.std::__cxx11::basic_string"* sret(%"class.std::__cxx11::basic_string") align 8 %ref.tmp787, %class.People* nonnull dereferenceable(192) %283)
          to label %invoke.cont792 unwind label %lpad791

invoke.cont792:                                   ; preds = %land.rhs786
  store i1 true, i1* %cleanup.cond793, align 1
  %call794 = call zeroext i1 @_ZSteqIcEN9__gnu_cxx11__enable_ifIXsr9__is_charIT_EE7__valueEbE6__typeERKNSt7__cxx1112basic_stringIS2_St11char_traitsIS2_ESaIS2_EEESC_(%"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %ref.tmp787, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %temp_2762) #3
  br label %land.end795

land.end795:                                      ; preds = %invoke.cont792, %invoke.cont784
  %284 = phi i1 [ false, %invoke.cont784 ], [ %call794, %invoke.cont792 ]
  %cleanup.is_active796 = load i1, i1* %cleanup.cond793, align 1
  br i1 %cleanup.is_active796, label %cleanup.action797, label %cleanup.done798

cleanup.action797:                                ; preds = %land.end795
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %ref.tmp787) #3
  br label %cleanup.done798

cleanup.done798:                                  ; preds = %cleanup.action797, %land.end795
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %ref.tmp780) #3
  br i1 %284, label %if.then800, label %if.end801

if.then800:                                       ; preds = %cleanup.done798
  %285 = load i32, i32* %i, align 4
  store i32 %285, i32* %updated763, align 4
  br label %if.end801

lpad764:                                          ; preds = %invoke.cont834, %invoke.cont832, %invoke.cont830, %invoke.cont828, %invoke.cont821, %invoke.cont819, %invoke.cont817, %invoke.cont815, %invoke.cont808, %invoke.cont806, %invoke.cont804, %while.end803, %while.body779, %invoke.cont773, %invoke.cont771, %invoke.cont769, %invoke.cont767, %invoke.cont765, %if.then760
  %286 = landingpad { i8*, i32 }
          cleanup
  %287 = extractvalue { i8*, i32 } %286, 0
  store i8* %287, i8** %exn.slot, align 8
  %288 = extractvalue { i8*, i32 } %286, 1
  store i32 %288, i32* %ehselector.slot, align 4
  br label %ehcleanup843

lpad791:                                          ; preds = %land.rhs786
  %289 = landingpad { i8*, i32 }
          cleanup
  %290 = extractvalue { i8*, i32 } %289, 0
  store i8* %290, i8** %exn.slot, align 8
  %291 = extractvalue { i8*, i32 } %289, 1
  store i32 %291, i32* %ehselector.slot, align 4
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %ref.tmp780) #3
  br label %ehcleanup843

if.end801:                                        ; preds = %if.then800, %cleanup.done798
  %292 = load i32, i32* %i, align 4
  %inc802 = add nsw i32 %292, 1
  store i32 %inc802, i32* %i, align 4
  br label %while.cond777, !llvm.loop !21

while.end803:                                     ; preds = %while.cond777
  %call805 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) @_ZSt4cout, i8* getelementptr inbounds ([26 x i8], [26 x i8]* @.str.58, i64 0, i64 0))
          to label %invoke.cont804 unwind label %lpad764

invoke.cont804:                                   ; preds = %while.end803
  %call807 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* nonnull dereferenceable(8) %call805, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
          to label %invoke.cont806 unwind label %lpad764

invoke.cont806:                                   ; preds = %invoke.cont804
  %call809 = invoke nonnull align 8 dereferenceable(16) %"class.std::basic_istream"* @_ZStrsIcSt11char_traitsIcESaIcEERSt13basic_istreamIT_T0_ES7_RNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_istream"* nonnull align 8 dereferenceable(16) @_ZSt3cin, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %temp_761)
          to label %invoke.cont808 unwind label %lpad764

invoke.cont808:                                   ; preds = %invoke.cont806
  %293 = load i32, i32* %updated763, align 4
  %idxprom810 = sext i32 %293 to i64
  %arrayidx811 = getelementptr inbounds [100 x %class.Appointment], [100 x %class.Appointment]* %_A, i64 0, i64 %idxprom810
  invoke void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1ERKS4_(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp812, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %temp_761)
          to label %invoke.cont813 unwind label %lpad764

invoke.cont813:                                   ; preds = %invoke.cont808
  invoke void @_ZN11Appointment7setDateENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE(%class.Appointment* nonnull dereferenceable(544) %arrayidx811, %"class.std::__cxx11::basic_string"* %agg.tmp812)
          to label %invoke.cont815 unwind label %lpad814

invoke.cont815:                                   ; preds = %invoke.cont813
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp812) #3
  %call818 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) @_ZSt4cout, i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.59, i64 0, i64 0))
          to label %invoke.cont817 unwind label %lpad764

invoke.cont817:                                   ; preds = %invoke.cont815
  %call820 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* nonnull dereferenceable(8) %call818, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
          to label %invoke.cont819 unwind label %lpad764

invoke.cont819:                                   ; preds = %invoke.cont817
  %call822 = invoke nonnull align 8 dereferenceable(16) %"class.std::basic_istream"* @_ZStrsIcSt11char_traitsIcESaIcEERSt13basic_istreamIT_T0_ES7_RNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_istream"* nonnull align 8 dereferenceable(16) @_ZSt3cin, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %temp_761)
          to label %invoke.cont821 unwind label %lpad764

invoke.cont821:                                   ; preds = %invoke.cont819
  %294 = load i32, i32* %updated763, align 4
  %idxprom823 = sext i32 %294 to i64
  %arrayidx824 = getelementptr inbounds [100 x %class.Appointment], [100 x %class.Appointment]* %_A, i64 0, i64 %idxprom823
  invoke void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1ERKS4_(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp825, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %temp_761)
          to label %invoke.cont826 unwind label %lpad764

invoke.cont826:                                   ; preds = %invoke.cont821
  invoke void @_ZN11Appointment8setStartENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE(%class.Appointment* nonnull dereferenceable(544) %arrayidx824, %"class.std::__cxx11::basic_string"* %agg.tmp825)
          to label %invoke.cont828 unwind label %lpad827

invoke.cont828:                                   ; preds = %invoke.cont826
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp825) #3
  %call831 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) @_ZSt4cout, i8* getelementptr inbounds ([21 x i8], [21 x i8]* @.str.60, i64 0, i64 0))
          to label %invoke.cont830 unwind label %lpad764

invoke.cont830:                                   ; preds = %invoke.cont828
  %call833 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* nonnull dereferenceable(8) %call831, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
          to label %invoke.cont832 unwind label %lpad764

invoke.cont832:                                   ; preds = %invoke.cont830
  %call835 = invoke nonnull align 8 dereferenceable(16) %"class.std::basic_istream"* @_ZStrsIcSt11char_traitsIcESaIcEERSt13basic_istreamIT_T0_ES7_RNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_istream"* nonnull align 8 dereferenceable(16) @_ZSt3cin, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %temp_761)
          to label %invoke.cont834 unwind label %lpad764

invoke.cont834:                                   ; preds = %invoke.cont832
  %295 = load i32, i32* %updated763, align 4
  %idxprom836 = sext i32 %295 to i64
  %arrayidx837 = getelementptr inbounds [100 x %class.Appointment], [100 x %class.Appointment]* %_A, i64 0, i64 %idxprom836
  invoke void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1ERKS4_(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp838, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %temp_761)
          to label %invoke.cont839 unwind label %lpad764

invoke.cont839:                                   ; preds = %invoke.cont834
  invoke void @_ZN11Appointment6setEndENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE(%class.Appointment* nonnull dereferenceable(544) %arrayidx837, %"class.std::__cxx11::basic_string"* %agg.tmp838)
          to label %invoke.cont841 unwind label %lpad840

invoke.cont841:                                   ; preds = %invoke.cont839
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp838) #3
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %temp_2762) #3
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %temp_761) #3
  br label %if.end850

lpad814:                                          ; preds = %invoke.cont813
  %296 = landingpad { i8*, i32 }
          cleanup
  %297 = extractvalue { i8*, i32 } %296, 0
  store i8* %297, i8** %exn.slot, align 8
  %298 = extractvalue { i8*, i32 } %296, 1
  store i32 %298, i32* %ehselector.slot, align 4
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp812) #3
  br label %ehcleanup843

lpad827:                                          ; preds = %invoke.cont826
  %299 = landingpad { i8*, i32 }
          cleanup
  %300 = extractvalue { i8*, i32 } %299, 0
  store i8* %300, i8** %exn.slot, align 8
  %301 = extractvalue { i8*, i32 } %299, 1
  store i32 %301, i32* %ehselector.slot, align 4
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp825) #3
  br label %ehcleanup843

lpad840:                                          ; preds = %invoke.cont839
  %302 = landingpad { i8*, i32 }
          cleanup
  %303 = extractvalue { i8*, i32 } %302, 0
  store i8* %303, i8** %exn.slot, align 8
  %304 = extractvalue { i8*, i32 } %302, 1
  store i32 %304, i32* %ehselector.slot, align 4
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.tmp838) #3
  br label %ehcleanup843

ehcleanup843:                                     ; preds = %lpad840, %lpad827, %lpad814, %lpad791, %lpad764
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %temp_2762) #3
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %temp_761) #3
  br label %ehcleanup1099

if.else845:                                       ; preds = %if.else758
  %call847 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) @_ZSt4cout, i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.31, i64 0, i64 0))
          to label %invoke.cont846 unwind label %lpad37

invoke.cont846:                                   ; preds = %if.else845
  %call849 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* nonnull dereferenceable(8) %call847, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
          to label %invoke.cont848 unwind label %lpad37

invoke.cont848:                                   ; preds = %invoke.cont846
  br label %if.end850

if.end850:                                        ; preds = %invoke.cont848, %invoke.cont841
  br label %if.end851

if.end851:                                        ; preds = %if.end850, %for.end755
  br label %if.end852

if.end852:                                        ; preds = %if.end851, %for.end703
  br label %if.end853

if.end853:                                        ; preds = %if.end852, %invoke.cont685
  br label %if.end1089

if.else854:                                       ; preds = %if.else601
  %305 = load i32, i32* %menuLoop, align 4
  %cmp855 = icmp eq i32 %305, 4
  br i1 %cmp855, label %if.then856, label %if.else1075

if.then856:                                       ; preds = %if.else854
  invoke void @_ZNSt14basic_ofstreamIcSt11char_traitsIcEEC1Ev(%"class.std::basic_ofstream"* nonnull dereferenceable(248) %writeToFile)
          to label %invoke.cont857 unwind label %lpad37

invoke.cont857:                                   ; preds = %if.then856
  invoke void @_ZNSt14basic_ofstreamIcSt11char_traitsIcEE4openEPKcSt13_Ios_Openmode(%"class.std::basic_ofstream"* nonnull dereferenceable(248) %writeToFile, i8* getelementptr inbounds ([18 x i8], [18 x i8]* @.str.61, i64 0, i64 0), i32 16)
          to label %invoke.cont859 unwind label %lpad858

invoke.cont859:                                   ; preds = %invoke.cont857
  store i32 0, i32* %i, align 4
  br label %for.cond860

for.cond860:                                      ; preds = %for.inc933, %invoke.cont859
  %306 = load i32, i32* %i, align 4
  %307 = load i32, i32* %x, align 4
  %cmp861 = icmp slt i32 %306, %307
  br i1 %cmp861, label %for.body862, label %for.end935

for.body862:                                      ; preds = %for.cond860
  %308 = bitcast %"class.std::basic_ofstream"* %writeToFile to %"class.std::basic_ostream"*
  %309 = load i32, i32* %i, align 4
  %idxprom864 = sext i32 %309 to i64
  %arrayidx865 = getelementptr inbounds [100 x %class.Student], [100 x %class.Student]* %_S, i64 0, i64 %idxprom864
  %310 = bitcast %class.Student* %arrayidx865 to %class.People*
  invoke void @_ZN6People9getNumberB5cxx11Ev(%"class.std::__cxx11::basic_string"* sret(%"class.std::__cxx11::basic_string") align 8 %ref.tmp863, %class.People* nonnull dereferenceable(192) %310)
          to label %invoke.cont866 unwind label %lpad858

invoke.cont866:                                   ; preds = %for.body862
  %call869 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsIcSt11char_traitsIcESaIcEERSt13basic_ostreamIT_T0_ES7_RKNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) %308, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %ref.tmp863)
          to label %invoke.cont868 unwind label %lpad867

invoke.cont868:                                   ; preds = %invoke.cont866
  %call871 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) %call869, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.62, i64 0, i64 0))
          to label %invoke.cont870 unwind label %lpad867

invoke.cont870:                                   ; preds = %invoke.cont868
  %311 = load i32, i32* %i, align 4
  %idxprom873 = sext i32 %311 to i64
  %arrayidx874 = getelementptr inbounds [100 x %class.Student], [100 x %class.Student]* %_S, i64 0, i64 %idxprom873
  %312 = bitcast %class.Student* %arrayidx874 to %class.People*
  invoke void @_ZN6People7getNameB5cxx11Ev(%"class.std::__cxx11::basic_string"* sret(%"class.std::__cxx11::basic_string") align 8 %ref.tmp872, %class.People* nonnull dereferenceable(192) %312)
          to label %invoke.cont875 unwind label %lpad867

invoke.cont875:                                   ; preds = %invoke.cont870
  %call878 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsIcSt11char_traitsIcESaIcEERSt13basic_ostreamIT_T0_ES7_RKNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) %call871, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %ref.tmp872)
          to label %invoke.cont877 unwind label %lpad876

invoke.cont877:                                   ; preds = %invoke.cont875
  %call880 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) %call878, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.62, i64 0, i64 0))
          to label %invoke.cont879 unwind label %lpad876

invoke.cont879:                                   ; preds = %invoke.cont877
  %313 = load i32, i32* %i, align 4
  %idxprom882 = sext i32 %313 to i64
  %arrayidx883 = getelementptr inbounds [100 x %class.Student], [100 x %class.Student]* %_S, i64 0, i64 %idxprom882
  %314 = bitcast %class.Student* %arrayidx883 to %class.People*
  invoke void @_ZN6People10getSurnameB5cxx11Ev(%"class.std::__cxx11::basic_string"* sret(%"class.std::__cxx11::basic_string") align 8 %ref.tmp881, %class.People* nonnull dereferenceable(192) %314)
          to label %invoke.cont884 unwind label %lpad876

invoke.cont884:                                   ; preds = %invoke.cont879
  %call887 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsIcSt11char_traitsIcESaIcEERSt13basic_ostreamIT_T0_ES7_RKNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) %call880, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %ref.tmp881)
          to label %invoke.cont886 unwind label %lpad885

invoke.cont886:                                   ; preds = %invoke.cont884
  %call889 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) %call887, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.62, i64 0, i64 0))
          to label %invoke.cont888 unwind label %lpad885

invoke.cont888:                                   ; preds = %invoke.cont886
  %315 = load i32, i32* %i, align 4
  %idxprom891 = sext i32 %315 to i64
  %arrayidx892 = getelementptr inbounds [100 x %class.Student], [100 x %class.Student]* %_S, i64 0, i64 %idxprom891
  %316 = bitcast %class.Student* %arrayidx892 to %class.People*
  invoke void @_ZN6People13getDepartmentB5cxx11Ev(%"class.std::__cxx11::basic_string"* sret(%"class.std::__cxx11::basic_string") align 8 %ref.tmp890, %class.People* nonnull dereferenceable(192) %316)
          to label %invoke.cont893 unwind label %lpad885

invoke.cont893:                                   ; preds = %invoke.cont888
  %call896 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsIcSt11char_traitsIcESaIcEERSt13basic_ostreamIT_T0_ES7_RKNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) %call889, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %ref.tmp890)
          to label %invoke.cont895 unwind label %lpad894

invoke.cont895:                                   ; preds = %invoke.cont893
  %call898 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) %call896, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.62, i64 0, i64 0))
          to label %invoke.cont897 unwind label %lpad894

invoke.cont897:                                   ; preds = %invoke.cont895
  %317 = load i32, i32* %i, align 4
  %idxprom900 = sext i32 %317 to i64
  %arrayidx901 = getelementptr inbounds [100 x %class.Student], [100 x %class.Student]* %_S, i64 0, i64 %idxprom900
  invoke void @_ZN7Student7getYearB5cxx11Ev(%"class.std::__cxx11::basic_string"* sret(%"class.std::__cxx11::basic_string") align 8 %ref.tmp899, %class.Student* nonnull dereferenceable(224) %arrayidx901)
          to label %invoke.cont902 unwind label %lpad894

invoke.cont902:                                   ; preds = %invoke.cont897
  %call905 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsIcSt11char_traitsIcESaIcEERSt13basic_ostreamIT_T0_ES7_RKNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) %call898, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %ref.tmp899)
          to label %invoke.cont904 unwind label %lpad903

invoke.cont904:                                   ; preds = %invoke.cont902
  %call907 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) %call905, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.62, i64 0, i64 0))
          to label %invoke.cont906 unwind label %lpad903

invoke.cont906:                                   ; preds = %invoke.cont904
  %318 = load i32, i32* %i, align 4
  %idxprom909 = sext i32 %318 to i64
  %arrayidx910 = getelementptr inbounds [100 x %class.Student], [100 x %class.Student]* %_S, i64 0, i64 %idxprom909
  %319 = bitcast %class.Student* %arrayidx910 to %class.People*
  invoke void @_ZN6People8getEmailB5cxx11Ev(%"class.std::__cxx11::basic_string"* sret(%"class.std::__cxx11::basic_string") align 8 %ref.tmp908, %class.People* nonnull dereferenceable(192) %319)
          to label %invoke.cont911 unwind label %lpad903

invoke.cont911:                                   ; preds = %invoke.cont906
  %call914 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsIcSt11char_traitsIcESaIcEERSt13basic_ostreamIT_T0_ES7_RKNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) %call907, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %ref.tmp908)
          to label %invoke.cont913 unwind label %lpad912

invoke.cont913:                                   ; preds = %invoke.cont911
  %call916 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) %call914, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.62, i64 0, i64 0))
          to label %invoke.cont915 unwind label %lpad912

invoke.cont915:                                   ; preds = %invoke.cont913
  %320 = load i32, i32* %i, align 4
  %idxprom918 = sext i32 %320 to i64
  %arrayidx919 = getelementptr inbounds [100 x %class.Student], [100 x %class.Student]* %_S, i64 0, i64 %idxprom918
  %321 = bitcast %class.Student* %arrayidx919 to %class.People*
  invoke void @_ZN6People8getPhoneB5cxx11Ev(%"class.std::__cxx11::basic_string"* sret(%"class.std::__cxx11::basic_string") align 8 %ref.tmp917, %class.People* nonnull dereferenceable(192) %321)
          to label %invoke.cont920 unwind label %lpad912

invoke.cont920:                                   ; preds = %invoke.cont915
  %call923 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsIcSt11char_traitsIcESaIcEERSt13basic_ostreamIT_T0_ES7_RKNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) %call916, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %ref.tmp917)
          to label %invoke.cont922 unwind label %lpad921

invoke.cont922:                                   ; preds = %invoke.cont920
  %call925 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* nonnull dereferenceable(8) %call923, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
          to label %invoke.cont924 unwind label %lpad921

invoke.cont924:                                   ; preds = %invoke.cont922
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %ref.tmp917) #3
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %ref.tmp908) #3
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %ref.tmp899) #3
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %ref.tmp890) #3
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %ref.tmp881) #3
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %ref.tmp872) #3
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %ref.tmp863) #3
  br label %for.inc933

for.inc933:                                       ; preds = %invoke.cont924
  %322 = load i32, i32* %i, align 4
  %inc934 = add nsw i32 %322, 1
  store i32 %inc934, i32* %i, align 4
  br label %for.cond860, !llvm.loop !22

lpad858:                                          ; preds = %for.body1018, %invoke.cont1014, %for.end1013, %for.body940, %invoke.cont936, %for.end935, %for.body862, %invoke.cont857
  %323 = landingpad { i8*, i32 }
          cleanup
  %324 = extractvalue { i8*, i32 } %323, 0
  store i8* %324, i8** %exn.slot, align 8
  %325 = extractvalue { i8*, i32 } %323, 1
  store i32 %325, i32* %ehselector.slot, align 4
  br label %ehcleanup1074

lpad867:                                          ; preds = %invoke.cont870, %invoke.cont868, %invoke.cont866
  %326 = landingpad { i8*, i32 }
          cleanup
  %327 = extractvalue { i8*, i32 } %326, 0
  store i8* %327, i8** %exn.slot, align 8
  %328 = extractvalue { i8*, i32 } %326, 1
  store i32 %328, i32* %ehselector.slot, align 4
  br label %ehcleanup932

lpad876:                                          ; preds = %invoke.cont879, %invoke.cont877, %invoke.cont875
  %329 = landingpad { i8*, i32 }
          cleanup
  %330 = extractvalue { i8*, i32 } %329, 0
  store i8* %330, i8** %exn.slot, align 8
  %331 = extractvalue { i8*, i32 } %329, 1
  store i32 %331, i32* %ehselector.slot, align 4
  br label %ehcleanup931

lpad885:                                          ; preds = %invoke.cont888, %invoke.cont886, %invoke.cont884
  %332 = landingpad { i8*, i32 }
          cleanup
  %333 = extractvalue { i8*, i32 } %332, 0
  store i8* %333, i8** %exn.slot, align 8
  %334 = extractvalue { i8*, i32 } %332, 1
  store i32 %334, i32* %ehselector.slot, align 4
  br label %ehcleanup930

lpad894:                                          ; preds = %invoke.cont897, %invoke.cont895, %invoke.cont893
  %335 = landingpad { i8*, i32 }
          cleanup
  %336 = extractvalue { i8*, i32 } %335, 0
  store i8* %336, i8** %exn.slot, align 8
  %337 = extractvalue { i8*, i32 } %335, 1
  store i32 %337, i32* %ehselector.slot, align 4
  br label %ehcleanup929

lpad903:                                          ; preds = %invoke.cont906, %invoke.cont904, %invoke.cont902
  %338 = landingpad { i8*, i32 }
          cleanup
  %339 = extractvalue { i8*, i32 } %338, 0
  store i8* %339, i8** %exn.slot, align 8
  %340 = extractvalue { i8*, i32 } %338, 1
  store i32 %340, i32* %ehselector.slot, align 4
  br label %ehcleanup928

lpad912:                                          ; preds = %invoke.cont915, %invoke.cont913, %invoke.cont911
  %341 = landingpad { i8*, i32 }
          cleanup
  %342 = extractvalue { i8*, i32 } %341, 0
  store i8* %342, i8** %exn.slot, align 8
  %343 = extractvalue { i8*, i32 } %341, 1
  store i32 %343, i32* %ehselector.slot, align 4
  br label %ehcleanup927

lpad921:                                          ; preds = %invoke.cont922, %invoke.cont920
  %344 = landingpad { i8*, i32 }
          cleanup
  %345 = extractvalue { i8*, i32 } %344, 0
  store i8* %345, i8** %exn.slot, align 8
  %346 = extractvalue { i8*, i32 } %344, 1
  store i32 %346, i32* %ehselector.slot, align 4
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %ref.tmp917) #3
  br label %ehcleanup927

ehcleanup927:                                     ; preds = %lpad921, %lpad912
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %ref.tmp908) #3
  br label %ehcleanup928

ehcleanup928:                                     ; preds = %ehcleanup927, %lpad903
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %ref.tmp899) #3
  br label %ehcleanup929

ehcleanup929:                                     ; preds = %ehcleanup928, %lpad894
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %ref.tmp890) #3
  br label %ehcleanup930

ehcleanup930:                                     ; preds = %ehcleanup929, %lpad885
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %ref.tmp881) #3
  br label %ehcleanup931

ehcleanup931:                                     ; preds = %ehcleanup930, %lpad876
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %ref.tmp872) #3
  br label %ehcleanup932

ehcleanup932:                                     ; preds = %ehcleanup931, %lpad867
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %ref.tmp863) #3
  br label %ehcleanup1074

for.end935:                                       ; preds = %for.cond860
  invoke void @_ZNSt14basic_ofstreamIcSt11char_traitsIcEE5closeEv(%"class.std::basic_ofstream"* nonnull dereferenceable(248) %writeToFile)
          to label %invoke.cont936 unwind label %lpad858

invoke.cont936:                                   ; preds = %for.end935
  invoke void @_ZNSt14basic_ofstreamIcSt11char_traitsIcEE4openEPKcSt13_Ios_Openmode(%"class.std::basic_ofstream"* nonnull dereferenceable(248) %writeToFile, i8* getelementptr inbounds ([22 x i8], [22 x i8]* @.str.63, i64 0, i64 0), i32 16)
          to label %invoke.cont937 unwind label %lpad858

invoke.cont937:                                   ; preds = %invoke.cont936
  store i32 0, i32* %i, align 4
  br label %for.cond938

for.cond938:                                      ; preds = %for.inc1011, %invoke.cont937
  %347 = load i32, i32* %i, align 4
  %348 = load i32, i32* %y, align 4
  %cmp939 = icmp slt i32 %347, %348
  br i1 %cmp939, label %for.body940, label %for.end1013

for.body940:                                      ; preds = %for.cond938
  %349 = bitcast %"class.std::basic_ofstream"* %writeToFile to %"class.std::basic_ostream"*
  %350 = load i32, i32* %i, align 4
  %idxprom942 = sext i32 %350 to i64
  %arrayidx943 = getelementptr inbounds [100 x %class.Lecturer], [100 x %class.Lecturer]* %_L, i64 0, i64 %idxprom942
  %351 = bitcast %class.Lecturer* %arrayidx943 to %class.People*
  invoke void @_ZN6People9getNumberB5cxx11Ev(%"class.std::__cxx11::basic_string"* sret(%"class.std::__cxx11::basic_string") align 8 %ref.tmp941, %class.People* nonnull dereferenceable(192) %351)
          to label %invoke.cont944 unwind label %lpad858

invoke.cont944:                                   ; preds = %for.body940
  %call947 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsIcSt11char_traitsIcESaIcEERSt13basic_ostreamIT_T0_ES7_RKNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) %349, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %ref.tmp941)
          to label %invoke.cont946 unwind label %lpad945

invoke.cont946:                                   ; preds = %invoke.cont944
  %call949 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) %call947, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.62, i64 0, i64 0))
          to label %invoke.cont948 unwind label %lpad945

invoke.cont948:                                   ; preds = %invoke.cont946
  %352 = load i32, i32* %i, align 4
  %idxprom951 = sext i32 %352 to i64
  %arrayidx952 = getelementptr inbounds [100 x %class.Lecturer], [100 x %class.Lecturer]* %_L, i64 0, i64 %idxprom951
  %353 = bitcast %class.Lecturer* %arrayidx952 to %class.People*
  invoke void @_ZN6People7getNameB5cxx11Ev(%"class.std::__cxx11::basic_string"* sret(%"class.std::__cxx11::basic_string") align 8 %ref.tmp950, %class.People* nonnull dereferenceable(192) %353)
          to label %invoke.cont953 unwind label %lpad945

invoke.cont953:                                   ; preds = %invoke.cont948
  %call956 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsIcSt11char_traitsIcESaIcEERSt13basic_ostreamIT_T0_ES7_RKNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) %call949, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %ref.tmp950)
          to label %invoke.cont955 unwind label %lpad954

invoke.cont955:                                   ; preds = %invoke.cont953
  %call958 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) %call956, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.62, i64 0, i64 0))
          to label %invoke.cont957 unwind label %lpad954

invoke.cont957:                                   ; preds = %invoke.cont955
  %354 = load i32, i32* %i, align 4
  %idxprom960 = sext i32 %354 to i64
  %arrayidx961 = getelementptr inbounds [100 x %class.Student], [100 x %class.Student]* %_S, i64 0, i64 %idxprom960
  %355 = bitcast %class.Student* %arrayidx961 to %class.People*
  invoke void @_ZN6People10getSurnameB5cxx11Ev(%"class.std::__cxx11::basic_string"* sret(%"class.std::__cxx11::basic_string") align 8 %ref.tmp959, %class.People* nonnull dereferenceable(192) %355)
          to label %invoke.cont962 unwind label %lpad954

invoke.cont962:                                   ; preds = %invoke.cont957
  %call965 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsIcSt11char_traitsIcESaIcEERSt13basic_ostreamIT_T0_ES7_RKNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) %call958, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %ref.tmp959)
          to label %invoke.cont964 unwind label %lpad963

invoke.cont964:                                   ; preds = %invoke.cont962
  %call967 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) %call965, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.62, i64 0, i64 0))
          to label %invoke.cont966 unwind label %lpad963

invoke.cont966:                                   ; preds = %invoke.cont964
  %356 = load i32, i32* %i, align 4
  %idxprom969 = sext i32 %356 to i64
  %arrayidx970 = getelementptr inbounds [100 x %class.Lecturer], [100 x %class.Lecturer]* %_L, i64 0, i64 %idxprom969
  %357 = bitcast %class.Lecturer* %arrayidx970 to %class.People*
  invoke void @_ZN6People13getDepartmentB5cxx11Ev(%"class.std::__cxx11::basic_string"* sret(%"class.std::__cxx11::basic_string") align 8 %ref.tmp968, %class.People* nonnull dereferenceable(192) %357)
          to label %invoke.cont971 unwind label %lpad963

invoke.cont971:                                   ; preds = %invoke.cont966
  %call974 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsIcSt11char_traitsIcESaIcEERSt13basic_ostreamIT_T0_ES7_RKNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) %call967, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %ref.tmp968)
          to label %invoke.cont973 unwind label %lpad972

invoke.cont973:                                   ; preds = %invoke.cont971
  %call976 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) %call974, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.62, i64 0, i64 0))
          to label %invoke.cont975 unwind label %lpad972

invoke.cont975:                                   ; preds = %invoke.cont973
  %358 = load i32, i32* %i, align 4
  %idxprom978 = sext i32 %358 to i64
  %arrayidx979 = getelementptr inbounds [100 x %class.Lecturer], [100 x %class.Lecturer]* %_L, i64 0, i64 %idxprom978
  %359 = bitcast %class.Lecturer* %arrayidx979 to %class.People*
  invoke void @_ZN6People8getEmailB5cxx11Ev(%"class.std::__cxx11::basic_string"* sret(%"class.std::__cxx11::basic_string") align 8 %ref.tmp977, %class.People* nonnull dereferenceable(192) %359)
          to label %invoke.cont980 unwind label %lpad972

invoke.cont980:                                   ; preds = %invoke.cont975
  %call983 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsIcSt11char_traitsIcESaIcEERSt13basic_ostreamIT_T0_ES7_RKNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) %call976, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %ref.tmp977)
          to label %invoke.cont982 unwind label %lpad981

invoke.cont982:                                   ; preds = %invoke.cont980
  %call985 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) %call983, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.62, i64 0, i64 0))
          to label %invoke.cont984 unwind label %lpad981

invoke.cont984:                                   ; preds = %invoke.cont982
  %360 = load i32, i32* %i, align 4
  %idxprom987 = sext i32 %360 to i64
  %arrayidx988 = getelementptr inbounds [100 x %class.Lecturer], [100 x %class.Lecturer]* %_L, i64 0, i64 %idxprom987
  %361 = bitcast %class.Lecturer* %arrayidx988 to %class.People*
  invoke void @_ZN6People8getPhoneB5cxx11Ev(%"class.std::__cxx11::basic_string"* sret(%"class.std::__cxx11::basic_string") align 8 %ref.tmp986, %class.People* nonnull dereferenceable(192) %361)
          to label %invoke.cont989 unwind label %lpad981

invoke.cont989:                                   ; preds = %invoke.cont984
  %call992 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsIcSt11char_traitsIcESaIcEERSt13basic_ostreamIT_T0_ES7_RKNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) %call985, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %ref.tmp986)
          to label %invoke.cont991 unwind label %lpad990

invoke.cont991:                                   ; preds = %invoke.cont989
  %call994 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) %call992, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.62, i64 0, i64 0))
          to label %invoke.cont993 unwind label %lpad990

invoke.cont993:                                   ; preds = %invoke.cont991
  %362 = load i32, i32* %i, align 4
  %idxprom996 = sext i32 %362 to i64
  %arrayidx997 = getelementptr inbounds [100 x %class.Lecturer], [100 x %class.Lecturer]* %_L, i64 0, i64 %idxprom996
  invoke void @_ZN8Lecturer8getChairB5cxx11Ev(%"class.std::__cxx11::basic_string"* sret(%"class.std::__cxx11::basic_string") align 8 %ref.tmp995, %class.Lecturer* nonnull dereferenceable(224) %arrayidx997)
          to label %invoke.cont998 unwind label %lpad990

invoke.cont998:                                   ; preds = %invoke.cont993
  %call1001 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsIcSt11char_traitsIcESaIcEERSt13basic_ostreamIT_T0_ES7_RKNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) %call994, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %ref.tmp995)
          to label %invoke.cont1000 unwind label %lpad999

invoke.cont1000:                                  ; preds = %invoke.cont998
  %call1003 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* nonnull dereferenceable(8) %call1001, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
          to label %invoke.cont1002 unwind label %lpad999

invoke.cont1002:                                  ; preds = %invoke.cont1000
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %ref.tmp995) #3
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %ref.tmp986) #3
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %ref.tmp977) #3
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %ref.tmp968) #3
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %ref.tmp959) #3
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %ref.tmp950) #3
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %ref.tmp941) #3
  br label %for.inc1011

for.inc1011:                                      ; preds = %invoke.cont1002
  %363 = load i32, i32* %i, align 4
  %inc1012 = add nsw i32 %363, 1
  store i32 %inc1012, i32* %i, align 4
  br label %for.cond938, !llvm.loop !23

lpad945:                                          ; preds = %invoke.cont948, %invoke.cont946, %invoke.cont944
  %364 = landingpad { i8*, i32 }
          cleanup
  %365 = extractvalue { i8*, i32 } %364, 0
  store i8* %365, i8** %exn.slot, align 8
  %366 = extractvalue { i8*, i32 } %364, 1
  store i32 %366, i32* %ehselector.slot, align 4
  br label %ehcleanup1010

lpad954:                                          ; preds = %invoke.cont957, %invoke.cont955, %invoke.cont953
  %367 = landingpad { i8*, i32 }
          cleanup
  %368 = extractvalue { i8*, i32 } %367, 0
  store i8* %368, i8** %exn.slot, align 8
  %369 = extractvalue { i8*, i32 } %367, 1
  store i32 %369, i32* %ehselector.slot, align 4
  br label %ehcleanup1009

lpad963:                                          ; preds = %invoke.cont966, %invoke.cont964, %invoke.cont962
  %370 = landingpad { i8*, i32 }
          cleanup
  %371 = extractvalue { i8*, i32 } %370, 0
  store i8* %371, i8** %exn.slot, align 8
  %372 = extractvalue { i8*, i32 } %370, 1
  store i32 %372, i32* %ehselector.slot, align 4
  br label %ehcleanup1008

lpad972:                                          ; preds = %invoke.cont975, %invoke.cont973, %invoke.cont971
  %373 = landingpad { i8*, i32 }
          cleanup
  %374 = extractvalue { i8*, i32 } %373, 0
  store i8* %374, i8** %exn.slot, align 8
  %375 = extractvalue { i8*, i32 } %373, 1
  store i32 %375, i32* %ehselector.slot, align 4
  br label %ehcleanup1007

lpad981:                                          ; preds = %invoke.cont984, %invoke.cont982, %invoke.cont980
  %376 = landingpad { i8*, i32 }
          cleanup
  %377 = extractvalue { i8*, i32 } %376, 0
  store i8* %377, i8** %exn.slot, align 8
  %378 = extractvalue { i8*, i32 } %376, 1
  store i32 %378, i32* %ehselector.slot, align 4
  br label %ehcleanup1006

lpad990:                                          ; preds = %invoke.cont993, %invoke.cont991, %invoke.cont989
  %379 = landingpad { i8*, i32 }
          cleanup
  %380 = extractvalue { i8*, i32 } %379, 0
  store i8* %380, i8** %exn.slot, align 8
  %381 = extractvalue { i8*, i32 } %379, 1
  store i32 %381, i32* %ehselector.slot, align 4
  br label %ehcleanup1005

lpad999:                                          ; preds = %invoke.cont1000, %invoke.cont998
  %382 = landingpad { i8*, i32 }
          cleanup
  %383 = extractvalue { i8*, i32 } %382, 0
  store i8* %383, i8** %exn.slot, align 8
  %384 = extractvalue { i8*, i32 } %382, 1
  store i32 %384, i32* %ehselector.slot, align 4
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %ref.tmp995) #3
  br label %ehcleanup1005

ehcleanup1005:                                    ; preds = %lpad999, %lpad990
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %ref.tmp986) #3
  br label %ehcleanup1006

ehcleanup1006:                                    ; preds = %ehcleanup1005, %lpad981
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %ref.tmp977) #3
  br label %ehcleanup1007

ehcleanup1007:                                    ; preds = %ehcleanup1006, %lpad972
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %ref.tmp968) #3
  br label %ehcleanup1008

ehcleanup1008:                                    ; preds = %ehcleanup1007, %lpad963
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %ref.tmp959) #3
  br label %ehcleanup1009

ehcleanup1009:                                    ; preds = %ehcleanup1008, %lpad954
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %ref.tmp950) #3
  br label %ehcleanup1010

ehcleanup1010:                                    ; preds = %ehcleanup1009, %lpad945
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %ref.tmp941) #3
  br label %ehcleanup1074

for.end1013:                                      ; preds = %for.cond938
  invoke void @_ZNSt14basic_ofstreamIcSt11char_traitsIcEE5closeEv(%"class.std::basic_ofstream"* nonnull dereferenceable(248) %writeToFile)
          to label %invoke.cont1014 unwind label %lpad858

invoke.cont1014:                                  ; preds = %for.end1013
  invoke void @_ZNSt14basic_ofstreamIcSt11char_traitsIcEE4openEPKcSt13_Ios_Openmode(%"class.std::basic_ofstream"* nonnull dereferenceable(248) %writeToFile, i8* getelementptr inbounds ([18 x i8], [18 x i8]* @.str.64, i64 0, i64 0), i32 16)
          to label %invoke.cont1015 unwind label %lpad858

invoke.cont1015:                                  ; preds = %invoke.cont1014
  store i32 0, i32* %i, align 4
  br label %for.cond1016

for.cond1016:                                     ; preds = %for.inc1071, %invoke.cont1015
  %385 = load i32, i32* %i, align 4
  %386 = load i32, i32* %z, align 4
  %cmp1017 = icmp slt i32 %385, %386
  br i1 %cmp1017, label %for.body1018, label %for.end1073

for.body1018:                                     ; preds = %for.cond1016
  %387 = bitcast %"class.std::basic_ofstream"* %writeToFile to %"class.std::basic_ostream"*
  %388 = load i32, i32* %i, align 4
  %idxprom1020 = sext i32 %388 to i64
  %arrayidx1021 = getelementptr inbounds [100 x %class.Appointment], [100 x %class.Appointment]* %_A, i64 0, i64 %idxprom1020
  %S1022 = getelementptr inbounds %class.Appointment, %class.Appointment* %arrayidx1021, i32 0, i32 4
  %389 = bitcast %class.Student* %S1022 to %class.People*
  invoke void @_ZN6People9getNumberB5cxx11Ev(%"class.std::__cxx11::basic_string"* sret(%"class.std::__cxx11::basic_string") align 8 %ref.tmp1019, %class.People* nonnull dereferenceable(192) %389)
          to label %invoke.cont1023 unwind label %lpad858

invoke.cont1023:                                  ; preds = %for.body1018
  %call1026 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsIcSt11char_traitsIcESaIcEERSt13basic_ostreamIT_T0_ES7_RKNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) %387, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %ref.tmp1019)
          to label %invoke.cont1025 unwind label %lpad1024

invoke.cont1025:                                  ; preds = %invoke.cont1023
  %call1028 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) %call1026, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.62, i64 0, i64 0))
          to label %invoke.cont1027 unwind label %lpad1024

invoke.cont1027:                                  ; preds = %invoke.cont1025
  %390 = load i32, i32* %i, align 4
  %idxprom1030 = sext i32 %390 to i64
  %arrayidx1031 = getelementptr inbounds [100 x %class.Appointment], [100 x %class.Appointment]* %_A, i64 0, i64 %idxprom1030
  %L1032 = getelementptr inbounds %class.Appointment, %class.Appointment* %arrayidx1031, i32 0, i32 3
  %391 = bitcast %class.Lecturer* %L1032 to %class.People*
  invoke void @_ZN6People9getNumberB5cxx11Ev(%"class.std::__cxx11::basic_string"* sret(%"class.std::__cxx11::basic_string") align 8 %ref.tmp1029, %class.People* nonnull dereferenceable(192) %391)
          to label %invoke.cont1033 unwind label %lpad1024

invoke.cont1033:                                  ; preds = %invoke.cont1027
  %call1036 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsIcSt11char_traitsIcESaIcEERSt13basic_ostreamIT_T0_ES7_RKNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) %call1028, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %ref.tmp1029)
          to label %invoke.cont1035 unwind label %lpad1034

invoke.cont1035:                                  ; preds = %invoke.cont1033
  %call1038 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) %call1036, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.62, i64 0, i64 0))
          to label %invoke.cont1037 unwind label %lpad1034

invoke.cont1037:                                  ; preds = %invoke.cont1035
  %392 = load i32, i32* %i, align 4
  %idxprom1040 = sext i32 %392 to i64
  %arrayidx1041 = getelementptr inbounds [100 x %class.Appointment], [100 x %class.Appointment]* %_A, i64 0, i64 %idxprom1040
  invoke void @_ZN11Appointment7getDateB5cxx11Ev(%"class.std::__cxx11::basic_string"* sret(%"class.std::__cxx11::basic_string") align 8 %ref.tmp1039, %class.Appointment* nonnull dereferenceable(544) %arrayidx1041)
          to label %invoke.cont1042 unwind label %lpad1034

invoke.cont1042:                                  ; preds = %invoke.cont1037
  %call1045 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsIcSt11char_traitsIcESaIcEERSt13basic_ostreamIT_T0_ES7_RKNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) %call1038, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %ref.tmp1039)
          to label %invoke.cont1044 unwind label %lpad1043

invoke.cont1044:                                  ; preds = %invoke.cont1042
  %call1047 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) %call1045, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.62, i64 0, i64 0))
          to label %invoke.cont1046 unwind label %lpad1043

invoke.cont1046:                                  ; preds = %invoke.cont1044
  %393 = load i32, i32* %i, align 4
  %idxprom1049 = sext i32 %393 to i64
  %arrayidx1050 = getelementptr inbounds [100 x %class.Appointment], [100 x %class.Appointment]* %_A, i64 0, i64 %idxprom1049
  invoke void @_ZN11Appointment8getStartB5cxx11Ev(%"class.std::__cxx11::basic_string"* sret(%"class.std::__cxx11::basic_string") align 8 %ref.tmp1048, %class.Appointment* nonnull dereferenceable(544) %arrayidx1050)
          to label %invoke.cont1051 unwind label %lpad1043

invoke.cont1051:                                  ; preds = %invoke.cont1046
  %call1054 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsIcSt11char_traitsIcESaIcEERSt13basic_ostreamIT_T0_ES7_RKNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) %call1047, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %ref.tmp1048)
          to label %invoke.cont1053 unwind label %lpad1052

invoke.cont1053:                                  ; preds = %invoke.cont1051
  %call1056 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) %call1054, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.62, i64 0, i64 0))
          to label %invoke.cont1055 unwind label %lpad1052

invoke.cont1055:                                  ; preds = %invoke.cont1053
  %394 = load i32, i32* %i, align 4
  %idxprom1058 = sext i32 %394 to i64
  %arrayidx1059 = getelementptr inbounds [100 x %class.Appointment], [100 x %class.Appointment]* %_A, i64 0, i64 %idxprom1058
  invoke void @_ZN11Appointment6getEndB5cxx11Ev(%"class.std::__cxx11::basic_string"* sret(%"class.std::__cxx11::basic_string") align 8 %ref.tmp1057, %class.Appointment* nonnull dereferenceable(544) %arrayidx1059)
          to label %invoke.cont1060 unwind label %lpad1052

invoke.cont1060:                                  ; preds = %invoke.cont1055
  %call1063 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsIcSt11char_traitsIcESaIcEERSt13basic_ostreamIT_T0_ES7_RKNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) %call1056, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %ref.tmp1057)
          to label %invoke.cont1062 unwind label %lpad1061

invoke.cont1062:                                  ; preds = %invoke.cont1060
  %call1065 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* nonnull dereferenceable(8) %call1063, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
          to label %invoke.cont1064 unwind label %lpad1061

invoke.cont1064:                                  ; preds = %invoke.cont1062
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %ref.tmp1057) #3
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %ref.tmp1048) #3
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %ref.tmp1039) #3
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %ref.tmp1029) #3
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %ref.tmp1019) #3
  br label %for.inc1071

for.inc1071:                                      ; preds = %invoke.cont1064
  %395 = load i32, i32* %i, align 4
  %inc1072 = add nsw i32 %395, 1
  store i32 %inc1072, i32* %i, align 4
  br label %for.cond1016, !llvm.loop !24

lpad1024:                                         ; preds = %invoke.cont1027, %invoke.cont1025, %invoke.cont1023
  %396 = landingpad { i8*, i32 }
          cleanup
  %397 = extractvalue { i8*, i32 } %396, 0
  store i8* %397, i8** %exn.slot, align 8
  %398 = extractvalue { i8*, i32 } %396, 1
  store i32 %398, i32* %ehselector.slot, align 4
  br label %ehcleanup1070

lpad1034:                                         ; preds = %invoke.cont1037, %invoke.cont1035, %invoke.cont1033
  %399 = landingpad { i8*, i32 }
          cleanup
  %400 = extractvalue { i8*, i32 } %399, 0
  store i8* %400, i8** %exn.slot, align 8
  %401 = extractvalue { i8*, i32 } %399, 1
  store i32 %401, i32* %ehselector.slot, align 4
  br label %ehcleanup1069

lpad1043:                                         ; preds = %invoke.cont1046, %invoke.cont1044, %invoke.cont1042
  %402 = landingpad { i8*, i32 }
          cleanup
  %403 = extractvalue { i8*, i32 } %402, 0
  store i8* %403, i8** %exn.slot, align 8
  %404 = extractvalue { i8*, i32 } %402, 1
  store i32 %404, i32* %ehselector.slot, align 4
  br label %ehcleanup1068

lpad1052:                                         ; preds = %invoke.cont1055, %invoke.cont1053, %invoke.cont1051
  %405 = landingpad { i8*, i32 }
          cleanup
  %406 = extractvalue { i8*, i32 } %405, 0
  store i8* %406, i8** %exn.slot, align 8
  %407 = extractvalue { i8*, i32 } %405, 1
  store i32 %407, i32* %ehselector.slot, align 4
  br label %ehcleanup1067

lpad1061:                                         ; preds = %invoke.cont1062, %invoke.cont1060
  %408 = landingpad { i8*, i32 }
          cleanup
  %409 = extractvalue { i8*, i32 } %408, 0
  store i8* %409, i8** %exn.slot, align 8
  %410 = extractvalue { i8*, i32 } %408, 1
  store i32 %410, i32* %ehselector.slot, align 4
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %ref.tmp1057) #3
  br label %ehcleanup1067

ehcleanup1067:                                    ; preds = %lpad1061, %lpad1052
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %ref.tmp1048) #3
  br label %ehcleanup1068

ehcleanup1068:                                    ; preds = %ehcleanup1067, %lpad1043
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %ref.tmp1039) #3
  br label %ehcleanup1069

ehcleanup1069:                                    ; preds = %ehcleanup1068, %lpad1034
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %ref.tmp1029) #3
  br label %ehcleanup1070

ehcleanup1070:                                    ; preds = %ehcleanup1069, %lpad1024
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %ref.tmp1019) #3
  br label %ehcleanup1074

for.end1073:                                      ; preds = %for.cond1016
  call void @_ZNSt14basic_ofstreamIcSt11char_traitsIcEED1Ev(%"class.std::basic_ofstream"* nonnull dereferenceable(248) %writeToFile) #3
  br label %if.end1088

ehcleanup1074:                                    ; preds = %ehcleanup1070, %ehcleanup1010, %ehcleanup932, %lpad858
  call void @_ZNSt14basic_ofstreamIcSt11char_traitsIcEED1Ev(%"class.std::basic_ofstream"* nonnull dereferenceable(248) %writeToFile) #3
  br label %ehcleanup1099

if.else1075:                                      ; preds = %if.else854
  %411 = load i32, i32* %menuLoop, align 4
  %cmp1076 = icmp eq i32 %411, 5
  br i1 %cmp1076, label %if.then1077, label %if.else1082

if.then1077:                                      ; preds = %if.else1075
  %call1079 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) @_ZSt4cout, i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str.65, i64 0, i64 0))
          to label %invoke.cont1078 unwind label %lpad37

invoke.cont1078:                                  ; preds = %if.then1077
  %call1081 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* nonnull dereferenceable(8) %call1079, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
          to label %invoke.cont1080 unwind label %lpad37

invoke.cont1080:                                  ; preds = %invoke.cont1078
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.else1082:                                      ; preds = %if.else1075
  %call1084 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) @_ZSt4cout, i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.31, i64 0, i64 0))
          to label %invoke.cont1083 unwind label %lpad37

invoke.cont1083:                                  ; preds = %if.else1082
  %call1086 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* nonnull dereferenceable(8) %call1084, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
          to label %invoke.cont1085 unwind label %lpad37

invoke.cont1085:                                  ; preds = %invoke.cont1083
  br label %if.end1087

if.end1087:                                       ; preds = %invoke.cont1085
  br label %if.end1088

if.end1088:                                       ; preds = %if.end1087, %for.end1073
  br label %if.end1089

if.end1089:                                       ; preds = %if.end1088, %if.end853
  br label %if.end1090

if.end1090:                                       ; preds = %if.end1089, %if.end600
  br label %if.end1091

if.end1091:                                       ; preds = %if.end1090, %if.end325
  br label %while.cond, !llvm.loop !25

while.end1092:                                    ; preds = %while.cond
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %while.end1092, %invoke.cont1080
  %array.begin1093 = getelementptr inbounds [100 x %class.Appointment], [100 x %class.Appointment]* %_A, i32 0, i32 0
  %412 = getelementptr inbounds %class.Appointment, %class.Appointment* %array.begin1093, i64 100
  br label %arraydestroy.body1094

arraydestroy.body1094:                            ; preds = %arraydestroy.body1094, %cleanup
  %arraydestroy.elementPast1095 = phi %class.Appointment* [ %412, %cleanup ], [ %arraydestroy.element1096, %arraydestroy.body1094 ]
  %arraydestroy.element1096 = getelementptr inbounds %class.Appointment, %class.Appointment* %arraydestroy.elementPast1095, i64 -1
  call void @_ZN11AppointmentD2Ev(%class.Appointment* nonnull dereferenceable(544) %arraydestroy.element1096) #3
  %arraydestroy.done1097 = icmp eq %class.Appointment* %arraydestroy.element1096, %array.begin1093
  br i1 %arraydestroy.done1097, label %arraydestroy.done1098, label %arraydestroy.body1094

arraydestroy.done1098:                            ; preds = %arraydestroy.body1094
  %array.begin1107 = getelementptr inbounds [100 x %class.Lecturer], [100 x %class.Lecturer]* %_L, i32 0, i32 0
  %413 = getelementptr inbounds %class.Lecturer, %class.Lecturer* %array.begin1107, i64 100
  br label %arraydestroy.body1108

ehcleanup1099:                                    ; preds = %ehcleanup1074, %ehcleanup843, %ehcleanup756, %ehcleanup689, %ehcleanup591, %lpad456, %ehcleanup436, %ehcleanup316, %lpad185, %ehcleanup171, %lpad37
  %array.begin1100 = getelementptr inbounds [100 x %class.Appointment], [100 x %class.Appointment]* %_A, i32 0, i32 0
  %414 = getelementptr inbounds %class.Appointment, %class.Appointment* %array.begin1100, i64 100
  br label %arraydestroy.body1101

arraydestroy.body1101:                            ; preds = %arraydestroy.body1101, %ehcleanup1099
  %arraydestroy.elementPast1102 = phi %class.Appointment* [ %414, %ehcleanup1099 ], [ %arraydestroy.element1103, %arraydestroy.body1101 ]
  %arraydestroy.element1103 = getelementptr inbounds %class.Appointment, %class.Appointment* %arraydestroy.elementPast1102, i64 -1
  call void @_ZN11AppointmentD2Ev(%class.Appointment* nonnull dereferenceable(544) %arraydestroy.element1103) #3
  %arraydestroy.done1104 = icmp eq %class.Appointment* %arraydestroy.element1103, %array.begin1100
  br i1 %arraydestroy.done1104, label %arraydestroy.done1105, label %arraydestroy.body1101

arraydestroy.done1105:                            ; preds = %arraydestroy.body1101
  br label %ehcleanup1113

arraydestroy.body1108:                            ; preds = %arraydestroy.body1108, %arraydestroy.done1098
  %arraydestroy.elementPast1109 = phi %class.Lecturer* [ %413, %arraydestroy.done1098 ], [ %arraydestroy.element1110, %arraydestroy.body1108 ]
  %arraydestroy.element1110 = getelementptr inbounds %class.Lecturer, %class.Lecturer* %arraydestroy.elementPast1109, i64 -1
  call void @_ZN8LecturerD2Ev(%class.Lecturer* nonnull dereferenceable(224) %arraydestroy.element1110) #3
  %arraydestroy.done1111 = icmp eq %class.Lecturer* %arraydestroy.element1110, %array.begin1107
  br i1 %arraydestroy.done1111, label %arraydestroy.done1112, label %arraydestroy.body1108

arraydestroy.done1112:                            ; preds = %arraydestroy.body1108
  %array.begin1121 = getelementptr inbounds [100 x %class.Student], [100 x %class.Student]* %_S, i32 0, i32 0
  %415 = getelementptr inbounds %class.Student, %class.Student* %array.begin1121, i64 100
  br label %arraydestroy.body1122

ehcleanup1113:                                    ; preds = %arraydestroy.done1105, %arraydestroy.done31
  %array.begin1114 = getelementptr inbounds [100 x %class.Lecturer], [100 x %class.Lecturer]* %_L, i32 0, i32 0
  %416 = getelementptr inbounds %class.Lecturer, %class.Lecturer* %array.begin1114, i64 100
  br label %arraydestroy.body1115

arraydestroy.body1115:                            ; preds = %arraydestroy.body1115, %ehcleanup1113
  %arraydestroy.elementPast1116 = phi %class.Lecturer* [ %416, %ehcleanup1113 ], [ %arraydestroy.element1117, %arraydestroy.body1115 ]
  %arraydestroy.element1117 = getelementptr inbounds %class.Lecturer, %class.Lecturer* %arraydestroy.elementPast1116, i64 -1
  call void @_ZN8LecturerD2Ev(%class.Lecturer* nonnull dereferenceable(224) %arraydestroy.element1117) #3
  %arraydestroy.done1118 = icmp eq %class.Lecturer* %arraydestroy.element1117, %array.begin1114
  br i1 %arraydestroy.done1118, label %arraydestroy.done1119, label %arraydestroy.body1115

arraydestroy.done1119:                            ; preds = %arraydestroy.body1115
  br label %ehcleanup1127

arraydestroy.body1122:                            ; preds = %arraydestroy.body1122, %arraydestroy.done1112
  %arraydestroy.elementPast1123 = phi %class.Student* [ %415, %arraydestroy.done1112 ], [ %arraydestroy.element1124, %arraydestroy.body1122 ]
  %arraydestroy.element1124 = getelementptr inbounds %class.Student, %class.Student* %arraydestroy.elementPast1123, i64 -1
  call void @_ZN7StudentD2Ev(%class.Student* nonnull dereferenceable(224) %arraydestroy.element1124) #3
  %arraydestroy.done1125 = icmp eq %class.Student* %arraydestroy.element1124, %array.begin1121
  br i1 %arraydestroy.done1125, label %arraydestroy.done1126, label %arraydestroy.body1122

arraydestroy.done1126:                            ; preds = %arraydestroy.body1122
  br label %return

ehcleanup1127:                                    ; preds = %arraydestroy.done1119, %arraydestroy.done16
  %array.begin1128 = getelementptr inbounds [100 x %class.Student], [100 x %class.Student]* %_S, i32 0, i32 0
  %417 = getelementptr inbounds %class.Student, %class.Student* %array.begin1128, i64 100
  br label %arraydestroy.body1129

arraydestroy.body1129:                            ; preds = %arraydestroy.body1129, %ehcleanup1127
  %arraydestroy.elementPast1130 = phi %class.Student* [ %417, %ehcleanup1127 ], [ %arraydestroy.element1131, %arraydestroy.body1129 ]
  %arraydestroy.element1131 = getelementptr inbounds %class.Student, %class.Student* %arraydestroy.elementPast1130, i64 -1
  call void @_ZN7StudentD2Ev(%class.Student* nonnull dereferenceable(224) %arraydestroy.element1131) #3
  %arraydestroy.done1132 = icmp eq %class.Student* %arraydestroy.element1131, %array.begin1128
  br i1 %arraydestroy.done1132, label %arraydestroy.done1133, label %arraydestroy.body1129

arraydestroy.done1133:                            ; preds = %arraydestroy.body1129
  br label %eh.resume

return:                                           ; preds = %arraydestroy.done1126, %if.then
  %418 = load i32, i32* %retval, align 4
  ret i32 %418

eh.resume:                                        ; preds = %arraydestroy.done1133, %arraydestroy.done4
  %exn = load i8*, i8** %exn.slot, align 8
  %sel = load i32, i32* %ehselector.slot, align 4
  %lpad.val = insertvalue { i8*, i32 } undef, i8* %exn, 0
  %lpad.val1134 = insertvalue { i8*, i32 } %lpad.val, i32 %sel, 1
  resume { i8*, i32 } %lpad.val1134
}

; Function Attrs: noinline optnone uwtable
define linkonce_odr dso_local void @_ZN11AppointmentC2Ev(%class.Appointment* nonnull dereferenceable(544) %this) unnamed_addr #4 comdat align 2 personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
entry:
  %this.addr = alloca %class.Appointment*, align 8
  %exn.slot = alloca i8*, align 8
  %ehselector.slot = alloca i32, align 4
  store %class.Appointment* %this, %class.Appointment** %this.addr, align 8
  %this1 = load %class.Appointment*, %class.Appointment** %this.addr, align 8
  %date = getelementptr inbounds %class.Appointment, %class.Appointment* %this1, i32 0, i32 0
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %date) #3
  %start = getelementptr inbounds %class.Appointment, %class.Appointment* %this1, i32 0, i32 1
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %start) #3
  %end = getelementptr inbounds %class.Appointment, %class.Appointment* %this1, i32 0, i32 2
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %end) #3
  %L = getelementptr inbounds %class.Appointment, %class.Appointment* %this1, i32 0, i32 3
  invoke void @_ZN8LecturerC2Ev(%class.Lecturer* nonnull dereferenceable(224) %L)
          to label %invoke.cont unwind label %lpad

invoke.cont:                                      ; preds = %entry
  %S = getelementptr inbounds %class.Appointment, %class.Appointment* %this1, i32 0, i32 4
  invoke void @_ZN7StudentC2Ev(%class.Student* nonnull dereferenceable(224) %S)
          to label %invoke.cont3 unwind label %lpad2

invoke.cont3:                                     ; preds = %invoke.cont
  ret void

lpad:                                             ; preds = %entry
  %0 = landingpad { i8*, i32 }
          cleanup
  %1 = extractvalue { i8*, i32 } %0, 0
  store i8* %1, i8** %exn.slot, align 8
  %2 = extractvalue { i8*, i32 } %0, 1
  store i32 %2, i32* %ehselector.slot, align 4
  br label %ehcleanup

lpad2:                                            ; preds = %invoke.cont
  %3 = landingpad { i8*, i32 }
          cleanup
  %4 = extractvalue { i8*, i32 } %3, 0
  store i8* %4, i8** %exn.slot, align 8
  %5 = extractvalue { i8*, i32 } %3, 1
  store i32 %5, i32* %ehselector.slot, align 4
  call void @_ZN8LecturerD2Ev(%class.Lecturer* nonnull dereferenceable(224) %L) #3
  br label %ehcleanup

ehcleanup:                                        ; preds = %lpad2, %lpad
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %end) #3
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %start) #3
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %date) #3
  br label %eh.resume

eh.resume:                                        ; preds = %ehcleanup
  %exn = load i8*, i8** %exn.slot, align 8
  %sel = load i32, i32* %ehselector.slot, align 4
  %lpad.val = insertvalue { i8*, i32 } undef, i8* %exn, 0
  %lpad.val6 = insertvalue { i8*, i32 } %lpad.val, i32 %sel, 1
  resume { i8*, i32 } %lpad.val6
}

declare dso_local nonnull align 8 dereferenceable(16) %"class.std::basic_istream"* @_ZNSirsERi(%"class.std::basic_istream"* nonnull dereferenceable(16), i32* nonnull align 4 dereferenceable(4)) #1

; Function Attrs: noinline optnone uwtable mustprogress
define linkonce_odr dso_local void @_ZN7Student7displayEv(%class.Student* nonnull dereferenceable(224) %this) #6 comdat align 2 personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
entry:
  %this.addr = alloca %class.Student*, align 8
  %ref.tmp = alloca %"class.std::__cxx11::basic_string", align 8
  %exn.slot = alloca i8*, align 8
  %ehselector.slot = alloca i32, align 4
  %ref.tmp4 = alloca %"class.std::__cxx11::basic_string", align 8
  %ref.tmp10 = alloca %"class.std::__cxx11::basic_string", align 8
  %ref.tmp16 = alloca %"class.std::__cxx11::basic_string", align 8
  %ref.tmp22 = alloca %"class.std::__cxx11::basic_string", align 8
  %ref.tmp28 = alloca %"class.std::__cxx11::basic_string", align 8
  %ref.tmp34 = alloca %"class.std::__cxx11::basic_string", align 8
  store %class.Student* %this, %class.Student** %this.addr, align 8
  %this1 = load %class.Student*, %class.Student** %this.addr, align 8
  %0 = bitcast %class.Student* %this1 to %class.People*
  call void @_ZN6People9getNumberB5cxx11Ev(%"class.std::__cxx11::basic_string"* sret(%"class.std::__cxx11::basic_string") align 8 %ref.tmp, %class.People* nonnull dereferenceable(192) %0)
  %call = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsIcSt11char_traitsIcESaIcEERSt13basic_ostreamIT_T0_ES7_RKNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) @_ZSt4cout, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %ref.tmp)
          to label %invoke.cont unwind label %lpad

invoke.cont:                                      ; preds = %entry
  %call3 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) %call, i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.66, i64 0, i64 0))
          to label %invoke.cont2 unwind label %lpad

invoke.cont2:                                     ; preds = %invoke.cont
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %ref.tmp) #3
  %1 = bitcast %class.Student* %this1 to %class.People*
  call void @_ZN6People7getNameB5cxx11Ev(%"class.std::__cxx11::basic_string"* sret(%"class.std::__cxx11::basic_string") align 8 %ref.tmp4, %class.People* nonnull dereferenceable(192) %1)
  %call7 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsIcSt11char_traitsIcESaIcEERSt13basic_ostreamIT_T0_ES7_RKNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) @_ZSt4cout, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %ref.tmp4)
          to label %invoke.cont6 unwind label %lpad5

invoke.cont6:                                     ; preds = %invoke.cont2
  %call9 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) %call7, i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.66, i64 0, i64 0))
          to label %invoke.cont8 unwind label %lpad5

invoke.cont8:                                     ; preds = %invoke.cont6
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %ref.tmp4) #3
  %2 = bitcast %class.Student* %this1 to %class.People*
  call void @_ZN6People10getSurnameB5cxx11Ev(%"class.std::__cxx11::basic_string"* sret(%"class.std::__cxx11::basic_string") align 8 %ref.tmp10, %class.People* nonnull dereferenceable(192) %2)
  %call13 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsIcSt11char_traitsIcESaIcEERSt13basic_ostreamIT_T0_ES7_RKNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) @_ZSt4cout, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %ref.tmp10)
          to label %invoke.cont12 unwind label %lpad11

invoke.cont12:                                    ; preds = %invoke.cont8
  %call15 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) %call13, i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.66, i64 0, i64 0))
          to label %invoke.cont14 unwind label %lpad11

invoke.cont14:                                    ; preds = %invoke.cont12
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %ref.tmp10) #3
  %3 = bitcast %class.Student* %this1 to %class.People*
  call void @_ZN6People13getDepartmentB5cxx11Ev(%"class.std::__cxx11::basic_string"* sret(%"class.std::__cxx11::basic_string") align 8 %ref.tmp16, %class.People* nonnull dereferenceable(192) %3)
  %call19 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsIcSt11char_traitsIcESaIcEERSt13basic_ostreamIT_T0_ES7_RKNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) @_ZSt4cout, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %ref.tmp16)
          to label %invoke.cont18 unwind label %lpad17

invoke.cont18:                                    ; preds = %invoke.cont14
  %call21 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) %call19, i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.66, i64 0, i64 0))
          to label %invoke.cont20 unwind label %lpad17

invoke.cont20:                                    ; preds = %invoke.cont18
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %ref.tmp16) #3
  call void @_ZN7Student7getYearB5cxx11Ev(%"class.std::__cxx11::basic_string"* sret(%"class.std::__cxx11::basic_string") align 8 %ref.tmp22, %class.Student* nonnull dereferenceable(224) %this1)
  %call25 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsIcSt11char_traitsIcESaIcEERSt13basic_ostreamIT_T0_ES7_RKNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) @_ZSt4cout, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %ref.tmp22)
          to label %invoke.cont24 unwind label %lpad23

invoke.cont24:                                    ; preds = %invoke.cont20
  %call27 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) %call25, i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.66, i64 0, i64 0))
          to label %invoke.cont26 unwind label %lpad23

invoke.cont26:                                    ; preds = %invoke.cont24
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %ref.tmp22) #3
  %4 = bitcast %class.Student* %this1 to %class.People*
  call void @_ZN6People8getEmailB5cxx11Ev(%"class.std::__cxx11::basic_string"* sret(%"class.std::__cxx11::basic_string") align 8 %ref.tmp28, %class.People* nonnull dereferenceable(192) %4)
  %call31 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsIcSt11char_traitsIcESaIcEERSt13basic_ostreamIT_T0_ES7_RKNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) @_ZSt4cout, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %ref.tmp28)
          to label %invoke.cont30 unwind label %lpad29

invoke.cont30:                                    ; preds = %invoke.cont26
  %call33 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) %call31, i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.66, i64 0, i64 0))
          to label %invoke.cont32 unwind label %lpad29

invoke.cont32:                                    ; preds = %invoke.cont30
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %ref.tmp28) #3
  %5 = bitcast %class.Student* %this1 to %class.People*
  call void @_ZN6People8getPhoneB5cxx11Ev(%"class.std::__cxx11::basic_string"* sret(%"class.std::__cxx11::basic_string") align 8 %ref.tmp34, %class.People* nonnull dereferenceable(192) %5)
  %call37 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsIcSt11char_traitsIcESaIcEERSt13basic_ostreamIT_T0_ES7_RKNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) @_ZSt4cout, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %ref.tmp34)
          to label %invoke.cont36 unwind label %lpad35

invoke.cont36:                                    ; preds = %invoke.cont32
  %call39 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* nonnull dereferenceable(8) %call37, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
          to label %invoke.cont38 unwind label %lpad35

invoke.cont38:                                    ; preds = %invoke.cont36
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %ref.tmp34) #3
  ret void

lpad:                                             ; preds = %invoke.cont, %entry
  %6 = landingpad { i8*, i32 }
          cleanup
  %7 = extractvalue { i8*, i32 } %6, 0
  store i8* %7, i8** %exn.slot, align 8
  %8 = extractvalue { i8*, i32 } %6, 1
  store i32 %8, i32* %ehselector.slot, align 4
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %ref.tmp) #3
  br label %eh.resume

lpad5:                                            ; preds = %invoke.cont6, %invoke.cont2
  %9 = landingpad { i8*, i32 }
          cleanup
  %10 = extractvalue { i8*, i32 } %9, 0
  store i8* %10, i8** %exn.slot, align 8
  %11 = extractvalue { i8*, i32 } %9, 1
  store i32 %11, i32* %ehselector.slot, align 4
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %ref.tmp4) #3
  br label %eh.resume

lpad11:                                           ; preds = %invoke.cont12, %invoke.cont8
  %12 = landingpad { i8*, i32 }
          cleanup
  %13 = extractvalue { i8*, i32 } %12, 0
  store i8* %13, i8** %exn.slot, align 8
  %14 = extractvalue { i8*, i32 } %12, 1
  store i32 %14, i32* %ehselector.slot, align 4
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %ref.tmp10) #3
  br label %eh.resume

lpad17:                                           ; preds = %invoke.cont18, %invoke.cont14
  %15 = landingpad { i8*, i32 }
          cleanup
  %16 = extractvalue { i8*, i32 } %15, 0
  store i8* %16, i8** %exn.slot, align 8
  %17 = extractvalue { i8*, i32 } %15, 1
  store i32 %17, i32* %ehselector.slot, align 4
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %ref.tmp16) #3
  br label %eh.resume

lpad23:                                           ; preds = %invoke.cont24, %invoke.cont20
  %18 = landingpad { i8*, i32 }
          cleanup
  %19 = extractvalue { i8*, i32 } %18, 0
  store i8* %19, i8** %exn.slot, align 8
  %20 = extractvalue { i8*, i32 } %18, 1
  store i32 %20, i32* %ehselector.slot, align 4
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %ref.tmp22) #3
  br label %eh.resume

lpad29:                                           ; preds = %invoke.cont30, %invoke.cont26
  %21 = landingpad { i8*, i32 }
          cleanup
  %22 = extractvalue { i8*, i32 } %21, 0
  store i8* %22, i8** %exn.slot, align 8
  %23 = extractvalue { i8*, i32 } %21, 1
  store i32 %23, i32* %ehselector.slot, align 4
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %ref.tmp28) #3
  br label %eh.resume

lpad35:                                           ; preds = %invoke.cont36, %invoke.cont32
  %24 = landingpad { i8*, i32 }
          cleanup
  %25 = extractvalue { i8*, i32 } %24, 0
  store i8* %25, i8** %exn.slot, align 8
  %26 = extractvalue { i8*, i32 } %24, 1
  store i32 %26, i32* %ehselector.slot, align 4
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %ref.tmp34) #3
  br label %eh.resume

eh.resume:                                        ; preds = %lpad35, %lpad29, %lpad23, %lpad17, %lpad11, %lpad5, %lpad
  %exn = load i8*, i8** %exn.slot, align 8
  %sel = load i32, i32* %ehselector.slot, align 4
  %lpad.val = insertvalue { i8*, i32 } undef, i8* %exn, 0
  %lpad.val40 = insertvalue { i8*, i32 } %lpad.val, i32 %sel, 1
  resume { i8*, i32 } %lpad.val40
}

; Function Attrs: noinline optnone uwtable mustprogress
define linkonce_odr dso_local void @_ZN6People9getNumberB5cxx11Ev(%"class.std::__cxx11::basic_string"* noalias sret(%"class.std::__cxx11::basic_string") align 8 %agg.result, %class.People* nonnull dereferenceable(192) %this) #6 comdat align 2 {
entry:
  %result.ptr = alloca i8*, align 8
  %this.addr = alloca %class.People*, align 8
  %0 = bitcast %"class.std::__cxx11::basic_string"* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 8
  store %class.People* %this, %class.People** %this.addr, align 8
  %this1 = load %class.People*, %class.People** %this.addr, align 8
  %number = getelementptr inbounds %class.People, %class.People* %this1, i32 0, i32 0
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1ERKS4_(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.result, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %number)
  ret void
}

; Function Attrs: noinline optnone uwtable mustprogress
define linkonce_odr dso_local void @_ZN8Lecturer7displayEv(%class.Lecturer* nonnull dereferenceable(224) %this) #6 comdat align 2 personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
entry:
  %this.addr = alloca %class.Lecturer*, align 8
  %ref.tmp = alloca %"class.std::__cxx11::basic_string", align 8
  %exn.slot = alloca i8*, align 8
  %ehselector.slot = alloca i32, align 4
  %ref.tmp4 = alloca %"class.std::__cxx11::basic_string", align 8
  %ref.tmp10 = alloca %"class.std::__cxx11::basic_string", align 8
  %ref.tmp16 = alloca %"class.std::__cxx11::basic_string", align 8
  %ref.tmp22 = alloca %"class.std::__cxx11::basic_string", align 8
  %ref.tmp28 = alloca %"class.std::__cxx11::basic_string", align 8
  %ref.tmp34 = alloca %"class.std::__cxx11::basic_string", align 8
  store %class.Lecturer* %this, %class.Lecturer** %this.addr, align 8
  %this1 = load %class.Lecturer*, %class.Lecturer** %this.addr, align 8
  %0 = bitcast %class.Lecturer* %this1 to %class.People*
  call void @_ZN6People9getNumberB5cxx11Ev(%"class.std::__cxx11::basic_string"* sret(%"class.std::__cxx11::basic_string") align 8 %ref.tmp, %class.People* nonnull dereferenceable(192) %0)
  %call = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsIcSt11char_traitsIcESaIcEERSt13basic_ostreamIT_T0_ES7_RKNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) @_ZSt4cout, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %ref.tmp)
          to label %invoke.cont unwind label %lpad

invoke.cont:                                      ; preds = %entry
  %call3 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) %call, i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.66, i64 0, i64 0))
          to label %invoke.cont2 unwind label %lpad

invoke.cont2:                                     ; preds = %invoke.cont
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %ref.tmp) #3
  %1 = bitcast %class.Lecturer* %this1 to %class.People*
  call void @_ZN6People7getNameB5cxx11Ev(%"class.std::__cxx11::basic_string"* sret(%"class.std::__cxx11::basic_string") align 8 %ref.tmp4, %class.People* nonnull dereferenceable(192) %1)
  %call7 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsIcSt11char_traitsIcESaIcEERSt13basic_ostreamIT_T0_ES7_RKNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) @_ZSt4cout, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %ref.tmp4)
          to label %invoke.cont6 unwind label %lpad5

invoke.cont6:                                     ; preds = %invoke.cont2
  %call9 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) %call7, i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.66, i64 0, i64 0))
          to label %invoke.cont8 unwind label %lpad5

invoke.cont8:                                     ; preds = %invoke.cont6
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %ref.tmp4) #3
  %2 = bitcast %class.Lecturer* %this1 to %class.People*
  call void @_ZN6People10getSurnameB5cxx11Ev(%"class.std::__cxx11::basic_string"* sret(%"class.std::__cxx11::basic_string") align 8 %ref.tmp10, %class.People* nonnull dereferenceable(192) %2)
  %call13 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsIcSt11char_traitsIcESaIcEERSt13basic_ostreamIT_T0_ES7_RKNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) @_ZSt4cout, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %ref.tmp10)
          to label %invoke.cont12 unwind label %lpad11

invoke.cont12:                                    ; preds = %invoke.cont8
  %call15 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) %call13, i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.66, i64 0, i64 0))
          to label %invoke.cont14 unwind label %lpad11

invoke.cont14:                                    ; preds = %invoke.cont12
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %ref.tmp10) #3
  %3 = bitcast %class.Lecturer* %this1 to %class.People*
  call void @_ZN6People13getDepartmentB5cxx11Ev(%"class.std::__cxx11::basic_string"* sret(%"class.std::__cxx11::basic_string") align 8 %ref.tmp16, %class.People* nonnull dereferenceable(192) %3)
  %call19 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsIcSt11char_traitsIcESaIcEERSt13basic_ostreamIT_T0_ES7_RKNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) @_ZSt4cout, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %ref.tmp16)
          to label %invoke.cont18 unwind label %lpad17

invoke.cont18:                                    ; preds = %invoke.cont14
  %call21 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) %call19, i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.66, i64 0, i64 0))
          to label %invoke.cont20 unwind label %lpad17

invoke.cont20:                                    ; preds = %invoke.cont18
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %ref.tmp16) #3
  %4 = bitcast %class.Lecturer* %this1 to %class.People*
  call void @_ZN6People8getEmailB5cxx11Ev(%"class.std::__cxx11::basic_string"* sret(%"class.std::__cxx11::basic_string") align 8 %ref.tmp22, %class.People* nonnull dereferenceable(192) %4)
  %call25 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsIcSt11char_traitsIcESaIcEERSt13basic_ostreamIT_T0_ES7_RKNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) @_ZSt4cout, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %ref.tmp22)
          to label %invoke.cont24 unwind label %lpad23

invoke.cont24:                                    ; preds = %invoke.cont20
  %call27 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) %call25, i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.66, i64 0, i64 0))
          to label %invoke.cont26 unwind label %lpad23

invoke.cont26:                                    ; preds = %invoke.cont24
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %ref.tmp22) #3
  %5 = bitcast %class.Lecturer* %this1 to %class.People*
  call void @_ZN6People8getPhoneB5cxx11Ev(%"class.std::__cxx11::basic_string"* sret(%"class.std::__cxx11::basic_string") align 8 %ref.tmp28, %class.People* nonnull dereferenceable(192) %5)
  %call31 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsIcSt11char_traitsIcESaIcEERSt13basic_ostreamIT_T0_ES7_RKNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) @_ZSt4cout, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %ref.tmp28)
          to label %invoke.cont30 unwind label %lpad29

invoke.cont30:                                    ; preds = %invoke.cont26
  %call33 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) %call31, i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.66, i64 0, i64 0))
          to label %invoke.cont32 unwind label %lpad29

invoke.cont32:                                    ; preds = %invoke.cont30
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %ref.tmp28) #3
  call void @_ZN8Lecturer8getChairB5cxx11Ev(%"class.std::__cxx11::basic_string"* sret(%"class.std::__cxx11::basic_string") align 8 %ref.tmp34, %class.Lecturer* nonnull dereferenceable(224) %this1)
  %call37 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsIcSt11char_traitsIcESaIcEERSt13basic_ostreamIT_T0_ES7_RKNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) @_ZSt4cout, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %ref.tmp34)
          to label %invoke.cont36 unwind label %lpad35

invoke.cont36:                                    ; preds = %invoke.cont32
  %call39 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* nonnull dereferenceable(8) %call37, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
          to label %invoke.cont38 unwind label %lpad35

invoke.cont38:                                    ; preds = %invoke.cont36
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %ref.tmp34) #3
  ret void

lpad:                                             ; preds = %invoke.cont, %entry
  %6 = landingpad { i8*, i32 }
          cleanup
  %7 = extractvalue { i8*, i32 } %6, 0
  store i8* %7, i8** %exn.slot, align 8
  %8 = extractvalue { i8*, i32 } %6, 1
  store i32 %8, i32* %ehselector.slot, align 4
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %ref.tmp) #3
  br label %eh.resume

lpad5:                                            ; preds = %invoke.cont6, %invoke.cont2
  %9 = landingpad { i8*, i32 }
          cleanup
  %10 = extractvalue { i8*, i32 } %9, 0
  store i8* %10, i8** %exn.slot, align 8
  %11 = extractvalue { i8*, i32 } %9, 1
  store i32 %11, i32* %ehselector.slot, align 4
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %ref.tmp4) #3
  br label %eh.resume

lpad11:                                           ; preds = %invoke.cont12, %invoke.cont8
  %12 = landingpad { i8*, i32 }
          cleanup
  %13 = extractvalue { i8*, i32 } %12, 0
  store i8* %13, i8** %exn.slot, align 8
  %14 = extractvalue { i8*, i32 } %12, 1
  store i32 %14, i32* %ehselector.slot, align 4
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %ref.tmp10) #3
  br label %eh.resume

lpad17:                                           ; preds = %invoke.cont18, %invoke.cont14
  %15 = landingpad { i8*, i32 }
          cleanup
  %16 = extractvalue { i8*, i32 } %15, 0
  store i8* %16, i8** %exn.slot, align 8
  %17 = extractvalue { i8*, i32 } %15, 1
  store i32 %17, i32* %ehselector.slot, align 4
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %ref.tmp16) #3
  br label %eh.resume

lpad23:                                           ; preds = %invoke.cont24, %invoke.cont20
  %18 = landingpad { i8*, i32 }
          cleanup
  %19 = extractvalue { i8*, i32 } %18, 0
  store i8* %19, i8** %exn.slot, align 8
  %20 = extractvalue { i8*, i32 } %18, 1
  store i32 %20, i32* %ehselector.slot, align 4
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %ref.tmp22) #3
  br label %eh.resume

lpad29:                                           ; preds = %invoke.cont30, %invoke.cont26
  %21 = landingpad { i8*, i32 }
          cleanup
  %22 = extractvalue { i8*, i32 } %21, 0
  store i8* %22, i8** %exn.slot, align 8
  %23 = extractvalue { i8*, i32 } %21, 1
  store i32 %23, i32* %ehselector.slot, align 4
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %ref.tmp28) #3
  br label %eh.resume

lpad35:                                           ; preds = %invoke.cont36, %invoke.cont32
  %24 = landingpad { i8*, i32 }
          cleanup
  %25 = extractvalue { i8*, i32 } %24, 0
  store i8* %25, i8** %exn.slot, align 8
  %26 = extractvalue { i8*, i32 } %24, 1
  store i32 %26, i32* %ehselector.slot, align 4
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %ref.tmp34) #3
  br label %eh.resume

eh.resume:                                        ; preds = %lpad35, %lpad29, %lpad23, %lpad17, %lpad11, %lpad5, %lpad
  %exn = load i8*, i8** %exn.slot, align 8
  %sel = load i32, i32* %ehselector.slot, align 4
  %lpad.val = insertvalue { i8*, i32 } undef, i8* %exn, 0
  %lpad.val40 = insertvalue { i8*, i32 } %lpad.val, i32 %sel, 1
  resume { i8*, i32 } %lpad.val40
}

; Function Attrs: noinline optnone uwtable mustprogress
define linkonce_odr dso_local void @_ZN11Appointment7displayEv(%class.Appointment* nonnull dereferenceable(544) %this) #6 comdat align 2 personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
entry:
  %this.addr = alloca %class.Appointment*, align 8
  %ref.tmp = alloca %"class.std::__cxx11::basic_string", align 8
  %exn.slot = alloca i8*, align 8
  %ehselector.slot = alloca i32, align 4
  %ref.tmp4 = alloca %"class.std::__cxx11::basic_string", align 8
  %ref.tmp10 = alloca %"class.std::__cxx11::basic_string", align 8
  %ref.tmp16 = alloca %"class.std::__cxx11::basic_string", align 8
  %ref.tmp22 = alloca %"class.std::__cxx11::basic_string", align 8
  store %class.Appointment* %this, %class.Appointment** %this.addr, align 8
  %this1 = load %class.Appointment*, %class.Appointment** %this.addr, align 8
  %S = getelementptr inbounds %class.Appointment, %class.Appointment* %this1, i32 0, i32 4
  %0 = bitcast %class.Student* %S to %class.People*
  call void @_ZN6People9getNumberB5cxx11Ev(%"class.std::__cxx11::basic_string"* sret(%"class.std::__cxx11::basic_string") align 8 %ref.tmp, %class.People* nonnull dereferenceable(192) %0)
  %call = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsIcSt11char_traitsIcESaIcEERSt13basic_ostreamIT_T0_ES7_RKNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) @_ZSt4cout, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %ref.tmp)
          to label %invoke.cont unwind label %lpad

invoke.cont:                                      ; preds = %entry
  %call3 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) %call, i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.66, i64 0, i64 0))
          to label %invoke.cont2 unwind label %lpad

invoke.cont2:                                     ; preds = %invoke.cont
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %ref.tmp) #3
  %L = getelementptr inbounds %class.Appointment, %class.Appointment* %this1, i32 0, i32 3
  %1 = bitcast %class.Lecturer* %L to %class.People*
  call void @_ZN6People9getNumberB5cxx11Ev(%"class.std::__cxx11::basic_string"* sret(%"class.std::__cxx11::basic_string") align 8 %ref.tmp4, %class.People* nonnull dereferenceable(192) %1)
  %call7 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsIcSt11char_traitsIcESaIcEERSt13basic_ostreamIT_T0_ES7_RKNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) @_ZSt4cout, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %ref.tmp4)
          to label %invoke.cont6 unwind label %lpad5

invoke.cont6:                                     ; preds = %invoke.cont2
  %call9 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) %call7, i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.66, i64 0, i64 0))
          to label %invoke.cont8 unwind label %lpad5

invoke.cont8:                                     ; preds = %invoke.cont6
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %ref.tmp4) #3
  call void @_ZN11Appointment7getDateB5cxx11Ev(%"class.std::__cxx11::basic_string"* sret(%"class.std::__cxx11::basic_string") align 8 %ref.tmp10, %class.Appointment* nonnull dereferenceable(544) %this1)
  %call13 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsIcSt11char_traitsIcESaIcEERSt13basic_ostreamIT_T0_ES7_RKNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) @_ZSt4cout, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %ref.tmp10)
          to label %invoke.cont12 unwind label %lpad11

invoke.cont12:                                    ; preds = %invoke.cont8
  %call15 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) %call13, i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.66, i64 0, i64 0))
          to label %invoke.cont14 unwind label %lpad11

invoke.cont14:                                    ; preds = %invoke.cont12
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %ref.tmp10) #3
  call void @_ZN11Appointment8getStartB5cxx11Ev(%"class.std::__cxx11::basic_string"* sret(%"class.std::__cxx11::basic_string") align 8 %ref.tmp16, %class.Appointment* nonnull dereferenceable(544) %this1)
  %call19 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsIcSt11char_traitsIcESaIcEERSt13basic_ostreamIT_T0_ES7_RKNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) @_ZSt4cout, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %ref.tmp16)
          to label %invoke.cont18 unwind label %lpad17

invoke.cont18:                                    ; preds = %invoke.cont14
  %call21 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) %call19, i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.66, i64 0, i64 0))
          to label %invoke.cont20 unwind label %lpad17

invoke.cont20:                                    ; preds = %invoke.cont18
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %ref.tmp16) #3
  call void @_ZN11Appointment6getEndB5cxx11Ev(%"class.std::__cxx11::basic_string"* sret(%"class.std::__cxx11::basic_string") align 8 %ref.tmp22, %class.Appointment* nonnull dereferenceable(544) %this1)
  %call25 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsIcSt11char_traitsIcESaIcEERSt13basic_ostreamIT_T0_ES7_RKNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) @_ZSt4cout, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %ref.tmp22)
          to label %invoke.cont24 unwind label %lpad23

invoke.cont24:                                    ; preds = %invoke.cont20
  %call27 = invoke nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* nonnull dereferenceable(8) %call25, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
          to label %invoke.cont26 unwind label %lpad23

invoke.cont26:                                    ; preds = %invoke.cont24
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %ref.tmp22) #3
  ret void

lpad:                                             ; preds = %invoke.cont, %entry
  %2 = landingpad { i8*, i32 }
          cleanup
  %3 = extractvalue { i8*, i32 } %2, 0
  store i8* %3, i8** %exn.slot, align 8
  %4 = extractvalue { i8*, i32 } %2, 1
  store i32 %4, i32* %ehselector.slot, align 4
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %ref.tmp) #3
  br label %eh.resume

lpad5:                                            ; preds = %invoke.cont6, %invoke.cont2
  %5 = landingpad { i8*, i32 }
          cleanup
  %6 = extractvalue { i8*, i32 } %5, 0
  store i8* %6, i8** %exn.slot, align 8
  %7 = extractvalue { i8*, i32 } %5, 1
  store i32 %7, i32* %ehselector.slot, align 4
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %ref.tmp4) #3
  br label %eh.resume

lpad11:                                           ; preds = %invoke.cont12, %invoke.cont8
  %8 = landingpad { i8*, i32 }
          cleanup
  %9 = extractvalue { i8*, i32 } %8, 0
  store i8* %9, i8** %exn.slot, align 8
  %10 = extractvalue { i8*, i32 } %8, 1
  store i32 %10, i32* %ehselector.slot, align 4
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %ref.tmp10) #3
  br label %eh.resume

lpad17:                                           ; preds = %invoke.cont18, %invoke.cont14
  %11 = landingpad { i8*, i32 }
          cleanup
  %12 = extractvalue { i8*, i32 } %11, 0
  store i8* %12, i8** %exn.slot, align 8
  %13 = extractvalue { i8*, i32 } %11, 1
  store i32 %13, i32* %ehselector.slot, align 4
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %ref.tmp16) #3
  br label %eh.resume

lpad23:                                           ; preds = %invoke.cont24, %invoke.cont20
  %14 = landingpad { i8*, i32 }
          cleanup
  %15 = extractvalue { i8*, i32 } %14, 0
  store i8* %15, i8** %exn.slot, align 8
  %16 = extractvalue { i8*, i32 } %14, 1
  store i32 %16, i32* %ehselector.slot, align 4
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %ref.tmp22) #3
  br label %eh.resume

eh.resume:                                        ; preds = %lpad23, %lpad17, %lpad11, %lpad5, %lpad
  %exn = load i8*, i8** %exn.slot, align 8
  %sel = load i32, i32* %ehselector.slot, align 4
  %lpad.val = insertvalue { i8*, i32 } undef, i8* %exn, 0
  %lpad.val28 = insertvalue { i8*, i32 } %lpad.val, i32 %sel, 1
  resume { i8*, i32 } %lpad.val28
}

declare dso_local void @_ZNSt14basic_ofstreamIcSt11char_traitsIcEEC1Ev(%"class.std::basic_ofstream"* nonnull dereferenceable(248)) unnamed_addr #1

declare dso_local void @_ZNSt14basic_ofstreamIcSt11char_traitsIcEE4openEPKcSt13_Ios_Openmode(%"class.std::basic_ofstream"* nonnull dereferenceable(248), i8*, i32) #1

; Function Attrs: noinline optnone uwtable mustprogress
define linkonce_odr dso_local void @_ZN6People7getNameB5cxx11Ev(%"class.std::__cxx11::basic_string"* noalias sret(%"class.std::__cxx11::basic_string") align 8 %agg.result, %class.People* nonnull dereferenceable(192) %this) #6 comdat align 2 {
entry:
  %result.ptr = alloca i8*, align 8
  %this.addr = alloca %class.People*, align 8
  %0 = bitcast %"class.std::__cxx11::basic_string"* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 8
  store %class.People* %this, %class.People** %this.addr, align 8
  %this1 = load %class.People*, %class.People** %this.addr, align 8
  %name = getelementptr inbounds %class.People, %class.People* %this1, i32 0, i32 1
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1ERKS4_(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.result, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %name)
  ret void
}

; Function Attrs: noinline optnone uwtable mustprogress
define linkonce_odr dso_local void @_ZN6People10getSurnameB5cxx11Ev(%"class.std::__cxx11::basic_string"* noalias sret(%"class.std::__cxx11::basic_string") align 8 %agg.result, %class.People* nonnull dereferenceable(192) %this) #6 comdat align 2 {
entry:
  %result.ptr = alloca i8*, align 8
  %this.addr = alloca %class.People*, align 8
  %0 = bitcast %"class.std::__cxx11::basic_string"* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 8
  store %class.People* %this, %class.People** %this.addr, align 8
  %this1 = load %class.People*, %class.People** %this.addr, align 8
  %surname = getelementptr inbounds %class.People, %class.People* %this1, i32 0, i32 2
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1ERKS4_(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.result, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %surname)
  ret void
}

; Function Attrs: noinline optnone uwtable mustprogress
define linkonce_odr dso_local void @_ZN6People13getDepartmentB5cxx11Ev(%"class.std::__cxx11::basic_string"* noalias sret(%"class.std::__cxx11::basic_string") align 8 %agg.result, %class.People* nonnull dereferenceable(192) %this) #6 comdat align 2 {
entry:
  %result.ptr = alloca i8*, align 8
  %this.addr = alloca %class.People*, align 8
  %0 = bitcast %"class.std::__cxx11::basic_string"* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 8
  store %class.People* %this, %class.People** %this.addr, align 8
  %this1 = load %class.People*, %class.People** %this.addr, align 8
  %department = getelementptr inbounds %class.People, %class.People* %this1, i32 0, i32 3
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1ERKS4_(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.result, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %department)
  ret void
}

; Function Attrs: noinline optnone uwtable mustprogress
define linkonce_odr dso_local void @_ZN7Student7getYearB5cxx11Ev(%"class.std::__cxx11::basic_string"* noalias sret(%"class.std::__cxx11::basic_string") align 8 %agg.result, %class.Student* nonnull dereferenceable(224) %this) #6 comdat align 2 {
entry:
  %result.ptr = alloca i8*, align 8
  %this.addr = alloca %class.Student*, align 8
  %0 = bitcast %"class.std::__cxx11::basic_string"* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 8
  store %class.Student* %this, %class.Student** %this.addr, align 8
  %this1 = load %class.Student*, %class.Student** %this.addr, align 8
  %year = getelementptr inbounds %class.Student, %class.Student* %this1, i32 0, i32 1
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1ERKS4_(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.result, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %year)
  ret void
}

; Function Attrs: noinline optnone uwtable mustprogress
define linkonce_odr dso_local void @_ZN6People8getEmailB5cxx11Ev(%"class.std::__cxx11::basic_string"* noalias sret(%"class.std::__cxx11::basic_string") align 8 %agg.result, %class.People* nonnull dereferenceable(192) %this) #6 comdat align 2 {
entry:
  %result.ptr = alloca i8*, align 8
  %this.addr = alloca %class.People*, align 8
  %0 = bitcast %"class.std::__cxx11::basic_string"* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 8
  store %class.People* %this, %class.People** %this.addr, align 8
  %this1 = load %class.People*, %class.People** %this.addr, align 8
  %email = getelementptr inbounds %class.People, %class.People* %this1, i32 0, i32 4
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1ERKS4_(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.result, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %email)
  ret void
}

; Function Attrs: noinline optnone uwtable mustprogress
define linkonce_odr dso_local void @_ZN6People8getPhoneB5cxx11Ev(%"class.std::__cxx11::basic_string"* noalias sret(%"class.std::__cxx11::basic_string") align 8 %agg.result, %class.People* nonnull dereferenceable(192) %this) #6 comdat align 2 {
entry:
  %result.ptr = alloca i8*, align 8
  %this.addr = alloca %class.People*, align 8
  %0 = bitcast %"class.std::__cxx11::basic_string"* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 8
  store %class.People* %this, %class.People** %this.addr, align 8
  %this1 = load %class.People*, %class.People** %this.addr, align 8
  %phone = getelementptr inbounds %class.People, %class.People* %this1, i32 0, i32 5
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1ERKS4_(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.result, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %phone)
  ret void
}

declare dso_local void @_ZNSt14basic_ofstreamIcSt11char_traitsIcEE5closeEv(%"class.std::basic_ofstream"* nonnull dereferenceable(248)) #1

; Function Attrs: noinline optnone uwtable mustprogress
define linkonce_odr dso_local void @_ZN8Lecturer8getChairB5cxx11Ev(%"class.std::__cxx11::basic_string"* noalias sret(%"class.std::__cxx11::basic_string") align 8 %agg.result, %class.Lecturer* nonnull dereferenceable(224) %this) #6 comdat align 2 {
entry:
  %result.ptr = alloca i8*, align 8
  %this.addr = alloca %class.Lecturer*, align 8
  %0 = bitcast %"class.std::__cxx11::basic_string"* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 8
  store %class.Lecturer* %this, %class.Lecturer** %this.addr, align 8
  %this1 = load %class.Lecturer*, %class.Lecturer** %this.addr, align 8
  %chair = getelementptr inbounds %class.Lecturer, %class.Lecturer* %this1, i32 0, i32 1
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1ERKS4_(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %agg.result, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %chair)
  ret void
}

; Function Attrs: nounwind
declare dso_local void @_ZNSt14basic_ofstreamIcSt11char_traitsIcEED1Ev(%"class.std::basic_ofstream"* nonnull dereferenceable(248)) unnamed_addr #2

declare dso_local nonnull align 8 dereferenceable(32) %"class.std::__cxx11::basic_string"* @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEaSERKS4_(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32), %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32)) #1

; Function Attrs: noinline optnone uwtable
define linkonce_odr dso_local nonnull align 8 dereferenceable(192) %class.People* @_ZN6PeopleaSERKS_(%class.People* nonnull dereferenceable(192) %this, %class.People* nonnull align 8 dereferenceable(192) %0) #4 comdat align 2 {
entry:
  %this.addr = alloca %class.People*, align 8
  %.addr = alloca %class.People*, align 8
  store %class.People* %this, %class.People** %this.addr, align 8
  store %class.People* %0, %class.People** %.addr, align 8
  %this1 = load %class.People*, %class.People** %this.addr, align 8
  %number = getelementptr inbounds %class.People, %class.People* %this1, i32 0, i32 0
  %1 = load %class.People*, %class.People** %.addr, align 8
  %number2 = getelementptr inbounds %class.People, %class.People* %1, i32 0, i32 0
  %call = call nonnull align 8 dereferenceable(32) %"class.std::__cxx11::basic_string"* @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEaSERKS4_(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %number, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %number2)
  %name = getelementptr inbounds %class.People, %class.People* %this1, i32 0, i32 1
  %2 = load %class.People*, %class.People** %.addr, align 8
  %name3 = getelementptr inbounds %class.People, %class.People* %2, i32 0, i32 1
  %call4 = call nonnull align 8 dereferenceable(32) %"class.std::__cxx11::basic_string"* @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEaSERKS4_(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %name, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %name3)
  %surname = getelementptr inbounds %class.People, %class.People* %this1, i32 0, i32 2
  %3 = load %class.People*, %class.People** %.addr, align 8
  %surname5 = getelementptr inbounds %class.People, %class.People* %3, i32 0, i32 2
  %call6 = call nonnull align 8 dereferenceable(32) %"class.std::__cxx11::basic_string"* @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEaSERKS4_(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %surname, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %surname5)
  %department = getelementptr inbounds %class.People, %class.People* %this1, i32 0, i32 3
  %4 = load %class.People*, %class.People** %.addr, align 8
  %department7 = getelementptr inbounds %class.People, %class.People* %4, i32 0, i32 3
  %call8 = call nonnull align 8 dereferenceable(32) %"class.std::__cxx11::basic_string"* @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEaSERKS4_(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %department, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %department7)
  %email = getelementptr inbounds %class.People, %class.People* %this1, i32 0, i32 4
  %5 = load %class.People*, %class.People** %.addr, align 8
  %email9 = getelementptr inbounds %class.People, %class.People* %5, i32 0, i32 4
  %call10 = call nonnull align 8 dereferenceable(32) %"class.std::__cxx11::basic_string"* @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEaSERKS4_(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %email, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %email9)
  %phone = getelementptr inbounds %class.People, %class.People* %this1, i32 0, i32 5
  %6 = load %class.People*, %class.People** %.addr, align 8
  %phone11 = getelementptr inbounds %class.People, %class.People* %6, i32 0, i32 5
  %call12 = call nonnull align 8 dereferenceable(32) %"class.std::__cxx11::basic_string"* @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEaSERKS4_(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32) %phone, %"class.std::__cxx11::basic_string"* nonnull align 8 dereferenceable(32) %phone11)
  ret %class.People* %this1
}

; Function Attrs: nounwind
declare dso_local i64 @_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE4sizeEv(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32)) #2

; Function Attrs: noinline nounwind optnone uwtable mustprogress
define linkonce_odr dso_local i32 @_ZNSt11char_traitsIcE7compareEPKcS2_m(i8* %__s1, i8* %__s2, i64 %__n) #7 comdat align 2 {
entry:
  %retval = alloca i32, align 4
  %__s1.addr = alloca i8*, align 8
  %__s2.addr = alloca i8*, align 8
  %__n.addr = alloca i64, align 8
  store i8* %__s1, i8** %__s1.addr, align 8
  store i8* %__s2, i8** %__s2.addr, align 8
  store i64 %__n, i64* %__n.addr, align 8
  %0 = load i64, i64* %__n.addr, align 8
  %cmp = icmp eq i64 %0, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load i8*, i8** %__s1.addr, align 8
  %2 = load i8*, i8** %__s2.addr, align 8
  %3 = load i64, i64* %__n.addr, align 8
  %call = call i32 @memcmp(i8* %1, i8* %2, i64 %3) #3
  store i32 %call, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %4 = load i32, i32* %retval, align 4
  ret i32 %4
}

; Function Attrs: nounwind
declare dso_local i8* @_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE4dataEv(%"class.std::__cxx11::basic_string"* nonnull dereferenceable(32)) #2

; Function Attrs: noinline noreturn nounwind
define linkonce_odr hidden void @__clang_call_terminate(i8* %0) #9 comdat {
  %2 = call i8* @__cxa_begin_catch(i8* %0) #3
  call void @_ZSt9terminatev() #10
  unreachable
}

declare dso_local i8* @__cxa_begin_catch(i8*)

declare dso_local void @_ZSt9terminatev()

; Function Attrs: nounwind
declare dso_local i32 @memcmp(i8*, i8*, i64) #2

; Function Attrs: noinline uwtable
define internal void @_GLOBAL__sub_I_appointment.cpp() #0 section ".text.startup" {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { noinline uwtable "frame-pointer"="all" "min-legal-vector-width"="0" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "tune-cpu"="generic" }
attributes #1 = { "frame-pointer"="all" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "tune-cpu"="generic" }
attributes #2 = { nounwind "frame-pointer"="all" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "tune-cpu"="generic" }
attributes #3 = { nounwind }
attributes #4 = { noinline optnone uwtable "frame-pointer"="all" "min-legal-vector-width"="0" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "tune-cpu"="generic" }
attributes #5 = { noinline nounwind optnone uwtable "frame-pointer"="all" "min-legal-vector-width"="0" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "tune-cpu"="generic" }
attributes #6 = { noinline optnone uwtable mustprogress "frame-pointer"="all" "min-legal-vector-width"="0" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "tune-cpu"="generic" }
attributes #7 = { noinline nounwind optnone uwtable mustprogress "frame-pointer"="all" "min-legal-vector-width"="0" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "tune-cpu"="generic" }
attributes #8 = { noinline norecurse optnone uwtable mustprogress "frame-pointer"="all" "min-legal-vector-width"="0" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "tune-cpu"="generic" }
attributes #9 = { noinline noreturn nounwind }
attributes #10 = { noreturn nounwind }

!llvm.module.flags = !{!0, !1, !2}
!llvm.ident = !{!3}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 7, !"uwtable", i32 1}
!2 = !{i32 7, !"frame-pointer", i32 2}
!3 = !{!"clang version 13.0.0 (https://gitlab.com/ash1kr/cd-project-llvm.git e1350b9f6a1a94b6961b8d5ec5f21e29474f6385)"}
!4 = distinct !{!4, !5}
!5 = !{!"llvm.loop.mustprogress"}
!6 = distinct !{!6, !5}
!7 = distinct !{!7, !5}
!8 = distinct !{!8, !5}
!9 = distinct !{!9, !5}
!10 = distinct !{!10, !5}
!11 = distinct !{!11, !5}
!12 = distinct !{!12, !5}
!13 = distinct !{!13, !5}
!14 = distinct !{!14, !5}
!15 = distinct !{!15, !5}
!16 = distinct !{!16, !5}
!17 = distinct !{!17, !5}
!18 = distinct !{!18, !5}
!19 = distinct !{!19, !5}
!20 = distinct !{!20, !5}
!21 = distinct !{!21, !5}
!22 = distinct !{!22, !5}
!23 = distinct !{!23, !5}
!24 = distinct !{!24, !5}
!25 = distinct !{!25, !5}

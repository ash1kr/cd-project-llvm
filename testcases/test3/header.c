#include <stdio.h>

void func1(int *i) {
    printf("%d\n", *i);
    *i += 10;
}

; ModuleID = 'merge.cpp'
source_filename = "merge.cpp"
target datalayout = "e-m:e-p270:32:32-p271:32:32-p272:64:64-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

%"class.std::ios_base::Init" = type { i8 }
%"class.std::basic_ostream" = type { i32 (...)**, %"class.std::basic_ios" }
%"class.std::basic_ios" = type { %"class.std::ios_base", %"class.std::basic_ostream"*, i8, i8, %"class.std::basic_streambuf"*, %"class.std::ctype"*, %"class.std::num_put"*, %"class.std::num_get"* }
%"class.std::ios_base" = type { i32 (...)**, i64, i64, i32, i32, i32, %"struct.std::ios_base::_Callback_list"*, %"struct.std::ios_base::_Words", [8 x %"struct.std::ios_base::_Words"], i32, %"struct.std::ios_base::_Words"*, %"class.std::locale" }
%"struct.std::ios_base::_Callback_list" = type { %"struct.std::ios_base::_Callback_list"*, void (i32, %"class.std::ios_base"*, i32)*, i32, i32 }
%"struct.std::ios_base::_Words" = type { i8*, i64 }
%"class.std::locale" = type { %"class.std::locale::_Impl"* }
%"class.std::locale::_Impl" = type { i32, %"class.std::locale::facet"**, i64, %"class.std::locale::facet"**, i8** }
%"class.std::locale::facet" = type <{ i32 (...)**, i32, [4 x i8] }>
%"class.std::basic_streambuf" = type { i32 (...)**, i8*, i8*, i8*, i8*, i8*, i8*, %"class.std::locale" }
%"class.std::ctype" = type <{ %"class.std::locale::facet.base", [4 x i8], %struct.__locale_struct*, i8, [7 x i8], i32*, i32*, i16*, i8, [256 x i8], [256 x i8], i8, [6 x i8] }>
%"class.std::locale::facet.base" = type <{ i32 (...)**, i32 }>
%struct.__locale_struct = type { [13 x %struct.__locale_data*], i16*, i32*, i32*, [13 x i8*] }
%struct.__locale_data = type opaque
%"class.std::num_put" = type { %"class.std::locale::facet.base", [4 x i8] }
%"class.std::num_get" = type { %"class.std::locale::facet.base", [4 x i8] }

@_ZStL8__ioinit = internal global %"class.std::ios_base::Init" zeroinitializer, align 1
@__dso_handle = external hidden global i8
@__const.main.mass = private unnamed_addr constant [8 x i32] [i32 12, i32 23, i32 1, i32 53, i32 5, i32 0, i32 -5, i32 213], align 16
@_ZSt4cout = external dso_local global %"class.std::basic_ostream", align 8
@.str = private unnamed_addr constant [14 x i8] c"Before Sort: \00", align 1
@.str.1 = private unnamed_addr constant [2 x i8] c" \00", align 1
@.str.2 = private unnamed_addr constant [2 x i8] c"\0A\00", align 1
@.str.3 = private unnamed_addr constant [13 x i8] c"After Sort: \00", align 1
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_merge.cpp, i8* null }]

; Function Attrs: noinline uwtable
define internal void @__cxx_global_var_init() #0 section ".text.startup" {
entry:
  call void @_ZNSt8ios_base4InitC1Ev(%"class.std::ios_base::Init"* nonnull dereferenceable(1) @_ZStL8__ioinit)
  %0 = call i32 @__cxa_atexit(void (i8*)* bitcast (void (%"class.std::ios_base::Init"*)* @_ZNSt8ios_base4InitD1Ev to void (i8*)*), i8* getelementptr inbounds (%"class.std::ios_base::Init", %"class.std::ios_base::Init"* @_ZStL8__ioinit, i32 0, i32 0), i8* @__dso_handle) #3
  ret void
}

declare dso_local void @_ZNSt8ios_base4InitC1Ev(%"class.std::ios_base::Init"* nonnull dereferenceable(1)) unnamed_addr #1

; Function Attrs: nounwind
declare dso_local void @_ZNSt8ios_base4InitD1Ev(%"class.std::ios_base::Init"* nonnull dereferenceable(1)) unnamed_addr #2

; Function Attrs: nounwind
declare dso_local i32 @__cxa_atexit(void (i8*)*, i8*, i8*) #3

; Function Attrs: noinline nounwind optnone uwtable mustprogress
define dso_local void @_Z5MergePiiii(i32* %arr, i32 %minIndex, i32 %middleIndex, i32 %maxIndex) #4 {
entry:
  %arr.addr = alloca i32*, align 8
  %minIndex.addr = alloca i32, align 4
  %middleIndex.addr = alloca i32, align 4
  %maxIndex.addr = alloca i32, align 4
  %left = alloca i32, align 4
  %right = alloca i32, align 4
  %count = alloca i32, align 4
  %tempArray = alloca [100 x i32], align 16
  %index = alloca i32, align 4
  %i = alloca i32, align 4
  %i23 = alloca i32, align 4
  %i35 = alloca i32, align 4
  store i32* %arr, i32** %arr.addr, align 8
  store i32 %minIndex, i32* %minIndex.addr, align 4
  store i32 %middleIndex, i32* %middleIndex.addr, align 4
  store i32 %maxIndex, i32* %maxIndex.addr, align 4
  %0 = load i32, i32* %minIndex.addr, align 4
  store i32 %0, i32* %left, align 4
  %1 = load i32, i32* %middleIndex.addr, align 4
  %add = add nsw i32 %1, 1
  store i32 %add, i32* %right, align 4
  %2 = load i32, i32* %maxIndex.addr, align 4
  %3 = load i32, i32* %minIndex.addr, align 4
  %sub = sub nsw i32 %2, %3
  %add1 = add nsw i32 %sub, 1
  store i32 %add1, i32* %count, align 4
  store i32 0, i32* %index, align 4
  br label %while.cond

while.cond:                                       ; preds = %if.end, %entry
  %4 = load i32, i32* %left, align 4
  %5 = load i32, i32* %middleIndex.addr, align 4
  %cmp = icmp sle i32 %4, %5
  br i1 %cmp, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %while.cond
  %6 = load i32, i32* %right, align 4
  %7 = load i32, i32* %maxIndex.addr, align 4
  %cmp2 = icmp sle i32 %6, %7
  br label %land.end

land.end:                                         ; preds = %land.rhs, %while.cond
  %8 = phi i1 [ false, %while.cond ], [ %cmp2, %land.rhs ]
  br i1 %8, label %while.body, label %while.end

while.body:                                       ; preds = %land.end
  %9 = load i32*, i32** %arr.addr, align 8
  %10 = load i32, i32* %left, align 4
  %idxprom = sext i32 %10 to i64
  %arrayidx = getelementptr inbounds i32, i32* %9, i64 %idxprom
  %11 = load i32, i32* %arrayidx, align 4
  %12 = load i32*, i32** %arr.addr, align 8
  %13 = load i32, i32* %right, align 4
  %idxprom3 = sext i32 %13 to i64
  %arrayidx4 = getelementptr inbounds i32, i32* %12, i64 %idxprom3
  %14 = load i32, i32* %arrayidx4, align 4
  %cmp5 = icmp slt i32 %11, %14
  br i1 %cmp5, label %if.then, label %if.else

if.then:                                          ; preds = %while.body
  %15 = load i32*, i32** %arr.addr, align 8
  %16 = load i32, i32* %left, align 4
  %idxprom6 = sext i32 %16 to i64
  %arrayidx7 = getelementptr inbounds i32, i32* %15, i64 %idxprom6
  %17 = load i32, i32* %arrayidx7, align 4
  %18 = load i32, i32* %index, align 4
  %idxprom8 = sext i32 %18 to i64
  %arrayidx9 = getelementptr inbounds [100 x i32], [100 x i32]* %tempArray, i64 0, i64 %idxprom8
  store i32 %17, i32* %arrayidx9, align 4
  %19 = load i32, i32* %left, align 4
  %inc = add nsw i32 %19, 1
  store i32 %inc, i32* %left, align 4
  br label %if.end

if.else:                                          ; preds = %while.body
  %20 = load i32*, i32** %arr.addr, align 8
  %21 = load i32, i32* %right, align 4
  %idxprom10 = sext i32 %21 to i64
  %arrayidx11 = getelementptr inbounds i32, i32* %20, i64 %idxprom10
  %22 = load i32, i32* %arrayidx11, align 4
  %23 = load i32, i32* %index, align 4
  %idxprom12 = sext i32 %23 to i64
  %arrayidx13 = getelementptr inbounds [100 x i32], [100 x i32]* %tempArray, i64 0, i64 %idxprom12
  store i32 %22, i32* %arrayidx13, align 4
  %24 = load i32, i32* %right, align 4
  %inc14 = add nsw i32 %24, 1
  store i32 %inc14, i32* %right, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %25 = load i32, i32* %index, align 4
  %inc15 = add nsw i32 %25, 1
  store i32 %inc15, i32* %index, align 4
  br label %while.cond, !llvm.loop !4

while.end:                                        ; preds = %land.end
  %26 = load i32, i32* %left, align 4
  store i32 %26, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %while.end
  %27 = load i32, i32* %i, align 4
  %28 = load i32, i32* %middleIndex.addr, align 4
  %cmp16 = icmp sle i32 %27, %28
  br i1 %cmp16, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %29 = load i32*, i32** %arr.addr, align 8
  %30 = load i32, i32* %i, align 4
  %idxprom17 = sext i32 %30 to i64
  %arrayidx18 = getelementptr inbounds i32, i32* %29, i64 %idxprom17
  %31 = load i32, i32* %arrayidx18, align 4
  %32 = load i32, i32* %index, align 4
  %idxprom19 = sext i32 %32 to i64
  %arrayidx20 = getelementptr inbounds [100 x i32], [100 x i32]* %tempArray, i64 0, i64 %idxprom19
  store i32 %31, i32* %arrayidx20, align 4
  %33 = load i32, i32* %index, align 4
  %inc21 = add nsw i32 %33, 1
  store i32 %inc21, i32* %index, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %34 = load i32, i32* %i, align 4
  %inc22 = add nsw i32 %34, 1
  store i32 %inc22, i32* %i, align 4
  br label %for.cond, !llvm.loop !6

for.end:                                          ; preds = %for.cond
  %35 = load i32, i32* %right, align 4
  store i32 %35, i32* %i23, align 4
  br label %for.cond24

for.cond24:                                       ; preds = %for.inc32, %for.end
  %36 = load i32, i32* %i23, align 4
  %37 = load i32, i32* %maxIndex.addr, align 4
  %cmp25 = icmp sle i32 %36, %37
  br i1 %cmp25, label %for.body26, label %for.end34

for.body26:                                       ; preds = %for.cond24
  %38 = load i32*, i32** %arr.addr, align 8
  %39 = load i32, i32* %i23, align 4
  %idxprom27 = sext i32 %39 to i64
  %arrayidx28 = getelementptr inbounds i32, i32* %38, i64 %idxprom27
  %40 = load i32, i32* %arrayidx28, align 4
  %41 = load i32, i32* %index, align 4
  %idxprom29 = sext i32 %41 to i64
  %arrayidx30 = getelementptr inbounds [100 x i32], [100 x i32]* %tempArray, i64 0, i64 %idxprom29
  store i32 %40, i32* %arrayidx30, align 4
  %42 = load i32, i32* %index, align 4
  %inc31 = add nsw i32 %42, 1
  store i32 %inc31, i32* %index, align 4
  br label %for.inc32

for.inc32:                                        ; preds = %for.body26
  %43 = load i32, i32* %i23, align 4
  %inc33 = add nsw i32 %43, 1
  store i32 %inc33, i32* %i23, align 4
  br label %for.cond24, !llvm.loop !7

for.end34:                                        ; preds = %for.cond24
  store i32 0, i32* %i35, align 4
  br label %for.cond36

for.cond36:                                       ; preds = %for.inc44, %for.end34
  %44 = load i32, i32* %i35, align 4
  %45 = load i32, i32* %count, align 4
  %cmp37 = icmp slt i32 %44, %45
  br i1 %cmp37, label %for.body38, label %for.end46

for.body38:                                       ; preds = %for.cond36
  %46 = load i32, i32* %i35, align 4
  %idxprom39 = sext i32 %46 to i64
  %arrayidx40 = getelementptr inbounds [100 x i32], [100 x i32]* %tempArray, i64 0, i64 %idxprom39
  %47 = load i32, i32* %arrayidx40, align 4
  %48 = load i32*, i32** %arr.addr, align 8
  %49 = load i32, i32* %minIndex.addr, align 4
  %50 = load i32, i32* %i35, align 4
  %add41 = add nsw i32 %49, %50
  %idxprom42 = sext i32 %add41 to i64
  %arrayidx43 = getelementptr inbounds i32, i32* %48, i64 %idxprom42
  store i32 %47, i32* %arrayidx43, align 4
  br label %for.inc44

for.inc44:                                        ; preds = %for.body38
  %51 = load i32, i32* %i35, align 4
  %inc45 = add nsw i32 %51, 1
  store i32 %inc45, i32* %i35, align 4
  br label %for.cond36, !llvm.loop !8

for.end46:                                        ; preds = %for.cond36
  ret void
}

; Function Attrs: noinline optnone uwtable mustprogress
define dso_local void @_Z9MergeSortPiii(i32* %arr, i32 %minIndex, i32 %maxIndex) #5 {
entry:
  %arr.addr = alloca i32*, align 8
  %minIndex.addr = alloca i32, align 4
  %maxIndex.addr = alloca i32, align 4
  %middleImdex = alloca i32, align 4
  store i32* %arr, i32** %arr.addr, align 8
  store i32 %minIndex, i32* %minIndex.addr, align 4
  store i32 %maxIndex, i32* %maxIndex.addr, align 4
  %0 = load i32, i32* %minIndex.addr, align 4
  %1 = load i32, i32* %maxIndex.addr, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %minIndex.addr, align 4
  %3 = load i32, i32* %maxIndex.addr, align 4
  %add = add nsw i32 %2, %3
  %div = sdiv i32 %add, 2
  store i32 %div, i32* %middleImdex, align 4
  %4 = load i32*, i32** %arr.addr, align 8
  %5 = load i32, i32* %minIndex.addr, align 4
  %6 = load i32, i32* %middleImdex, align 4
  call void @_Z9MergeSortPiii(i32* %4, i32 %5, i32 %6)
  %7 = load i32*, i32** %arr.addr, align 8
  %8 = load i32, i32* %middleImdex, align 4
  %add1 = add nsw i32 %8, 1
  %9 = load i32, i32* %maxIndex.addr, align 4
  call void @_Z9MergeSortPiii(i32* %7, i32 %add1, i32 %9)
  %10 = load i32*, i32** %arr.addr, align 8
  %11 = load i32, i32* %minIndex.addr, align 4
  %12 = load i32, i32* %middleImdex, align 4
  %13 = load i32, i32* %maxIndex.addr, align 4
  call void @_Z5MergePiiii(i32* %10, i32 %11, i32 %12, i32 %13)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline norecurse optnone uwtable mustprogress
define dso_local i32 @main() #6 {
entry:
  %retval = alloca i32, align 4
  %n = alloca i32, align 4
  %mass = alloca [8 x i32], align 16
  %i = alloca i32, align 4
  %i5 = alloca i32, align 4
  store i32 0, i32* %retval, align 4
  store i32 8, i32* %n, align 4
  %0 = bitcast [8 x i32]* %mass to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* align 16 %0, i8* align 16 bitcast ([8 x i32]* @__const.main.mass to i8*), i64 32, i1 false)
  %call = call nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) @_ZSt4cout, i8* getelementptr inbounds ([14 x i8], [14 x i8]* @.str, i64 0, i64 0))
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %n, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load i32, i32* %i, align 4
  %idxprom = sext i32 %3 to i64
  %arrayidx = getelementptr inbounds [8 x i32], [8 x i32]* %mass, i64 0, i64 %idxprom
  %4 = load i32, i32* %arrayidx, align 4
  %call1 = call nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEi(%"class.std::basic_ostream"* nonnull dereferenceable(8) @_ZSt4cout, i32 %4)
  %call2 = call nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) %call1, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.1, i64 0, i64 0))
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond, !llvm.loop !9

for.end:                                          ; preds = %for.cond
  %call3 = call nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) @_ZSt4cout, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.2, i64 0, i64 0))
  %arraydecay = getelementptr inbounds [8 x i32], [8 x i32]* %mass, i64 0, i64 0
  %6 = load i32, i32* %n, align 4
  %sub = sub nsw i32 %6, 1
  call void @_Z9MergeSortPiii(i32* %arraydecay, i32 0, i32 %sub)
  %call4 = call nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) @_ZSt4cout, i8* getelementptr inbounds ([13 x i8], [13 x i8]* @.str.3, i64 0, i64 0))
  store i32 0, i32* %i5, align 4
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc13, %for.end
  %7 = load i32, i32* %i5, align 4
  %8 = load i32, i32* %n, align 4
  %cmp7 = icmp slt i32 %7, %8
  br i1 %cmp7, label %for.body8, label %for.end15

for.body8:                                        ; preds = %for.cond6
  %9 = load i32, i32* %i5, align 4
  %idxprom9 = sext i32 %9 to i64
  %arrayidx10 = getelementptr inbounds [8 x i32], [8 x i32]* %mass, i64 0, i64 %idxprom9
  %10 = load i32, i32* %arrayidx10, align 4
  %call11 = call nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEi(%"class.std::basic_ostream"* nonnull dereferenceable(8) @_ZSt4cout, i32 %10)
  %call12 = call nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8) %call11, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.1, i64 0, i64 0))
  br label %for.inc13

for.inc13:                                        ; preds = %for.body8
  %11 = load i32, i32* %i5, align 4
  %inc14 = add nsw i32 %11, 1
  store i32 %inc14, i32* %i5, align 4
  br label %for.cond6, !llvm.loop !10

for.end15:                                        ; preds = %for.cond6
  %12 = load i32, i32* %retval, align 4
  ret i32 %12
}

; Function Attrs: argmemonly nofree nosync nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i64(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i64, i1 immarg) #7

declare dso_local nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* nonnull align 8 dereferenceable(8), i8*) #1

declare dso_local nonnull align 8 dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEi(%"class.std::basic_ostream"* nonnull dereferenceable(8), i32) #1

; Function Attrs: noinline uwtable
define internal void @_GLOBAL__sub_I_merge.cpp() #0 section ".text.startup" {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { noinline uwtable "frame-pointer"="all" "min-legal-vector-width"="0" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "tune-cpu"="generic" }
attributes #1 = { "frame-pointer"="all" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "tune-cpu"="generic" }
attributes #2 = { nounwind "frame-pointer"="all" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "tune-cpu"="generic" }
attributes #3 = { nounwind }
attributes #4 = { noinline nounwind optnone uwtable mustprogress "frame-pointer"="all" "min-legal-vector-width"="0" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "tune-cpu"="generic" }
attributes #5 = { noinline optnone uwtable mustprogress "frame-pointer"="all" "min-legal-vector-width"="0" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "tune-cpu"="generic" }
attributes #6 = { noinline norecurse optnone uwtable mustprogress "frame-pointer"="all" "min-legal-vector-width"="0" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "tune-cpu"="generic" }
attributes #7 = { argmemonly nofree nosync nounwind willreturn }

!llvm.module.flags = !{!0, !1, !2}
!llvm.ident = !{!3}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 7, !"uwtable", i32 1}
!2 = !{i32 7, !"frame-pointer", i32 2}
!3 = !{!"clang version 13.0.0 (https://gitlab.com/ash1kr/cd-project-llvm.git e1350b9f6a1a94b6961b8d5ec5f21e29474f6385)"}
!4 = distinct !{!4, !5}
!5 = !{!"llvm.loop.mustprogress"}
!6 = distinct !{!6, !5}
!7 = distinct !{!7, !5}
!8 = distinct !{!8, !5}
!9 = distinct !{!9, !5}
!10 = distinct !{!10, !5}

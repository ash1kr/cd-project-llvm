#include <iostream>

void Merge(int arr[], int minIndex, int middleIndex, int maxIndex) {
	int left = minIndex;
	int right = middleIndex + 1;
	int count = maxIndex - minIndex + 1;
	int tempArray[100];
	int index = 0;

	while ((left <= middleIndex) && (right <= maxIndex))
	{
		if (arr[left] < arr[right])
		{
			tempArray[index] = arr[left];
			left++;
		}
		else
		{
			tempArray[index] = arr[right];
			right++;
		}

		index++;
	}

	for (int i = left; i <= middleIndex; i++)
	{
		tempArray[index] = arr[i];
		index++;
	}

	for (int i = right; i <= maxIndex; i++)
	{
		tempArray[index] = arr[i];
		index++;
	}

	for (int i = 0; i < count; i++)
	{
		arr[minIndex + i] = tempArray[i];
	}

}

void MergeSort(int arr[], int minIndex, int maxIndex) {
	if (minIndex < maxIndex) {
		int middleImdex = (minIndex + maxIndex) / 2;
		MergeSort(arr, minIndex, middleImdex);
		MergeSort(arr, middleImdex + 1, maxIndex);
		Merge(arr, minIndex, middleImdex, maxIndex);
	}
}

int main()
{
    int n = 8;
	int mass[8] = {12,23,1, 53, 5, 0, -5, 213};

    std::cout << "Before Sort: ";
	for (int i = 0; i < n; i++) {
		std::cout << mass[i] << " ";
	}
    std::cout << "\n";

    MergeSort(mass, 0, n-1);

    std::cout << "After Sort: ";    
	for (int i = 0; i < n; i++) {
		std::cout << mass[i] << " ";
	}
}

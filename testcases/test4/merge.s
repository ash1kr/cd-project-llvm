	.text
	.file	"merge.cpp"
	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90                         # -- Begin function __cxx_global_var_init
	.type	__cxx_global_var_init,@function
__cxx_global_var_init:                  # @__cxx_global_var_init
	.cfi_startproc
# %bb.0:                                # %entry
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	movl	$_ZStL8__ioinit, %edi
	callq	_ZNSt8ios_base4InitC1Ev
	movl	$_ZNSt8ios_base4InitD1Ev, %edi
	movl	$_ZStL8__ioinit, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end0:
	.size	__cxx_global_var_init, .Lfunc_end0-__cxx_global_var_init
	.cfi_endproc
                                        # -- End function
	.text
	.globl	_Z5MergePiiii                   # -- Begin function _Z5MergePiiii
	.p2align	4, 0x90
	.type	_Z5MergePiiii,@function
_Z5MergePiiii:                          # @_Z5MergePiiii
	.cfi_startproc
# %bb.0:                                # %entry
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	subq	$320, %rsp                      # imm = 0x140
	movq	%rdi, -32(%rbp)
	movl	%esi, -44(%rbp)
	movl	%edx, -40(%rbp)
	movl	%ecx, -36(%rbp)
	movl	-44(%rbp), %eax
	movl	%eax, -12(%rbp)
	movl	-40(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -8(%rbp)
	movl	-36(%rbp), %eax
	subl	-44(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -48(%rbp)
	movl	$0, -4(%rbp)
.LBB1_1:                                # %while.cond
                                        # =>This Inner Loop Header: Depth=1
	movl	-12(%rbp), %ecx
	xorl	%eax, %eax
	cmpl	-40(%rbp), %ecx
	jg	.LBB1_3
# %bb.2:                                # %land.rhs
                                        #   in Loop: Header=BB1_1 Depth=1
	movl	-8(%rbp), %eax
	cmpl	-36(%rbp), %eax
	setle	%al
.LBB1_3:                                # %land.end
                                        #   in Loop: Header=BB1_1 Depth=1
	testb	$1, %al
	jne	.LBB1_4
	jmp	.LBB1_8
.LBB1_4:                                # %while.body
                                        #   in Loop: Header=BB1_1 Depth=1
	movq	-32(%rbp), %rcx
	movslq	-12(%rbp), %rax
	movl	(%rcx,%rax,4), %edx
	movq	-32(%rbp), %rcx
	movslq	-8(%rbp), %rax
	cmpl	(%rcx,%rax,4), %edx
	jge	.LBB1_6
# %bb.5:                                # %if.then
                                        #   in Loop: Header=BB1_1 Depth=1
	movq	-32(%rbp), %rcx
	movslq	-12(%rbp), %rax
	movl	(%rcx,%rax,4), %ecx
	movslq	-4(%rbp), %rax
	movl	%ecx, -448(%rbp,%rax,4)
	movl	-12(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -12(%rbp)
	jmp	.LBB1_7
.LBB1_6:                                # %if.else
                                        #   in Loop: Header=BB1_1 Depth=1
	movq	-32(%rbp), %rcx
	movslq	-8(%rbp), %rax
	movl	(%rcx,%rax,4), %ecx
	movslq	-4(%rbp), %rax
	movl	%ecx, -448(%rbp,%rax,4)
	movl	-8(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -8(%rbp)
.LBB1_7:                                # %if.end
                                        #   in Loop: Header=BB1_1 Depth=1
	movl	-4(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -4(%rbp)
	jmp	.LBB1_1
.LBB1_8:                                # %while.end
	movl	-12(%rbp), %eax
	movl	%eax, -24(%rbp)
.LBB1_9:                                # %for.cond
                                        # =>This Inner Loop Header: Depth=1
	movl	-24(%rbp), %eax
	cmpl	-40(%rbp), %eax
	jg	.LBB1_12
# %bb.10:                               # %for.body
                                        #   in Loop: Header=BB1_9 Depth=1
	movq	-32(%rbp), %rcx
	movslq	-24(%rbp), %rax
	movl	(%rcx,%rax,4), %ecx
	movslq	-4(%rbp), %rax
	movl	%ecx, -448(%rbp,%rax,4)
	movl	-4(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -4(%rbp)
# %bb.11:                               # %for.inc
                                        #   in Loop: Header=BB1_9 Depth=1
	movl	-24(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -24(%rbp)
	jmp	.LBB1_9
.LBB1_12:                               # %for.end
	movl	-8(%rbp), %eax
	movl	%eax, -20(%rbp)
.LBB1_13:                               # %for.cond24
                                        # =>This Inner Loop Header: Depth=1
	movl	-20(%rbp), %eax
	cmpl	-36(%rbp), %eax
	jg	.LBB1_16
# %bb.14:                               # %for.body26
                                        #   in Loop: Header=BB1_13 Depth=1
	movq	-32(%rbp), %rcx
	movslq	-20(%rbp), %rax
	movl	(%rcx,%rax,4), %ecx
	movslq	-4(%rbp), %rax
	movl	%ecx, -448(%rbp,%rax,4)
	movl	-4(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -4(%rbp)
# %bb.15:                               # %for.inc32
                                        #   in Loop: Header=BB1_13 Depth=1
	movl	-20(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -20(%rbp)
	jmp	.LBB1_13
.LBB1_16:                               # %for.end34
	movl	$0, -16(%rbp)
.LBB1_17:                               # %for.cond36
                                        # =>This Inner Loop Header: Depth=1
	movl	-16(%rbp), %eax
	cmpl	-48(%rbp), %eax
	jge	.LBB1_20
# %bb.18:                               # %for.body38
                                        #   in Loop: Header=BB1_17 Depth=1
	movslq	-16(%rbp), %rax
	movl	-448(%rbp,%rax,4), %edx
	movq	-32(%rbp), %rcx
	movl	-44(%rbp), %eax
	addl	-16(%rbp), %eax
	cltq
	movl	%edx, (%rcx,%rax,4)
# %bb.19:                               # %for.inc44
                                        #   in Loop: Header=BB1_17 Depth=1
	movl	-16(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -16(%rbp)
	jmp	.LBB1_17
.LBB1_20:                               # %for.end46
	addq	$320, %rsp                      # imm = 0x140
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end1:
	.size	_Z5MergePiiii, .Lfunc_end1-_Z5MergePiiii
	.cfi_endproc
                                        # -- End function
	.globl	_Z9MergeSortPiii                # -- Begin function _Z9MergeSortPiii
	.p2align	4, 0x90
	.type	_Z9MergeSortPiii,@function
_Z9MergeSortPiii:                       # @_Z9MergeSortPiii
	.cfi_startproc
# %bb.0:                                # %entry
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movq	%rdi, -24(%rbp)
	movl	%esi, -8(%rbp)
	movl	%edx, -4(%rbp)
	movl	-8(%rbp), %eax
	cmpl	-4(%rbp), %eax
	jge	.LBB2_2
# %bb.1:                                # %if.then
	movl	-8(%rbp), %eax
	addl	-4(%rbp), %eax
	movl	$2, %ecx
	cltd
	idivl	%ecx
	movl	%eax, -12(%rbp)
	movq	-24(%rbp), %rdi
	movl	-8(%rbp), %esi
	movl	-12(%rbp), %edx
	callq	_Z9MergeSortPiii
	movq	-24(%rbp), %rdi
	movl	-12(%rbp), %esi
	addl	$1, %esi
	movl	-4(%rbp), %edx
	callq	_Z9MergeSortPiii
	movq	-24(%rbp), %rdi
	movl	-8(%rbp), %esi
	movl	-12(%rbp), %edx
	movl	-4(%rbp), %ecx
	callq	_Z5MergePiiii
.LBB2_2:                                # %if.end
	addq	$32, %rsp
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end2:
	.size	_Z9MergeSortPiii, .Lfunc_end2-_Z9MergeSortPiii
	.cfi_endproc
                                        # -- End function
	.globl	main                            # -- Begin function main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# %bb.0:                                # %entry
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	subq	$48, %rsp
	movl	$0, -16(%rbp)
	movl	$8, -12(%rbp)
	movq	.L__const.main.mass, %rax
	movq	%rax, -48(%rbp)
	movq	.L__const.main.mass+8, %rax
	movq	%rax, -40(%rbp)
	movq	.L__const.main.mass+16, %rax
	movq	%rax, -32(%rbp)
	movq	.L__const.main.mass+24, %rax
	movq	%rax, -24(%rbp)
	movabsq	$_ZSt4cout, %rdi
	movabsq	$.L.str, %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movl	$0, -8(%rbp)
.LBB3_1:                                # %for.cond
                                        # =>This Inner Loop Header: Depth=1
	movl	-8(%rbp), %eax
	cmpl	-12(%rbp), %eax
	jge	.LBB3_4
# %bb.2:                                # %for.body
                                        #   in Loop: Header=BB3_1 Depth=1
	movslq	-8(%rbp), %rax
	movl	-48(%rbp,%rax,4), %esi
	movabsq	$_ZSt4cout, %rdi
	callq	_ZNSolsEi
	movabsq	$.L.str.1, %rsi
	movq	%rax, %rdi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
# %bb.3:                                # %for.inc
                                        #   in Loop: Header=BB3_1 Depth=1
	movl	-8(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -8(%rbp)
	jmp	.LBB3_1
.LBB3_4:                                # %for.end
	movabsq	$_ZSt4cout, %rdi
	movabsq	$.L.str.2, %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	leaq	-48(%rbp), %rdi
	movl	-12(%rbp), %edx
	subl	$1, %edx
	xorl	%esi, %esi
	callq	_Z9MergeSortPiii
	movabsq	$_ZSt4cout, %rdi
	movabsq	$.L.str.3, %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movl	$0, -4(%rbp)
.LBB3_5:                                # %for.cond6
                                        # =>This Inner Loop Header: Depth=1
	movl	-4(%rbp), %eax
	cmpl	-12(%rbp), %eax
	jge	.LBB3_8
# %bb.6:                                # %for.body8
                                        #   in Loop: Header=BB3_5 Depth=1
	movslq	-4(%rbp), %rax
	movl	-48(%rbp,%rax,4), %esi
	movabsq	$_ZSt4cout, %rdi
	callq	_ZNSolsEi
	movabsq	$.L.str.1, %rsi
	movq	%rax, %rdi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
# %bb.7:                                # %for.inc13
                                        #   in Loop: Header=BB3_5 Depth=1
	movl	-4(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -4(%rbp)
	jmp	.LBB3_5
.LBB3_8:                                # %for.end15
	movl	-16(%rbp), %eax
	addq	$48, %rsp
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end3:
	.size	main, .Lfunc_end3-main
	.cfi_endproc
                                        # -- End function
	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90                         # -- Begin function _GLOBAL__sub_I_merge.cpp
	.type	_GLOBAL__sub_I_merge.cpp,@function
_GLOBAL__sub_I_merge.cpp:               # @_GLOBAL__sub_I_merge.cpp
	.cfi_startproc
# %bb.0:                                # %entry
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	callq	__cxx_global_var_init
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end4:
	.size	_GLOBAL__sub_I_merge.cpp, .Lfunc_end4-_GLOBAL__sub_I_merge.cpp
	.cfi_endproc
                                        # -- End function
	.type	_ZStL8__ioinit,@object          # @_ZStL8__ioinit
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.hidden	__dso_handle
	.type	.L__const.main.mass,@object     # @__const.main.mass
	.section	.rodata.cst32,"aM",@progbits,32
	.p2align	4
.L__const.main.mass:
	.long	12                              # 0xc
	.long	23                              # 0x17
	.long	1                               # 0x1
	.long	53                              # 0x35
	.long	5                               # 0x5
	.long	0                               # 0x0
	.long	4294967291                      # 0xfffffffb
	.long	213                             # 0xd5
	.size	.L__const.main.mass, 32

	.type	.L.str,@object                  # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Before Sort: "
	.size	.L.str, 14

	.type	.L.str.1,@object                # @.str.1
.L.str.1:
	.asciz	" "
	.size	.L.str.1, 2

	.type	.L.str.2,@object                # @.str.2
.L.str.2:
	.asciz	"\n"
	.size	.L.str.2, 2

	.type	.L.str.3,@object                # @.str.3
.L.str.3:
	.asciz	"After Sort: "
	.size	.L.str.3, 13

	.section	.init_array,"aw",@init_array
	.p2align	3
	.quad	_GLOBAL__sub_I_merge.cpp
	.ident	"clang version 13.0.0 (https://gitlab.com/ash1kr/cd-project-llvm.git e1350b9f6a1a94b6961b8d5ec5f21e29474f6385)"
	.section	".note.GNU-stack","",@progbits

	.text
	.file	"bubble.c"
	.globl	main                            # -- Begin function main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# %bb.0:                                # %entry
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	subq	$96, %rsp
	movl	$0, -12(%rbp)
	movl	$7, -8(%rbp)
	leaq	-96(%rbp), %rdi
	movabsq	$.L__const.main.array, %rsi
	movl	$80, %edx
	callq	memcpy@PLT
	movabsq	$.L.str, %rdi
	movb	$0, %al
	callq	printf
	movl	$0, -4(%rbp)
.LBB0_1:                                # %for.cond
                                        # =>This Inner Loop Header: Depth=1
	movl	-4(%rbp), %eax
	cmpl	-8(%rbp), %eax
	jge	.LBB0_4
# %bb.2:                                # %for.body
                                        #   in Loop: Header=BB0_1 Depth=1
	movslq	-4(%rbp), %rax
	movl	-96(%rbp,%rax,4), %esi
	movabsq	$.L.str.1, %rdi
	movb	$0, %al
	callq	printf
# %bb.3:                                # %for.inc
                                        #   in Loop: Header=BB0_1 Depth=1
	movl	-4(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -4(%rbp)
	jmp	.LBB0_1
.LBB0_4:                                # %for.end
	movl	-8(%rbp), %edi
	leaq	-96(%rbp), %rsi
	callq	BubbleSort
	movabsq	$.L.str.2, %rdi
	movb	$0, %al
	callq	printf
	movl	$0, -4(%rbp)
.LBB0_5:                                # %for.cond4
                                        # =>This Inner Loop Header: Depth=1
	movl	-4(%rbp), %eax
	cmpl	-8(%rbp), %eax
	jge	.LBB0_8
# %bb.6:                                # %for.body6
                                        #   in Loop: Header=BB0_5 Depth=1
	movslq	-4(%rbp), %rax
	movl	-96(%rbp,%rax,4), %esi
	movabsq	$.L.str.1, %rdi
	movb	$0, %al
	callq	printf
# %bb.7:                                # %for.inc10
                                        #   in Loop: Header=BB0_5 Depth=1
	movl	-4(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -4(%rbp)
	jmp	.LBB0_5
.LBB0_8:                                # %for.end12
	movabsq	$.L.str.3, %rdi
	movb	$0, %al
	callq	printf
	xorl	%eax, %eax
	addq	$96, %rsp
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cfi_endproc
                                        # -- End function
	.globl	BubbleSort                      # -- Begin function BubbleSort
	.p2align	4, 0x90
	.type	BubbleSort,@function
BubbleSort:                             # @BubbleSort
	.cfi_startproc
# %bb.0:                                # %entry
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	movl	%edi, -24(%rbp)
	movq	%rsi, -16(%rbp)
	movl	-24(%rbp), %eax
	subl	$2, %eax
	movl	%eax, -8(%rbp)
.LBB1_1:                                # %for.cond
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_3 Depth 2
	cmpl	$0, -8(%rbp)
	jl	.LBB1_10
# %bb.2:                                # %for.body
                                        #   in Loop: Header=BB1_1 Depth=1
	movl	$0, -4(%rbp)
.LBB1_3:                                # %for.cond1
                                        #   Parent Loop BB1_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-4(%rbp), %eax
	cmpl	-8(%rbp), %eax
	jg	.LBB1_8
# %bb.4:                                # %for.body3
                                        #   in Loop: Header=BB1_3 Depth=2
	movq	-16(%rbp), %rcx
	movslq	-4(%rbp), %rax
	movl	(%rcx,%rax,4), %edx
	movq	-16(%rbp), %rcx
	movl	-4(%rbp), %eax
	addl	$1, %eax
	cltq
	cmpl	(%rcx,%rax,4), %edx
	jle	.LBB1_6
# %bb.5:                                # %if.then
                                        #   in Loop: Header=BB1_3 Depth=2
	movq	-16(%rbp), %rcx
	movslq	-4(%rbp), %rax
	movl	(%rcx,%rax,4), %eax
	movl	%eax, -20(%rbp)
	movq	-16(%rbp), %rcx
	movl	-4(%rbp), %eax
	addl	$1, %eax
	cltq
	movl	(%rcx,%rax,4), %edx
	movq	-16(%rbp), %rcx
	movslq	-4(%rbp), %rax
	movl	%edx, (%rcx,%rax,4)
	movl	-20(%rbp), %edx
	movq	-16(%rbp), %rcx
	movl	-4(%rbp), %eax
	addl	$1, %eax
	cltq
	movl	%edx, (%rcx,%rax,4)
.LBB1_6:                                # %if.end
                                        #   in Loop: Header=BB1_3 Depth=2
	jmp	.LBB1_7
.LBB1_7:                                # %for.inc
                                        #   in Loop: Header=BB1_3 Depth=2
	movl	-4(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -4(%rbp)
	jmp	.LBB1_3
.LBB1_8:                                # %for.end
                                        #   in Loop: Header=BB1_1 Depth=1
	jmp	.LBB1_9
.LBB1_9:                                # %for.inc17
                                        #   in Loop: Header=BB1_1 Depth=1
	movl	-8(%rbp), %eax
	addl	$-1, %eax
	movl	%eax, -8(%rbp)
	jmp	.LBB1_1
.LBB1_10:                               # %for.end18
	movl	$1, %eax
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end1:
	.size	BubbleSort, .Lfunc_end1-BubbleSort
	.cfi_endproc
                                        # -- End function
	.type	.L__const.main.array,@object    # @__const.main.array
	.section	.rodata,"a",@progbits
	.p2align	4
.L__const.main.array:
	.long	1                               # 0x1
	.long	23                              # 0x17
	.long	12                              # 0xc
	.long	9                               # 0x9
	.long	52                              # 0x34
	.long	11                              # 0xb
	.long	4                               # 0x4
	.zero	52
	.size	.L__const.main.array, 80

	.type	.L.str,@object                  # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Before Sorting: "
	.size	.L.str, 17

	.type	.L.str.1,@object                # @.str.1
.L.str.1:
	.asciz	" %d"
	.size	.L.str.1, 4

	.type	.L.str.2,@object                # @.str.2
.L.str.2:
	.asciz	"After Sorting: "
	.size	.L.str.2, 16

	.type	.L.str.3,@object                # @.str.3
.L.str.3:
	.asciz	"\n"
	.size	.L.str.3, 2

	.ident	"clang version 13.0.0 (https://gitlab.com/ash1kr/cd-project-llvm.git e1350b9f6a1a94b6961b8d5ec5f21e29474f6385)"
	.section	".note.GNU-stack","",@progbits

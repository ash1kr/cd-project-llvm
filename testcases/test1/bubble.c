
#include <stdio.h>

int BubbleSort(int size, int *array);

int main(void){
	
	int size = 7, i, array[20] = {1, 23, 12, 9, 52, 11, 4};
	
    printf("Before Sorting: ");
	for(i=0; i<size; i++){
		printf(" %d", array[i]);
	}
	
	//Run the Bubble Sort Algorithm to sort the list of elements
	BubbleSort(size, array);
	
	printf("After Sorting: ");
	for(i=0; i<size; i++){
		printf(" %d", array[i]);
	}
	
	printf("\n");
	//system("pause"); // comment this line if you are not using Windows OS
	return 0; 
}

int BubbleSort(int size, int *array){
	
	int i, j, temp;
	
	//Bubble sorting algorthm
	for(i=size-2; i>= 0; i--){
		for(j=0; j<=i; j++){
			
			//Swap
			if(array[j] > array[j+1]){
				temp = array[j];
				array[j] = array[j+1];
				array[j+1]= temp;
			}
		}
	}
	
	return 1;
}

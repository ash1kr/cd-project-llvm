//===-- RegAllocTree.cpp - Tree Register Allocator ----------------------===//
//
//
//===----------------------------------------------------------------------===//
//
// This file defines the RegAllocTree Function pass
//
//===----------------------------------------------------------------------===//
#include <iostream>
#include <bits/stdc++.h>
#include <set>
#include <map>

#include "AllocationOrder.h"
#include "LiveDebugVariables.h"
#include "llvm/ADT/SmallPtrSet.h"
#include "llvm/ADT/Statistic.h"
#include "llvm/Analysis/AliasAnalysis.h"
#include "llvm/CodeGen/CalcSpillWeights.h"
#include "llvm/CodeGen/LiveIntervals.h"
#include "llvm/CodeGen/LiveRangeEdit.h"
#include "llvm/CodeGen/LiveRegMatrix.h"
#include "llvm/CodeGen/LiveStacks.h"
#include "llvm/CodeGen/MachineBlockFrequencyInfo.h"
#include "llvm/CodeGen/MachineDominators.h"
#include "llvm/CodeGen/MachineFunctionPass.h"
#include "llvm/CodeGen/MachineInstr.h"
#include "llvm/CodeGen/MachineInstrBuilder.h"
#include "llvm/CodeGen/MachineLoopInfo.h"
#include "llvm/CodeGen/MachineModuleInfo.h"
#include "llvm/CodeGen/MachineRegisterInfo.h"
#include "llvm/CodeGen/Passes.h"
#include "llvm/CodeGen/RegAllocRegistry.h"
#include "llvm/CodeGen/Register.h"
#include "llvm/CodeGen/RegisterClassInfo.h"
#include "llvm/CodeGen/Spiller.h"
#include "llvm/CodeGen/TargetRegisterInfo.h"
#include "llvm/CodeGen/TargetInstrInfo.h"
#include "llvm/CodeGen/VirtRegMap.h"
#include "llvm/Pass.h"
#include "llvm/Support/CommandLine.h"
#include "llvm/Support/Debug.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Support/ErrorHandling.h"
#include "llvm/Support/Timer.h"
#include "llvm/Support/GenericDomTree.h"

#include <cstdlib>
#include <queue>

using namespace llvm;

#define DEBUG_TYPE "regalloc"

STATISTIC(NumNewQueued    , "Number of new live ranges queued");


// Registering with llvm
static RegisterRegAlloc treeRegAlloc("tree", "tree based register allocator",
		createTreeRegisterAllocator);

namespace {
struct CompSpillWeight {
	bool operator()(LiveInterval *A, LiveInterval *B) const {
		return A->weight() < B->weight();
	}
};
}

namespace {

using SG = std::set<int, std::greater<int>>;
using MS = std::map<int, SG>;

/// RABasic provides a minimal implementation of the basic register allocation
/// algorithm. It prioritizes live virtual registers by spill weight and spills
/// whenever a register is unavailable. This is not practical in production but
/// provides a useful baseline both for measuring other allocators and comparing
/// the speed of the basic algorithm against other styles of allocators.
class RATree : public MachineFunctionPass,
private LiveRangeEdit::Delegate {
	virtual void anchor();

	// context
	MachineFunction *MF;

	// state
	std::unique_ptr<Spiller> SpillerInstance;
	std::priority_queue<LiveInterval*, std::vector<LiveInterval*>,
	CompSpillWeight> Queue;

	// Scratch space.  Allocated here to avoid repeated malloc calls in
	// selectOrSplit().
	BitVector UsableRegs;
	const TargetInstrInfo TII;

	bool LRE_CanEraseVirtReg(Register) override;
	void LRE_WillShrinkVirtReg(Register) override;

protected:
	SlotIndexes *SIS;
	const TargetRegisterInfo *TRI = nullptr;
	MachineRegisterInfo *MRI = nullptr;
	VirtRegMap *VRM = nullptr;
	LiveIntervals *LIS = nullptr;
	LiveRegMatrix *Matrix = nullptr;
	MachineDominatorTree *MDT = nullptr;
	RegisterClassInfo RegClassInfo;

	/// Inst which is a def of an original reg and whose defs are already all
	/// dead after remat is saved in DeadRemats. The deletion of such inst is
	/// postponed till all the allocations are done, so its remat expr is
	/// always available for the remat of all the siblings of the original reg.
	SmallPtrSet<MachineInstr *, 32> DeadRemats;

	//  RATree() = default;
	//  virtual ~RATree() = default;

	// A RegAlloc pass should call this before allocatePhysRegs.
	void init(VirtRegMap &vrm, LiveIntervals &lis, LiveRegMatrix &mat);

	// The top-level driver. The output is a VirtRegMap that us updated with
	// physical register assignments.
	void allocatePhysRegs();

	// Use this group name for NamedRegionTimer.
	static const char TimerGroupName[];
	static const char TimerGroupDescription[];

	/// Method called when the allocator is about to remove a LiveInterval.
	virtual void aboutToRemoveInterval(LiveInterval &LI) {}

public:
	/// VerifyEnabled - True when -verify-regalloc is given.
	static bool VerifyEnabled;

private:

	void seedLiveRegsOfMI(MachineInstr &MI, std::set<int> *regAlloc);
	void allocatePhysRegsBB();
	void topological(DomTreeNodeBase<MachineBasicBlock> *mdtn, std::set<int> *regAlloc);
	void matchDataFlow();
	void liveInAnalysis(MS* LiveIn, MS* LiveOut);
	void calculateGenKillSet(MS* Gen, MS* Kill);
	void initLiveInOut(MS* LiveIn, MS* LiveOut);

public:
	RATree();

	/// Return the pass name.
	StringRef getPassName() const override { return "Tree based Register Allocator"; }

	/// RABasic analysis usage.
	void getAnalysisUsage(AnalysisUsage &AU) const override;

	void releaseMemory() override;

	Spiller &spiller()  { return *SpillerInstance; }

	void enqueue(LiveInterval *LI)  {
		Queue.push(LI);
	}

	LiveInterval *dequeue()  {
		if (Queue.empty())
			return nullptr;
		LiveInterval *LI = Queue.top();
		Queue.pop();
		return LI;
	}

	MCRegister selectOrSplit(LiveInterval &VirtReg,
			SmallVectorImpl<Register> &SplitVRegs);

	/// Perform register allocation.
	bool runOnMachineFunction(MachineFunction &mf) override;



	MachineFunctionProperties getRequiredProperties() const override {
		return MachineFunctionProperties().set(
				MachineFunctionProperties::Property::NoPHIs);
	}

	MachineFunctionProperties getClearedProperties() const override {
		return MachineFunctionProperties().set(
				MachineFunctionProperties::Property::IsSSA);
	}

	// Helper for spilling all live virtual registers currently unified under preg
	// that interfere with the most recently queried lvr.  Return true if spilling
	// was successful, and append any new spilled/split intervals to splitLVRs.
	bool spillInterferences(LiveInterval &VirtReg, MCRegister PhysReg,
			SmallVectorImpl<Register> &SplitVRegs);

	static char ID;
};

char RATree::ID = 0;

} // end anonymous namespace



const char RATree::TimerGroupName[] = "regalloc";
const char RATree::TimerGroupDescription[] = "Register Allocation";
bool RATree::VerifyEnabled = false;

char &llvm::RATreeID = RATree::ID;

INITIALIZE_PASS_BEGIN(RATree, "regalloctree", "Tree based Register Allocator",
		false, false)
INITIALIZE_PASS_DEPENDENCY(LiveDebugVariables)
INITIALIZE_PASS_DEPENDENCY(SlotIndexes)
INITIALIZE_PASS_DEPENDENCY(LiveIntervals)
INITIALIZE_PASS_DEPENDENCY(RegisterCoalescer)
INITIALIZE_PASS_DEPENDENCY(MachineScheduler)
INITIALIZE_PASS_DEPENDENCY(LiveStacks)
INITIALIZE_PASS_DEPENDENCY(MachineDominatorTree)
INITIALIZE_PASS_DEPENDENCY(MachineLoopInfo)
INITIALIZE_PASS_DEPENDENCY(VirtRegMap)
INITIALIZE_PASS_DEPENDENCY(LiveRegMatrix)
INITIALIZE_PASS_END(RATree, "regalloctree", "Tree based Register Allocator", false,
		false)

		// Pin the vtable to this file.
		void RATree::anchor() {}

void RATree::init(VirtRegMap &vrm,
		LiveIntervals &lis,
		LiveRegMatrix &mat) {
	TRI = &vrm.getTargetRegInfo();
	MRI = &vrm.getRegInfo();
	VRM = &vrm;
	LIS = &lis;
	Matrix = &mat;
	MRI->freezeReservedRegs(vrm.getMachineFunction());
	RegClassInfo.runOnMachineFunction(vrm.getMachineFunction());
}

// Visit all the live registers. If they are already assigned to a physical
// register, unify them with the corresponding LiveIntervalUnion, otherwise push
// them on the priority queue for later assignment.



bool LIComparator1(LiveInterval *LI1, LiveInterval *LI2) {
	return (LI1->beginIndex() < LI2->beginIndex());
}

void RATree::seedLiveRegsOfMI( MachineInstr &MI, std::set<int> *regAlloc) {
	for (unsigned i = 0, e = MI.getNumOperands(); i != e; ++i) {

		MachineOperand *MO = &(MI.getOperand(i));
		if (MO->isReg() && MO->isDef()) {
			Register Reg = MO->getReg();

			if (!Register::isVirtualRegister(Reg) || VRM->hasPhys(Reg) || MRI->reg_nodbg_empty(Reg))
				continue;
			int virtRegInd = Register::virtReg2Index(Reg);

			if (regAlloc->count(virtRegInd) > 0) continue;

			LiveInterval *LI = &LIS->getInterval(Reg);

			regAlloc->insert(virtRegInd);
			enqueue(LI);
		}
	}
}

void RATree::topological(DomTreeNodeBase<MachineBasicBlock> *mdtn, std::set<int> *regAlloc) {

	MachineBasicBlock *MBB = mdtn->getBlock();
	auto begin = MBB->begin();
	auto end = MBB->end();

	for (; begin != end; begin++) {
		MachineInstr &MI = *begin;
		seedLiveRegsOfMI(MI, regAlloc);
	}

	allocatePhysRegsBB();
	if (mdtn->getNumChildren() > 0) {
		for(DomTreeNodeBase<MachineBasicBlock> *mstnc: mdtn->children()) {
			topological(mstnc, regAlloc);
		}
	}
}


void RATree::allocatePhysRegsBB() {
	// Continue assigning vregs one at a time to available physical registers.
	while (LiveInterval *VirtReg = dequeue()) {
		assert(!VRM->hasPhys(VirtReg->reg()) && "Register already assigned");

		// Unused registers can appear when the spiller coalesces snippets.
		if (MRI->reg_nodbg_empty(VirtReg->reg())) {
			LLVM_DEBUG(dbgs() << "Dropping unused " << *VirtReg << '\n');
			aboutToRemoveInterval(*VirtReg);
			LIS->removeInterval(VirtReg->reg());
			continue;
		}

		// Invalidate all interference queries, live ranges could have changed.
		Matrix->invalidateVirtRegs();

		// selectOrSplit requests the allocator to return an available physical
		// register if possible and populate a list of new live intervals that
		// result from splitting.
		LLVM_DEBUG(dbgs() << "\nselectOrSplit "
				<< TRI->getRegClassName(MRI->getRegClass(VirtReg->reg()))
				<< ':' << *VirtReg << " w=" << VirtReg->weight() << '\n');

		using VirtRegVec = SmallVector<Register, 4>;

		VirtRegVec SplitVRegs;
		MCRegister AvailablePhysReg = selectOrSplit(*VirtReg, SplitVRegs);

		if (AvailablePhysReg == ~0u) {
			// selectOrSplit failed to find a register!
			// Probably caused by an inline asm.
			MachineInstr *MI = nullptr;
			for (MachineRegisterInfo::reg_instr_iterator
					I = MRI->reg_instr_begin(VirtReg->reg()),
					E = MRI->reg_instr_end();
					I != E;) {
				MI = &*(I++);
				if (MI->isInlineAsm())
					break;
			}

			const TargetRegisterClass *RC = MRI->getRegClass(VirtReg->reg());
			ArrayRef<MCPhysReg> AllocOrder = RegClassInfo.getOrder(RC);
			if (AllocOrder.empty())
				report_fatal_error("no registers from class available to allocate");
			else if (MI && MI->isInlineAsm()) {
				MI->emitError("inline assembly requires more registers than available");
			} else if (MI) {
				LLVMContext &Context =
						MI->getParent()->getParent()->getMMI().getModule()->getContext();
				Context.emitError("ran out of registers during register allocation");
			} else {
				report_fatal_error("ran out of registers during register allocation");
			}

			// Keep going after reporting the error.
			VRM->assignVirt2Phys(VirtReg->reg(), AllocOrder.front());
			continue;
		}

		if (AvailablePhysReg)
			Matrix->assign(*VirtReg, AvailablePhysReg);

		for (Register Reg : SplitVRegs) {
			assert(LIS->hasInterval(Reg));

			LiveInterval *SplitVirtReg = &LIS->getInterval(Reg);
			assert(!VRM->hasPhys(SplitVirtReg->reg()) && "Register already assigned");
			if (MRI->reg_nodbg_empty(SplitVirtReg->reg())) {
				assert(SplitVirtReg->empty() && "Non-empty but used interval");
				LLVM_DEBUG(dbgs() << "not queueing unused  " << *SplitVirtReg << '\n');
				aboutToRemoveInterval(*SplitVirtReg);
				LIS->removeInterval(SplitVirtReg->reg());
				continue;
			}
			LLVM_DEBUG(dbgs() << "queuing new interval: " << *SplitVirtReg << "\n");
			assert(Register::isVirtualRegister(SplitVirtReg->reg()) &&
					"expect split value in virtual register");
			enqueue(SplitVirtReg);
			++NumNewQueued;
		}
	}
}

// Top-level driver to manage the queue of unassigned VirtRegs and call the
// selectOrSplit implementation.
void RATree::allocatePhysRegs() {

	NamedRegionTimer T("allocate", "allocate phys Regs", TimerGroupName,
			TimerGroupDescription, TimePassesIsEnabled);

	std::set<int> regAlloc = {};

	DomTreeNodeBase<MachineBasicBlock> *mdtn = MDT->getRootNode();
	topological(mdtn, & regAlloc);


}

bool RATree::LRE_CanEraseVirtReg(Register VirtReg) {
	LiveInterval &LI = LIS->getInterval(VirtReg);
	if (VRM->hasPhys(VirtReg)) {
		Matrix->unassign(LI);
		aboutToRemoveInterval(LI);
		return true;
	}
	// Unassigned virtreg is probably in the priority queue.
	// RATree will erase it after dequeueing.
	// Nonetheless, clear the live-range so that the debug
	// dump will show the right state for that VirtReg.
	LI.clear();
	return false;
}

void RATree::LRE_WillShrinkVirtReg(Register VirtReg) {
	if (!VRM->hasPhys(VirtReg))
		return;

	// Register is assigned, put it back on the queue for reassignment.
	LiveInterval &LI = LIS->getInterval(VirtReg);
	Matrix->unassign(LI);
	enqueue(&LI);
}

RATree::RATree(): MachineFunctionPass(ID) {
}

void RATree::getAnalysisUsage(AnalysisUsage &AU) const {
	AU.setPreservesCFG();
	AU.addRequired<AAResultsWrapperPass>();
	AU.addPreserved<AAResultsWrapperPass>();
	AU.addRequired<LiveIntervals>();
	AU.addPreserved<LiveIntervals>();
	AU.addPreserved<SlotIndexes>();
	AU.addRequired<LiveDebugVariables>();
	AU.addPreserved<LiveDebugVariables>();
	AU.addRequired<LiveStacks>();
	AU.addPreserved<LiveStacks>();
	AU.addRequired<MachineBlockFrequencyInfo>();
	AU.addPreserved<MachineBlockFrequencyInfo>();
	AU.addRequiredID(MachineDominatorsID);
	AU.addPreservedID(MachineDominatorsID);
	AU.addRequired<MachineLoopInfo>();
	AU.addPreserved<MachineLoopInfo>();
	AU.addRequired<VirtRegMap>();
	AU.addPreserved<VirtRegMap>();
	AU.addRequired<LiveRegMatrix>();
	AU.addPreserved<LiveRegMatrix>();
	MachineFunctionPass::getAnalysisUsage(AU);
}

void RATree::releaseMemory() {
	SpillerInstance.reset();
}


bool RATree::spillInterferences(LiveInterval &VirtReg, MCRegister PhysReg,
		SmallVectorImpl<Register> &SplitVRegs) {
	// Record each interference and determine if all are spillable before mutating
	// either the union or live intervals.
	SmallVector<LiveInterval*, 8> Intfs;

	// Collect interferences assigned to any alias of the physical register.
	for (MCRegUnitIterator Units(PhysReg, TRI); Units.isValid(); ++Units) {
		LiveIntervalUnion::Query &Q = Matrix->query(VirtReg, *Units);
		Q.collectInterferingVRegs();
		for (unsigned i = Q.interferingVRegs().size(); i; --i) {
			LiveInterval *Intf = Q.interferingVRegs()[i - 1];
			if (!Intf->isSpillable() || Intf->weight() > VirtReg.weight())
				return false;
			Intfs.push_back(Intf);
		}
	}
	LLVM_DEBUG(dbgs() << "spilling " << printReg(PhysReg, TRI)
			<< " interferences with " << VirtReg << "\n");
	assert(!Intfs.empty() && "expected interference");

	// Spill each interfering vreg allocated to PhysReg or an alias.
	for (unsigned i = 0, e = Intfs.size(); i != e; ++i) {
		LiveInterval &Spill = *Intfs[i];

		// Skip duplicates.
		if (!VRM->hasPhys(Spill.reg()))
			continue;

		// Deallocate the interfering vreg by removing it from the union.
		// A LiveInterval instance may not be in a union during modification!
		Matrix->unassign(Spill);

		// Spill the extracted interval.
		LiveRangeEdit LRE(&Spill, SplitVRegs, *MF, *LIS, VRM, this, &DeadRemats);
		spiller().spill(LRE);
	}
	return true;
}


MCRegister RATree::selectOrSplit(LiveInterval &VirtReg,
		SmallVectorImpl<Register> &SplitVRegs) {
	// Populate a list of physical register spill candidates.
	SmallVector<MCRegister, 8> PhysRegSpillCands;

	// Check for an available register in this class.
	auto Order =
			AllocationOrder::create(VirtReg.reg(), *VRM, RegClassInfo, Matrix);
	for (MCRegister PhysReg : Order) {
		assert(PhysReg.isValid());
		// Check for interference in PhysReg
		switch (Matrix->checkInterference(VirtReg, PhysReg)) {
		case LiveRegMatrix::IK_Free:
			// PhysReg is available, allocate it.
			return PhysReg;

		case LiveRegMatrix::IK_VirtReg:
			// Only virtual registers in the way, we may be able to spill them.
			PhysRegSpillCands.push_back(PhysReg);
			continue;

		default:
			// RegMask or RegUnit interference.
			continue;
		}
	}

	// Try to spill another interfering reg with less spill weight.
	for (MCRegister &PhysReg : PhysRegSpillCands) {
		if (!spillInterferences(VirtReg, PhysReg, SplitVRegs))
			continue;

		assert(!Matrix->checkInterference(VirtReg, PhysReg) &&
				"Interference after spill.");
		// Tell the caller to allocate to this newly freed physical- register.
		return PhysReg;
	}

	// No other spill candidates were found, so spill the current VirtReg.
	LLVM_DEBUG(dbgs() << "spilling: " << VirtReg << '\n');
	if (!VirtReg.isSpillable())
		return ~0u;
	LiveRangeEdit LRE(&VirtReg, SplitVRegs, *MF, *LIS, VRM, this, &DeadRemats);
	spiller().spill(LRE);

	// The live virtual register requesting allocation was spilled, so tell
	// the caller not to allocate anything during this round.
	return 0;
}

void RATree::initLiveInOut(MS* LiveIn, MS* LiveOut) {
	for (MachineBasicBlock &MBB: *MF) {
		int bbn = (&MBB)->getNumber();
		SG sgin, sgout, skill, sgen;
		(*LiveIn)[bbn] = sgin;
		(*LiveOut)[bbn] = sgin;
	}
}

void RATree::calculateGenKillSet(MS* Gen, MS* Kill) {
	for (MachineBasicBlock &MBB: *MF) {
		int bbn = (&MBB)->getNumber();
		SG sgin, sgout, skill, sgen;
		(*Gen)[bbn] = sgen;
		(*Kill)[bbn] = skill;


		SG Use, Def;
		int usec=0, defc=0;

		for (MachineInstr &MI: MBB) {
			for (unsigned i = 0, e = MI.getNumOperands(); i != e; ++i) {
				MachineOperand *MO = &(MI.getOperand(i));

				if (MO->isReg()) {
					Register Reg = MO->getReg();

					if (!Register::isVirtualRegister(Reg) || MRI->reg_nodbg_empty(Reg))
						continue;


					unsigned RegNo = Reg.virtRegIndex();

					if (MO->isDef()) {
						Def.insert(RegNo);
						defc++;

						if (Use.find(RegNo) == Use.end()) {
							(*Kill)[bbn].insert(RegNo);
						}
					}
					else if (MO->isUse()) {
						Use.insert(RegNo);
						usec++;

						if (Def.find(RegNo) == Def.end()) {
							(*Gen)[bbn].insert(RegNo);
						}
					}
				}
			}
		}

	}

}


void RATree::liveInAnalysis(MS* LiveIn, MS* LiveOut) {
	MS Gen;
	MS Kill;

	initLiveInOut(LiveIn, LiveOut);
	calculateGenKillSet(&Gen, &Kill);

	bool changed = true;
	while(changed) {
		changed = false;
		for (MachineBasicBlock &MBB: *MF) {
			int bbn = (&MBB)->getNumber();
			auto begin = (&MBB)->succ_begin();
			auto end = (&MBB)->succ_end();
			(*LiveOut)[bbn] = {};

			for (; begin != end; begin++) {
				MachineBasicBlock *s_MBB = *begin;
				unsigned s_bbn = (s_MBB)->getNumber();

				(*LiveOut)[bbn].insert((*LiveIn)[s_bbn].begin(), (*LiveIn)[s_bbn].end());
			}

			SG OldLiveIn;
			OldLiveIn.insert((*LiveIn)[bbn].begin(), (*LiveIn)[bbn].end());

			(*LiveIn)[bbn] = {};
			(*LiveIn)[bbn].insert(Gen[bbn].begin(), Gen[bbn].end());
			SG GenMinusKill;
			std::set_difference((*LiveOut)[bbn].begin(), (*LiveOut)[bbn].end(), Kill[bbn].begin(), Kill[bbn].end(),
					std::inserter(GenMinusKill, GenMinusKill.end()));
			(*LiveIn)[bbn].insert(GenMinusKill.begin(), GenMinusKill.end());

			if(OldLiveIn != (*LiveIn)[bbn]) {
				changed = true;
			}
		}
	}
}


void RATree::matchDataFlow() {
	MS LiveIn;
	MS LiveOut;

	liveInAnalysis(&LiveIn, &LiveOut);

	for (MachineBasicBlock &MBB: *MF) {
		int bbn = (&MBB)->getNumber();



		for (MachineInstr &MI: MBB) {
			for (unsigned i = 0, e = MI.getNumOperands(); i != e; ++i) {
				MachineOperand *MO = &(MI.getOperand(i));

				if (MO->isReg() && MO->isDef()) {
					Register Reg = MO->getReg();

					if (!Register::isVirtualRegister(Reg) || VRM->hasPhys(Reg) || MRI->reg_nodbg_empty(Reg))
						continue;
					int virtRegInd = Register::virtReg2Index(Reg);
					MCRegister PhysReg = (VRM->getPhys(Reg));

					auto begin = (&MBB)->pred_begin();
					auto end = (&MBB)->pred_end();
					for(; begin != end; begin++) {
						MachineBasicBlock *pMBB = *begin;
						unsigned pMBBn = pMBB->getNumber();

						if (LiveOut[pMBBn].find(virtRegInd) != LiveOut[pMBBn].end()) {
							Register PredReg = Register::index2VirtReg(pMBBn);

							MCRegister PredPhysReg = VRM->getPhys(PredReg);

							if (PhysReg != PredPhysReg) {
								if ((&MBB)->isKnownSentinel()) {
									continue;
								}

								auto b = (pMBB)->instr_begin();
								auto e = (pMBB)->instr_end();
								if (b == e) {
									continue;
								}


								MachineInstr &LastMI = *e;
								const DebugLoc &FirstLoc = (&LastMI)->getDebugLoc();

								BuildMI(&MBB, FirstLoc, TII.get(0xB8), PhysReg).addReg(PredPhysReg);
							}
						}
					}
				}
			}
		}
	}
}

bool RATree::runOnMachineFunction(MachineFunction &mf) {
	LLVM_DEBUG(dbgs() << "********** TREE BASED REGISTER ALLOCATION **********\n"
			<< "********** Function: " << mf.getName() << '\n');

	MF = &mf;

	MDT = &(getAnalysis<MachineDominatorTree>());

	init(getAnalysis<VirtRegMap>(),
			getAnalysis<LiveIntervals>(),
			getAnalysis<LiveRegMatrix>());
	VirtRegAuxInfo VRAI(*MF, *LIS, *VRM, getAnalysis<MachineLoopInfo>(),
			getAnalysis<MachineBlockFrequencyInfo>());
	VRAI.calculateSpillWeightsAndHints();

	SpillerInstance.reset(createInlineSpiller(*this, *MF, *VRM, VRAI));


	allocatePhysRegs();
	matchDataFlow();
	//  postOptimization();

	// Diagnostic output before rewriting
	LLVM_DEBUG(dbgs() << "Post alloc VirtRegMap:\n" << *VRM << "\n");

	releaseMemory();
	return true;
}

FunctionPass* llvm::createTreeRegisterAllocator()
{
	return new RATree();
}
